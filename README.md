# Flexibee pro Java runtime 17 a PostgreSQL 13

## Postup instalace

1) Upravit soubor "java.security" v příslušné verzi JRE podle vašeho systému (ve složce "etc" v tomto repozitáři je upravený soubor, diff a informace)
2) Nainstalovat upravený deb soubor (ve složce DEB, soubor "flexibee_2021.8.0_all.repack.deb")
3) Pokud máte jinou verzi `java` tak v konfiguračním souboru `/etc/default/flexibee` upravit `FLEXIBEE_JAVA=/absolutni/cesta/k/adresari/java-openjdk-amd64/bin/java`

## Postup vývoje / aktualizace na novou verzi

1) vývoj a/nebo aktualizace
  - Stáhnout nový .deb soubor z flexibee.eu a rozbalit do složky "flexibee_2021.8.0_all" příkazem `dpkg-deb -R nazev-noveho-balicku.deb flexibee_2021.8.0_all"
  - Nebo upravit zdrojové soubory ve složce "flexibee_2021.8.0_all"
3) Vyřešit a zkontrolovat v `git diff` a min. udělat stage `git add -A`
4) Sestavit nový balíček pomocí `dpkg-deb -b flexibee_2021.8.0_all DEB/flexibee_2021.8.0_all.repack.deb`
5) Vyzkoušet na čistém systému i na systému se starší verzí již nainstalovanou

## Debugging / Možnosti ladění a hledání problémů

1) prohledejte log soubor "/var/log/flexibee.log"
2) V souboru /etc/default/flexibee zkontrolujte nastavení DEBUG=1, případně porovnejte se souborem v repozitáři, kde jsou úpravy v příslušných JAVA_ARGS
3) V případě že repack.deb nejde nainstalovat kvůli závislostem, vymazat z DEBIAN/control požadavky na balíčky, které se u vás jmenují jinak (nebo klidně vymazat úplně) a znovu zabalit
4) V případě že nějaký ze skriptů nefunguje správně, upravit v hlavičce na `#!/bin/bash -x` (nebo `#!/bin/sh -x`), spustit znovu a zkontrolovat proces/výstupy
5) Zkontrolovat chyby v `journalctl -u flexibee.service`

## Známé problémy

1) Aktualizace certifikátu přes API `/certificate` nefunguje, kvůli parsování klíčů, třeba použít [workaround](https://gitlab.com/Ch4rlieB/flexibee-letsencrypt/-/issues/2)
2) V logu se objevují varování na "použití deprecated crypt metod", těchto se asi nejde zbavit
