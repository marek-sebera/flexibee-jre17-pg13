// FlexiBee WallPaper 1.0
// 2014 (c) FlexiBee Systems s.r.o.

(function( $ ) {
 
    $.fn.flexiBeeWallpaper = function(args) {
	    var resolutionX = 1920, resolutionY = 1200;
	    if (screen.width < 1900) {
        	resolutionX = screen.width;
	    }
	    
	    if (!args || !args.no_awesome) {
            $('head').append('<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">');
        }
        
	    var height = 50;

	    if (screen.height < 1900) {
        	resolutionY = screen.height - height;
	    }

	    $("body, html").css({
    		'height': '100%',
    		'background-color': '#3a3a3a'
	    });

        var self = this;

        var serverUrl = 'https://support.flexibee.eu';
    //    serverUrl = '';

	    $.ajax({
          dataType: "json",
          url: serverUrl + '/wallpaper/' + resolutionX + '/' + resolutionY + '/',
          success: function( data ) {
              var url = data.wallpaper.img;
              
              if (args && args.bw && data.wallpaper.img_bw) {
                  url = data.wallpaper.img_bw;
              }
              
              $("body").css({
                   'background': 'url(' + url + ') no-repeat center center fixed', 
                   '-webkit-background-size': 'cover', 
                   '-moz-background-size': 'cover', 
                   '-o-background-size': 'cover', 
                   'background-size': 'cover',
                   'width': '100%',
                   'height': '100%',
                   'min-height': '100%'
              });
          
              if (data.wallpaper.name != '') {
              
                  var el = $('<div style=\"position: fixed; bottom: 40px; left: 40px; padding:6px 9px;background-color:rgba(0,0,0,0.5);-webkit-border-radius: 4px;-moz-border-radius: 4px;border-radius: 4px;color:white; font-size: 1em\" class="hidden-xs"><a href="' + data.wallpaper.url + '" style="color:white"><i class="fa fa-info-circle"></i> ' + data.wallpaper.name + '</a></div>');
                  el.click(
                    function() {
                        var str = '<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" data-show="true">';
                        str += '  <div class="modal-dialog">';
                        str += '    <div class="modal-content">';
                        str += '      <div class="modal-header">';
                        str += '        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
                        str += '        <h4 class="modal-title">' + data.wallpaper.name + '</h4>';
                        str += '      </div>';
                        str += '      <div class="modal-body">';
                        if (data.wallpaper.author) {
                            str += '        <p style="color: grey">' + data.wallpaper.year + ' &copy; ' + data.wallpaper.author;
                            if (data.wallpaper.author_title) {
                                str += ' (' + data.wallpaper.author_title + ')';
                            }
                            str += '</p>';
                        }

                        str += '        <p>' + data.wallpaper.description + '</p>';
                        if (data.wallpaper.url) {
                            str += '        <a href="' + data.wallpaper.url + '">' + data.wallpaper.name + '...</a>';
                        }
                        str += '     </div>';
                        str += '      <div class="modal-footer">';
                        str += '        <a href="' + data.wallpaper.img + '" class="btn btn-default pull-left"><i class="fa fa-search-plus"></i></a>';
                        str += '        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
                        str += '      </div>';
                        str += '    </div><!-- /.modal-content -->';
                        str += '  </div><!-- /.modal-dialog -->';
                        str += '</div>';
                    
                        var modal = $(str);
                        modal.appendTo(self);
                        modal.modal();
                    
                        return false;
                    }  
                  );
              }
              
              el.appendTo(self)
              
          }
        });
	    


        return this;
    };
 
}( jQuery ));