(function($, window, undefined) {

// pozn.: jQueryMobile 1.0b2 rozbíjí vnořené dialogy v toolbaru (-> služby -> sestavy), otestovat, až vyjde 1.0b3!

// událostní model prohlížečů na mobilech se liší od událostního modelu desktopových prohlížečů
// kdybych použil jenom události pro mobilní prohlížeče, tak by ?isMobile=true na desktopu bylo prakticky k ničemu,
// takže se snažím podporovat obojí -- kvůli tomu ale potřebuju vědět, jestli jsem na skutečném mobilu nebo ne
// detekce mobilu viz FreemarkerTemplateProvider
var isRealMobile = /mobile|midp|opera mobi|opera mini|minimo|palmos|windows ce|pda|phone|symbian|microb|kindle|ipad/.test(navigator.userAgent.toLowerCase());

// $.onReady(callback) a $(...).onClick(callback) jsou speciální varianty $(callback) a $(...).click(callback)
// pro použití v situacích, kdy standardní způsob na mobilech nefunguje
//
// nerozumím tomu, proč standardní způsob jinde funguje, ale v dialozích v i-toolbar.ftl jsem musel použít tohle,
// zřejmě nějaká magie jQueryMobile...

$.extend({
    onReady: function(callback) {
        var _this = this;
        var context = $("div:jqmData(role='page'):last");
        context.bind("pagecreate", function() {
            callback.call(_this, context);
        });
    }
});

var dialogUniqueId = 1;

$.extend($.fn, {
    onClick: function(callback) {
        return callback ? this.bind(isRealMobile ? "vclick" : "click", callback) : this.trigger(isRealMobile ? "vclick" : "click");
    },

    clickData: function(data, handler) {
        return this.each(function() {
            $(this).bind(isRealMobile ? "vclick" : "click", data, handler);
        });
    },

    flexibeeInitDialog: function(options) {
        options = $.extend({
            title: ""
        }, options);

        var dialogId = "flexibeeDialog" + (dialogUniqueId++);
        var content = this.detach().show();
        var html = $('<div id="' + dialogId + '" class="ui-dialog" data-role="dialog" data-theme="flexibee"><div data-role="header"><h1></h1></div><div data-role="content"></div></div>');
        $("h1", html).append(options.title);
        $("div[data-role='content']", html).append(content);
        html.appendTo($.mobile.pageContainer);

        this.jqmData("dialogId", dialogId);
    },

    flexibeeOpenDialog: function() {
        var dialogId = this.jqmData("dialogId");
        $.mobile.changePage($("#" + dialogId), { transition: "pop", role: "dialog" });
    }
});

$.extend($.mobile, {
    // monkeypatch jQueryMobile -- naše vlastní indikace načítání stránky
    showPageLoadingMsg: function() {
        $("html").addClass("ui-loading");
        $.showWait();
    },

    hidePageLoadingMsg: function() {
        $.hideWait();
        $("html").removeClass("ui-loading");
    }
});

if (isRealMobile) {
    // monkeypatch jQuery UI Tabs
    $.ui.tabs.prototype.options.event = "vclick";

    var originalTabify = $.ui.tabs.prototype._tabify;
    $.ui.tabs.prototype._tabify = function(init) {
        originalTabify.call(this, init);
        this.anchors.bind('vclick.tabs', function(){return false;});
    };
}

window.flexibee = window.flexibee || {};
window.flexibee.isMobile = true;

})(jQuery, window);
