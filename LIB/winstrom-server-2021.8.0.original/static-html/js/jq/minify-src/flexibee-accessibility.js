(function($, window, undefined) {

var State = { // enum
    HIDDEN:   "hidden",
    DISABLED: "disabled",
    ENABLED:  "enabled"
};

// prefix není povinné
var Accessibility = function(form, prefix) {
    this.form = form;
    this.prefix = prefix || "";
    
    this.startsWithPrefixRegexp = new RegExp("^" + this.prefix);

    this.current = {}; // název políčka -> State (kromě ENABLED, i když by to ničemu nevadilo, jen kvůli snazšímu ladění)
};

// nutné zavolat před prvním použitím!
Accessibility.prototype.setFormHelper = function(formHelper) {
    this.formHelper = formHelper;
};

Accessibility.prototype.update = function(obj) {
    var _this = this;

    console.log("updating accessibility", _this.prefix); /*<STRIP-LINE>*/
    
    _this.beforeUpdate();

    $.each(obj, function(propName, value) {
        if (/@/.test(propName)) {
            return;
        }

        var desiredPropState = _this.getDesiredState(obj, propName);
        _this.changeState(propName, desiredPropState);

        _this.changeLabel(obj, propName);
    });

    _this.afterUpdate();

    console.log("accessibility updated, current state is ", _this.current); /*<STRIP-LINE>*/
};

Accessibility.prototype.enableAll = function() {
    var _this = this;

    $.each(_this.current, function(name, state) {
        $(_this.formHelper.allFieldsForProperty(_this.prefix + name)).flexibeeShowInput();
    });
    _this.current = {};
};

// --- private ---

// zobrazení selectů s více možnostmi se musí provést před samotnou aktualizací, protože ta může selecty zase skrýt
// naopak skrytí selectů s jedinou možností se musí provést až po aktualizaci, protože ta je může zobrazit
Accessibility.prototype.beforeUpdate = function() {
    var _this = this;

    $("select[id^='flexibee-inp'] option:first-child:not(:only-child)", _this.form).each(function() {
        var propName = $(this).parent("select").flexibeeName();
        propName = propName.replace(_this.startsWithPrefixRegexp, "");
        _this.changeState(propName, State.ENABLED);
    });
};

Accessibility.prototype.afterUpdate = function() {
    var _this = this;

    $("select[id^='flexibee-inp'] option:only-child", _this.form).each(function() {
        var propName = $(this).parent("select").flexibeeName();
        propName = propName.replace(_this.startsWithPrefixRegexp, "");
        _this.changeState(propName, State.HIDDEN);
    });
};

Accessibility.prototype.changeState = function(propName, newState) {
    var _this = this;

    var oldState = _this.current[propName] || State.ENABLED;
    if (newState == oldState) {
        return;
    }

    _this.current[propName] = newState;

    var list = _this.formHelper.allFieldsForProperty(_this.prefix + propName);

    if (newState == State.ENABLED) {
        if (oldState == State.DISABLED) {
            $(list).flexibeeEnableInput();
        } else if (oldState == State.HIDDEN) {
            $(list).flexibeeShowInput();
        }
        delete _this.current[propName]; // ENABLED tam být nemá
    } else if (newState == State.DISABLED) {
        if (oldState == State.HIDDEN) {
            $(list).flexibeeShowInput();
        }
        $(list).flexibeeDisableInput();
    } else if (newState = State.HIDDEN) {
        if (oldState == State.DISABLED) {
            $(list).flexibeeEnableInput();
        }
        $(list).flexibeeHideInput();
    }
};

Accessibility.prototype.getDesiredState = function(obj, propName) {
    var visible = JSON.parse(obj[propName + "@visible"] || "true");
    var enabled = JSON.parse(obj[propName + "@enabled"] || "true");
    var editable = JSON.parse(obj[propName + "@editable"] || "true");

    // pozor: pokud by se tohle někdy začalo používat nejen pro editační formuláře, ale i pro prohlížení, budou nejspíš
    // jednotlivým stavům odpovídat _jiné_ kombinace příznaků visible/enabled/editable

    if (visible && enabled && editable) {
        return State.ENABLED;
    }

    if (visible && enabled) { // úmysl!
        return State.DISABLED;
    }

    return State.HIDDEN;
};

Accessibility.prototype.changeLabel = function(obj, propName) {
    var label = obj[propName + "@label"];
    if (label) {
        var elem = $("label[for='" + propName + "']");
        if (!elem.length) {
            elem = $("#flexibee-lbl-" + propName);
        }

        elem.text(label);
    }
};

// ---

// expose to global object
window.flexibee = window.flexibee || {};
window.flexibee.Accessibility = Accessibility;

})(jQuery, window);
