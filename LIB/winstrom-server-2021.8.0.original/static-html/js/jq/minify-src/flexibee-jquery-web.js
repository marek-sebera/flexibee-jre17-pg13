(function($, window, undefined) {

$.extend({
    onReady: function(callback) {
        var _this = this;
        $(function() {
            callback.call(_this, $(document));
        });
    }
});

$.extend($.fn, {
    onClick: function(callback) {
        return callback ? this.bind("click", callback) : this.trigger("click");
    },

    clickData: function(data, handler) {
        return this.each(function() {
            $(this).bind("click", data, handler);
        });
    },

    flexibeeInitDialog: function(options) {
        var _this = this;

        options = $.extend({
            title: ""
        }, options);

        _this.dialog({ autoOpen : false, width: "auto", title: options.title });
        $("a", _this).click(function() {
            _this.dialog("close");
        });
    },

    flexibeeOpenDialog: function(event) {
        this.dialog("option", "position", [event.pageX, event.pageY]).dialog("open");
    }
});

window.flexibee = window.flexibee || {};
window.flexibee.isMobile = false;

})(jQuery, window);
