/**
 * Upload souboru na server bez submitu formulare. Inspiroval jsem se zde:
 * 
 * https://github.com/bassjobsen/jqueryupload/
 * https://developer.tizen.org/dev-guide/2.2.1/org.tizen.web.appprogramming/html/tutorials/w3c_tutorial/comm_tutorial/upload_ajax.htm
 */
(function($) {

    var client; 
    
    function createListener(client, callback) {
        
        function listener(event) {
            if (client.readyState == 4) {
                callback(client.responseText);
            }
        };
        
        return listener;
    };
    
    $.fn.upload = function(url, name, callback) {
        
        client = new XMLHttpRequest();
        client.onreadystatechange = createListener(client, callback);
        
        var fileInput = $(this)[0]; 

        var formData = new FormData();
        formData.append(name, fileInput.files[0]);

        client.open("post", url, true);
        client.send(formData);
    };

})(jQuery);
