 
;(function($){
    
  $.fn.infinitescroll = function(options,callback){
    
    // console log wrapper.
    function debug(){
      if (opts.debug) { window.console && console.log.call(console,arguments)}
    }
    

    // 'document' means the full document usually, but sometimes the content of the overflow'd div in local mode
    function getDocumentHeight(){
      // weird doubletouch of scrollheight because http://soulpass.com/2006/07/24/ie-and-scrollheight/
      return opts.bind.scrollHeight;
    }
    
    
        
    function isNearBottom() {
      // distance remaining in the scroll
      // computed as: document height - distance already scroll - viewport height - buffer
      var pixelsFromWindowBottomToBottom = 0 +
                getDocumentHeight()  - (
                    $(opts.bind).scrollTop() + $(document).scrollTop()
                    );// - $(opts.bind).height();

      debug('math:',pixelsFromWindowBottomToBottom,props.pixelsFromNavToBottom);
    
  
      // if distance remaining in the scroll (including buffer) is less than the orignal nav to bottom....
      var pixels = (pixelsFromWindowBottomToBottom  - opts.bufferPx < props.pixelsFromNavToBottom);
      return pixels;    
    }    
    
    function showDoneMsg() {
      props.loadingMsg
        .find('img').hide()
        .parent()
          .find('div').html(opts.donetext).animate({opacity: 1},2000).fadeOut('normal');
      
      // user provided callback when done    
      opts.errorCallback();
    }

    function infscrSetupClick(){
        if (props.isDuringAjax || props.isInvalidPage || props.isDone) return; 
        $(document).trigger('retrieve.infscr');
    }


    function infscrSetup(){
        if (props.isDuringAjax || props.isInvalidPage || props.isDone) return; 
        
        if ( !isNearBottom(opts,props) ) return; 
        
        $(document).trigger('retrieve.infscr');
                
                
    }  // end of infscrSetup()
          
  
      
    function kickOffAjax(){
        // we dont want to fire the ajax multiple times
        props.isDuringAjax = true; 
        
        // show the loading message and hide the previous/next links
	$(opts.contentSelector).parent().append(props.loadingMsg);
        props.nextPage.hide();
        $( opts.navSelector ).hide(); 
        
        // increment the URL bit. e.g. /page/3/
        opts.currPage += opts.limit;

        debug('heading into ajax',path);
        
        // if we're dealing with a table we can't use DIVs
        box = $(opts.contentSelector).is('table') ? $('<tbody/>') : $('<div/>');
        frag = document.createDocumentFragment();


	var newUrl = path + opts.currPage + " " + opts.itemSelector;
        box.load( newUrl, null,loadCallback); 
    }
    
    function loadCallback(){
        // if we've hit the last page...
        if (props.isDone) { 
            showDoneMsg();
            return false;    
              
        } else {
          
            var children = box.children().children().get();

            // if it didn't return anything
            if (children.length == 0){
              // fake an ajaxError so we can quit.
              return $.event.trigger( "ajaxError", [{status:404}] ); 
            } 
            
            // use a documentFragment because it works when content is going into a table or UL
            while (box[0].firstChild){
              frag.appendChild(  box[0].firstChild );
            }

           	$(opts.contentSelector)[0].appendChild(frag);
            
            // fadeout currently makes the <em>'d text ugly in IE6
            props.loadingMsg.fadeOut('normal' ); 
	    if (props.isDone == false) {
		props.nextPage.show();
	    }

            // smooth scroll to ease in the new content
            if (opts.animate){ 
                var scrollTo = $(window).scrollTop() + $('#infscr-loading').height() + opts.extraScrollPx + 'px';
                $('html,body').animate({scrollTop: scrollTo}, 800,function(){ props.isDuringAjax = false; }); 
            }
        
            // previously, we would pass in the new DOM element as context for the callback
            // however we're now using a documentfragment, which doesnt havent parents or children,
            // so the context is the contentContainer guy, and we pass in an array
            //   of the elements collected as the first argument.
            callback.call( $(opts.contentSelector)[0], children );
        
            if (!opts.animate) props.isDuringAjax = false; // once the call is done, we can allow it again.
        }
    }
    
    var opts    = $.extend({}, $.infinitescroll.defaults, options),
        props   = $.infinitescroll, // shorthand
        box, frag;
        
    callback    = callback || function(){};

     // we doing this on an overflow:auto div?
    props.container   =  this;
                          
    // contentSelector we'll use for our .load()
    opts.contentSelector = opts.contentSelector || this; 
    
    // set the path to be a relative URL from root.
    path          = opts.path + "?isPanel=true" + (opts.pathSuffix || "") + "&start=";
    
    if (props.container.length == 0) {
	return;
    }

    // reset scrollTop in case of page refresh:
    if (opts.localMode) $(props.container)[0].scrollTop = 0;

    // distance from nav links to bottom
    // computed as: height of the document + top offset of container - top offset of nav link
    props.pixelsFromNavToBottom =  getDocumentHeight()  +
                                     (props.container == document.documentElement ? 0 : $(props.container).offset().top )- 
                                     $(opts.navSelector).offset().top;
    
    // define loading msg
    props.loadingMsg = $('<ul class="flexibee-rounded" style="padding-bottom:2em;"><li><span class="flexibee-clickable" style="background:url(&quot;'+opts.loadingImg+'&quot;) left no-repeat;padding-left:3.5em">'+opts.loadingText+'</span></li></ul>');
     // preload the image
    (new Image()).src    = opts.loadingImg;


    props.nextPage = $('<ul class="flexibee-rounded" style="padding-bottom:2em"><li><span class="flexibee-clickable">' +opts.loadNextText+ '</span></li></ul>').click(infscrSetupClick);
    $(opts.contentSelector).parent().append(props.nextPage);
              

  
    // set up our bindings
    $(document).ajaxError(function(e,xhr,opt) {
      debug('Page not found. Self-destructing...');    
      
      // die if we're out of pages.
      if (xhr.status == 404){ 
        showDoneMsg();
        props.isDone = true; 
        $(opts.bind).unbind('scroll.infscr');
      } 
    });
    
    // bind scroll handler to element (if its a local scroll) or window  
    $(opts.bind)
      .bind('scroll.infscr', infscrSetup);
    $(document)
      .bind('scroll.infscr', infscrSetup); 

    $(document).bind('retrieve.infscr',kickOffAjax);
    $( opts.navSelector ).hide(); 

    $(opts.bind).trigger('scroll.infscr'); // trigger the event, in case it's a short page


    return this;
  
  }  // end of $.fn.infinitescroll()
  

  
  // options and read-only properties object
  
  $.infinitescroll = {     
        defaults      : {
                          debug           : false,
                          preload         : false,
			  limit		  : 20,
			  path		  : null,
                          loadingImg      : "",
                          loadingText     : "<em>Loading the next set of posts...</em>",
                          loadNextText    : "<em>Load next page</em>",
                          donetext        : "<em>Congratulations, you've reached the end of the internet.</em>",
                          navSelector     : "div.navigation",
                          contentSelector : null,           // not really a selector. :) it's whatever the method was called on..
                          extraScrollPx   : 150,
                          itemSelector    : "div.post",
                          animate         : false,
                          bufferPx        : 40,
                          errorCallback   : function(){},
			              bind            : null,
                          currPage        : 1
                        }, 
        loadingImg    : undefined,
        loadingMsg    : undefined,
        container     : undefined,
        currDOMChunk  : null,  // defined in setup()'s load()
        isDuringAjax  : false,
        isInvalidPage : false,
        isDone        : false  // for when it goes all the way through the archive.
  };
  


})(jQuery);
