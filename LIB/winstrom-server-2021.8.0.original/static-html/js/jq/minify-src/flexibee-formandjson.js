(function($, window, undefined) {

var FormAndJson = function(prefix, props, filledProps, dependencies) {
    this.prefix = prefix; // prefix názvů všech políček v tomto formuláři (ovlivňuje atributy "id" a "name"), může být prázdný
    this.props = props; // typy vlastností (zda je to datum, číslo, text apod.)
    this.filledProps = filledProps; // vyplněné vlastnosti (mapa název vlastnosti -> boolean)

    this.dependencies = dependencies || {};
    // definuje závislosti mezi políčky
    // parametr je mapa v následujícím formátu:
    // {
    //   a: ["b", "c"],
    //   b: ["c"],
    //   c: ["a", "b"]
    // }
    // význam: pokud má dojít k doplnění formuláře vlivem změny na políčku "a", nebudou se na server posílat hodnoty políček "b" a "c",
    // pokud vlivem změny "b", nebude se posílat "c" atd.
};

// field je zadáno, pokud se doplňuje formulář a mají se brát v úvahu závislosti mezi políčky
// previousValues je zadáno, pokud se doplňuje formulář a mají se na serveru spustit postvalidy
// skipFields: pokud je zadáno, má to být mapa field->boolean a položky, které v ní mají true, nebudou ve výsledu uvedeny
FormAndJson.prototype.convertForm2Json = function(form, field, previousValues, skipFields) {
    var _this = this;

    var data = {};
    $(form).allInputs().not(".flexibee-dup").each(function() {
        var key = $(this).flexibeeName().replace(_this.prefix, "");
        var val = $(this).val();
        if ($(this).is(":checkbox")) {
            val = $(this).is(":checked");
        }

        // prázdné hodnoty pouze pokud šlo o vyplněné políčko
        var wasFilled = _this.filledProps[key];
        if (wasFilled || (val !== undefined && val !== "")) {
            var convertedVal = _this.valueForm2Json(key, val);
            if (convertedVal) {
                data[key] = convertedVal;
            } else if (wasFilled) {
                data[key] = "";
            }
        }
    });

    if (field) {
        var rawField = field.replace(_this.prefix, "");
        if (_this.dependencies[rawField]) {
            console.log("will strip out ", _this.dependencies[rawField], " because of dependencies on ", rawField); /*<STRIP-LINE>*/
            $.each(_this.dependencies[rawField], function(index, value) {
                delete data[value];
            });
        }
    }

    if (previousValues) {
        console.log("adding previous values ", previousValues, " for prefix ", _this.prefix); /*<STRIP-LINE>*/
        $.each(previousValues, function(key, val) {
            if (val != undefined) {
                var rawKey = key.replace(_this.prefix, "");
                var convertedVal = _this.valueForm2Json(rawKey, val);
                if (convertedVal) {
                    data[rawKey + "-previousValue"] = convertedVal;
                }
            }
        });
    }

    if (skipFields) {
        $.each(skipFields, function(key, val) {
            if (val) {
                delete data[key];
            }
        });
    }

    return data;
};

FormAndJson.prototype.repairJson = function(json) {
    var _this = this;

    var data = {};
    $.each(json, function(propName, value) {
        var convertedVal = _this.valueForm2Json(propName, value);
        if (convertedVal != undefined) {
            data[propName] = convertedVal;
        }
    });
    return data;
};

// skipFields: pokud je zadáno, má to být mapa field->boolean a položky, které v ní mají true, nebudou ve formuláři měněny
FormAndJson.prototype.convertJson2Form = function(json, form, skipFields) {
    var _this = this;

    $.each(json, function(propName, value) {
        if (/@|\$/.test(propName)) { // @ -- firma@ref, firma@showAs; $ -- $readonly, mj$nazev
            return;
        }

        // v některých situacích se políčko, které by se mělo na první pohled přeskočit, ve skutečnosti nepřeskakuje
        var shouldSkip = skipFields && skipFields[propName];

        var convertedVal = _this.valueJson2Form(propName, value, json);
        if (convertedVal !== null && convertedVal !== undefined) {
            var elems = $("#flexibee-inp-" + _this.prefix + propName + ", .flexibee-dup[id^='flexibee-inp-" + _this.prefix + propName + "-']", form);
            if (!elems.length) {
                elems = $("#flexibee-inp-" + _this.prefix + propName + "-flexbox");
            }

            if (elems.hasClass("flexibee-flexbox")) {
                // TODO pořešit duplicitní FlexBoxy (zatím se asi nepoužívají)
                // FlexBoxy se řeší úplně speciálně, je to prostě chucpe

                if (shouldSkip) return;

                if (json[propName + "@internalId"] && json[propName + "@showAs"]) {
                    var idVal = json[propName + "@internalId"];
                    $("#flexibee-inp-" + _this.prefix + propName + "-flexbox_hidden", form).val(idVal);
    
                    var showVal = json[propName + "@showAs"];
                    $("#flexibee-inp-" + _this.prefix + propName + "-flexbox_input").val(showVal);

                    $("#flexibee-inp-" + _this.prefix + propName).select2("data", {id: idVal, text: showVal}, false);
    
                    if ($("#flexibee-inp-" + _this.prefix + propName + "-flexbox_input-ro-label").length) {
                        $("#flexibee-inp-" + _this.prefix + propName + "-flexbox_input-ro-label").text(showVal);
                    }
                } else {
                    $("#flexibee-inp-" + _this.prefix + propName + "-flexbox_hidden", form).val("");
                    if (!$("#flexibee-inp-" + _this.prefix + propName + "-flexbox_input").hasClass("watermark")) { // watermark neodstraňovat
                        $("#flexibee-inp-" + _this.prefix + propName + "-flexbox_input").val("");
                        if ($("#flexibee-inp-" + _this.prefix + propName + "-flexbox_input-ro-label").length) {
                            $("#flexibee-inp-" + _this.prefix + propName + "-flexbox_input-ro-label").text("");
                        }
                    }
                }

                return;
            }

            $(elems).each(function() {
                var elem = $(this);

                // neměnit políčko, které má právě focus (uživatel ho dost možná edituje!)
                shouldSkip = shouldSkip || elem.is(":focus");
                var isEmptyOrZero = $.trim(elem.val()).length == 0 || /^0+[,.]?0*$/.test(elem.val());
                if (shouldSkip && elem.val() && !isEmptyOrZero) {
                    return;
                }

                if ($.isFunction(convertedVal)) {
                    convertedVal(elem);
                } else if (elem.is(":input")) {
                    elem.valWithAlert(convertedVal);
                } else {
                    convertedVal = json[propName + "@showAs"] || convertedVal;
                    elem.text(convertedVal);
                }

                elem.change();
            });
        }
    });
};

// --- private static ---

FormAndJson.CONVERTERS = {
    _identity: {
        form2json: function(key, value) { return value; },
        json2form: function(key, value, obj) { return value; }
    },
    integer: {
        form2json: function(key, value) {
            value = value.replace(/\s/, "");
            var num = parseInt(value, 10);
            if (num == 0 || num == NaN) {
                return null;
            }
            return value;
        }
    },
    numeric: {
        form2json: function(key, value) {
            value = value.replace(/\s/, "").replace(",", ".");
            var num = parseFloat(value);
            if (num == 0 || num == NaN) {
                return null;
            }
            return value;
        },
        json2form: function(key, value, obj) {
            var f = parseFloat(value);
            if (isNaN(f)) {
                return "";
            }
            return $.formatNumber(f);
        }
    },
    date: {
        form2json: function(key, value) {
            if (/^\d+\.\d+\.\d+$/.test(value)) {
                // M.D.YYYY -> YYYY-MM-DD
                var parts = value.split(/\./, 3);
                if (parts[0].length == 1) { parts[0] = "0" + parts[0]; }
                if (parts[1].length == 1) { parts[1] = "0" + parts[1]; }
                return parts[2] + "-" + parts[1] + "-" + parts[0];
            }
            return value;
        },
        json2form: function(key, value, obj) {
            if (/^\d+-\d+-\d+\+\d+:\d+$/.test(value)) {
                // YYYY-MM-DD+ZZ:ZZ -> D.M.YYYY
                var parts = value.replace(/\+.*$/, "").split(/-/, 3);
                var date = new Date(parseInt(parts[0], 10), parseInt(parts[1], 10) - 1, parseInt(parts[2], 10));
                return date.getDate() + "." + (date.getMonth() + 1) + "." + date.getFullYear();
            }
            return value;
        }
    },
    datetime: {
        form2json: function(key, value) {
            if (/^\d+\.\d+\.\d+,?\s+\d+:\d+(:\d+)?$/.test(value) || /^\d+\.\d+\.\d+$/.test(value)) {
                // M.D.YYYY H:MM(:SS)? -> YYYY-MM-DD'T'HH:MM:SS nebo M.D.YYYY -> YYYY-MM-DD
                var parts = value.split(/[\.: ]/);
                if (parts[0].length == 1) { parts[0] = "0" + parts[0]; }
                if (parts[1].length == 1) { parts[1] = "0" + parts[1]; }

                if (parts.length > 3) {
                    if (!parts[3]) { parts[3] = "0"; }
                    if (!parts[4]) { parts[4] = "0"; }
                    if (!parts[5]) { parts[5] = "0"; }
                    if (parts[3] && parts[3].length == 1) { parts[3] = "0" + parts[3]; }
                    if (parts[4] && parts[4].length == 1) { parts[4] = "0" + parts[4]; }
                    if (parts[5] && parts[5].length == 1) { parts[5] = "0" + parts[5]; }

                	return parts[2] + "-" + parts[1] + "-" + parts[0] + "T" + parts[3] + ":" + parts[4] + ":" + parts[5];
        	    } else {
                    return parts[2] + "-" + parts[1] + "-" + parts[0];
    	        }
            }
            return value;
        },
        json2form: function(key, value, obj) {
            if (/\d+-\d+-\d+T\d+:\d+:\d+\+\d+:\d+/.test(value) || /^\d+-\d+-\d+\+\d+:\d+$/.test(value)) {
                // YYYY-MM-DD'T'HH:MM:SS+ZZ:ZZ -> D.M.YYYY H:MM nebo YYYY-MM-DD+ZZ:ZZ -> D.M.YYYY
                var parts = value.replace(/\+.*$/, "").split(/[-:T]/);
                if (parts.length > 3) {
                    var dateTime = new Date(parseInt(parts[0], 10), parseInt(parts[1], 10) - 1, parseInt(parts[2], 10),
                            parseInt(parts[3], 10), parseInt(parts[4], 10));
                    var minutes = "" + dateTime.getMinutes();
                    if (minutes.length == 1) { minutes = "0" + minutes; }
                    return dateTime.getDate() + "." + (dateTime.getMonth() + 1) + "." + dateTime.getFullYear()
                            + " " + dateTime.getHours() + ":" + minutes;
                } else {
                    var date = new Date(parseInt(parts[0], 10), parseInt(parts[1], 10) - 1, parseInt(parts[2], 10));
                    return date.getDate() + "." + (date.getMonth() + 1) + "." + date.getFullYear();
                }
            }
            return value;
        }
    },
    logic: {
        form2json: function(key, value) {
            if (typeof(value) === "boolean") {
                return JSON.stringify(value); // WinStrom Server očekává logické hodnoty jako stringy
            }
            return value;
        },
        json2form: function(key, value, obj) {
            return function(elem) {
                $(elem).attr("checked", JSON.parse(value)); // WinStrom Server posílá logické hodnoty jako stringy
            };
        }
    },
    relation: {
        json2form: function(key, value, obj) {
            return obj[key + "@internalId"] || value;
        }
    }
};

// --- private ---

FormAndJson.prototype.getConverter = function(propName, direction) {
    var converters = this.constructor.CONVERTERS[this.props[propName]] || this.constructor.CONVERTERS._identity;
    return converters[direction] || this.constructor.CONVERTERS._identity[direction];
};

FormAndJson.prototype.valueJson2Form = function(key, value, obj) {
    if (/@/.test(key)) {
        return null;
    }

    var converter = this.getConverter(key, "json2form");
    return converter(key, value, obj);
};

FormAndJson.prototype.valueForm2Json = function(key, value) {
    var converter = this.getConverter(key, "form2json");
    return converter(key, value);
};

// ---

// expose to global object
window.flexibee = window.flexibee || {};
window.flexibee.FormAndJson = FormAndJson;

})(jQuery, window);
