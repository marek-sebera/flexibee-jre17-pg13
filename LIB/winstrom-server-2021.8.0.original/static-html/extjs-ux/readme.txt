Uživatelská rozšíření (User eXtensions) pro ExtJS:

- z extjs-4.1.1-rc2/examples/ux
  * BoxReorderer.js
  * PreviewPlugin.js
  * TabCloseMenu.js
  * TabReorderer.js
  * form/*
  * grid/*
  * statusbar/*

- z https://github.com/kveeiv/extjs-boxselect
  * form/field/BoxSelect.*

Rozšíření dodávaná s ExtJS distribuujeme sami, abychom nemuseli řešit konflikty
s rozšířeními odjinud. Namespace Ext.ux se totiž běžně používá pro všechna
uživatelská rozšíření (např. ta z diskusních fór sencha.com).
