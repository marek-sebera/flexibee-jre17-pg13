// ukázka, jak upravit hlavní menu
Ext.define('com.example.MenuPluginSample', {
    modifyMainMenu: function(menu) {
        //menu.length = 0;
        menu.push({
            id: 'customMenu',
            name: 'Moje menu',
            items: [
                {
                    id: 'faktury',
                    name: 'Odkaz přímo do faktur',
                    handler: {
                        type: 'evidence',
                        id: 'faktura_vydana',
                        name: 'Faktury'
                    }
                },
                {
                    id: 'dumbController',
                    name: 'Hloupý kontrolér',
                    handler: {
                        type: 'mvc',
                        controller: 'com.example.DumbController',
                        name: 'Můj hloupý panel'
                    }
                },
                {
                    id: 'cleverController',
                    name: 'Chytrý kontrolér',
                    handler: {
                        type: 'mvc',
                        controller: 'com.example.CleverController',
                        name: 'Můj chytrý panel'
                    }
                },
                {
                    id: 'FlexiBeeEU',
                    name: 'Otevřít flexibee.eu',
                    handler: {
                        type: 'iframe',
                        name: 'FlexiBee.eu',
                        link: 'http://www.flexibee.eu/'
                    }
                },
                {
                    id: 'alert',
                    name: 'Jednoduchý alert',
                    handler: {
                        type: 'custom',
                        action: function() {
                            window.alert('BAGR!!');
                        }
                    }
                }
            ]
        });
    }
});
