// ukázka, jak přidat položky do hlavního statusbaru
//
// metoda defineAdditionalItems může vrátit jednu položku nebo pole
Ext.define('com.example.MainStatusbarPluginSample', {
    defineAdditionalItems: function() {
        return [
            ' ',
            {
                xtype: 'container',
                html: '<a href="http://www.flexibee.eu/produkty/flexibee/licencni-podminky/">Licenční ujednání</a>'
            }
        ];
    }
});
