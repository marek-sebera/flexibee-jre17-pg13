// ukázka vlastního panelu (viz CleverController.js)
Ext.define('com.example.AdresarForm', {
    extend: 'Ext.form.Panel',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    items: Ext.create('FlexiBee.adresar.Properties').formItems(function(property) {
        return [
            {
                xtype: 'fieldset',
                title: 'Políčka',
                margin: 10,
                items: [
                    property('kod', {
                        emptyText: "Kód je povinný"
                    }),
                    property('nazev')
                ]
            },
            {
                xtype: 'fieldset',
                title: 'Nesmysly',
                margin: 10,
                items: [
                    {
                        xtype: 'checkboxgroup',
                        fieldLabel: 'Výběr',
                        items: [
                            {
                                xtype: 'checkbox',
                                boxLabel: 'Jedna'
                            },
                            {
                                xtype: 'checkbox',
                                boxLabel: 'Dva'
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'fieldset',
                title: 'Operace',
                margin: 10,
                items: [
                    {
                        xtype: 'button',
                        text: 'Uložit'
                    }
                ]
            }
        ];
    })
});
