// ukázka, jak upravit toolbar v adresáři
Ext.define('com.example.ToolbarPluginSample', {
    modifyToolbar: function(items) {
        items.push(
            {
                id: 'ares',
                name: 'ARES',
                icon: false,
                show: function(ctx) {
                    return ctx.isRecordInList || ctx.isDetailNew || ctx.isDetailExisting;
                },
                handler: {
                    type: 'custom',
                    action: function(record) {
                        if (!record) {
                            // nastalo by jen kdyby funkce 'show' výše vrátila true i v případě, že platí jen ctx.inList
                            Ext.MessageBox.alert('Žádný záznam', 'Není vybrán žádný záznam');
                            return;
                        }
                        if (!record.ic) {
                            Ext.MessageBox.alert('Neuvedené IČO', 'Vybraný záznam neobsahuje IČO');
                            return;
                        }

                        var nazev = record.nazev;
                        var ic = record.ic;

                        var tab = Ext.create('Ext.panel.Panel', {
                            closable: true,
                            layout: 'fit',
                            title: "ARES " + nazev,
                            items: [
                                {
                                    xtype: 'component',
                                    mode: 'iframe',
                                    autoEl: {
                                        tag: "iframe",
                                        src: 'http://wwwinfo.mfcr.cz/cgi-bin/ares/darv_or.cgi?ico=' + ic + '&jazyk=cz&xml=1'
                                    }
                                }
                            ]
                        });

                        var tabs = Ext.getCmp('content');
                        tabs.add(tab);
                        tabs.setActiveTab(tab);
                    }
                }
            }
        );
    }
});
