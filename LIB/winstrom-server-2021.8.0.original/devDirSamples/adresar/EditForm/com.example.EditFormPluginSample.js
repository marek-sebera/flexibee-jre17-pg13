// ukázka, jak udělat vlastní editační formulář
Ext.define('com.example.EditFormPluginSample', {
    defineEditFormItems: function(property) {
        return [
            {
                xtype: 'fieldset',
                title: 'Políčka z FlexiBee',
                margin: 10,
                items: [
                    property('kod', {
                        emptyText: "Kód je třeba vyplnit"
                    }),
                    property('nazev'),
                    property('stitky')
                ]
            },
            {
                xtype: 'fieldset',
                title: 'Moje políčka',
                margin: 10,
                items: [
                    {
                        xtype: 'radiogroup',
                        fieldLabel: 'Výběr',
                        items: [
                            {
                                xtype: 'radiofield',
                                boxLabel: 'Jedna'
                            },
                            {
                                xtype: 'radiofield',
                                boxLabel: 'Dva'
                            }
                        ]
                    }
                ]
            }
        ];
    }
});
