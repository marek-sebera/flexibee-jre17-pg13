// ukázka, jak přeuspořádat sloupečky v gridu
Ext.define('com.example.GridPluginSample', {
    defineGridColumns: function(property) {
        return [
            property('kod', {
                text: 'Kód objektu'
            }),
            property('nazev'),
            {
                xtype: 'templatecolumn',
                text: 'Adresa',
                tpl: '<tpl if="ulice">{ulice},</tpl> {psc} {mesto}'
            }
        ];
    }
});
