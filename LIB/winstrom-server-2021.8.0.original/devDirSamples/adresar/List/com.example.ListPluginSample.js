// ukázka, jak přidat položky do panelu s výpisem záznamů
//
// samostatný výpis je v gridu, který je umístěn jako 'center' položka tohoto panelu (ten má totiž layout 'border')
// okraje panelu jsou dostupné pro přizpůsobování
//
// metoda defineAdditionalItems může vrátit jednu položku nebo pole
Ext.define('com.example.ListPluginSample', {
    defineAdditionalItems: function() {
        return {
            xtype: 'panel',
            region: 'east',
            width: 100,
            html: 'aaa'
        };
    }
});
