// ukázka, jak přidat položky do panelu s detailem záznamu
//
// samostatný detail je v editačním formuláři, který je umístěn jako 'center' položka tohoto panelu (ten má totiž layout 'border')
// okraje panelu jsou dostupné pro přizpůsobování
//
// metoda defineAdditionalItems může vrátit jednu položku nebo pole
Ext.define('com.example.DetailPluginSample', {
    defineAdditionalItems: function() {
        return {
            xtype: 'panel',
            region: 'east',
            width: 100,
            html: 'bbb'
        };
    }
});
