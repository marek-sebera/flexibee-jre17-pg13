// ukázka, jak přidat panel do relací u záznamu
//
// metoda defineAdditionalItems může vrátit jednu položku nebo pole
Ext.define('com.example.RelationsPluginSample', {
    template: new Ext.Template('<strong>Obsah další záložky</strong> (firma <em>{name}</em>)').compile(),

    defineAdditionalItems: function(record) {
        return {
            xtype: 'panel',
            title: 'Další záložka',
            html: this.template.apply({ name: record.nazev }) 
        };
    }
});
