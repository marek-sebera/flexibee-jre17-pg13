function(doc, req) {

    var type;

    if (doc._deleted) {

        // u smazanych dokumentu nemam jinou moznost nez zjistit typ z IDcka; viz tez ConventionDocument.getType
        var id = doc._id;
        var instanceDelimiterIdx = id.indexOf("@");
        var typeDelimiterIdx = id.indexOf("#");

        if (typeDelimiterIdx == -1) {
            type = id.substring(instanceDelimiterIdx + 1);
        } else {
            type = id.substring(instanceDelimiterIdx + 1, typeDelimiterIdx);
        }

    } else {
        type = doc.type;
    }

    return type && (type == "instance"
                || type == "group"
                || type == "dbServerPool"
                || type == "appServerPool"
                || type == "companyPools"
                || type == "setting"
                || type == "company"
                || type == "user"
        );
}
