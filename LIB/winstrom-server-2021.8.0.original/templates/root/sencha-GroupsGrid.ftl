<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.root.GroupsGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.fbGroupsGrid',

    requires: [
        'FlexiBee.root.GroupsStore',
        'FlexiBee.root.ReloadButton'
    ],

    store: 'FlexiBee.root.GroupsStore',
    columns: [
        {
            text: 'Název',
            flex: 1,
            dataIndex: 'nazev'
        }, {
            text: 'ID license',
            dataIndex: 'licenseId'
        }, {
            text: 'ID',
            width: '50%',
            dataIndex: 'id'
        }
    ],
    tbar: {
        xtype: 'toolbar',
        items: [
            {
                xtype: 'fbReloadButton'
            }
        ]
    },    initComponent: function() {
       var me = this;

       // every grid needs its own instance of the store (because store is stateful!)
       me.store = Ext.create('FlexiBee.root.GroupsStore');

       me.callParent(arguments);
    }
});

</#escape>