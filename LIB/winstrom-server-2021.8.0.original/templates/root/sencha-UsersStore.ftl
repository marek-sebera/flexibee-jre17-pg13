<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.root.UsersStore', {
    extend: 'Ext.data.Store',

    requires: [
        'FlexiBee.root.UsersModel'
    ],

    autoLoad: true,
    autoSync: true,
    model: 'FlexiBee.root.UsersModel',
    proxy: {
       type: 'rest',
       url: '/u.json',
       reader: {
           type: 'json',
           root: 'users.user'
       },
       writer: {
           type: 'json'
       }
    }
});

</#escape>