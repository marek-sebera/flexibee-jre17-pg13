<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.root.Toolbar', {
    extend: 'Ext.toolbar.Toolbar',
    alias: 'widget.fbRootToolbar',

    requires: [
    ],

    baseCls: 'flexibee-background-dark',
    margin: '0 15px 0 0',

    defaults: {
        cls: 'flexibee-app-toolbar',
        scale: 'medium'
    },

    items: [
        {
            xtype: 'container',
            margin: '0',
            style: 'color: white',
            html: '<h1>${dlgText('admin.title')}</h1>'
        },
        <#if isPanel == false>
        '->',
        {
            xtype: 'button',
            text: '<#if loggedUser.prijmeni?? || loggedUser.jmeno??>${loggedUser.prijmeni} ${loggedUser.jmeno}<#else>${loggedUser.name}</#if>',
            icon: '${staticPrefix}/img/skin/${skin}/sencha/user.png',
            <#if (it.getVariableValue("SSO_CHNG_PASSWD_URL")!)??>
            handler: function() {
                window.location = '${it.getVariableValue("SSO_CHNG_PASSWD_URL")}';
            },
            </#if>
            itemId: 'openCurrentUser'
        },
        {
            xtype: 'button',
            text: '${actText('logout')}',
            handler: function() {
                window.location = '/login-logout/logout';
            }
        },
        </#if>
    ],

    initComponent: function() {
        var me = this;

        var additionalItems = FlexiBee.Plugins.invoke(me, 'defineAdditionalItems');
        Ext.iterate(additionalItems, function(additionalItem) {
            if (Ext.isArray(additionalItem)) {
                me.items = me.items.concat(additionalItem);
            } else {
                me.items.push(additionalItem);
            }
        });

        this.callParent(arguments);
    }
});

<#import "/l-sencha.ftl" as sencha>
<@sencha.includePlugins path="root.Toolbar"/>

</#escape>
