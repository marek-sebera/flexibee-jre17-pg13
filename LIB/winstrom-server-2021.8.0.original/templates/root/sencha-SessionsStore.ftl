<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.root.SessionsStore', {
    extend: 'Ext.data.Store',

    requires: [
        'FlexiBee.root.SessionsModel'
    ],

    autoLoad: true,
    autoSync: true,
    model: 'FlexiBee.root.SessionsModel',
    proxy: {
       type: 'rest',
       url: '/status/session.json',
       reader: {
           type: 'json',
           root: 'authSessionInfos.connection'
       },
       writer: {
           type: 'json'
       }
    }
});

</#escape>