<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.root.ReloadButton', {
    extend: 'Ext.Button',
    alias: 'widget.fbReloadButton',

    requires: [
        'Ext.Button',
        'Ext.toolbar.Paging'
    ],

    tooltip: Ext.create('Ext.toolbar.Paging').refreshText,
    overflowText: Ext.create('Ext.toolbar.Paging').refreshText,
    scale: 'medium',
    icon: '${staticPrefix}/img/images/win/refresh.png',
    handler: function() {
        this.up().up().store.reload();
    }

});

</#escape>
