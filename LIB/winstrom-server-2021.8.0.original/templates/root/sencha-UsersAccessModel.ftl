<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.root.UsersAccessModel', {
    extend: 'Ext.data.Model',
    alias: 'widget.fbUsersAccessModel',

    requires: [
        'FlexiBee.root.GroupsModel',
        'FlexiBee.root.AccessModel',
    ],

    idProperty: 'username',

    fields: [
        {
            name: 'username',
            type: 'string'
        },
        {
            name: 'userId',
            type: 'int',
            defaultValue: -1
        },
        {
            name: 'fullname',
            type: 'string'
        },
        {
            name: 'email',
            type: 'string'
        },
        {
            name: 'phone',
            type: 'string'
        },
        {
            name: 'manageAll',
            type: 'boolean'
        },
        {
            name: 'accessList',
            convert: function(value, record) {
                console.log('record=' + record.data.username + ', accessList=' + Ext.encode(value));
                return value;
            }
        },
        {
            name: 'groupId',
            type: 'string'
        },
        {
            name: 'companies',
            mapping: 'accessList',
            convert: function(value, record) {
                if (value) {
                    return value.companyId;
                }
                return null;
            }
        }
    ],

    <#-- aktuálně nevyužité -->
    associations: [
        {
            type: 'hasMany',
            model: 'FlexiBee.root.AccessModel',
            name: 'accessList'
        }
    ],

    isSaved: function() {
        return this.get('userId') != -1;
    }
});

</#escape>