<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.root.UsersAccessStore', {
    extend: 'Ext.data.Store',
    alias: 'widget.fbUsersAccessStore',

    requires: [
        'FlexiBee.root.UsersAccessModel'
    ],

    autoLoad: true,
//    autoSync: true,
    model: 'FlexiBee.root.UsersAccessModel',

    proxy: {
        type: 'rest',
        url: '/a',
        format: 'json',

        extraParams: {
            token: '${csrfToken}'
        },

        reader: {
            type: 'json',
            successProperty: 'success',
            root: 'userAccessList.userAccess'
        },
        writer: {
            type: 'json',
            //root: 'userAccess',
            writeAllFields: false
        },
        listeners: {
            exception: function(proxy, response, operation) {
                Ext.MessageBox.show({
                    title: 'Chyba čtení dat',
                    msg: operation.getError(),
                    icon: Ext.MessageBox.ERROR,
                    buttons: Ext.Msg.OK
                });
            }
        }
    },

    load: function() {
        console.log('Users access loading...');
        this.callParent([this.loadCallback]);
    },

    loadCallback: function(records, operation, success) {
        if (success) {
            console.log('Users access loaded=' + success + ', records=' + records.length);
        } else {
            console.log('Users access loaded=' + success);
        }
    },

    listeners: {
        write: function(store, operation) {
            var record = operation.getRecords()[0],
                name = Ext.String.capitalize(operation.action),
                verb;

            if (name == 'Destroy') {
                //record = operation.records[0];
                verb = name + 'ed';
            } else {
                verb = name + 'd';
            }

            Ext.example.msg(name, Ext.String.format("{0} user: {1}", verb, record.getId()));
        }
    }
});

</#escape>
