<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.root.CompaniesPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.fbCompaniesPanel',

    style: 'background:transparent none repeat scroll 0 0',

    requires: [
        'FlexiBee.root.CompaniesModel',
        'FlexiBee.root.CompaniesStore',
        'FlexiBee.root.CompaniesGrid'

    ],

    store: 'FlexiBee.root.CompaniesStore',
    columns: [
        {
            text: 'Logo',
            width: 100,
            dataIndex: 'dbNazev',
            renderer: function(value) {
                return '<img src="/c/' + value + '/logo/thumbnail" />';
            }
        },
        {
            text: 'Název',
            flex: 1,
            dataIndex: 'nazev'
        }
    ],

    hideHeaders: true,
    frame: false,
    border: 0,
    bodyBorder: 0,
    maxWidth: '300px',
    rowLines: false,

    initComponent: function() {
       var me = this;

       // every grid needs its own instance of the store (because store is stateful!)
       me.store = Ext.create('FlexiBee.root.CompaniesStore');

       me.callParent(arguments);
    }
});

</#escape>
