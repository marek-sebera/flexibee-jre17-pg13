<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.root.SessionsGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.fbSessionsGrid',

    requires: [
        'FlexiBee.root.SessionsStore',
        'FlexiBee.root.ReloadButton'
    ],

    store: 'FlexiBee.root.SessionsStore',
    columns: [
        {
            text: 'Uživatel',
            flex: 1,
            dataIndex: 'username'
        }, {
            text: 'Typ',
            dataIndex: 'clientType'
        }, {
            text: 'Blokuje licenci',
            dataIndex: 'consumesLicense',
            trueText: 'Ano',
            falseText: 'Ne',
            xtype: 'booleancolumn'
        }, {
            width: 100,
            text: 'Datum přihlášení',
            dataIndex: 'createDateAsString'
        }, {
            width: 100,
            text: 'Datum posledního přístupu',
            dataIndex: 'keepAliveAsString'
        }, {
            text: 'ID',
            width: 100,
            dataIndex: 'shortSessionId'
        }
    ],
    tbar: [
        {
            xtype: 'fbReloadButton'
        },
        {
            text: 'Odpojit',
            scale: 'medium',
            icon: '${staticPrefix}/img/images/win/zamek.png',
            handler : function(){
            }
        }
    ],
    initComponent: function() {
       var me = this;

       // every grid needs its own instance of the store (because store is stateful!)
       me.store = Ext.create('FlexiBee.root.SessionsStore');

       me.callParent(arguments);
    }
});

</#escape>