<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.root.CompaniesModel', {
    extend: 'Ext.data.Model',

    requires: [
        'FlexiBee.root.GroupsModel'
    ],

    <#--//TODO až bude CouchdbCentralServerDatabase.convertCompany nastavovat smysluplné ID, tak lze změnit na "id" -->
    idProperty: 'dbNazev',

    fields: [
        {
            name: 'id',
            type: 'int'
        },
        {
            name: 'dbNazev',
            type: 'string'
        },
        {
            name: 'createDt',
            type: 'date'
        },
        {
            name: 'nazev',
            type: 'string'
        },
        {
            name: 'licenseGroup',
            type: 'string'
        },
        {
            name: 'show',
            type: 'boolean'
        }
    ],

    associations: [{
        type: 'hasOne',
        model: 'FlexiBee.root.GroupsModel',
        foreignKey: 'id',
        associationKey: 'licenseGroup',
        autoLoad: true
    }]

});

</#escape>