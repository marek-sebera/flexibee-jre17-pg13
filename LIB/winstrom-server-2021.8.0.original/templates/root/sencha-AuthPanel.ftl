<#ftl encoding="utf-8"/>
<#include "/i-localization.ftl"/>
<#escape x as x?default("")?js_string>
<#macro chkbox varName lblKey>
{
    xtype: 'checkbox',
    boxLabel: '<@act lblKey/>',
    name: '${varName}',
    checked: <#if it.getVariableValue(varName)>true<#else>false</#if>,
    inputValue: 'true',
    uncheckedValue: 'false'
}
</#macro>

<#macro idpSettingsList protocolName>
<ul style="list-style: disc !important;">\
<#list it.getIdpSettingsModel(protocolName) as opt>
    <li style="list-style: inherit;"><@lbl "authn.idp_settings.${opt.id}"/>: <b><tt>${opt.value}</tt></b></li>\
</#list>
</ul>\
</#macro>


function showIfHasCls(elem, clsName) {
    if (elem.hasCls(clsName)) {
        elem.show();
    } else {
        if (elem.cls != null && elem.cls != '') {
            elem.hide();
        }
    }
}

function authnTypeSelected(rb, state) {
    if (!state)
        return;

    var authnType = rb.inputValue;
    var ssoSettingsPanel = rb.up('form').down('#sso_settings');
    showIfHasCls(ssoSettingsPanel, authnType);

    var fields = ssoSettingsPanel.query('');
    if (fields.length) {
        for (var i = 0, l = fields.length; i < l; i++) {
            showIfHasCls(fields[i], authnType);
        }
    }
}


// vlastní validace vstupu SHA-1 otisku
var sha1HashTest = /^([0-9A-F]{2}:){19}[0-9A-F]{2}$/i;
Ext.apply(Ext.form.field.VTypes, {
    sha1Hash: function(val, field) {
        return sha1HashTest.test(val);
    },
    sha1HashText: '<@msg "error.not_sha1_hash"/>',
    sha1HashMask: /[0-9A-F:]/i
});


Ext.onReady(function() {
    var rb = Ext.getCmp('${it.getVariableValue("AUTHN_TYPE")}');
    if (rb) {
        authnTypeSelected(rb, true);
    }
});


Ext.define('FlexiBee.root.AuthPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.fbAuthPanel',

    requires: [
        'FlexiBee.root.Plugins',
        'FlexiBee.root.BaseForm',
        'Ext.form.Panel',
        'Ext.panel.Panel',
        'Ext.JSON'
    ],

    autoScroll: true,
    border: 0,
    defaultButton: 0,

    onSuccess: function(form, action) {
    },
    onFailure: function(form, action) {
        if (action.failureType == 'server') {
            obj = Ext.JSON.decode(action.response.responseText);
            Ext.Msg.alert('Login Failed!', obj.errors.reason);
        } else {
            Ext.Msg.alert('Warning!', 'Authentication server is unreachable : ' + action.response.responseText);
        }
    },

    items: [
        {
            xtype: 'fbBaseForm',
            url: '/setting.json?token=${csrfToken}',

            items: [
                {
                    xtype: 'container',
                    border: 1,
                    items: [
                    <#if it.getVariableVisible("SUPPORT_MZDY") || it.getVariableVisible("LISTEN_PUBLIC")>
                        {
                            xtype: 'container',
                            padding: '0 0 15px 0',
                            html: '<h1 style="border-bottom: 1px solid black;"><@dlg "authn.basic_settings.header"/></h1>'
                        }
                        <#if it.getVariableVisible("LISTEN_PUBLIC")>,
                            <@chkbox "LISTEN_PUBLIC" "nastavServerVSiti"/>
                        </#if>
                        <#if it.getVariableVisible("SUPPORT_MZDY")>,
                            <@chkbox "SUPPORT_MZDY" "nastavServerMzdyPristup"/>
                        </#if>
                    </#if>
                        ,
                        {
                            xtype: 'container',
                            padding: '0 0 15px 0',
                            html: '<h1 style="border-bottom: 1px solid black;"><@dlg "authn.authn_settings.header"/></h1>'
                        }
                        ,
                        {
                            xtype: 'container',

                            items: [
                                <#list it.getAuthnTypeOptionsModel() as ato>
                                    {
                                        xtype: 'radiofield',
                                        id: '${ato.id}',
                                        name: 'AUTHN_TYPE',
                                        boxLabel: '<@act "authn.type.${ato.id}"/>',
                                        inputValue: '${ato.id}',
                                        <#-- disabled: ${(!ato.enabled)?string}, -->
                                        checked: ${ato.selected?string},
                                        handler: authnTypeSelected
                                    }
                                    ,
                                    {
                                        xtype: 'container',
                                        padding: '0 0 10px 15px',
                                        html: '<small><@lbl "authn.type.${ato.id}.description" ""/></small>'
                                    }
                                    <#if ato_has_next>,</#if>
                                </#list>
                            ]
                        }
                        ,
                        {
                            xtype: 'container',
                            padding: '0px 0 15px 0',
                            html: '<h1 style="border-bottom: 1px solid black;"><@dlg "authn.additional_settings.header"/></h1>'
                        }
                        <#if it.getVariableVisible("OTP_REQUIRE")>,
                            <@chkbox "OTP_REQUIRE" "authn.require_otp"/>
                        </#if>
                        <#if it.getVariableVisible("WEB_DISABLE_HTML_LOGIN")>,
                            <@chkbox "WEB_DISABLE_HTML_LOGIN" "authn.use_old_http_authn"/>
                        </#if>
                        <#if it.getVariableVisible("PASSWORD_QUALITY")>,
                        {
                            xtype: 'numberfield',
                            minValue: 1,
                            maxValue: 5,
                            size: 5,
                            fieldLabel: '<@lbl "authn.password_quality"/>',
                            name: 'PASSWORD_QUALITY',
                            value: '${it.getVariableValue("PASSWORD_QUALITY")!1}'
                        }
                        </#if>
                        <#if it.getVariableVisible("ACCESS_CONTROL_ALLOW_ORIGIN")>,
                        {
                            name: 'ACCESS_CONTROL_ALLOW_ORIGIN',
                            xtype: 'textfield',
                            cls: '',
                            size: 80,
                            fieldLabel: '<@lbl "authn.ACCESS_CONTROL_ALLOW_ORIGIN"/>',
                            value: '${it.getVariableValue("ACCESS_CONTROL_ALLOW_ORIGIN")!""}'
                        },
                        {
                            xtype: 'container',
                            padding: '0 0 10px 15px',
                            html: '<small><@lbl "authn.ACCESS_CONTROL_ALLOW_ORIGIN.description" ""/></small>'
                        }
                        </#if>,
                        {
                            xtype: 'container',
                            id: 'sso_settings',
                            cls: 'OPENID SAML IT_HIGHWAY',

                            defaults: {
                                xtype: 'container'
                            },

                            items: [
                                {
                                    padding: '20px 0 15px 0',
                                    html: '<h1 style="border-bottom: 1px solid black;"><@dlg "authn.additional_sso_settings.header"/></h1>'
                                }
                                <#if it.getVariableVisible("SSO_LOGIN_URL")>,
                                {
                                    xtype: 'textfield',
                                    name: 'SSO_LOGIN_URL',
                                    size: 80,
                                    vtype: 'url',
                                    fieldLabel: '<@lbl "authn.sso_login_url"/>',
                                    <#if !it.getVariableEditable("SSO_LOGIN_URL")>disabled: true,</#if>
                                    value: '${it.getVariableValue("SSO_LOGIN_URL")!""}'
                                }
                                ,
                                {
                                    padding: '0 0 5px 15px',
                                    html: '<small><@lbl "authn.sso_login_url.description"/></small>'
                                }
                                ,
                                {
                                    cls: 'SAML IT_HIGHWAY',
                                    padding: '0 0 10px 45px',
                                    html: '<small><@idpSettingsList "SAML"/></small>'
                                }
                                ,
                                {
                                    cls: 'OPENID',
                                    padding: '0 0 10px 45px',
                                    html: '<small><@idpSettingsList "OPENID"/></small>'
                                }
                                </#if><#if it.getVariableVisible("SSO_LOGOUT_URL")>,
                                {
                                    xtype: 'textfield',
                                    name: 'SSO_LOGOUT_URL',
                                    size: 80,
                                    vtype: 'url',
                                    fieldLabel: '<@lbl "authn.sso_logout_url"/>',
                                    value: '${it.getVariableValue("SSO_LOGOUT_URL")!""}'
                                }
                                </#if><#if it.getVariableVisible("SSO_CHNG_PASSWD_URL")>,
                                {
                                    xtype: 'textfield',
                                    name: 'SSO_CHNG_PASSWD_URL',
                                    cls: 'OPENID SAML',
                                    size: 80,
                                    vtype: 'url',
                                    fieldLabel: '<@lbl "authn.sso_chng_passwd_url"/>',
                                    <#if !it.getVariableEditable("SSO_CHNG_PASSWD_URL")>disabled: true,</#if>
                                    value: '${it.getVariableValue("SSO_CHNG_PASSWD_URL")!""}'
                                }
                                ,
                                {
                                    padding: '0 0 10px 15px',
                                    html: '<small><@lbl "authn.sso_chng_passwd_url.description"/></small>'
                                }
                                </#if><#if it.getVariableVisible("SSO_CERT_FINGERPRINT")>,
                                {
                                    xtype: 'textfield',
                                    name: 'SSO_CERT_FINGERPRINT',
                                    cls: 'SAML IT_HIGHWAY',
                                    size: 64,
                                    vtype: 'sha1Hash',
                                    fieldLabel: '<@lbl "authn.sso_cert_fingerprint"/>',
                                    value: '${it.getVariableValue("SSO_CERT_FINGERPRINT")!""}'
                                }
                                ,
                                {
                                    cls: 'SAML IT_HIGHWAY',
                                    padding: '0 0 10px 15px',
                                    html: '<small><@lbl "authn.sso_cert_fingerprint.description"/></small>'
                                }
                                </#if><#if it.getVariableVisible("SSO_SERVICE_PROVIDER_ID")>,
                                {
                                    xtype: 'textfield',
                                    name: 'SSO_SERVICE_PROVIDER_ID',
                                    cls: 'SAML',
                                    fieldLabel: '<@lbl "authn.service_provider_id"/>',
                                    value: '${it.getVariableValue("SSO_SERVICE_PROVIDER_ID")!""}'
                                }
                                ,
                                {
                                    cls: 'SAML',
                                    padding: '0 0 10px 15px',
                                    html: '<small><@lbl "authn.service_provider_id.description"/></small>'
                                }
                                </#if>
                            ]
                        }
                    ]
                }
            ],
            // All the magic happens after the user clicks the button
            buttons: [{
                text: '<@lbl "ulozit" "Uložit"/>',
                formBind: true,
                // Function that fires when user clicks the button
                handler: function() {
                    var form = this.up('form');
                    form.getForm().submit({
                         success: form.onSuccess,
                         failure: form.onFailure
                    });
                }
            }]
        }
    ]
});

</#escape>
