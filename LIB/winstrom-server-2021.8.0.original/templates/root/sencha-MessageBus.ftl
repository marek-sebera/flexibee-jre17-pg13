<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.root.MessageBus', {
    singleton: true,
    alternateClassName: 'FlexiBee.MessageBus',

    extend: 'Ext.util.Observable',

    publish: function(topic, message) {
        this.fireEvent(topic, message);
    }
});

</#escape>
