<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>


Ext.define('FlexiBee.root.CompaniesGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.fbCompaniesGrid',

    requires: [
        'FlexiBee.root.CompaniesStore',
        'FlexiBee.root.CompaniesModel',
        'FlexiBee.root.ReloadButton'
    ],

    store: 'FlexiBee.root.CompaniesStore',
    columns: [
        {
            text: 'ID',
            width: 40,
            dataIndex: 'id'
        }, {
            text: 'Název',
            flex: 1,
            dataIndex: 'nazev'
        }, {
            width: 100,
            text: 'Datum vytvoření',
            xtype: 'datecolumn',
            format: 'd.m.Y H:i',
            dataIndex: 'createDt'
        }, {
            text: 'Licenční skupina',
            width: 210,
            dataIndex: 'licenseGroup'
        }
    ],
    tbar: {
        xtype: 'toolbar',
        items: [
            {
                xtype: 'fbReloadButton'
            }
        ]
    },
    initComponent: function() {
       var me = this;

       // every grid needs its own instance of the store (because store is stateful!)
       me.store = Ext.create('FlexiBee.root.CompaniesStore');

       me.callParent(arguments);
    }
});

</#escape>