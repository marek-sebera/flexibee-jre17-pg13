<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-sencha-header.ftl"/>

<script>

Ext.require([
    'Ext.tip.QuickTipManager',
    <#if isProduction>
    'FlexiBee.root.All'
    <#else>
    'FlexiBee.root.StatusBar',
    'FlexiBee.root.CompaniesPanel'

    </#if>
]);

Ext.onReady(function() {

    Ext.QuickTips.init();

    var resolutionX = 1920, resolutionY = 1200;
    if (screen.width < 1900) {
        resolutionX = screen.width;
    }

    var height = 50;

    if (screen.height < 1900) {
        resolutionY = screen.height - height;
    }

    Ext.define('FlexiBee.root.CompaniesSelection', {
        extend: 'Ext.view.View',
        alias: 'widget.fbCompaniesSelection',

        requires: [
            'FlexiBee.root.CompaniesStore',
            'FlexiBee.root.CompaniesModel',
            'Ext.view.View'
        ],

        id: 'images-view',

        itemSelector: 'div.flexibee-selection-wrap',
        tpl: [
            '<tpl for=".">',
                '<div class="flexibee-selection-wrap">',
                  '<a href="/c/{dbNazev}"><span class="flexibee-selection-img"><img src="/c/{dbNazev}/logo/thumbnail" title="{nazev}"/></span>',
                  '<span class="flexibee-selection-text">{nazev}</span></a>',
                '</div>',
            '</tpl>',
            '<div class="x-clear"></div>'
        ],
        overflowY: true,
        trackOver: true,
        overItemCls: 'x-item-over',
        initComponent: function() {
           var me = this;

           // every grid needs its own instance of the store (because store is stateful!)
           me.store = Ext.create('FlexiBee.root.CompaniesStore');

           me.callParent(arguments);
        }
    });


    new Ext.Viewport({
        layout: 'fit',
        baseCls: 'flexibee-background-dark',
    <#if isMobile == false && isTablet == false>
        style: 'background: url(<#if isSecure>https<#else>http</#if>://lorempixel.com/g/' + resolutionX + '/' + resolutionY + ') no-repeat center center fixed; -webkit-background-size: cover; /*for webKit*/ -moz-background-size: cover; /*Mozilla*/ -o-background-size: cover; /*opera*/ background-size: cover; /*generic*/',
    </#if>
        items: [
            {
                xtype: 'container',
                layout: 'border',
                style: 'background:transparent none repeat scroll 0 0',

                items: [
                    {
                        region: 'north',
                        xtype: 'container',
                        cls: 'flexibee-big-title',
                        padding: '50px 0 0 100px',
                        style: 'background:transparent none repeat scroll 0 0',
                        html: '<h1>Výběr firmy</h1>'
                    },
                    {
                        region: 'center',
                        xtype: 'container',
                        style: 'background:transparent none repeat scroll 0 0',
                        items: [
                            {
                                xtype: 'fbCompaniesSelection',
                                margin: '60px 200px 80px 100px'
                            }
                        ]
                    }
                    <#if isDesktop == false>
                    ,
                    {
                        xtype: 'container',
                        layout: 'hbox',
                        region: 'south',
                        cls: 'flexibee-app-statusbar',
                        height: height,
                        padding: '15px 30px 0 30px',
                        items: [
                            {
                                xtype: 'component',
                                padding: '4 0 0 0',
                                html: '<small><#if customerNumber??><a href="https://www.flexibee.eu/muj/${licenseKey}/" target="_new">${lang('labels', 'statusBar.cisloZakaznika', '')} ${customerNumber}</a></#if> <span style="color: grey;padding-left:1em">${licenseUserName}</span></small>'
                            },
                             {
                                xtype: 'fbRootStatusBar',
                                id: 'mainstatusbar',
                                flex: 1
                            }
                        ]
                    }
                    </#if>
                ]
            }
            ]
        });


});

</script>

<#include "/i-sencha-footer.ftl"/>
</#escape>
