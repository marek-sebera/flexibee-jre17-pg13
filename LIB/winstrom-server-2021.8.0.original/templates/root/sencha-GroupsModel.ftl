<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.root.GroupsModel', {
    extend: 'Ext.data.Model',

    idProperty: 'id',

    fields: [
        {
            name: 'id',
            type: 'string'
        },
        {
            name: 'nazev',
            type: 'string'
        },
        {
            name: 'default',
            type: 'boolean'
        },
        {
            name: 'licenseId',
            type: 'string'
        },
        {
            name: 'licenseName',
            type: 'string'
        },
        {
            name: 'licenseVariant',
            type: 'string'
        },
        {
            name: 'maxLicenseCompanies',
            type: 'integer'
        },
        {
            name: 'maxLicenseViewers',
            type: 'integer'
        },
        {
            name: 'maxLicenseWriters',
            type: 'integer'
        },
        {
            name: 'currentLicenseWriters',
            type: 'integer'
        },
        {
            name: 'currentLicenseReaders',
            type: 'integer'
        }
    ]
});

</#escape>