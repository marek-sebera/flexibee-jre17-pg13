<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.root.BaseForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.fbBaseForm',

    labelWidth:80,
    border: false,
    plain: true,
    bodyPadding: 15,
    defaultType:'textfield',
    monitorValid:true,
    fieldDefaults: {
           labelWidth: 200,
    },
    submit: {
        reader: {
            type: 'json',
            rootProperty: function(data) {
                alert(data);
                if (data.winstrom) {
                    return data.winstrom;
                } else {
                    return [];
                }
            },
            successProperty: '@success'
        },
        method:'POST',
        waitTitle:'',
        waitMsg:''
    }
});

</#escape>
