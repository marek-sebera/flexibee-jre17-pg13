<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.root.grid.BoxSelectColumn', {
    extend: 'Ext.grid.column.Column',
    alias: ['widget.boxselectcolumn'],

    requires: [
        'Ext.ux.form.field.BoxSelect',
        'FlexiBee.root.data.ComboSuggestStore'
    ],

    valueProperty: 'id',
    displayProperty: 'name',

    valueSeparator: ' | ',

    proxyUrl: undefined,
    dataRoot: undefined,
    optionsStore: undefined,

    editor: {},

    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
        var values = '';

        if (value) {
            var me = this.columns[colIndex];
            var dataMap = me.optionsStore.data.map;
            Ext.Array.each(Array.isArray(value) ? value : [value], function(item, index) {
                values += (index > 0 ? me.valueSeparator : '') + (dataMap[item] ? dataMap[item].get(me.displayProperty) : item);
                //values += '<li class="x-boxselect-item" style="margin: 0 1px; border: none;">' + (dataMap[item] ? dataMap[item].get('nazev') : item) + '</li>';
            });
        }

        return values;
        //return '<ul class="x-boxselect-list">' + values + '</ul>';
    },

    initComponent: function() {

        if (this.proxyUrl == undefined) {
            Ext.Error.raise('proxyUrl property is not defined');
        }

        this.optionsStore = Ext.create('FlexiBee.root.data.ComboSuggestStore', {
            proxyUrl: this.proxyUrl,
            dataRoot: this.dataRoot,
            idProperty: this.valueProperty,
            nameProperty: this.displayProperty,
            autoLoad: true
        });

        Ext.applyIf(this.editor, {
            xtype: 'boxselect',
            delimiter: ',',
            typeAhead: true,
            minChars: 2,
            pageSize: 15,
            minWidth: 250,
            matchFieldWidth: true,
            queryParam: 'q',

            store: this.optionsStore,
            valueField: this.valueProperty,
            displayField: this.displayProperty,
            multiSelect: this.multiSelect || true
        });

        this.callParent(arguments);
    }
});

</#escape>
