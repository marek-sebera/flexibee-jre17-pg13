<#ftl encoding="utf-8"/>
<#include "/i-localization.ftl"/>
<#escape x as x?default("")?js_string>

<#macro chkbox varName lblKey dscKey>
{
    xtype: 'checkbox',
    boxLabel: '<@lbl lblKey/>',
    name: '${varName}',
    checked: <#if it.getVariableValue(varName)>true<#else>false</#if>,
    inputValue: 'true',
    uncheckedValue: 'false'
}
<#if dscKey??>
,
{
    xtype: 'container',
    padding: '0 0 10px 15px',
    html: '<small><@lbl dscKey/></small>'
}
</#if>
</#macro>

<#macro chkbox_desc varName lblKey dscKey=lblKey+".description">
    <@chkbox varName lblKey dscKey/>
</#macro>


Ext.define('FlexiBee.root.ServerParams', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.fbServerParams',

    requires: [
        'FlexiBee.root.Plugins',
        'FlexiBee.root.BaseForm',
        'Ext.form.Panel',
        'Ext.panel.Panel',
        'Ext.JSON'
    ],

    autoScroll: true,
    border: 0,
    defaultButton: 0,

    onSuccess: function(form, action) {
    },
    onFailure: function(form, action) {
        if (action.failureType == 'server') {
            obj = Ext.JSON.decode(action.response.responseText);
            Ext.Msg.alert('Login Failed!', obj.errors.reason);
        } else {
            Ext.Msg.alert('Warning!', 'Authentication server is unreachable : ' + action.response.responseText);
        }
    },

    items: [
        {
            xtype: 'fbBaseForm',
            url: '/setting.json?token=${csrfToken}',

            items: [
                {
                    xtype: 'container',
                    border: 1,
                    items: [
                        <#if it.getVariableVisible("ITH_USERS_MANAGEMENT")>
                            <@chkbox_desc "ITH_USERS_MANAGEMENT" "settings.ITH_USERS_MANAGEMENT"/>
                        </#if>
                    ]
                }
            ],
            // All the magic happens after the user clicks the button
            buttons: [{
                text: '<@lbl "ulozit" "Uložit"/>',
                formBind: true,
                // Function that fires when user clicks the button
                handler: function() {
                    var form = this.up('form');
                    form.getForm().submit({
                         success: form.onSuccess,
                         failure: form.onFailure
                    });
                }
            }]
        }
    ]
});

</#escape>
