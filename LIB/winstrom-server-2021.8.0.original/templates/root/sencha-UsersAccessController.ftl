<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.root.UsersAccessController', {
    extend: 'Ext.app.Controller',

    requires: [
        'FlexiBee.root.UsersAccessStore'
    ],

    stores: [
        'UsersAccessStore'
    ],
    models: [
        'UsersAccessModel'
    ],
    views: [
        'UsersAccessGrid'
    ],

    init: function() {

        console.log('init');

        this.control({
            'fbUsersAccessGrid button[action=add]': {
                click: this.addNewUserAccess
            }
        });
    },

    addNewUserAccess: function(b) {
        console.log('addNewUserAccess()');
        var newAccess = Ext.create('FlexiBee.root.UsersAccessModel');
        this.store.insert(0, newAccess);
    },

    editUser: function(b) {
        console.log('Double clicked on ' + record.get('username'));
    }
});

</#escape>