<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.root.data.ComboSuggestStore', {
    extend: 'Ext.data.Store',

    dataRoot: 'results',
    idProperty: 'id',
    nameProperty: 'name',

    autoLoad: false,
    remoteFilter: true,

    //fields: [this.idProperty, this.nameProperty],

    proxy: {
        type: 'rest',
        //url: this.proxyUrl,

        extraParams: {
            'mode': 'suggest'
        },

        // startParam, limitParam, sortParam a directionParam mají defaultní hodnoty kompatibilní s našimi názvy
        pageParam: undefined,
        groupParam: undefined,

        reader: {
            type: 'json',
            //root: this.dataRoot,
            //idProperty: this.idProperty,
            totalProperty: 'total'
        }
    },

    constructor: function(args) {

        console.log(this.self.getName() + '.constructor(' + Ext.encode(args) + ')');
        if (!args.proxyUrl) {
            Ext.Error.raise('proxyUrl param is required');
        }

        Ext.apply(this, args);

        this.fields = [this.idProperty, this.nameProperty];

        this.proxy.url = this.proxyUrl;
        this.proxy.reader.root = this.dataRoot;
        this.proxy.reader.idProperty = this.idProperty;

        this.callParent(arguments);
    },

    load: function() {
        console.log('ComboSuggestStore.load(' + Ext.encode(arguments) + '): url=' + this.proxy.url);
        this.callParent(arguments);
    }
});

</#escape>
