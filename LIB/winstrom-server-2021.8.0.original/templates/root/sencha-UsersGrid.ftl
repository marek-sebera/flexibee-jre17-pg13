<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.root.UsersGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.fbUsersGrid',

    requires: [
        'FlexiBee.root.UsersStore',
        'FlexiBee.root.ReloadButton'
    ],

    store: 'FlexiBee.root.UsersStore',
    columns: [
        {
            text: 'ID',
            width: 40,
            dataIndex: 'id'
        }, {
            text: 'Jméno',
            flex: 1,
            dataIndex: 'username'
        }, {
            text: 'Zablokován',
            dataIndex: 'blocked',
            trueText: 'Ano',
            falseText: 'Ne',
            xtype: 'booleancolumn'
        }, {
            width: 100,
            text: 'Datum vytvoření',
            xtype: 'datecolumn',
            format: 'd.m.Y H:i',
            dataIndex: 'createDt'
        }, {
            text: 'Licenční skupina',
            width: 210,
            dataIndex: 'licenseGroup'
        }
    ],
    tbar: [
        {
            xtype: 'fbReloadButton'
        }
        <#--,
        {
            text: 'Přidat',
            handler : function(){
            }
        },
        {
            text: 'Upravit',
            handler : function(){
            }
        },
        {
            text: 'Změnit heslo',
            handler : function(){
            }
        },
        {
            text: 'Zablokovat',
            handler : function(){
            }
        } -->
    ],
    initComponent: function() {
       var me = this;

       // every grid needs its own instance of the store (because store is stateful!)
       me.store = Ext.create('FlexiBee.root.UsersStore');

       me.callParent(arguments);
    }
});

</#escape>