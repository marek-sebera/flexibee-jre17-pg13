<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.root.BrotherStore', {
    extend: 'Ext.data.Store',

    requires: [
        'FlexiBee.root.BrotherModel'
    ],

    autoLoad: true,
    autoSync: true,
    model: 'FlexiBee.root.BrotherModel',
    proxy: {
       type: 'rest',
       url: '/status/cluster.json',
       reader: {
           type: 'json',
           root: 'clusters.cluster'
       },
       writer: {
           type: 'json'
       }
    }
});

</#escape>