<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.root.BrotherGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.fbBrotherGrid',

    requires: [
        'FlexiBee.root.BrotherStore',
        'FlexiBee.root.ReloadButton'
    ],

    store: 'FlexiBee.root.BrotherStore',
    columns: [
        {
            text: 'Host',
            flex: 1,
            dataIndex: 'host'
        }, {
            text: 'Poslední kontrola',
            dataIndex: 'lastCheckAsString'
        }, {
            text: 'UUID',
            dataIndex: 'uuid'
        }, {
            width: 100,
            text: 'Poslední synchronizace',
            xtype: 'datecolumn',
            format: 'd.m.Y H:i',
            dataIndex: 'lastUpdate'
        }
    ],
    tbar: {
        xtype: 'toolbar',
        items: [
            {
                xtype: 'fbReloadButton'
            }
        ]
    },
    initComponent: function() {
       var me = this;

       // every grid needs its own instance of the store (because store is stateful!)
       me.store = Ext.create('FlexiBee.root.BrotherStore');

       me.callParent(arguments);
    }
});

</#escape>