<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.root.UsersModel', {
    extend: 'Ext.data.Model',

    requires: [
        'FlexiBee.root.GroupsModel'
    ],


    <#--//TODO až bude CouchdbCentralServerDatabase.convertUser nastavovat smysluplné ID, tak lze změnit na "id" -->
    idProperty: 'username',

    fields: [
        {
            name: 'id',
            type: 'int'
        },
        {
            name: 'username',
            type: 'string'
        },
        {
            name: 'createDt',
            type: 'date'
        },
        {
            name: 'licenseGroup',
            type: 'string'
        },
        {
            name: 'blocked',
            type: 'boolean'
        },
        {
            name: 'changePassword',
            type: 'boolean'
        },
        {
            name: 'createCompany',
            type: 'boolean'
        },
        {
            name: 'createUser',
            type: 'boolean'
        },
        {
            name: 'deleteCompany',
            type: 'boolean'
        },
        {
            name: 'grantPermission',
            type: 'boolean'
        },
        {
            name: 'licenseMgmt',
            type: 'boolean'
        },
        {
            name: 'manageAll',
            type: 'boolean'
        }
    ],
    associations: [{
        type: 'hasOne',
        model: 'FlexiBee.root.GroupsModel',
        foreignKey: 'id',
        associationKey: 'licenseGroup',
        autoLoad: true
    }]
});

</#escape>