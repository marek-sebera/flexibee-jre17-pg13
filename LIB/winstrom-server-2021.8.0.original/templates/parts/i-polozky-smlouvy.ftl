<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#macro generatePolozkySmlouvy polozkyPropName label>

<#assign polozkyIt=it.getSubEvidence("smlouva-polozka"), polozkyItems=polozkyIt.editHandler.propertiesMap, polozkyMarker=newMarker() />

<#-- === Položky ============================= -->
${marker.mark(polozkyPropName)}
<#if object[polozkyPropName]?? && object[polozkyPropName]?size &gt; 0>
<div class="flexibee-tabs">
    <ul>
        <li><a tabindex="-1" href="#fragment-1"><span>${label}</span></a></li>
    </ul>
    <div id="fragment-1">
        <div class="flexibee-table-border">
            <table class="flexibee-rada-polozky">
                <col width="16.6%"/><col width="16.6%"/>
                <col width="16.6%"/><col width="16.6%"/>
                <col width="16.6%"/><col width="16.6%"/>
                <#list object[polozkyPropName] as polozka>
                <tbody <#if polozka_index &gt; 0>class="flexibee-row"</#if>>
                    <tr>
                        <td><@tool.label name='kod' items=polozkyItems />:</td><td><@tool.place object=polozka name='kod' items=polozkyItems marker=polozkyMarker /></td>
                        <td><@tool.label name='nazev' items=polozkyItems />:</td><td><@tool.place object=polozka name='nazev' items=polozkyItems marker=polozkyMarker /></td>
                        <td><@tool.label name='cenik' items=polozkyItems />:</td><td><@tool.place object=polozka name='cenik' items=polozkyItems marker=polozkyMarker /></td>
                    </tr>
                    <tr>
                        <td><@tool.label name='mnozMj' items=polozkyItems />:</td><td><@tool.place object=polozka name='mnozMj' items=polozkyItems marker=polozkyMarker /></td>
                        <td><@tool.label name='cenaMj' items=polozkyItems />:</td><td><@tool.place object=polozka name='cenaMj' items=polozkyItems marker=polozkyMarker /></td>
                        <td><@tool.label name='cenaCelkem' items=polozkyItems />:</td><td><@tool.place object=polozka name='cenaCelkem' items=polozkyItems marker=polozkyMarker /></td>
                    </tr>
                </tbody>
                </#list>
            </table>
        </div>
    </div>
</div>
</#if>

</#macro>

<@generatePolozkySmlouvy 'polozkySmlouvyInt' 'Standardní položky smlouvy'/>
<@generatePolozkySmlouvy 'polozkySmlouvyExt' 'Externí položky smlouvy'/>

</#escape>
