<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#-- === PRIJEMCE ============================= -->
<div class="flexibee-blok flexibee-evidence-prijemce flexibee-blok-right">
${marker.mark('nazFirmy')}
${marker.mark('dic')}
${marker.mark('ic')}
${marker.mark('vatId')}
${marker.mark('postovniShodna')}
<table>
    <col width="40%"/>
<tr class="flexibee-small"><th colspan="2"><@tool.label items=items name='firma' />:</th></tr>
<tr class="flexibee-big"><td colspan="2"><strong><#if object['firma']?exists><@tool.place marker=marker object=object name='firma' items=items /><#else><@tool.place marker=marker object=object items=items name='firma' /></#if></strong></td></tr>
<#if object['ic']?exists>
    <tr class="small"><th><@tool.label items=items name='ic' />:</th><td><@tool.place marker=marker object=object name='ic' items=items /></td></tr>
<#elseif object['vatId']?exists>
    <tr class="small"><th><@tool.label items=items name='vatId' />:</th><td><@tool.place marker=marker object=object name='vatId' items=items /></td></tr>
<#elseif object['dic']?exists>
    <tr class="small"><th><@tool.label items=items name='dic' />:</th><td><@tool.place marker=marker object=object name='dic' items=items /></td></tr>
</#if>
</table>

<table>
    <col width="40%"/>
    <tr class="flexibee-small"><td><@tool.place marker=marker object=object name=['ulice', 'mesto', 'psc', 'stat'] items=items /> </td></tr>
<#if object['postovniShodna']?exists && object['postovniShodna'] == false>
    <tr class="flexibee-small"><td><strong>Poštovní adresa:</strong><br/><#if object['faNazev']?exists><strong><@tool.place marker=marker object=object name='faNazev' items=items /></strong><br/></#if>
<@tool.place marker=marker object=object name=['faUlice', 'faMesto', 'faPsc', 'faStat'] items=items /> </td></tr>
</#if>
</table>
</div>

</#escape>