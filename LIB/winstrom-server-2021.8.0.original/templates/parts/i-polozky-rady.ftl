<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#assign polozkyIt=it.getSubEvidence("rocni-rada"), polozkyItems=polozkyIt.editHandler.propertiesMap, polozkyMarker=newMarker() />

<#-- === Položky ============================= -->
${marker.mark('polozkyRady')}
<#if object.polozkyRady?? && object.polozkyRady?size &gt; 0>
<div class="flexibee-tabs">
    <ul>
        <li><a tabindex="-1" href="#fragment-1"><span>Roční položky číselné řady</span></a></li>
    </ul>
    <div id="fragment-1">
        <div class="flexibee-table-border">
            <table class="flexibee-rada-polozky">
                <col width="12.5%"/><col width="12.5%"/>
                <col width="12.5%"/><col width="12.5%"/>
                <col width="12.5%"/><col width="12.5%"/>
                <col width="12.5%"/><col width="12.5%"/>
                <#list object.polozkyRady as polozka>
                <tbody <#if polozka_index &gt; 0>class="flexibee-row"</#if>>
                    <tr>
                        <td><@tool.label name='ucetObdobi' items=polozkyItems />:</td><td><@tool.place object=polozka name='ucetObdobi' items=polozkyItems marker=polozkyMarker /></td>
                        <td><@tool.label name='prefix' items=polozkyItems />:</td><td><@tool.place object=polozka name='prefix' items=polozkyItems marker=polozkyMarker /></td>
                        <td><@tool.label name='postfix' items=polozkyItems />:</td><td><@tool.place object=polozka name='postfix' items=polozkyItems marker=polozkyMarker /></td>
                    </tr>
                    <tr>
                        <td><@tool.label name='cisAkt' items=polozkyItems />:</td><td><@tool.place object=polozka name='cisAkt' items=polozkyItems marker=polozkyMarker /></td>
                        <td><@tool.label name='cisPoc' items=polozkyItems />:</td><td><@tool.place object=polozka name='cisPoc' items=polozkyItems marker=polozkyMarker /></td>
                        <td><@tool.label name='cisDelka' items=polozkyItems />:</td><td><@tool.place object=polozka name='cisDelka' items=polozkyItems marker=polozkyMarker /></td>
                        <td><@tool.label name='zobrazNuly' items=polozkyItems />:</td><td><@tool.place object=polozka name='zobrazNuly' items=polozkyItems marker=polozkyMarker /></td>
                    </tr>
                </tbody>
                </#list>
            </table>
        </div>
    </div>
</div>
</#if>

</#escape>
