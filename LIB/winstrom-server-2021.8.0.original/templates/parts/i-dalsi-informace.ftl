<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#if tool.hasItems(items, object, marker) && isDevel && !isModal>
    <div class="flexibee-blok flexibee-blok-right flexibee-more">
    <span class="flexibee-small"><strong>Další informace:</strong></span>
    <br clear="all"/>
    <div class="flexibee-hidden">
    <@tool.listItems items=items object=object marker=marker />
    </div>
    </div>
</#if>

</#escape>