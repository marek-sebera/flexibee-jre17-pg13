<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#-- === Položky ============================= -->
${marker.mark('polozkyDokladu')}
<#if object['bezPolozek'] == false>
<div class="flexibee-tabs">
        <ul>
            <li><a tabindex="-1" href="#fragment-1"><span>Položky</span></a></li>
    </ul>
    <div id="fragment-1">
            <div class="flexibee-table-border">

    <table class="flexibee-doklad-polozky">
    <col width="70%"/>
    <col width="10%"/>
    <col width="20%"/>
    <#list object['polozkyDokladu'] as polozka>
    <tbody <#if polozka_index &gt; 0>class="flexibee-row"</#if>>
        <tr>
        <td class="flexibee-small flexibee-grey">${polozka.kod}</td>
        <td class="flexibee-small"><#if polozka.mnozMj?? && polozka.mnozMj != 0>${polozka.mnozMj}</#if> <#if polozka.mj??>${polozka.mj.nazev}</#if></td>
        <#if polozka.sumCelkemMen != 0>
            <td class="flexibee-r"><strong>${polozka.sumZklMen?string(",##0.00")}&nbsp;<@tool.menaSymbol polozka.mena/></strong></td>
        <#else>
            <td class="flexibee-r"><strong>${polozka.sumZkl?string(",##0.00")}&nbsp;<@tool.menaSymbol polozka.mena/></strong></td>
        </#if>
        </tr>
        <tr>
        <td colspan="2">${polozka.nazev}</td>
        <#if polozka.sumCelkemMen != 0 && polozka.sumCelkemMen != polozka.sumZklMen>
            <td class="flexibee-r"><small>${polozka.sumCelkemMen?string(",##0.00")}&nbsp;<@tool.menaSymbol polozka.mena/> s&nbsp;DPH</small></td>
        <#elseif polozka.sumCelkem != polozka.sumZkl>
            <td class="flexibee-r"><small>${polozka.sumCelkem?string(",##0.00")}&nbsp;<@tool.menaSymbol polozka.mena/> s&nbsp;DPH</small></td>
        <#else>
            <td>&nbsp;</td>
        </#if>
        </tr>
    </tbody>
    </#list>
    </table>

    </div>

    </div>
</div>
</#if>

</#escape>