<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-common.ftl" />
<#if it.editHandler??>
<#assign items=it.editHandler.propertiesMap />
</#if>


<#assign noObject = false/>
<#if it.editHandler?? && it.editHandler.object??>
    <#assign object=it.editHandler.object />
</#if>

<#if !myTitle??>
    <#assign myTitle=lang('dialogs', it.name, it.name) />
<#else>
    <#assign myTitle = myTitle + " " />
</#if>

<#if !title3??>
    <#if object?? && it.getObjectTitle?? && it.getObjectTitle(object)??>
        <#assign title3=it.getObjectTitle(object)/>
    <#elseif object?? && object.nazev??>
        <#assign title3=" ${object.nazev}"/>
    <#elseif object?? && object.kod??>
        <#assign title3=" ${object.kod}"/>
    <#elseif object?? && object.id?? && object.persistent>
        <#assign title3=" ${object?string!''}"/>
    <#else>
        <#assign title3=" nový záznam"/>
        <#assign noObject = true/>
    </#if>
<#else>
    <#assign noObject = true/>
</#if>
<#assign title2="${myTitle}${title3}"/>
<#if title3 != ''>
<#assign title="${title3} | ${myTitle}"/>
<#else>
<#assign title="${myTitle}"/>
</#if>
<#if noObject>
    <#if title3 == "">
        <#assign title3="${myTitle}"/>
    <#else>
        <#assign title3="${myTitle}:${title3}"/>
    </#if>
</#if>

<#if it.editHandler??>
<#assign goBackPathUrl=it.editHandler.goBackURI/>
<#else>
<#assign goBackPathUrl=""/>
</#if>
<#assign goBackPathName="Zpět"/>

<#include "/header-html-start.ftl"/>
<@tool.showTitle title=title3 finish=false menu=false back=false>
    <#if isMobile>
    <a href="${goBackPathUrl}" class="flexibee-clickable ui-btn-left" data-icon="back" onclick="javascript:history.go(-1)" <#if deviceType != "tablet">data-iconpos="notext"</#if>>${lang('actions', 'cancel', 'Zrušit')}</a>
    <a href="" class="flexibee-clickable ui-btn-right" data-icon="check" <#if deviceType != "tablet">data-iconpos="notext"</#if>>${lang('actions', 'save', 'Uložit')}</a>

    </#if>
</@tool.showTitle>
<@tool.showTitleFinish title=title3/>
<#if isModal == false && !hideToolbar?? && hideToolbar == false>
<#include "/i-toolbar.ftl"/>
</#if>

<#import "/l-edits.ftl" as edit />

<@edit.beginForm2 multipart=isFile?default(false) query=passedQuery?default("")/>

<#if isModal == true>
 <div class="modal-header">
 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
 ${title3}
 </div>
 <div class="modal-body">
</#if>


<#if it.editHandler??>
<#include "/i-error.ftl"/>
</#if>

</#escape>
