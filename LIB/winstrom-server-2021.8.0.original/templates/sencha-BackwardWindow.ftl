<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.main.BackwardWindow', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.backwardWindow',

    requires: [
        'Ext.panel.Panel'
    ],

    autoScroll: true,
    url: '',

    initComponent: function () {
        var me = this;

        if (me.url.indexOf("?") != -1) {
            me.url = me.url + "&isPanel=true&hideHelp=true&hideRelations=true&hideToolbar=true";
        } else {
            me.url = me.url + "?isPanel=true&hideHelp=true&hideRelations=true&hideToolbar=true";
        }

        this.autoLoad = {
            showMask: true,
            url: me.url,
            mode: "iframe"
        };

        this.callParent();
    }
});

</#escape>
