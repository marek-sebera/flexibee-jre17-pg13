<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<ol class="breadcrumb hidden-print">
            <li><a href="/c/${companyId}"><span class="glyphicon glyphicon-home"></span></a></li>
        ${naviPath("/i-navipath.ftl")}
                    <#if title3??>
                        <#assign naviPathTitle = title3/>
                    <#elseif title2??>
                        <#assign naviPathTitle = title2/>
                    <#else>
                        <#assign naviPathTitle = title/>
                    </#if>

                    <#if naviPathTitle?length &gt; 30>
                        <li id="flexibee-path-title" class="active">${naviPathTitle?substring(0, 25)?trim}&hellip;</li>
                    <#elseif naviPathTitle?trim?length &gt; 0>
                        <li id="flexibee-path-title" class="active">${naviPathTitle}</li>
                    <#else>
                        <li id="flexibee-path-title" class="active">&nbsp;</li>
                    </#if>
</ol>

</#escape>