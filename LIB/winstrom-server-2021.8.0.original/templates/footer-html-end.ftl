<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#if !isPlain>
<script type="text/javascript">
    window.wasChanged = false;
    window.onbeforeunload = function(e) { // https://developer.mozilla.org/en/DOM/window.onbeforeunload
        if (!window.wasChanged) {
            return;
        }

        var msg = "${lang('messages', 'unsavedChanges.confirm', 'Na stránce jsou neuložené změny. Opravdu ji chcete opustit?')}";

        var e = e || window.event;
        if (e) {
            e.returnValue = msg;
        }
        return msg;
    };

<#if !isSlowMobile>

    var myScroll;

<#if useJQuery>
<#if isMobile>
    $.mobile.page.prototype.options.backBtnText = "${lang('buttons', 'goBack', 'Zpět')}";
    $('div').live('pagecreate', function(event) {
    var _this = $(this);
<#else>
    $(document).ready(function() {
    var _this = $(document);
</#if> <#-- isMobile -->
<#if !isMenu>
    $.ajaxSetup({
      cache: true
    });

    $('textarea', _this).TextAreaResizer();
    $(".flexibee-tab", _this).tab();
    $(".flexibee-tabs", _this).tabs();
    //$('.flexibee-level-3', _this).inlineEdit();
    $(".flexibee-inp-uppercase", _this).capitalize();
    $(".flexibee-toolbar-item-javascript-only").show();
    $(".flexibee-show-wait", _this).click(function() {
        $.showWait();
        return true;
    });

    $("input").on('invalid', function() {
        $.hideWait();
    });

    /* off-canvas sidebar toggle */
    $('[data-toggle=offcanvas]').click(function() {
        $(this).find('i').toggleClass('fa-angle-double-right fa-angle-double-left');
        $('.row-offcanvas').toggleClass('small-sidebar');
        $('#sidebar').toggleClass('hidden-xs');
        $('#lg-menu').toggleClass('hide');
        $('#xs-menu').toggleClass('hide');
        $('#btnShow').toggle();
        if ($('.row-offcanvas').is('.small-sidebar')) {
            $.cookie("useSmallMenu", 'true', { path: '/' });
        } else {
            $.cookie("useSmallMenu", 'false', { path: '/' });
        }
    });

    $(".flexibee-more .flexibee-more-for-button", _this).each(function() {
        if (!$(this).hasClass('flexibee-more-already-added')) {
            $(this).addClass('flexibee-more-already-added');
            $(this).append('<button type="button" class="flexibee-toggle-button btn btn-link"><span aria-hidden="true"><span class="glyphicon glyphicon-chevron-down"></span></span></button>');
        }
    });

    $(".flexibee-toggle-button", _this).click(function(event) {
        var more = $(this).parents('.flexibee-more');
        var hidden = $(more).children('.flexibee-hidden');
        $(this).children().children('.glyphicon').toggleClass('glyphicon-chevron-down glyphicon-chevron-up');

        $(hidden).toggleClass('hidden');
    });

    <#if !isMobile> <#-- TODO datepicker prozatím na mobilu neumíme (jQuery UI závisí na události "click"; možná by fungovalo s jQM 1.0b2, ale viz flexibee-jquery-mobile) -->
    $(".flexibee-edit-date", _this).datepicker({
        duration: 'fast',
        dateFormat: "d.m.yy",
        minDate: new Date(1900, 1 - 1, 1),
        maxDate: new Date(9999, 12 - 1, 31)
    });
    $(".flexibee-edit-datetime", _this).datetimepicker({
        duration: 'fast',
        dateFormat: "d.m.yy",
        minDate: new Date(1900, 1 - 1, 1),
        maxDate: new Date(9999, 12 - 1, 31),
        timeFormat: "h:mm",
        hourGrid: 4,
        minuteGrid: 10
    });
    </#if>

    $('.flexibee-keynav').keynav('flexibee-keynav-active','flexibee-keynav');

    setTimeout(function() {
        $(".flexibee-focus:first", _this).focus();
        <#if isMobile>
        $(".flexibee-focus:first", _this).removeClass('flexibee-keynav').addClass('flexibee-keynav-active');
        </#if> <#-- isMobile -->
    }, 300); // FormHelpery se inicializují až po 200 ms

    <#if isModal == false && loggedUser??>
    // udržování session každých 60 vteřin
    setInterval(function() {
        $.ajax({
           <#if companyId??>
           url: '/c/${companyId}/session-keep-alive.js',
           <#else>
           url: '/login-logout/session-keep-alive.js',
           </#if>
           dataType: 'json',
           cache: false,
           success: function(data) {
               if (data.success) {
                   var l = window.location;
                   if (l.pathname != '/login-logout/remote-logout') {
                       if (data.obsolete) {<#-- sezení přešlo do stavu OBSOLETE -->
                           var currentPath = l.href.substr(l.origin.length);
                           window.location = '/login-logout/remote-logout?returnUrl=' + encodeURIComponent(currentPath);
                       }
                   } else {
                       if (!data.obsolete) {<#-- obnova OBSOLETE sezení -->
                           var urlParams = window.location.search.substr(1).split("&");
                           for (var index in urlParams) {
                               var keyVal = urlParams[index].split("=");
                               if (keyVal[0] == 'returnUrl') {
                                   var returnUrl = decodeURIComponent(keyVal[1]).trim();
                                   if (returnUrl && returnUrl.length > 1) {
                                       window.location = returnUrl;
                                   }
                                   return;
                               }
                           }
                           window.location = '/start';
                       }
                   }
               }
           }
        });
    }, 60000);
    </#if>

</#if> <#-- !isMenu-->
});
</#if> <#-- useJQuery -->
</#if> <#-- !isSlowMobile -->
</script>
</#if>

<#if isMenu || isPanel || isPlain>
    </div> <!-- flexibee-panel -->
</#if> <#-- isMenu || isPanel || isPlain -->

<#if durationTool??>
<!-- Request Duration: ${durationTool()} -->
</#if>

<#if !isPanel>

<#if cloudOverride.isTemplate("footer", "content")>
<#include cloudOverride.include("footer", "content") />
</#if>

</body>
</html>
</#if> <#-- !isPlain -->
</#escape>
