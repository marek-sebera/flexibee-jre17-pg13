<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-common.ftl"/>

<#if it.object??>
    <#assign object=it.object />
</#if>

<#if !myTitle??>
    <#assign myTitle=lang('dialogs', it.name, it.name) />
</#if>

<#if object?? && it.getObjectTitle?? && it.getObjectTitle(object)??>
    <#assign title3=it.getObjectTitle(object)/>
<#elseif object?? && object.nazev??>
    <#assign title3="${object.nazev}"/>
<#elseif object?? && object.kod??>
    <#assign title3="${object.kod}"/>
<#elseif object??>
    <#assign title3="${object?string!''}"/>
<#else>
    <#assign title3=myTitle/>
</#if>
<#assign title2="${myTitle} ${title3}"/>
<#if title3 != ''>
<#assign title="${title3} | ${myTitle}"/>
<#else>
<#assign title="${myTitle}"/>
</#if>


<#include "/header-html-start.ftl"/>
<@tool.showTitle title=title3 finish=false showRight=true>
    <#if isMobile>
    <#if it.evidenceResource?? && it.evidenceResource.allowToolbar && it.evidenceResource.getToolBar('edit', object)??>
        <#assign a = it.evidenceResource.getToolBar('edit', object)/>
        <a href="${a.url}" class="flexibee-clickable ui-btn-right" data-icon="arrow-r" <#if deviceType != "tablet">data-iconpos="notext"</#if> rel="external">${lang('buttons', 'modify', 'Změnit')}</a>
    </#if>
    </#if>
</@tool.showTitle>
<@tool.showTitleFinish title=title3/>
<#include "/i-toolbar.ftl"/>

<div class="flexibee-article-view">


</#escape>
