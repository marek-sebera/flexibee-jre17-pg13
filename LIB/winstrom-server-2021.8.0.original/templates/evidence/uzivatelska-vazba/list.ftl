<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-list.ftl" />
<#include "/evidence/i-list-begin.ftl" />

<#if it.rowCount &gt; 0>
<table class="table flexibee-tbl-list table-striped table-hover table-condensed" summary="${lang('dialogs', it.name, it.name)}">
    <thead>
    <tr>
        <#list it.devDocItemsForHtml as item>
        <#if item.showToUser && item.isVisible>
            <th><@tool.listHeader name=item.propertyName items=it.devDocItemsForHtmlMap url=url urlSuffix=filterQueryParam orderProperty=orderProperty orderDirection=orderDirection /></th>
        </#if>
        </#list>
        <th>Formulář</th>
        <th>&nbsp;</th>
    </tr>
    </thead>
    <tbody class="flexibee-tbl-list-body">
    <#list it.list as object>
    <tr class="<#if object_index % 2 = 0>flexibee-even</#if> flexibee-clickable flexibee-keynav">
        <#list it.devDocItemsForHtml as item>
        <#if item.showToUser && item.isVisible>
            <td <#if item.type=='numeric'>class="flexibee-r"</#if>>
              <a href="${baseUrl + "/" + object.idVazbaView?c}">
              <@tool.placeList marker=marker object=object name=item.propertyName items=it.devDocItemsForHtmlMap />
              </a>
            </td>
        </#if>
        </#list>
        <#assign otherType = it.evidenceResource.getEvidenceTypeFor(object.beanKey)/>
        <#assign otherUrl = formListUrl(it.companyResource, otherType, object.vazId?c)/>
        <td><a href="${otherUrl}">${formInfo(otherType).name}</a></td>
        <td><a href="${otherUrl}">Otevřít</a></td>
    </tr>
        </#list>
    </tbody>
</table>
</#if>

<#include "/evidence/i-list-end.ftl" />

<#include "/footer.ftl" />
</#escape>