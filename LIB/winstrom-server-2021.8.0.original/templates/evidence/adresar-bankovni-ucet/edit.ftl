<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>

${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}
${marker.mark('stitky')}
${marker.mark('modul')}
${marker.mark('platiOd')}
${marker.mark('platiDo')}
${marker.mark('zapocet')}
${marker.mark('elBanFormat')}
${marker.mark('sloVypis')}
${marker.mark('sloPrikaz')}
${marker.mark('priVypis')}
${marker.mark('priPrikaz')}
${marker.mark('zkrKlienta')}
${marker.mark('bucKonverze')}
${marker.mark('formaUhrK')}
${marker.mark('splatDny')}

<@edit.beginForm />
    <#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canEditAtLeastOne(items, object, ['firma', 'buc', 'iban', 'smerKod', 'bic', 'konSym', 'specSym', 'primarni', 'varSym'])>
    <table class="flexibee-tbl-1">
        <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
        <#if canEditAtLeastOne(items, object, ['firma'])>
        <tr>
            <td><@edit.label name='firma' items=items /></td><td colspan="3"><@edit.place object=object name='firma' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['buc', 'iban'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['buc'])>
            <td><@edit.label name='buc' items=items /></td><td><@edit.place object=object name='buc' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['iban'])>
            <td><@edit.label name='iban' items=items /></td><td><@edit.place object=object name='iban' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['smerKod', 'bic'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['smerKod'])>
            <td><@edit.label name='smerKod' items=items /></td><td><@edit.place object=object name='smerKod' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['bic'])>
            <td><@edit.label name='bic' items=items /></td><td><@edit.place object=object name='bic' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['konSym'])>
        <tr>
            <td><@edit.label name='konSym' items=items /></td><td><@edit.place object=object name='konSym' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['specSym', 'primarni'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['specSym'])>
            <td><@edit.label name='specSym' items=items /></td><td><@edit.place object=object name='specSym' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['primarni'])>
            <td><@edit.label name='primarni' items=items /></td><td><@edit.place object=object name='primarni' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['varSym'])>
        <tr>
            <td><@edit.label name='varSym' items=items /></td><td><@edit.place object=object name='varSym' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canEditAtLeastOne(items, object, ['nazBanky', 'ulice', 'psc', 'mesto', 'stat', 'popis', 'poznam'])>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canEditAtLeastOne(items, object, ['nazBanky', 'ulice', 'psc', 'mesto', 'stat'])>
            <li class="active"><a href="#edit-fragment-1" data-toggle="tab"><span>${lang('labels', 'bankaPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <li><a href="#edit-fragment-2" data-toggle="tab"><span>${lang('labels', 'textyPN')}</span></a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canEditAtLeastOne(items, object, ['nazBanky', 'ulice', 'psc', 'mesto', 'stat'])>
            <div id="edit-fragment-1" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['nazBanky', 'ulice', 'psc', 'mesto', 'stat'])>
                <table class="flexibee-tbl-1">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canEditAtLeastOne(items, object, ['nazBanky'])>
                    <tr>
                        <td><@edit.label name='nazBanky' items=items /></td><td colspan="3"><@edit.place object=object name='nazBanky' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['ulice'])>
                    <tr>
                        <td><@edit.label name='ulice' items=items /></td><td colspan="3"><@edit.place object=object name='ulice' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['psc', 'mesto'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['psc'])>
                        <td><@edit.label name='psc' items=items /></td><td><@edit.place object=object name='psc' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['mesto'])>
                        <td><@edit.label name='mesto' items=items /></td><td><@edit.place object=object name='mesto' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['stat'])>
                    <tr>
                        <td><@edit.label name='stat' items=items /></td><td colspan="3"><@edit.place object=object name='stat' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <div id="edit-fragment-2" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
                <table class="flexibee-tbl-1">
                    <#if canEditAtLeastOne(items, object, ['popis'])>
                    <tr>
                        <td><@edit.label name='popis' items=items /><br><@edit.textarea object=object name='popis' items=items marker=marker rows=10 /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['poznam'])>
                    <tr>
                        <td><@edit.label name='poznam' items=items /><br><@edit.textarea object=object name='poznam' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

    <@edit.listItems items=items object=object marker=marker />

    <@edit.saveButton />
<@edit.endForm />

<@edit.generateNaiveJavascript object=object it=it />

<#include "/parts/i-dalsi-informace.ftl" />



<#include "/i-footer-edit.ftl" />
</#escape>
