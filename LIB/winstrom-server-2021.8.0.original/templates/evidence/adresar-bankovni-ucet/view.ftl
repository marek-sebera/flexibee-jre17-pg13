<#ftl encoding="utf-8" strip_whitespace=true/>
<#escape x as x?default("")?html>

<#include "/i-header-view.ftl" />

${marker.mark('platiOd')}
${marker.mark('platiDo')}
${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}
${marker.mark('stitky')}
${marker.mark('modul')}
${marker.mark('zapocet')}
${marker.mark('elBanFormat')}
${marker.mark('sloVypis')}
${marker.mark('sloPrikaz')}
${marker.mark('priVypis')}
${marker.mark('priPrikaz')}
${marker.mark('zkrKlienta')}
${marker.mark('bucKonverze')}
${marker.mark('formaUhrK')}
${marker.mark('splatDny')}


<#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canShowAtLeastOne(items, object, ['firma', 'buc', 'iban', 'smerKod', 'bic', 'konSym', 'specSym', 'primarni', 'varSym'], it)>
    <table class="flexibee-tbl-1">
        <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
        <#if canShowAtLeastOne(items, object, ['firma'], it)>
        <tr>
            <td><@tool.label name='firma' items=items />:</td><td colspan="3"><@tool.place object=object name='firma' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['buc', 'iban'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['buc'], it)>
            <td><@tool.label name='buc' items=items />:</td><td><@tool.place object=object name='buc' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['iban'], it)>
            <td><@tool.label name='iban' items=items />:</td><td><@tool.place object=object name='iban' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['smerKod', 'bic'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['smerKod'], it)>
            <td><@tool.label name='smerKod' items=items />:</td><td><@tool.place object=object name='smerKod' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['bic'], it)>
            <td><@tool.label name='bic' items=items />:</td><td><@tool.place object=object name='bic' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['konSym'], it)>
        <tr>
            <td><@tool.label name='konSym' items=items />:</td><td><@tool.place object=object name='konSym' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['specSym', 'primarni'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['specSym'], it)>
            <td><@tool.label name='specSym' items=items />:</td><td><@tool.place object=object name='specSym' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['primarni'], it)>
            <td><@tool.label name='primarni' items=items />:</td><td><@tool.place object=object name='primarni' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['varSym'], it)>
        <tr>
            <td><@tool.label name='varSym' items=items />:</td><td><@tool.place object=object name='varSym' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canShowAtLeastOne(items, object, ['nazBanky', 'ulice', 'psc', 'mesto', 'stat', 'popis', 'poznam'], it)>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canShowAtLeastOne(items, object, ['nazBanky', 'ulice', 'psc', 'mesto', 'stat'], it)>
            <li class="active"><a href="#view-fragment-1" data-toggle="tab">${lang('labels', 'bankaPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
            <li><a href="#view-fragment-2" data-toggle="tab">${lang('labels', 'textyPN')}</a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canShowAtLeastOne(items, object, ['nazBanky', 'ulice', 'psc', 'mesto', 'stat'], it)>
            <div id="view-fragment-1" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['nazBanky', 'ulice', 'psc', 'mesto', 'stat'], it)>
                <table class="flexibee-tbl-1">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canShowAtLeastOne(items, object, ['nazBanky'], it)>
                    <tr>
                        <td><@tool.label name='nazBanky' items=items />:</td><td colspan="3"><@tool.place object=object name='nazBanky' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['ulice'], it)>
                    <tr>
                        <td><@tool.label name='ulice' items=items />:</td><td colspan="3"><@tool.place object=object name='ulice' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['psc', 'mesto'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['psc'], it)>
                        <td><@tool.label name='psc' items=items />:</td><td><@tool.place object=object name='psc' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['mesto'], it)>
                        <td><@tool.label name='mesto' items=items />:</td><td><@tool.place object=object name='mesto' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['stat'], it)>
                    <tr>
                        <td><@tool.label name='stat' items=items />:</td><td colspan="3"><@tool.place object=object name='stat' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
            <div id="view-fragment-2" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
                <table class="flexibee-tbl-1">
                    <#if canShowAtLeastOne(items, object, ['popis'], it)>
                    <tr>
                        <td><@tool.label name='popis' items=items />:<br><@tool.placeArea object=object name='popis' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['poznam'], it)>
                    <tr>
                        <td><@tool.label name='poznam' items=items />:<br><@tool.placeArea object=object name='poznam' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

<@tool.listItems items=items object=object marker=marker />

<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />

<#include "/parts/i-dalsi-informace.ftl" />


<#include "/i-footer-view.ftl" />
</#escape>