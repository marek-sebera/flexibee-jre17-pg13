<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>

${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}
${marker.mark('visible')}


<@edit.beginForm />
    <#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canEditAtLeastOne(items, object, ['kod', 'nazev'])>
    <table class="flexibee-tbl-1">
        <col width="20%" /><col width="80%" />
        <#if canEditAtLeastOne(items, object, ['kod'])>
        <tr>
            <td><@edit.label name='kod' items=items /></td><td><@edit.place object=object name='kod' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['nazev'])>
        <tr>
            <td><@edit.label name='nazev' items=items /></td><td><@edit.place object=object name='nazev' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canEditAtLeastOne(items, object, ['formaUhrK', 'mena', 'kurz', 'kurzMnozstvi', 'limitVratky', 'metodaZaokrDoklK', 'zaokrNaSumK', 'vsbFak', 'vsbPok', 'vsbKasa', 'popis', 'poznam', 'platiOd', 'platiDo'])>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canEditAtLeastOne(items, object, ['formaUhrK', 'mena', 'kurz', 'kurzMnozstvi', 'limitVratky', 'metodaZaokrDoklK', 'zaokrNaSumK'])>
            <li class="active"><a href="#edit-fragment-1" data-toggle="tab"><span>${lang('labels', 'zaklInformacePN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['vsbFak', 'vsbPok', 'vsbKasa'])>
            <li><a href="#edit-fragment-2" data-toggle="tab"><span>${lang('labels', 'formaUhrady.pouzitiPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <li><a href="#edit-fragment-3" data-toggle="tab"><span>${lang('labels', 'textyPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
            <li><a href="#edit-fragment-4" data-toggle="tab"><span>${lang('labels', 'spravaPN')}</span></a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canEditAtLeastOne(items, object, ['formaUhrK', 'mena', 'kurz', 'kurzMnozstvi', 'limitVratky', 'metodaZaokrDoklK', 'zaokrNaSumK'])>
            <div id="edit-fragment-1" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['formaUhrK', 'mena', 'kurz', 'kurzMnozstvi', 'limitVratky', 'metodaZaokrDoklK', 'zaokrNaSumK'])>
                <table class="flexibee-tbl-1">
                    <#if canEditAtLeastOne(items, object, ['formaUhrK'])>
                    <tr>
                        <td><@edit.label name='formaUhrK' items=items /></td><td><@edit.place object=object name='formaUhrK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['mena'])>
                    <tr>
                        <td><@edit.label name='mena' items=items /></td><td><@edit.place object=object name='mena' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['kurz'])>
                    <tr>
                        <td><@edit.label name='kurz' items=items /></td><td><@edit.place object=object name='kurz' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['kurzMnozstvi'])>
                    <tr>
                        <td><@edit.label name='kurzMnozstvi' items=items /></td><td><@edit.place object=object name='kurzMnozstvi' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['limitVratky'])>
                    <tr>
                        <td><@edit.label name='limitVratky' items=items /></td><td><@edit.place object=object name='limitVratky' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['metodaZaokrDoklK'])>
                    <tr>
                        <td><@edit.label name='metodaZaokrDoklK' items=items /></td><td><@edit.place object=object name='metodaZaokrDoklK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['zaokrNaSumK'])>
                    <tr>
                        <td><@edit.label name='zaokrNaSumK' items=items /></td><td><@edit.place object=object name='zaokrNaSumK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['vsbFak', 'vsbPok', 'vsbKasa'])>
            <div id="edit-fragment-2" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['vsbFak', 'vsbPok', 'vsbKasa'])>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <#if canEditAtLeastOne(items, object, ['vsbFak'])>
                    <tr>
                        <td><@edit.label name='vsbFak' items=items /></td><td><@edit.place object=object name='vsbFak' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['vsbPok'])>
                    <tr>
                        <td><@edit.label name='vsbPok' items=items /></td><td><@edit.place object=object name='vsbPok' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['vsbKasa'])>
                    <tr>
                        <td><@edit.label name='vsbKasa' items=items /></td><td><@edit.place object=object name='vsbKasa' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <div id="edit-fragment-3" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
                <table class="flexibee-tbl-1">
                    <#if canEditAtLeastOne(items, object, ['popis'])>
                    <tr>
                        <td><@edit.label name='popis' items=items /><br><@edit.textarea object=object name='popis' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['poznam'])>
                    <tr>
                        <td><@edit.label name='poznam' items=items /><br><@edit.textarea object=object name='poznam' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
            <div id="edit-fragment-4" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <tr>
                        <td colspan="2">Platnost se udává v kalendářních rocích, pro neomezenou platnost zadejte 0-9999</td>
                    </tr>

                    <#if canEditAtLeastOne(items, object, ['platiOd'])>
                    <tr>
                        <td><@edit.label name='platiOd' items=items /></td><td><@edit.place object=object name='platiOd' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['platiDo'])>
                    <tr>
                        <td><@edit.label name='platiDo' items=items /></td><td><@edit.place object=object name='platiDo' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

    <br/>
    <@edit.listItems items=items object=object marker=marker />

    <@edit.saveButton />
<@edit.endForm />

<@edit.generateNaiveJavascript object=object it=it />

<#include "/parts/i-dalsi-informace.ftl" />

<#include "/i-footer-edit.ftl" />
</#escape>
