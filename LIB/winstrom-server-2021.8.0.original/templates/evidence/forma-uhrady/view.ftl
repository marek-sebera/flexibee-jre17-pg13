<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-view.ftl" />

${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}
${marker.mark('visible')}

<#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canShowAtLeastOne(items, object, ['kod', 'nazev'], it)>
    <table class="flexibee-tbl-1">
        <col width="20%" /><col width="80%" />
        <#if canShowAtLeastOne(items, object, ['kod'], it)>
        <tr>
            <td><@tool.label name='kod' items=items />:</td><td><@tool.place object=object name='kod' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['nazev'], it)>
        <tr>
            <td><@tool.label name='nazev' items=items />:</td><td><@tool.place object=object name='nazev' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canShowAtLeastOne(items, object, ['formaUhrK', 'mena', 'kurz', 'kurzMnozstvi', 'limitVratky', 'metodaZaokrDoklK', 'zaokrNaSumK', 'vsbFak', 'vsbPok', 'vsbKasa', 'popis', 'poznam', 'platiOd', 'platiDo'], it)>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canShowAtLeastOne(items, object, ['formaUhrK', 'mena', 'kurz', 'kurzMnozstvi', 'limitVratky', 'metodaZaokrDoklK', 'zaokrNaSumK'], it)>
            <li class="active"><a href="#view-fragment-1" data-toggle="tab">${lang('labels', 'zaklInformacePN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['vsbFak', 'vsbPok', 'vsbKasa'], it)>
            <li><a href="#view-fragment-2" data-toggle="tab">${lang('labels', 'formaUhrady.pouzitiPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
            <li><a href="#view-fragment-3" data-toggle="tab">${lang('labels', 'textyPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['platiOd', 'platiDo'], it)>
            <li><a href="#view-fragment-4" data-toggle="tab">${lang('labels', 'spravaPN')}</a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canShowAtLeastOne(items, object, ['formaUhrK', 'mena', 'kurz', 'kurzMnozstvi', 'limitVratky', 'metodaZaokrDoklK', 'zaokrNaSumK'], it)>
            <div id="view-fragment-1" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['formaUhrK', 'mena', 'kurz', 'kurzMnozstvi', 'limitVratky', 'metodaZaokrDoklK', 'zaokrNaSumK'], it)>
                <table class="flexibee-tbl-1">
                    <#if canShowAtLeastOne(items, object, ['formaUhrK'], it)>
                    <tr>
                        <td><@tool.label name='formaUhrK' items=items />:</td><td><@tool.place object=object name='formaUhrK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['mena'], it)>
                    <tr>
                        <td><@tool.label name='mena' items=items />:</td><td><@tool.place object=object name='mena' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['kurz'], it)>
                    <tr>
                        <td><@tool.label name='kurz' items=items />:</td><td><@tool.place object=object name='kurz' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['kurzMnozstvi'], it)>
                    <tr>
                        <td><@tool.label name='kurzMnozstvi' items=items />:</td><td><@tool.place object=object name='kurzMnozstvi' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['limitVratky'], it)>
                    <tr>
                        <td><@tool.label name='limitVratky' items=items />:</td><td><@tool.place object=object name='limitVratky' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['metodaZaokrDoklK'], it)>
                    <tr>
                        <td><@tool.label name='metodaZaokrDoklK' items=items />:</td><td><@tool.place object=object name='metodaZaokrDoklK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['zaokrNaSumK'], it)>
                    <tr>
                        <td><@tool.label name='zaokrNaSumK' items=items />:</td><td><@tool.place object=object name='zaokrNaSumK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['vsbFak', 'vsbPok', 'vsbKasa'], it)>
            <div id="view-fragment-2" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['vsbFak', 'vsbPok', 'vsbKasa'], it)>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <#if canShowAtLeastOne(items, object, ['vsbFak'], it)>
                    <tr>
                        <td><@tool.label name='vsbFak' items=items />:</td><td><@tool.place object=object name='vsbFak' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['vsbPok'], it)>
                    <tr>
                        <td><@tool.label name='vsbPok' items=items />:</td><td><@tool.place object=object name='vsbPok' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['vsbKasa'], it)>
                    <tr>
                        <td><@tool.label name='vsbKasa' items=items />:</td><td><@tool.place object=object name='vsbKasa' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
            <div id="view-fragment-3" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
                <table class="flexibee-tbl-1">
                    <#if canShowAtLeastOne(items, object, ['popis'], it)>
                    <tr>
                        <td><@tool.label name='popis' items=items />:<br><@tool.placeArea object=object name='popis' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['poznam'], it)>
                    <tr>
                        <td><@tool.label name='poznam' items=items />:<br><@tool.placeArea object=object name='poznam' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['platiOd', 'platiDo'], it)>
            <div id="view-fragment-4" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['platiOd', 'platiDo'], it)>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <tr>
                        <td colspan="2">Platnost se udává v kalendářních rocích, pro neomezenou platnost zadejte 0-9999</td>
                    </tr>

                    <#if canShowAtLeastOne(items, object, ['platiOd'], it)>
                    <tr>
                        <td><@tool.label name='platiOd' items=items />:</td><td><@tool.place object=object name='platiOd' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['platiDo'], it)>
                    <tr>
                        <td><@tool.label name='platiDo' items=items />:</td><td><@tool.place object=object name='platiDo' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

<@tool.listItems items=items object=object marker=marker />

<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />

<#include "/i-footer-view.ftl" />
</#escape>
