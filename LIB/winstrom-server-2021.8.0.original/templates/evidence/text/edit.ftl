<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>

<@edit.beginForm />
    <#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canEditAtLeastOne(items, object, ['nazev', 'popis'])>
    <table class="flexibee-tbl-1">
        <col width="20%" /><col width="80%" />
        <#if canEditAtLeastOne(items, object, ['nazev'])>
        <tr>
            <td><@edit.label name='nazev' items=items /></td><td><@edit.place object=object name='nazev' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['popis'])>
        <tr>
            <td><@edit.label name='popis' items=items /></td><td><@edit.textarea object=object name='popis' items=items marker=marker rows=5 /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canEditAtLeastOne(items, object, ['vsbFav', 'vsbPhl', 'vsbFap', 'vsbZav', 'vsbBan', 'vsbPok', 'vsbInt', 'vsbSkl', 'vsbNap', 'vsbObp', 'vsbPpp', 'vsbNav', 'vsbObv', 'vsbPpv'])>
    <h4>${lang('labels', 'vybKliceViditelnost')}</h4>
    <div id="edit-fragment-1" class="tab-pane">
        <div class="flexibee-table-border">
        <#if canEditAtLeastOne(items, object, ['vsbFav', 'vsbPhl', 'vsbFap', 'vsbZav', 'vsbBan', 'vsbPok', 'vsbInt', 'vsbSkl', 'vsbNap', 'vsbObp', 'vsbPpp', 'vsbNav', 'vsbObv', 'vsbPpv'])>
        <table class="flexibee-tbl-1">
            <col width="18%" /><col width="15%" /><col width="18%" /><col width="15%" /><col width="18%" /><col width="15%" />
            <#if canEditAtLeastOne(items, object, ['vsbFav', 'vsbPhl'])>
            <tr>
                <#if canEditAtLeastOne(items, object, ['vsbFav'])>
                <td><@edit.label name='vsbFav' items=items /></td><td><@edit.place object=object name='vsbFav' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canEditAtLeastOne(items, object, ['vsbPhl'])>
                <td><@edit.label name='vsbPhl' items=items /></td><td><@edit.place object=object name='vsbPhl' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
            <#if canEditAtLeastOne(items, object, ['vsbFap', 'vsbZav'])>
            <tr>
                <#if canEditAtLeastOne(items, object, ['vsbFap'])>
                <td><@edit.label name='vsbFap' items=items /></td><td><@edit.place object=object name='vsbFap' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canEditAtLeastOne(items, object, ['vsbZav'])>
                <td><@edit.label name='vsbZav' items=items /></td><td><@edit.place object=object name='vsbZav' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
            <#if canEditAtLeastOne(items, object, ['vsbBan', 'vsbPok', 'vsbInt'])>
            <tr>
                <#if canEditAtLeastOne(items, object, ['vsbBan'])>
                <td><@edit.label name='vsbBan' items=items /></td><td><@edit.place object=object name='vsbBan' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canEditAtLeastOne(items, object, ['vsbPok'])>
                <td><@edit.label name='vsbPok' items=items /></td><td><@edit.place object=object name='vsbPok' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canEditAtLeastOne(items, object, ['vsbInt'])>
                <td><@edit.label name='vsbInt' items=items /></td><td><@edit.place object=object name='vsbInt' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
            <#if canEditAtLeastOne(items, object, ['vsbSkl'])>
            <tr>
                <td><@edit.label name='vsbSkl' items=items /></td><td><@edit.place object=object name='vsbSkl' items=items marker=marker /></td>
            </tr>
            </#if>
            <#if canEditAtLeastOne(items, object, ['vsbNap', 'vsbObp', 'vsbPpp'])>
            <tr>
                <#if canEditAtLeastOne(items, object, ['vsbNap'])>
                <td><@edit.label name='vsbNap' items=items /></td><td><@edit.place object=object name='vsbNap' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canEditAtLeastOne(items, object, ['vsbObp'])>
                <td><@edit.label name='vsbObp' items=items /></td><td><@edit.place object=object name='vsbObp' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canEditAtLeastOne(items, object, ['vsbPpp'])>
                <td><@edit.label name='vsbPpp' items=items /></td><td><@edit.place object=object name='vsbPpp' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
            <#if canEditAtLeastOne(items, object, ['vsbNav', 'vsbObv', 'vsbPpv'])>
            <tr>
                <#if canEditAtLeastOne(items, object, ['vsbNav'])>
                <td><@edit.label name='vsbNav' items=items /></td><td><@edit.place object=object name='vsbNav' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canEditAtLeastOne(items, object, ['vsbObv'])>
                <td><@edit.label name='vsbObv' items=items /></td><td><@edit.place object=object name='vsbObv' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canEditAtLeastOne(items, object, ['vsbPpv'])>
                <td><@edit.label name='vsbPpv' items=items /></td><td><@edit.place object=object name='vsbPpv' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
        </table>
        </#if>
        </div>

    </div>
    </#if>

    <#if canEditAtLeastOne(items, object, ['vsbPopis', 'vsbPoznamka', 'vsbDoprava', 'vsbUvod', 'vsbZaver', 'vsbNazevPol', 'vsbPoznamPol'])>
    <h4>Viditelnost v místech použití</h4>
    <div id="edit-fragment-2" class="tab-pane">
        <div class="flexibee-table-border">
        <#if canEditAtLeastOne(items, object, ['vsbPopis', 'vsbPoznamka', 'vsbDoprava', 'vsbUvod', 'vsbZaver', 'vsbNazevPol', 'vsbPoznamPol'])>
        <table class="flexibee-tbl-1">
            <col width="18%" /><col width="15%" /><col width="18%" /><col width="15%" /><col width="18%" /><col width="15%" />
            <#if canEditAtLeastOne(items, object, ['vsbPopis', 'vsbPoznamka', 'vsbDoprava'])>
            <tr>
                <#if canEditAtLeastOne(items, object, ['vsbPopis'])>
                <td><@edit.label name='vsbPopis' items=items /></td><td><@edit.place object=object name='vsbPopis' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canEditAtLeastOne(items, object, ['vsbPoznamka'])>
                <td><@edit.label name='vsbPoznamka' items=items /></td><td><@edit.place object=object name='vsbPoznamka' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canEditAtLeastOne(items, object, ['vsbDoprava'])>
                <td><@edit.label name='vsbDoprava' items=items /></td><td><@edit.place object=object name='vsbDoprava' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
            <#if canEditAtLeastOne(items, object, ['vsbUvod', 'vsbZaver'])>
            <tr>
                <#if canEditAtLeastOne(items, object, ['vsbUvod'])>
                <td><@edit.label name='vsbUvod' items=items /></td><td><@edit.place object=object name='vsbUvod' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canEditAtLeastOne(items, object, ['vsbZaver'])>
                <td><@edit.label name='vsbZaver' items=items /></td><td><@edit.place object=object name='vsbZaver' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
            <#if canEditAtLeastOne(items, object, ['vsbNazevPol', 'vsbPoznamPol'])>
            <tr>
                <#if canEditAtLeastOne(items, object, ['vsbNazevPol'])>
                <td><@edit.label name='vsbNazevPol' items=items /></td><td><@edit.place object=object name='vsbNazevPol' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canEditAtLeastOne(items, object, ['vsbPoznamPol'])>
                <td><@edit.label name='vsbPoznamPol' items=items /></td><td><@edit.place object=object name='vsbPoznamPol' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
        </table>
        </#if>
        </div>

    </div>
    </#if>

<#--AUTOGEN END-->

    <br/>
    <@edit.listItems items=items object=object marker=marker />

    <@edit.saveButton />
<@edit.endForm />

<@edit.generateNaiveJavascript object=object it=it />

<#include "/parts/i-dalsi-informace.ftl" />

<#include "/i-footer-edit.ftl" />
</#escape>
