<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-view.ftl" />

<#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canShowAtLeastOne(items, object, ['nazev', 'popis'], it)>
    <table class="flexibee-tbl-1">
        <col width="20%" /><col width="80%" />
        <#if canShowAtLeastOne(items, object, ['nazev'], it)>
        <tr>
            <td><@tool.label name='nazev' items=items />:</td><td><@tool.place object=object name='nazev' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['popis'], it)>
        <tr>
            <td><@tool.label name='popis' items=items />:<br><@tool.placeArea object=object name='popis' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canShowAtLeastOne(items, object, ['vsbFav', 'vsbPhl', 'vsbFap', 'vsbZav', 'vsbBan', 'vsbPok', 'vsbInt', 'vsbSkl', 'vsbNap', 'vsbObp', 'vsbPpp', 'vsbNav', 'vsbObv', 'vsbPpv'], it)>
    <h4>${lang('labels', 'vybKliceViditelnost')}</h4>
    <div id="view-fragment-1" class="tab-pane">
        <div class="flexibee-table-border">
        <#if canShowAtLeastOne(items, object, ['vsbFav', 'vsbPhl', 'vsbFap', 'vsbZav', 'vsbBan', 'vsbPok', 'vsbInt', 'vsbSkl', 'vsbNap', 'vsbObp', 'vsbPpp', 'vsbNav', 'vsbObv', 'vsbPpv'], it)>
        <table class="flexibee-tbl-1">
            <col width="18%" /><col width="15%" /><col width="18%" /><col width="15%" /><col width="18%" /><col width="15%" />
            <#if canShowAtLeastOne(items, object, ['vsbFav', 'vsbPhl'], it)>
            <tr>
                <#if canShowAtLeastOne(items, object, ['vsbFav'], it)>
                <td><@tool.label name='vsbFav' items=items />:</td><td><@tool.place object=object name='vsbFav' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canShowAtLeastOne(items, object, ['vsbPhl'], it)>
                <td><@tool.label name='vsbPhl' items=items />:</td><td><@tool.place object=object name='vsbPhl' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
            <#if canShowAtLeastOne(items, object, ['vsbFap', 'vsbZav'], it)>
            <tr>
                <#if canShowAtLeastOne(items, object, ['vsbFap'], it)>
                <td><@tool.label name='vsbFap' items=items />:</td><td><@tool.place object=object name='vsbFap' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canShowAtLeastOne(items, object, ['vsbZav'], it)>
                <td><@tool.label name='vsbZav' items=items />:</td><td><@tool.place object=object name='vsbZav' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
            <#if canShowAtLeastOne(items, object, ['vsbBan', 'vsbPok', 'vsbInt'], it)>
            <tr>
                <#if canShowAtLeastOne(items, object, ['vsbBan'], it)>
                <td><@tool.label name='vsbBan' items=items />:</td><td><@tool.place object=object name='vsbBan' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canShowAtLeastOne(items, object, ['vsbPok'], it)>
                <td><@tool.label name='vsbPok' items=items />:</td><td><@tool.place object=object name='vsbPok' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canShowAtLeastOne(items, object, ['vsbInt'], it)>
                <td><@tool.label name='vsbInt' items=items />:</td><td><@tool.place object=object name='vsbInt' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
            <#if canShowAtLeastOne(items, object, ['vsbSkl'], it)>
            <tr>
                <td><@tool.label name='vsbSkl' items=items />:</td><td><@tool.place object=object name='vsbSkl' items=items marker=marker /></td>
            </tr>
            </#if>
            <#if canShowAtLeastOne(items, object, ['vsbNap', 'vsbObp', 'vsbPpp'], it)>
            <tr>
                <#if canShowAtLeastOne(items, object, ['vsbNap'], it)>
                <td><@tool.label name='vsbNap' items=items />:</td><td><@tool.place object=object name='vsbNap' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canShowAtLeastOne(items, object, ['vsbObp'], it)>
                <td><@tool.label name='vsbObp' items=items />:</td><td><@tool.place object=object name='vsbObp' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canShowAtLeastOne(items, object, ['vsbPpp'], it)>
                <td><@tool.label name='vsbPpp' items=items />:</td><td><@tool.place object=object name='vsbPpp' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
            <#if canShowAtLeastOne(items, object, ['vsbNav', 'vsbObv', 'vsbPpv'], it)>
            <tr>
                <#if canShowAtLeastOne(items, object, ['vsbNav'], it)>
                <td><@tool.label name='vsbNav' items=items />:</td><td><@tool.place object=object name='vsbNav' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canShowAtLeastOne(items, object, ['vsbObv'], it)>
                <td><@tool.label name='vsbObv' items=items />:</td><td><@tool.place object=object name='vsbObv' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canShowAtLeastOne(items, object, ['vsbPpv'], it)>
                <td><@tool.label name='vsbPpv' items=items />:</td><td><@tool.place object=object name='vsbPpv' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
        </table>
        </#if>
        </div>

    </div>
    </#if>

    <#if canShowAtLeastOne(items, object, ['vsbPopis', 'vsbPoznamka', 'vsbDoprava', 'vsbUvod', 'vsbZaver', 'vsbNazevPol', 'vsbPoznamPol'], it)>
    <h4>Viditelnost v místech použití</h4>
    <div id="view-fragment-2" class="tab-pane">
        <div class="flexibee-table-border">
        <#if canShowAtLeastOne(items, object, ['vsbPopis', 'vsbPoznamka', 'vsbDoprava', 'vsbUvod', 'vsbZaver', 'vsbNazevPol', 'vsbPoznamPol'], it)>
        <table class="flexibee-tbl-1">
            <col width="18%" /><col width="15%" /><col width="18%" /><col width="15%" /><col width="18%" /><col width="15%" />
            <#if canShowAtLeastOne(items, object, ['vsbPopis', 'vsbPoznamka', 'vsbDoprava'], it)>
            <tr>
                <#if canShowAtLeastOne(items, object, ['vsbPopis'], it)>
                <td><@tool.label name='vsbPopis' items=items />:</td><td><@tool.place object=object name='vsbPopis' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canShowAtLeastOne(items, object, ['vsbPoznamka'], it)>
                <td><@tool.label name='vsbPoznamka' items=items />:</td><td><@tool.place object=object name='vsbPoznamka' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canShowAtLeastOne(items, object, ['vsbDoprava'], it)>
                <td><@tool.label name='vsbDoprava' items=items />:</td><td><@tool.place object=object name='vsbDoprava' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
            <#if canShowAtLeastOne(items, object, ['vsbUvod', 'vsbZaver'], it)>
            <tr>
                <#if canShowAtLeastOne(items, object, ['vsbUvod'], it)>
                <td><@tool.label name='vsbUvod' items=items />:</td><td><@tool.place object=object name='vsbUvod' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canShowAtLeastOne(items, object, ['vsbZaver'], it)>
                <td><@tool.label name='vsbZaver' items=items />:</td><td><@tool.place object=object name='vsbZaver' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
            <#if canShowAtLeastOne(items, object, ['vsbNazevPol', 'vsbPoznamPol'], it)>
            <tr>
                <#if canShowAtLeastOne(items, object, ['vsbNazevPol'], it)>
                <td><@tool.label name='vsbNazevPol' items=items />:</td><td><@tool.place object=object name='vsbNazevPol' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canShowAtLeastOne(items, object, ['vsbPoznamPol'], it)>
                <td><@tool.label name='vsbPoznamPol' items=items />:</td><td><@tool.place object=object name='vsbPoznamPol' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
        </table>
        </#if>
        </div>

    </div>
    </#if>

<#--AUTOGEN END-->

<@tool.listItems items=items object=object marker=marker />

<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />

<#include "/i-footer-view.ftl" />
</#escape>