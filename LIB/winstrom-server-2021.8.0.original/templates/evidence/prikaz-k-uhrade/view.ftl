<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-view.ftl" />

<#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canShowAtLeastOne(items, object, ['typDokl', 'celCastka', 'zahranicni', 'stavPrikazK', 'jmenoSoub', 'mena', 'datVytvor', 'konSym', 'poradiPrikaz', 'datSplat', 'nazFirmy', 'faUlice', 'faMesto', 'faPsc', 'faStat'], it)>
    <table class="flexibee-tbl-1">
        <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
        <#if canShowAtLeastOne(items, object, ['typDokl', 'celCastka'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['typDokl'], it)>
            <td><@tool.label name='typDokl' items=items />:</td><td><@tool.place object=object name='typDokl' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['celCastka'], it)>
            <td><@tool.label name='celCastka' items=items />:</td><td><@tool.place object=object name='celCastka' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['zahranicni'], it)>
        <tr>
            <td><@tool.label name='zahranicni' items=items />:</td><td><@tool.place object=object name='zahranicni' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['stavPrikazK', 'jmenoSoub'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['stavPrikazK'], it)>
            <td><@tool.label name='stavPrikazK' items=items />:</td><td><@tool.place object=object name='stavPrikazK' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['jmenoSoub'], it)>
            <td><@tool.label name='jmenoSoub' items=items />:</td><td><@tool.place object=object name='jmenoSoub' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['mena', 'datVytvor'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['mena'], it)>
            <td><@tool.label name='mena' items=items />:</td><td><@tool.place object=object name='mena' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['datVytvor'], it)>
            <td><@tool.label name='datVytvor' items=items />:</td><td><@tool.place object=object name='datVytvor' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['konSym', 'poradiPrikaz'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['konSym'], it)>
            <td><@tool.label name='konSym' items=items />:</td><td><@tool.place object=object name='konSym' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['poradiPrikaz'], it)>
            <td><@tool.label name='poradiPrikaz' items=items />:</td><td><@tool.place object=object name='poradiPrikaz' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['datSplat'], it)>
        <tr>
            <td><@tool.label name='datSplat' items=items />:</td><td><@tool.place object=object name='datSplat' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['nazFirmy', 'faUlice'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['nazFirmy'], it)>
            <td><@tool.label name='nazFirmy' items=items />:</td><td><@tool.place object=object name='nazFirmy' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['faUlice'], it)>
            <td><@tool.label name='faUlice' items=items />:</td><td><@tool.place object=object name='faUlice' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['faMesto', 'faPsc'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['faMesto'], it)>
            <td><@tool.label name='faMesto' items=items />:</td><td><@tool.place object=object name='faMesto' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['faPsc'], it)>
            <td><@tool.label name='faPsc' items=items />:</td><td><@tool.place object=object name='faPsc' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['faStat'], it)>
        <tr>
            <td><@tool.label name='faStat' items=items />:</td><td><@tool.place object=object name='faStat' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

<#--AUTOGEN END-->

<@tool.listItems items=items object=object marker=marker />

<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />

<#include "/i-footer-view.ftl" />
</#escape>