<#ftl encoding="utf-8" strip_whitespace=true/>
<#escape x as x?default("")?html>

<#include "/i-header-view.ftl" />

<#import "/evidence/l-doklad-view.ftl" as dokladView />

<@dokladView.showUhrada it=it object=object marker=marker />


${marker.mark('visible')}
${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}
${marker.mark('stitky')}

<#-- naprosto ořezaná verze -->
${marker.mark('zamekK')}
${marker.mark('stavUzivK')}
${marker.mark('bezPolozek')}
${marker.mark('ucetni')}
${marker.mark('szbDphSniz')}
${marker.mark('szbDphZakl')}
${marker.mark('stitky')}
${marker.mark('mistUrc')}
${marker.mark('uzivatel')}
${marker.mark('rada')}
${marker.mark('polozkaRady')}
${marker.mark('sazbaDphOsv')}
${marker.mark('sazbaDphSniz')}
${marker.mark('sazbaDphZakl')}
${marker.mark('podpisPrik')}
${marker.mark('prikazSum')}
${marker.mark('prikazSumMen')}

${marker.mark('stavOdpocetK')}
${marker.mark('generovatSkl')}
${marker.mark('hromFakt')}
${marker.mark('zdrojProSkl')}
${marker.mark('prodejka')}

<#--AUTOGEN BEGIN-->
    <div id="creditcheck-container">
    </div>


    <#if isMobile>
    <div id="flexibee-document-header">

    <#else>
        <#-- TODO tohle by patřilo spíš do nějakého stylopisu, a vůbec nejlíp by se to rozvržení mělo řešit jinak -->
        <style type="text/css">
            table#flexibee-document-header td { vertical-align: top; }
        </style>

        <table id="flexibee-document-header">
        <col width="50%">
        <col width="50%">
        <tr><td>
    </#if>

    <#if canShowAtLeastOne(items, object, ['typDokl', 'banka', 'pokladna', 'sklad', 'typPohybuK', 'cisDosle', 'varSym', 'popis', 'datVyst', 'datSplat', 'stavUhrK', 'cisSml', 'formaUhradyCis', 'formaUhrK', 'cisObj', 'datObj', 'cisDodak', 'formaDopravy', 'doprava', 'typDoklBan', 'bspBan', 'bankovniUcet', 'varSym', 'konSym', 'uvodTxt', 'zavTxt', 'poznam', 'datUp1', 'datUp2', 'datSmir'], it)>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canShowAtLeastOne(items, object, ['typDokl', 'banka', 'pokladna', 'sklad', 'typPohybuK', 'cisDosle', 'varSym', 'popis', 'datVyst', 'datSplat', 'stavUhrK'], it)>
            <li class="active"><a href="#view-fragment-1" data-toggle="tab">${lang('labels', 'hlavniPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['cisSml', 'formaUhradyCis', 'formaUhrK', 'cisObj', 'datObj', 'cisDodak', 'formaDopravy', 'doprava'], it)>
            <li><a href="#view-fragment-2" data-toggle="tab">${lang('labels', 'dokladEBDoplnekPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['typDoklBan', 'bspBan', 'bankovniUcet', 'varSym', 'konSym'], it)>
            <li><a href="#view-fragment-3" data-toggle="tab">${lang('labels', 'dokladEBPlatbaNaUcet')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['uvodTxt', 'zavTxt', 'poznam'], it)>
            <li><a href="#view-fragment-4" data-toggle="tab">${lang('labels', 'textyPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['datUp1', 'datUp2', 'datSmir'], it)>
            <li><a href="#view-fragment-5" data-toggle="tab">${lang('labels', 'dokladEBUpominkyPN')}</a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canShowAtLeastOne(items, object, ['typDokl', 'banka', 'pokladna', 'sklad', 'typPohybuK', 'cisDosle', 'varSym', 'popis', 'datVyst', 'datSplat', 'stavUhrK'], it)>
            <div id="view-fragment-1" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['typDokl', 'banka', 'pokladna', 'sklad', 'typPohybuK', 'cisDosle', 'varSym', 'popis', 'datVyst', 'datSplat', 'stavUhrK'], it)>
                <table class="flexibee-tbl-1">
                    <col width="35%" /><col width="65%" />
                    <#if canShowAtLeastOne(items, object, ['typDokl'], it)>
                    <tr>
                        <td><@tool.label name='typDokl' items=items />:</td><td><@tool.place object=object name='typDokl' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['banka'], it)>
                    <tr>
                        <td><@tool.label name='banka' items=items />:</td><td><@tool.place object=object name='banka' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['pokladna'], it)>
                    <tr>
                        <td><@tool.label name='pokladna' items=items />:</td><td><@tool.place object=object name='pokladna' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['sklad'], it)>
                    <tr>
                        <td><@tool.label name='sklad' items=items />:</td><td><@tool.place object=object name='sklad' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['typPohybuK'], it)>
                    <tr>
                        <td><@tool.label name='typPohybuK' items=items />:</td><td><@tool.place object=object name='typPohybuK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['cisDosle'], it)>
                    <tr>
                        <td><@tool.label name='cisDosle' items=items />:</td><td><@tool.place object=object name='cisDosle' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['varSym'], it)>
                    <tr>
                        <td><@tool.label name='varSym' items=items />:</td><td><@tool.place object=object name='varSym' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['popis'], it)>
                    <tr>
                        <td><@tool.label name='popis' items=items />:</td><td><@tool.place object=object name='popis' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['datVyst'], it)>
                    <tr>
                        <td><@tool.label name='datVyst' items=items />:</td><td><@tool.place object=object name='datVyst' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['datSplat'], it)>
                    <tr>
                        <td><@tool.label name='datSplat' items=items />:</td><td><@tool.place object=object name='datSplat' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['stavUhrK'], it)>
                    <tr>
                        <td><@tool.label name='stavUhrK' items=items />:</td><td><@tool.place object=object name='stavUhrK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['cisSml', 'formaUhradyCis', 'formaUhrK', 'cisObj', 'datObj', 'cisDodak', 'formaDopravy', 'doprava'], it)>
            <div id="view-fragment-2" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['cisSml', 'formaUhradyCis', 'formaUhrK', 'cisObj', 'datObj', 'cisDodak', 'formaDopravy', 'doprava'], it)>
                <table class="flexibee-tbl-1">
                    <col width="35%" /><col width="65%" />
                    <#if canShowAtLeastOne(items, object, ['cisSml'], it)>
                    <tr>
                        <td><@tool.label name='cisSml' items=items />:</td><td><@tool.place object=object name='cisSml' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['formaUhradyCis'], it)>
                    <tr>
                        <td><@tool.label name='formaUhradyCis' items=items />:</td><td><@tool.place object=object name='formaUhradyCis' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['formaUhrK'], it)>
                    <tr>
                        <td><@tool.label name='formaUhrK' items=items />:</td><td><@tool.place object=object name='formaUhrK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['cisObj'], it)>
                    <tr>
                        <td><@tool.label name='cisObj' items=items />:</td><td><@tool.place object=object name='cisObj' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['datObj'], it)>
                    <tr>
                        <td><@tool.label name='datObj' items=items />:</td><td><@tool.place object=object name='datObj' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['cisDodak'], it)>
                    <tr>
                        <td><@tool.label name='cisDodak' items=items />:</td><td><@tool.place object=object name='cisDodak' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['formaDopravy'], it)>
                    <tr>
                        <td><@tool.label name='formaDopravy' items=items />:</td><td><@tool.place object=object name='formaDopravy' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['doprava'], it)>
                    <tr>
                        <td colspan="4"><@tool.label name='doprava' items=items />:<br><@tool.placeArea object=object name='doprava' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['typDoklBan', 'bspBan', 'bankovniUcet', 'varSym', 'konSym'], it)>
            <div id="view-fragment-3" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['typDoklBan', 'bspBan', 'bankovniUcet', 'varSym', 'konSym'], it)>
                <table class="flexibee-tbl-1">
                    <col width="35%" /><col width="65%" />
                    <#if canShowAtLeastOne(items, object, ['typDoklBan'], it)>
                    <tr>
                        <td><@tool.label name='typDoklBan' items=items />:</td><td><@tool.place object=object name='typDoklBan' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['bspBan'], it)>
                    <tr>
                        <td><@tool.label name='bspBan' items=items />:</td><td><@tool.place object=object name='bspBan' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['bankovniUcet'], it)>
                    <tr>
                        <td><@tool.label name='bankovniUcet' items=items />:</td><td><@tool.place object=object name='bankovniUcet' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['varSym'], it)>
                    <tr>
                        <td><@tool.label name='varSym' items=items />:</td><td><@tool.place object=object name='varSym' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['konSym'], it)>
                    <tr>
                        <td><@tool.label name='konSym' items=items level='3'/>:</td><td><@tool.place object=object level='3' name='konSym' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['uvodTxt', 'zavTxt', 'poznam'], it)>
            <div id="view-fragment-4" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['uvodTxt', 'zavTxt', 'poznam'], it)>
                <table class="flexibee-tbl-1">
                    <#if canShowAtLeastOne(items, object, ['uvodTxt'], it)>
                    <tr>
                        <td><@tool.label name='uvodTxt' items=items />:<br><@tool.placeArea object=object name='uvodTxt' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['zavTxt'], it)>
                    <tr>
                        <td><@tool.label name='zavTxt' items=items />:<br><@tool.placeArea object=object name='zavTxt' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['poznam'], it)>
                    <tr>
                        <td><@tool.label name='poznam' items=items />:<br><@tool.placeArea object=object name='poznam' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['datUp1', 'datUp2', 'datSmir'], it)>
            <div id="view-fragment-5" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['datUp1', 'datUp2', 'datSmir'], it)>
                <table class="flexibee-tbl-1">
                    <col width="35%" /><col width="65%" />
                    <#if canShowAtLeastOne(items, object, ['datUp1'], it)>
                    <tr>
                        <td><@tool.label name='datUp1' items=items />:</td><td><@tool.place object=object name='datUp1' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['datUp2'], it)>
                    <tr>
                        <td><@tool.label name='datUp2' items=items />:</td><td><@tool.place object=object name='datUp2' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['datSmir'], it)>
                    <tr>
                        <td><@tool.label name='datSmir' items=items />:</td><td><@tool.place object=object name='datSmir' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

    <#if isMobile>
        <br>
    <#else>
        </td><td>
    </#if>

        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <li class="active"><a href="#view-fragment-6" data-toggle="tab">${lang('dials', 'typVztahu.dodavatel')}</a></li>

            <#if canShowAtLeastOne(items, object, ['faNazev', 'faUlice', 'faMesto', 'faPsc', 'faStat'], it)>
            <li><a href="#view-fragment-7" data-toggle="tab">${lang('labels', 'postovniPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['banSpojDod', 'buc', 'smerKod', 'iban', 'bic', 'specSym'], it)>
            <li><a href="#view-fragment-8" data-toggle="tab">${lang('labels', 'dokladEBUcetDodavatele')}</a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <div id="view-fragment-6" class="tab-pane active">
                <div class="flexibee-table-border">
                <table class="flexibee-tbl-2">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <tr>
                        <td><@tool.label name='firma' items=items />:</td><td colspan="3"><@tool.place object=object name='firma' items=items marker=marker /></td>
                    </tr>

                    <#if canShowAtLeastOne(items, object, ['nazFirmy'], it)>
                    <tr>
                        <td><@tool.label name='nazFirmy' items=items level='3'/>:</td><td colspan="3"><@tool.place object=object level='3' name='nazFirmy' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['ulice'], it)>
                    <tr>
                        <td><@tool.label name='ulice' items=items level='3'/>:</td><td colspan="3"><@tool.place object=object level='3' name='ulice' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['mesto', 'psc'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['mesto'], it)>
                        <td><@tool.label name='mesto' items=items level='3'/>:</td><td><@tool.place object=object level='3' name='mesto' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['psc'], it)>
                        <td><@tool.label name='psc' items=items level='3'/>:</td><td><@tool.place object=object level='3' name='psc' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['stat'], it)>
                    <tr>
                        <td><@tool.label name='stat' items=items level='3'/>:</td><td colspan="3"><@tool.place object=object level='3' name='stat' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['ic', 'dic'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['ic'], it)>
                        <td><@tool.label name='ic' items=items level='3'/>:</td><td><@tool.place object=object level='3' name='ic' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['dic'], it)>
                        <td><@tool.label name='dic' items=items level='3'/>:</td><td><@tool.place object=object level='3' name='dic' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['vatId'], it)>
                    <tr>
                        <td><@tool.label name='vatId' items=items level='3'/>:</td><td><@tool.place object=object level='3' name='vatId' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>

                </div>

            </div>

            <#if canShowAtLeastOne(items, object, ['faNazev', 'faUlice', 'faMesto', 'faPsc', 'faStat'], it)>
            <div id="view-fragment-7" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['faNazev', 'faUlice', 'faMesto', 'faPsc', 'faStat'], it)>
                <table class="flexibee-tbl-2">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canShowAtLeastOne(items, object, ['faNazev'], it)>
                    <tr>
                        <td><@tool.label name='faNazev' items=items level='3'/>:</td><td colspan="3"><@tool.place object=object level='3' name='faNazev' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['faUlice'], it)>
                    <tr>
                        <td><@tool.label name='faUlice' items=items level='3'/>:</td><td colspan="3"><@tool.place object=object level='3' name='faUlice' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['faMesto', 'faPsc'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['faMesto'], it)>
                        <td><@tool.label name='faMesto' items=items level='3'/>:</td><td><@tool.place object=object level='3' name='faMesto' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['faPsc'], it)>
                        <td><@tool.label name='faPsc' items=items level='3'/>:</td><td><@tool.place object=object level='3' name='faPsc' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['faStat'], it)>
                    <tr>
                        <td><@tool.label name='faStat' items=items level='3'/>:</td><td colspan="3"><@tool.place object=object level='3' name='faStat' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['banSpojDod', 'buc', 'smerKod', 'iban', 'bic', 'specSym'], it)>
            <div id="view-fragment-8" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['banSpojDod', 'buc', 'smerKod', 'iban', 'bic', 'specSym'], it)>
                <table class="flexibee-tbl-1">
                    <#if canShowAtLeastOne(items, object, ['banSpojDod'], it)>
                    <tr>
                        <td><@tool.label name='banSpojDod' items=items />:</td><td><@tool.place object=object name='banSpojDod' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['buc'], it)>
                    <tr>
                        <td><@tool.label name='buc' items=items />:</td><td><@tool.place object=object name='buc' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['smerKod'], it)>
                    <tr>
                        <td><@tool.label name='smerKod' items=items />:</td><td><@tool.place object=object name='smerKod' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['iban'], it)>
                    <tr>
                        <td><@tool.label name='iban' items=items />:</td><td><@tool.place object=object name='iban' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['bic'], it)>
                    <tr>
                        <td><@tool.label name='bic' items=items />:</td><td><@tool.place object=object name='bic' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['specSym'], it)>
                    <tr>
                        <td><@tool.label name='specSym' items=items />:</td><td><@tool.place object=object name='specSym' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    <#if isMobile>
        <br>
    <#else>
        </td></tr><tr><td>
    </#if>

    <div class="flexibee-more">
    <div class="flexibee-more-label">
         <table class="flexibee-tbl-1">
        <tr>
            <td colspan="2"><#if object['zuctovano']?? && object['zuctovano'] == true><div class="alert alert-success flexibee-more-for-button">Doklad je zaúčtován.</div><#elseif object['typDokl']?? && object['typDokl'].ucetni?? && object['typDokl'].ucetni == false>Doklad je neúčetní.<#else><div class="alert alert-warning flexibee-more-for-button" title="Doklad nemá vyplněné některé položky, které jsou nutné pro vstup do účetnictví.">Doklad není zaúčtován!</div></#if></td>
        </tr>
        <#if object['typUcOp']??>
            <tr>
                        <td><@tool.label name='typUcOp' items=items />:</td>
                <td><@tool.place object=object name='typUcOp' items=items marker=marker /></td>
            </tr>
        </#if>
        </table>
    </div>
     <div class="flexibee-hidden hidden">

    <#if canShowAtLeastOne(items, object, ['typUcOp', 'typDoklNabFak', 'stredisko', 'cinnost', 'zakazka', 'danEvidK', 'duzpPuv', 'duzpUcto', 'primUcet', 'protiUcet', 'vyloucitSaldo', 'dphZaklUcet', 'dphSnizUcet', 'dphSniz2Ucet', 'clenDph', 'statOdesl', 'obchTrans', 'statUrc', 'dodPodm', 'statPuvod', 'druhDopr', 'krajUrc', 'zvlPoh', 'zodpOsoba', 'kontaktOsoba', 'kontaktJmeno', 'kontaktEmail', 'kontaktTel'], it)>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canShowAtLeastOne(items, object, ['typUcOp', 'typDoklNabFak', 'stredisko', 'cinnost', 'zakazka'], it)>
            <li class="active"><a href="#view-fragment-9" data-toggle="tab">${lang('labels', 'specifikacePN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['danEvidK', 'duzpPuv', 'duzpUcto', 'primUcet', 'protiUcet', 'vyloucitSaldo', 'dphZaklUcet', 'dphSnizUcet', 'dphSniz2Ucet', 'clenDph'], it)>
            <li><a href="#view-fragment-10" data-toggle="tab">${lang('labels', 'uctoADphPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['statOdesl', 'obchTrans', 'statUrc', 'dodPodm', 'statPuvod', 'druhDopr', 'krajUrc', 'zvlPoh'], it)>
            <li><a href="#view-fragment-11" data-toggle="tab">${lang('labels', 'intrastatPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['zodpOsoba', 'kontaktOsoba', 'kontaktJmeno', 'kontaktEmail', 'kontaktTel'], it)>
            <li><a href="#view-fragment-12" data-toggle="tab">${lang('labels', 'zodpOsobaPN')}</a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canShowAtLeastOne(items, object, ['typUcOp', 'typDoklNabFak', 'stredisko', 'cinnost', 'zakazka'], it)>
            <div id="view-fragment-9" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['typUcOp', 'typDoklNabFak', 'stredisko', 'cinnost', 'zakazka'], it)>
                <table class="flexibee-tbl-1">
                    <col width="35%" /><col width="65%" />
                    <#if canShowAtLeastOne(items, object, ['typUcOp'], it)>
                    <tr>
                        <td><@tool.label name='typUcOp' items=items />:</td><td><@tool.place object=object name='typUcOp' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['typDoklNabFak'], it)>
                    <tr>
                        <td><@tool.label name='typDoklNabFak' items=items />:</td><td><@tool.place object=object name='typDoklNabFak' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['stredisko'], it)>
                    <tr>
                        <td><@tool.label name='stredisko' items=items />:</td><td><@tool.place object=object name='stredisko' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['cinnost'], it)>
                    <tr>
                        <td><@tool.label name='cinnost' items=items />:</td><td><@tool.place object=object name='cinnost' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['zakazka'], it)>
                    <tr>
                        <td><@tool.label name='zakazka' items=items />:</td><td><@tool.place object=object name='zakazka' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['danEvidK', 'duzpPuv', 'duzpUcto', 'primUcet', 'protiUcet', 'vyloucitSaldo', 'dphZaklUcet', 'dphSnizUcet', 'dphSniz2Ucet', 'clenDph'], it)>
            <div id="view-fragment-10" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['danEvidK', 'duzpPuv', 'duzpUcto', 'primUcet', 'protiUcet', 'vyloucitSaldo', 'dphZaklUcet', 'dphSnizUcet', 'dphSniz2Ucet', 'clenDph'], it)>
                <table class="flexibee-tbl-1">
                    <col width="35%" /><col width="65%" />
                    <#if project().danEvid>
                    <#if canShowAtLeastOne(items, object, ['danEvidK'], it)>
                    <tr>
                        <td><@tool.label name='danEvidK' items=items />:</td><td><@tool.place object=object name='danEvidK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['duzpPuv'], it)>
                    <tr>
                        <td><@tool.label name='duzpPuv' items=items />:</td><td><@tool.place object=object name='duzpPuv' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['duzpUcto'], it)>
                    <tr>
                        <td><@tool.label name='duzpUcto' items=items />:</td><td><@tool.place object=object name='duzpUcto' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['primUcet'], it)>
                    <tr>
                        <td><@tool.label name='primUcet' items=items />:</td><td><@tool.place object=object name='primUcet' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['protiUcet'], it)>
                    <tr>
                        <td><@tool.label name='protiUcet' items=items />:</td><td><@tool.place object=object name='protiUcet' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['vyloucitSaldo'], it)>
                    <tr>
                        <td><@tool.label name='vyloucitSaldo' items=items />:</td><td><@tool.place object=object name='vyloucitSaldo' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if settings(object.datumDokladu).platceDph>
                    <#if canShowAtLeastOne(items, object, ['dphZaklUcet'], it)>
                    <tr>
                        <td><@tool.label name='dphZaklUcet' items=items />:</td><td><@tool.place object=object name='dphZaklUcet' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['dphSnizUcet'], it)>
                    <tr>
                        <td><@tool.label name='dphSnizUcet' items=items />:</td><td><@tool.place object=object name='dphSnizUcet' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['dphSniz2Ucet'], it)>
                    <tr>
                        <td><@tool.label name='dphSniz2Ucet' items=items />:</td><td><@tool.place object=object name='dphSniz2Ucet' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['clenDph'], it)>
                    <tr>
                        <td><@tool.label name='clenDph' items=items />:</td><td><@tool.place object=object name='clenDph' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['statOdesl', 'obchTrans', 'statUrc', 'dodPodm', 'statPuvod', 'druhDopr', 'krajUrc', 'zvlPoh'], it)>
            <div id="view-fragment-11" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['statOdesl', 'obchTrans', 'statUrc', 'dodPodm', 'statPuvod', 'druhDopr', 'krajUrc', 'zvlPoh'], it)>
                <table class="flexibee-tbl-2">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canShowAtLeastOne(items, object, ['statOdesl', 'obchTrans'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['statOdesl'], it)>
                        <td><@tool.label name='statOdesl' items=items />:</td><td><@tool.place object=object name='statOdesl' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['obchTrans'], it)>
                        <td><@tool.label name='obchTrans' items=items />:</td><td><@tool.place object=object name='obchTrans' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['statUrc', 'dodPodm'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['statUrc'], it)>
                        <td><@tool.label name='statUrc' items=items />:</td><td><@tool.place object=object name='statUrc' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['dodPodm'], it)>
                        <td><@tool.label name='dodPodm' items=items />:</td><td><@tool.place object=object name='dodPodm' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['statPuvod', 'druhDopr'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['statPuvod'], it)>
                        <td><@tool.label name='statPuvod' items=items />:</td><td><@tool.place object=object name='statPuvod' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['druhDopr'], it)>
                        <td><@tool.label name='druhDopr' items=items />:</td><td><@tool.place object=object name='druhDopr' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['krajUrc', 'zvlPoh'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['krajUrc'], it)>
                        <td><@tool.label name='krajUrc' items=items />:</td><td><@tool.place object=object name='krajUrc' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['zvlPoh'], it)>
                        <td><@tool.label name='zvlPoh' items=items />:</td><td><@tool.place object=object name='zvlPoh' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['zodpOsoba', 'kontaktOsoba', 'kontaktJmeno', 'kontaktEmail', 'kontaktTel'], it)>
            <div id="view-fragment-12" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['zodpOsoba', 'kontaktOsoba', 'kontaktJmeno', 'kontaktEmail', 'kontaktTel'], it)>
                <table class="flexibee-tbl-1">
                    <col width="35%" /><col width="65%" />
                    <#if canShowAtLeastOne(items, object, ['zodpOsoba'], it)>
                    <tr>
                        <td><@tool.label name='zodpOsoba' items=items />:</td><td><@tool.place object=object name='zodpOsoba' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['kontaktOsoba'], it)>
                    <tr>
                        <td><@tool.label name='kontaktOsoba' items=items />:</td><td><@tool.place object=object name='kontaktOsoba' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['kontaktJmeno'], it)>
                    <tr>
                        <td><@tool.label name='kontaktJmeno' items=items />:</td><td><@tool.place object=object name='kontaktJmeno' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['kontaktEmail'], it)>
                    <tr>
                        <td><@tool.label name='kontaktEmail' items=items />:</td><td><@tool.placeEmail object=object name='kontaktEmail' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['kontaktTel'], it)>
                    <tr>
                        <td><@tool.label name='kontaktTel' items=items />:</td><td><@tool.placeTel object=object name='kontaktTel' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

    </div></div>

    <#if isMobile>
        <br>
    <#else>
        </td><td>
    </#if>

    <#if canShowAtLeastOne(items, object, ['slevaDokl', 'sumCelkem', 'sumZklCelkem', 'sumOsv', 'sumOsv', 'sumZklSniz2', 'sumDphSniz2', 'sumCelkSniz2', 'sumZklSniz', 'sumDphSniz', 'sumCelkSniz', 'sumZklZakl', 'sumDphZakl', 'sumCelkZakl', 'sumZklCelkem', 'sumDphCelkem', 'sumCelkem', 'sumOsv', 'sumCelkemMen', 'sumZklCelkemMen', 'kurz', 'kurzMnozstvi', 'sumOsvMen', 'sumOsvMen', 'sumZklSniz2Men', 'sumDphSniz2Men', 'sumCelkSniz2Men', 'sumZklSnizMen', 'sumDphSnizMen', 'sumCelkSnizMen', 'sumZklZaklMen', 'sumDphZaklMen', 'sumCelkZaklMen', 'sumZklCelkemMen', 'sumDphCelkemMen', 'sumCelkemMen', 'sumOsvMen', 'eetProvozovna', 'eetPokladniZarizeni', 'eetDicPoverujiciho', 'eetTypK', 'eetStavK', 'eetFik'], it)>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canShowAtLeastOne(items, object, ['slevaDokl', 'sumCelkem', 'sumZklCelkem', 'sumOsv', 'sumOsv', 'sumZklSniz2', 'sumDphSniz2', 'sumCelkSniz2', 'sumZklSniz', 'sumDphSniz', 'sumCelkSniz', 'sumZklZakl', 'sumDphZakl', 'sumCelkZakl', 'sumZklCelkem', 'sumDphCelkem', 'sumCelkem', 'sumOsv'], it)>
            <li class="active"><a href="#view-fragment-15" data-toggle="tab"><@tool.menaSymbol settings(object.datVyst).mena/></a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['sumCelkemMen', 'sumZklCelkemMen', 'kurz', 'kurzMnozstvi', 'sumOsvMen', 'sumOsvMen', 'sumZklSniz2Men', 'sumDphSniz2Men', 'sumCelkSniz2Men', 'sumZklSnizMen', 'sumDphSnizMen', 'sumCelkSnizMen', 'sumZklZaklMen', 'sumDphZaklMen', 'sumCelkZaklMen', 'sumZklCelkemMen', 'sumDphCelkemMen', 'sumCelkemMen', 'sumOsvMen'], it)>
            <li><a href="#view-fragment-18" data-toggle="tab">${lang('labels', 'dokladEBMenaPN')} <@tool.menaSymbol object['mena']/></a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['eetProvozovna', 'eetPokladniZarizeni', 'eetDicPoverujiciho', 'eetTypK', 'eetStavK', 'eetFik'], it)>
            <li><a href="#view-fragment-19" data-toggle="tab">${lang('labels', 'dokladEBEetPN')}</a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canShowAtLeastOne(items, object, ['slevaDokl', 'sumCelkem', 'sumZklCelkem', 'sumOsv', 'sumOsv', 'sumZklSniz2', 'sumDphSniz2', 'sumCelkSniz2', 'sumZklSniz', 'sumDphSniz', 'sumCelkSniz', 'sumZklZakl', 'sumDphZakl', 'sumCelkZakl', 'sumZklCelkem', 'sumDphCelkem', 'sumCelkem', 'sumOsv'], it)>
            <div id="view-fragment-15" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['slevaDokl', 'sumCelkem', 'sumZklCelkem'], it)>
                <table class="flexibee-tbl-1">
                    <col width="30%" /><col width="30%" /><col width="30%" /><col width="10%" />
                    <#if canShowAtLeastOne(items, object, ['slevaDokl'], it)>
                    <tr>
                        <td><@tool.label name='slevaDokl' items=items />:</td><td><@tool.place object=object name='slevaDokl' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['sumCelkem'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['sumCelkem'], it)>
                        <td><strong><#if settings(object.datVyst).platceDph>Celkem s DPH<#else>Celkem</#if></strong>:</td><td><@tool.place object=object level='0' name='sumCelkem' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <td><@tool.menaSymbol settings(object.datVyst).mena/></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['sumZklCelkem'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['sumZklCelkem'], it)>
                        <td><#if settings(object.datVyst).platceDph>Základ bez DPH<#else>Základ</#if>:</td><td><@tool.place object=object level='3' name='sumZklCelkem' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <td><@tool.menaSymbol settings(object.datVyst).mena/></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

                <#if isMobile == false>

                <div class="flexibee-more"><div class="flexibee-more-for-button"></div><div class="flexibee-hidden hidden">

                <div class="flexibee-doklad-sumy-platce-dph">

                <#if canShowAtLeastOne(items, object, ['sumOsv', 'sumOsv', 'sumZklSniz2', 'sumDphSniz2', 'sumCelkSniz2', 'sumZklSniz', 'sumDphSniz', 'sumCelkSniz', 'sumZklZakl', 'sumDphZakl', 'sumCelkZakl', 'sumZklCelkem', 'sumDphCelkem', 'sumCelkem'], it)>
                <div id="view-fragment-13" class="tab-pane">
                    <div class="flexibee-table-border-tbl">
                    <#if canShowAtLeastOne(items, object, ['sumOsv', 'sumOsv', 'sumZklSniz2', 'sumDphSniz2', 'sumCelkSniz2', 'sumZklSniz', 'sumDphSniz', 'sumCelkSniz', 'sumZklZakl', 'sumDphZakl', 'sumCelkZakl', 'sumZklCelkem', 'sumDphCelkem', 'sumCelkem'], it)>
                    <table class="flexibee-tbl-1">
                        <col width="25%" /><col width="25%" /><col width="25%" /><col width="25%" />
                        <tr class="flexibee-sep0">
                            <td></td>
                            <#if isMobile></tr><tr></#if>
                            <td>Základ</td>
                            <#if isMobile></tr><tr></#if>
                            <td>DPH</td>
                            <#if isMobile></tr><tr></#if>
                            <td>Celkem</td>
                        </tr>

                        <#if canShowAtLeastOne(items, object, ['sumOsv', 'sumOsv'], it)>
                        <tr class="flexibee-sep">
                            <td><span id="flexibee-lbl-sumOsv"> ${object.sazbaDphOsv}  </span></td>
                            <#if isMobile></tr><tr></#if>
                            <#if canShowAtLeastOne(items, object, ['sumOsv'], it)>
                            <td><@tool.place object=object name='sumOsv' items=items marker=marker /></td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <td></td>
                            <#if isMobile></tr><tr></#if>
                            <#if canShowAtLeastOne(items, object, ['sumOsv'], it)>
                            <td><@tool.place object=object name='sumOsv' items=items marker=marker /></td>
                            </#if>
                        </tr>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['sumZklSniz2', 'sumDphSniz2', 'sumCelkSniz2'], it)>
                        <tr class="flexibee-sep">
                            <td><span id="flexibee-lbl-sumZklSniz2"> ${object.sazbaDphSniz2}  </span></td>
                            <#if isMobile></tr><tr></#if>
                            <#if canShowAtLeastOne(items, object, ['sumZklSniz2'], it)>
                            <td><@tool.place object=object name='sumZklSniz2' items=items marker=marker /></td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <#if canShowAtLeastOne(items, object, ['sumDphSniz2'], it)>
                            <td><@tool.place object=object name='sumDphSniz2' items=items marker=marker /></td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <#if canShowAtLeastOne(items, object, ['sumCelkSniz2'], it)>
                            <td><@tool.place object=object name='sumCelkSniz2' items=items marker=marker /></td>
                            </#if>
                        </tr>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['sumZklSniz', 'sumDphSniz', 'sumCelkSniz'], it)>
                        <tr class="flexibee-sep">
                            <td><span id="flexibee-lbl-sumZklSniz"> ${object.sazbaDphSniz}  </span></td>
                            <#if isMobile></tr><tr></#if>
                            <#if canShowAtLeastOne(items, object, ['sumZklSniz'], it)>
                            <td><@tool.place object=object name='sumZklSniz' items=items marker=marker /></td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <#if canShowAtLeastOne(items, object, ['sumDphSniz'], it)>
                            <td><@tool.place object=object name='sumDphSniz' items=items marker=marker /></td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <#if canShowAtLeastOne(items, object, ['sumCelkSniz'], it)>
                            <td><@tool.place object=object name='sumCelkSniz' items=items marker=marker /></td>
                            </#if>
                        </tr>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['sumZklZakl', 'sumDphZakl', 'sumCelkZakl'], it)>
                        <tr class="flexibee-sep">
                            <td><span id="flexibee-lbl-sumZklZakl"> ${object.sazbaDphZakl}  </span></td>
                            <#if isMobile></tr><tr></#if>
                            <#if canShowAtLeastOne(items, object, ['sumZklZakl'], it)>
                            <td><@tool.place object=object name='sumZklZakl' items=items marker=marker /></td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <#if canShowAtLeastOne(items, object, ['sumDphZakl'], it)>
                            <td><@tool.place object=object name='sumDphZakl' items=items marker=marker /></td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <#if canShowAtLeastOne(items, object, ['sumCelkZakl'], it)>
                            <td><@tool.place object=object name='sumCelkZakl' items=items marker=marker /></td>
                            </#if>
                        </tr>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['sumZklCelkem', 'sumDphCelkem', 'sumCelkem'], it)>
                        <tr class="flexibee-sep2">
                            <td>Suma</td>
                            <#if isMobile></tr><tr></#if>
                            <#if canShowAtLeastOne(items, object, ['sumZklCelkem'], it)>
                            <td><@tool.place object=object name='sumZklCelkem' items=items marker=marker /></td>
                            <#else>
                            <td>&nbsp;</td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <#if canShowAtLeastOne(items, object, ['sumDphCelkem'], it)>
                            <td><@tool.place object=object name='sumDphCelkem' items=items marker=marker /></td>
                            <#else>
                            <td>&nbsp;</td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <#if canShowAtLeastOne(items, object, ['sumCelkem'], it)>
                            <td><@tool.place object=object name='sumCelkem' items=items marker=marker /></td>
                            <#else>
                            <td>&nbsp;</td>
                            </#if>
                        </tr>
                        </#if>
                    </table>
                    </#if>
                    </div>

                </div>
                </#if>

                </div><div class="flexibee-doklad-sumy-neplatce-dph" style="display: none">

                <#if canShowAtLeastOne(items, object, ['sumOsv'], it)>
                <div id="view-fragment-14" class="tab-pane">
                    <div class="flexibee-table-border">
                    <#if canShowAtLeastOne(items, object, ['sumOsv'], it)>
                    <table class="flexibee-tbl-1">
                        <#if canShowAtLeastOne(items, object, ['sumOsv'], it)>
                        <tr>
                            <td><span id="flexibee-lbl-sumOsv"> ${object.sazbaDphOsv}  </span></td>
                            <#if isMobile></tr><tr></#if>
                            <#if canShowAtLeastOne(items, object, ['sumOsv'], it)>
                            <td><@tool.place object=object name='sumOsv' items=items marker=marker /></td>
                            </#if>
                        </tr>
                        </#if>
                    </table>
                    </#if>
                    </div>

                </div>
                </#if>

                </div>

                </div></div>

                </#if>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['sumCelkemMen', 'sumZklCelkemMen', 'kurz', 'kurzMnozstvi', 'sumOsvMen', 'sumOsvMen', 'sumZklSniz2Men', 'sumDphSniz2Men', 'sumCelkSniz2Men', 'sumZklSnizMen', 'sumDphSnizMen', 'sumCelkSnizMen', 'sumZklZaklMen', 'sumDphZaklMen', 'sumCelkZaklMen', 'sumZklCelkemMen', 'sumDphCelkemMen', 'sumCelkemMen', 'sumOsvMen'], it)>
            <div id="view-fragment-18" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['sumCelkemMen', 'sumZklCelkemMen', 'kurz', 'kurzMnozstvi'], it)>
                <table class="flexibee-tbl-1">
                    <col width="30%" /><col width="30%" /><col width="30%" /><col width="10%" />
                    <#if canShowAtLeastOne(items, object, ['sumCelkemMen'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['sumCelkemMen'], it)>
                        <td><strong><#if settings(object.datVyst).platceDph>Celkem s DPH<#else>Celkem</#if></strong>:</td><td><@tool.place object=object level='0' name='sumCelkemMen' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <td><@tool.menaSymbol object['mena']/></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['sumZklCelkemMen'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['sumZklCelkemMen'], it)>
                        <td><#if settings(object.datVyst).platceDph>${lang('message', 'sumDoklZklCelk')}<#else>Základ</#if>:</td><td><@tool.place object=object level='3' name='sumZklCelkemMen' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <td><@tool.menaSymbol object['mena']/></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['kurz', 'kurzMnozstvi'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['kurz'], it)>
                        <td><@tool.label name='kurz' items=items level='3'/>:</td><td><@tool.place object=object level='3' name='kurz' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['kurzMnozstvi'], it)>
                        <td><@tool.label name='kurzMnozstvi' items=items level='3'/>:</td><td><@tool.place object=object level='3' name='kurzMnozstvi' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

                <#if isMobile == false>

                <div class="flexibee-more"><div class="flexibee-more-for-button"></div><div class="flexibee-hidden hidden">

                <div class="flexibee-doklad-sumy-men-platce-dph">

                <#if canShowAtLeastOne(items, object, ['sumOsvMen', 'sumOsvMen', 'sumZklSniz2Men', 'sumDphSniz2Men', 'sumCelkSniz2Men', 'sumZklSnizMen', 'sumDphSnizMen', 'sumCelkSnizMen', 'sumZklZaklMen', 'sumDphZaklMen', 'sumCelkZaklMen', 'sumZklCelkemMen', 'sumDphCelkemMen', 'sumCelkemMen'], it)>
                <div id="view-fragment-16" class="tab-pane">
                    <div class="flexibee-table-border-tbl">
                    <#if canShowAtLeastOne(items, object, ['sumOsvMen', 'sumOsvMen', 'sumZklSniz2Men', 'sumDphSniz2Men', 'sumCelkSniz2Men', 'sumZklSnizMen', 'sumDphSnizMen', 'sumCelkSnizMen', 'sumZklZaklMen', 'sumDphZaklMen', 'sumCelkZaklMen', 'sumZklCelkemMen', 'sumDphCelkemMen', 'sumCelkemMen'], it)>
                    <table class="flexibee-tbl-1">
                        <col width="25%" /><col width="25%" /><col width="25%" /><col width="25%" />
                        <tr class="flexibee-sep0">
                            <td></td>
                            <#if isMobile></tr><tr></#if>
                            <td>Základ</td>
                            <#if isMobile></tr><tr></#if>
                            <td>DPH</td>
                            <#if isMobile></tr><tr></#if>
                            <td>Celkem</td>
                        </tr>

                        <#if canShowAtLeastOne(items, object, ['sumOsvMen', 'sumOsvMen'], it)>
                        <tr class="flexibee-sep">
                            <td><span id="flexibee-lbl-sumOsvMen"> ${object.sazbaDphOsv}  </span></td>
                            <#if isMobile></tr><tr></#if>
                            <#if canShowAtLeastOne(items, object, ['sumOsvMen'], it)>
                            <td><@tool.place object=object name='sumOsvMen' items=items marker=marker /></td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <td></td>
                            <#if isMobile></tr><tr></#if>
                            <#if canShowAtLeastOne(items, object, ['sumOsvMen'], it)>
                            <td><@tool.place object=object name='sumOsvMen' items=items marker=marker /></td>
                            </#if>
                        </tr>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['sumZklSniz2Men', 'sumDphSniz2Men', 'sumCelkSniz2Men'], it)>
                        <tr class="flexibee-sep">
                            <td><span id="flexibee-lbl-sumZklSniz2Men"> ${object.sazbaDphSniz2}  </span></td>
                            <#if isMobile></tr><tr></#if>
                            <#if canShowAtLeastOne(items, object, ['sumZklSniz2Men'], it)>
                            <td><@tool.place object=object name='sumZklSniz2Men' items=items marker=marker /></td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <#if canShowAtLeastOne(items, object, ['sumDphSniz2Men'], it)>
                            <td><@tool.place object=object name='sumDphSniz2Men' items=items marker=marker /></td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <#if canShowAtLeastOne(items, object, ['sumCelkSniz2Men'], it)>
                            <td><@tool.place object=object name='sumCelkSniz2Men' items=items marker=marker /></td>
                            </#if>
                        </tr>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['sumZklSnizMen', 'sumDphSnizMen', 'sumCelkSnizMen'], it)>
                        <tr class="flexibee-sep">
                            <td><span id="flexibee-lbl-sumZklSnizMen"> ${object.sazbaDphSniz}  </span></td>
                            <#if isMobile></tr><tr></#if>
                            <#if canShowAtLeastOne(items, object, ['sumZklSnizMen'], it)>
                            <td><@tool.place object=object name='sumZklSnizMen' items=items marker=marker /></td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <#if canShowAtLeastOne(items, object, ['sumDphSnizMen'], it)>
                            <td><@tool.place object=object name='sumDphSnizMen' items=items marker=marker /></td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <#if canShowAtLeastOne(items, object, ['sumCelkSnizMen'], it)>
                            <td><@tool.place object=object name='sumCelkSnizMen' items=items marker=marker /></td>
                            </#if>
                        </tr>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['sumZklZaklMen', 'sumDphZaklMen', 'sumCelkZaklMen'], it)>
                        <tr class="flexibee-sep">
                            <td><span id="flexibee-lbl-sumZklZaklMen"> ${object.sazbaDphZakl}  </span></td>
                            <#if isMobile></tr><tr></#if>
                            <#if canShowAtLeastOne(items, object, ['sumZklZaklMen'], it)>
                            <td><@tool.place object=object name='sumZklZaklMen' items=items marker=marker /></td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <#if canShowAtLeastOne(items, object, ['sumDphZaklMen'], it)>
                            <td><@tool.place object=object name='sumDphZaklMen' items=items marker=marker /></td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <#if canShowAtLeastOne(items, object, ['sumCelkZaklMen'], it)>
                            <td><@tool.place object=object name='sumCelkZaklMen' items=items marker=marker /></td>
                            </#if>
                        </tr>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['sumZklCelkemMen', 'sumDphCelkemMen', 'sumCelkemMen'], it)>
                        <tr class="flexibee-sep2">
                            <td>Suma</td>
                            <#if isMobile></tr><tr></#if>
                            <#if canShowAtLeastOne(items, object, ['sumZklCelkemMen'], it)>
                            <td><@tool.place object=object name='sumZklCelkemMen' items=items marker=marker /></td>
                            <#else>
                            <td>&nbsp;</td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <#if canShowAtLeastOne(items, object, ['sumDphCelkemMen'], it)>
                            <td><@tool.place object=object name='sumDphCelkemMen' items=items marker=marker /></td>
                            <#else>
                            <td>&nbsp;</td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <#if canShowAtLeastOne(items, object, ['sumCelkemMen'], it)>
                            <td><@tool.place object=object name='sumCelkemMen' items=items marker=marker /></td>
                            <#else>
                            <td>&nbsp;</td>
                            </#if>
                        </tr>
                        </#if>
                    </table>
                    </#if>
                    </div>

                </div>
                </#if>

                </div><div class="flexibee-doklad-sumy-men-neplatce-dph" style="display: none">

                <#if canShowAtLeastOne(items, object, ['sumOsvMen'], it)>
                <div id="view-fragment-17" class="tab-pane">
                    <div class="flexibee-table-border">
                    <#if canShowAtLeastOne(items, object, ['sumOsvMen'], it)>
                    <table class="flexibee-tbl-1">
                        <#if canShowAtLeastOne(items, object, ['sumOsvMen'], it)>
                        <tr>
                            <td><span id="flexibee-lbl-sumOsvMen"> ${object.sazbaDphOsv}  </span></td>
                            <#if isMobile></tr><tr></#if>
                            <#if canShowAtLeastOne(items, object, ['sumOsvMen'], it)>
                            <td><@tool.place object=object name='sumOsvMen' items=items marker=marker /></td>
                            </#if>
                        </tr>
                        </#if>
                    </table>
                    </#if>
                    </div>

                </div>
                </#if>

                </div>

                </div></div>

                </#if>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['eetProvozovna', 'eetPokladniZarizeni', 'eetDicPoverujiciho', 'eetTypK', 'eetStavK', 'eetFik'], it)>
            <div id="view-fragment-19" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['eetProvozovna', 'eetPokladniZarizeni', 'eetDicPoverujiciho', 'eetTypK', 'eetStavK', 'eetFik'], it)>
                <table class="flexibee-tbl-1">
                    <col width="35%" /><col width="65%" />
                    <#if canShowAtLeastOne(items, object, ['eetProvozovna'], it)>
                    <tr>
                        <td><@tool.label name='eetProvozovna' items=items level='3'/>:</td><td><@tool.place object=object level='3' name='eetProvozovna' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['eetPokladniZarizeni'], it)>
                    <tr>
                        <td><@tool.label name='eetPokladniZarizeni' items=items level='3'/>:</td><td><@tool.place object=object level='3' name='eetPokladniZarizeni' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['eetDicPoverujiciho'], it)>
                    <tr>
                        <td><@tool.label name='eetDicPoverujiciho' items=items level='3'/>:</td><td><@tool.place object=object level='3' name='eetDicPoverujiciho' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['eetTypK'], it)>
                    <tr>
                        <td><@tool.label name='eetTypK' items=items level='3'/>:</td><td><@tool.place object=object level='3' name='eetTypK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['eetStavK'], it)>
                    <tr>
                        <td><@tool.label name='eetStavK' items=items level='3'/>:</td><td><@tool.place object=object level='3' name='eetStavK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['eetFik'], it)>
                    <tr>
                        <td><@tool.label name='eetFik' items=items level='3'/>:</td><td><@tool.place object=object level='3' name='eetFik' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

    <#if isMobile>
        </div>
    <#else>
        </td></tr></table>
    </#if>

    <#if object['bezPolozek'] == false>

    <#include "/parts/i-polozky.ftl" /><br><br>

    </#if>

<#--AUTOGEN END-->

<@tool.listItems items=items object=object marker=marker />

<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />

<@dokladView.generateJavaScript it=it object=object />


<#include "/parts/i-dalsi-informace.ftl" />


<#include "/i-footer-view.ftl" />
</#escape>