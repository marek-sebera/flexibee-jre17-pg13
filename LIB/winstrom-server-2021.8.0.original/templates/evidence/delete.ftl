<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl" />

<@edit.beginForm />
    <p>Smazat záznam?</p>

    <@edit.deleteButton />
<@edit.endForm />


<#include "/i-footer-edit.ftl" />
</#escape>