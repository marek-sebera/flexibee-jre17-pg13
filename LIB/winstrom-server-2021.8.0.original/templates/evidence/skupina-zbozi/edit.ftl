<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>

${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}
${marker.mark('typOrganizace')}

<#-- odpovídá základnímu ceníku -->
${marker.mark('typCenyDphK')}
${marker.mark('zaokrJakK')}
${marker.mark('zaokrNaK')}
${marker.mark('typCenyVychoziK')}
${marker.mark('typVypCenyK')}
${marker.mark('procZakl')}
${marker.mark('typCenyVychozi25K')}
${marker.mark('typVypCeny25K')}
${marker.mark('limMnoz2')}
${marker.mark('procento2')}
${marker.mark('limMnoz3')}
${marker.mark('procento3')}
${marker.mark('limMnoz4')}
${marker.mark('procento4')}
${marker.mark('limMnoz5')}
${marker.mark('procento5')}

<@edit.beginForm />
    <#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canEditAtLeastOne(items, object, ['nazev', 'kod'])>
    <table class="flexibee-tbl-1">
        <col width="20%" /><col width="80%" />
        <#if canEditAtLeastOne(items, object, ['nazev'])>
        <tr>
            <td><@edit.label name='nazev' items=items /></td><td><@edit.place object=object name='nazev' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['kod'])>
        <tr>
            <td><@edit.label name='kod' items=items level='3' /></td><td><@edit.place object=object name='kod' items=items marker=marker  level='3'/></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canEditAtLeastOne(items, object, ['popis', 'poznam', 'hlidatMinMarzi', 'minMarze', 'ucetProtiVfa', 'ucetProtiPfa', 'ucetProtiSklp', 'ucetProtiSklv', 'platiOd', 'platiDo'])>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <li class="active"><a href="#edit-fragment-1" data-toggle="tab"><span>${lang('labels', 'textyPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['hlidatMinMarzi', 'minMarze'])>
            <li><a href="#edit-fragment-2" data-toggle="tab"><span>Základní cenová úroveň</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['ucetProtiVfa', 'ucetProtiPfa', 'ucetProtiSklp', 'ucetProtiSklv'])>
            <li><a href="#edit-fragment-3" data-toggle="tab"><span>Účtování skupiny zboží</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
            <li><a href="#edit-fragment-4" data-toggle="tab"><span>${lang('labels', 'spravaPN')}</span></a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <div id="edit-fragment-1" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
                <table class="flexibee-tbl-1">
                    <#if canEditAtLeastOne(items, object, ['popis'])>
                    <tr>
                        <td><@edit.label name='popis' items=items /><br><@edit.textarea object=object name='popis' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['poznam'])>
                    <tr>
                        <td><@edit.label name='poznam' items=items /><br><@edit.textarea object=object name='poznam' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['hlidatMinMarzi', 'minMarze'])>
            <div id="edit-fragment-2" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['hlidatMinMarzi', 'minMarze'])>
                <table class="flexibee-tbl-1">
                    <#if canEditAtLeastOne(items, object, ['hlidatMinMarzi', 'minMarze'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['hlidatMinMarzi'])>
                        <td><@edit.label name='hlidatMinMarzi' items=items /></td><td><@edit.place object=object name='hlidatMinMarzi' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['minMarze'])>
                        <td><@edit.label name='minMarze' items=items /></td><td><@edit.place object=object name='minMarze' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['ucetProtiVfa', 'ucetProtiPfa', 'ucetProtiSklp', 'ucetProtiSklv'])>
            <div id="edit-fragment-3" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['ucetProtiVfa', 'ucetProtiPfa', 'ucetProtiSklp', 'ucetProtiSklv'])>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <#if canEditAtLeastOne(items, object, ['ucetProtiVfa'])>
                    <tr>
                        <td><label for="flexibee-inp-ucetProtiVfa">Pro vydané faktury / pokladna - příjem:</label></td><td><@edit.place object=object name='ucetProtiVfa' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['ucetProtiPfa'])>
                    <tr>
                        <td><label for="flexibee-inp-ucetProtiPfa">Pro přijaté faktury / pokladna - výdej:</label></td><td><@edit.place object=object name='ucetProtiPfa' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['ucetProtiSklp'])>
                    <tr>
                        <td><label for="flexibee-inp-ucetProtiSklp">Pro příjem na sklad:</label></td><td><@edit.place object=object name='ucetProtiSklp' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['ucetProtiSklv'])>
                    <tr>
                        <td><label for="flexibee-inp-ucetProtiSklv">Pro výdej ze skladu:</label></td><td><@edit.place object=object name='ucetProtiSklv' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
            <div id="edit-fragment-4" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <tr>
                        <td colspan="2">Platnost se udává v kalendářních rocích, pro neomezenou platnost zadejte 0-9999</td>
                    </tr>

                    <#if canEditAtLeastOne(items, object, ['platiOd'])>
                    <tr>
                        <td><@edit.label name='platiOd' items=items /></td><td><@edit.place object=object name='platiOd' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['platiDo'])>
                    <tr>
                        <td><@edit.label name='platiDo' items=items /></td><td><@edit.place object=object name='platiDo' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

    <br/>
    <@edit.listItems items=items object=object marker=marker />

    <@edit.saveButton />
<@edit.endForm />

<@edit.generateNaiveJavascript object=object it=it />

<#include "/parts/i-dalsi-informace.ftl" />

<#include "/i-footer-edit.ftl" />
</#escape>
