<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>

${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}
${marker.mark('stitky')}
${marker.mark('modul')}
${marker.mark('platiOd')}
${marker.mark('platiDo')}
${marker.mark('prodejka')}

<@edit.beginForm />
    <#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canEditAtLeastOne(items, object, ['kod', 'radaPrijem', 'radaVydej', 'nazev', 'typPohybuK'])>
    <table class="flexibee-tbl-1">
        <col width="15%" /><col width="18%" /><col width="15%" /><col width="18%" /><col width="15%" /><col width="18%" />
        <#if canEditAtLeastOne(items, object, ['kod', 'radaPrijem', 'radaVydej'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['kod'])>
            <td><@edit.label name='kod' items=items /></td><td><@edit.place object=object name='kod' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['radaPrijem'])>
            <td><@edit.label name='radaPrijem' items=items /></td><td><@edit.place object=object name='radaPrijem' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['radaVydej'])>
            <td><@edit.label name='radaVydej' items=items /></td><td><@edit.place object=object name='radaVydej' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['nazev'])>
        <tr>
            <td><@edit.label name='nazev' items=items /></td><td colspan="5"><@edit.place object=object name='nazev' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['typPohybuK'])>
        <tr>
            <td><@edit.label name='typPohybuK' items=items /></td><td><@edit.place object=object name='typPohybuK' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canEditAtLeastOne(items, object, ['ucetni', 'stredisko', 'cinnost', 'primUcet', 'mena', 'statDph', 'clenKonVykDph', 'typUcOpPrijem', 'typUcOpVydej', 'eetTypK', 'eetPokladniZarizeni', 'eetDicPoverujiciho', 'eetProvozovna', 'eetLimit', 'zaokrJakDphK', 'zaokrNaDphK', 'zaokrJakSumK', 'zaokrNaSumK', 'tiskAutomat', 'statOdesl', 'obchTrans', 'statUrc', 'dodPodm', 'statPuvod', 'druhDopr', 'krajUrc', 'zvlPoh', 'popisDoklad', 'uvodTxt', 'zavTxt', 'emailTxt', 'popis', 'poznam', 'ucetObdobiOd', 'ucetObdobiDo', 'razeniProTiskK', 'primarni'])>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canEditAtLeastOne(items, object, ['ucetni', 'stredisko', 'cinnost', 'primUcet', 'mena', 'statDph', 'clenKonVykDph', 'typUcOpPrijem', 'typUcOpVydej', 'eetTypK', 'eetPokladniZarizeni', 'eetDicPoverujiciho', 'eetProvozovna', 'eetLimit'])>
            <li class="active"><a href="#edit-fragment-2" data-toggle="tab"><span>Účtování</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['zaokrJakDphK', 'zaokrNaDphK', 'zaokrJakSumK', 'zaokrNaSumK'])>
            <li><a href="#edit-fragment-3" data-toggle="tab"><span>Zaokrouhlování</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['tiskAutomat'])>
            <li><a href="#edit-fragment-4" data-toggle="tab"><span>Tisk</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['statOdesl', 'obchTrans', 'statUrc', 'dodPodm', 'statPuvod', 'druhDopr', 'krajUrc', 'zvlPoh'])>
            <li><a href="#edit-fragment-5" data-toggle="tab"><span>Intrastat</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['popisDoklad', 'uvodTxt', 'zavTxt', 'emailTxt'])>
            <li><a href="#edit-fragment-6" data-toggle="tab"><span>Texty na doklad</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <li><a href="#edit-fragment-7" data-toggle="tab"><span>Texty</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['ucetObdobiOd', 'ucetObdobiDo', 'razeniProTiskK', 'primarni'])>
            <li><a href="#edit-fragment-8" data-toggle="tab"><span>Správa</span></a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canEditAtLeastOne(items, object, ['ucetni', 'stredisko', 'cinnost', 'primUcet', 'mena', 'statDph', 'clenKonVykDph', 'typUcOpPrijem', 'typUcOpVydej', 'eetTypK', 'eetPokladniZarizeni', 'eetDicPoverujiciho', 'eetProvozovna', 'eetLimit'])>
            <div id="edit-fragment-2" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['ucetni', 'stredisko', 'cinnost', 'primUcet', 'mena', 'statDph', 'clenKonVykDph', 'typUcOpPrijem', 'typUcOpVydej'])>
                <table class="flexibee-tbl-1">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canEditAtLeastOne(items, object, ['ucetni', 'stredisko'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['ucetni'])>
                        <td><@edit.label name='ucetni' items=items /></td><td><@edit.place object=object name='ucetni' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['stredisko'])>
                        <td><@edit.label name='stredisko' items=items /></td><td><@edit.place object=object name='stredisko' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['cinnost'])>
                    <tr>
                        <td><@edit.label name='cinnost' items=items /></td><td><@edit.place object=object name='cinnost' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['primUcet', 'mena'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['primUcet'])>
                        <td><@edit.label name='primUcet' items=items /></td><td><@edit.place object=object name='primUcet' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['mena'])>
                        <td><@edit.label name='mena' items=items /></td><td><@edit.place object=object name='mena' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['statDph'])>
                    <tr>
                        <td><@edit.label name='statDph' items=items /></td><td><@edit.place object=object name='statDph' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['clenKonVykDph'])>
                    <tr>
                        <td><@edit.label name='clenKonVykDph' items=items /></td><td><@edit.place object=object name='clenKonVykDph' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <tr>
                        <td colspan="2">Předpis zaúčtování:</td>
                    </tr>

                    <#if canEditAtLeastOne(items, object, ['typUcOpPrijem'])>
                    <tr>
                        <td><@edit.label name='typUcOpPrijem' items=items /></td><td><@edit.place object=object name='typUcOpPrijem' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['typUcOpVydej'])>
                    <tr>
                        <td><@edit.label name='typUcOpVydej' items=items /></td><td><@edit.place object=object name='typUcOpVydej' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

                <#if canEditAtLeastOne(items, object, ['eetTypK', 'eetPokladniZarizeni', 'eetDicPoverujiciho', 'eetProvozovna', 'eetLimit'])>
                <h4>${lang('labels', 'dokladEBEetPN')}</h4>
                <div id="edit-fragment-1" class="tab-pane">
                    <div class="flexibee-table-border">
                    <#if canEditAtLeastOne(items, object, ['eetTypK', 'eetPokladniZarizeni', 'eetDicPoverujiciho', 'eetProvozovna', 'eetLimit'])>
                    <table class="flexibee-tbl-1">
                        <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                        <#if canEditAtLeastOne(items, object, ['eetTypK'])>
                        <tr>
                            <td><@edit.label name='eetTypK' items=items /></td><td><@edit.place object=object name='eetTypK' items=items marker=marker /></td>
                        </tr>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['eetPokladniZarizeni', 'eetDicPoverujiciho'])>
                        <tr>
                            <#if canEditAtLeastOne(items, object, ['eetPokladniZarizeni'])>
                            <td><@edit.label name='eetPokladniZarizeni' items=items /></td><td><@edit.place object=object name='eetPokladniZarizeni' items=items marker=marker /></td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['eetDicPoverujiciho'])>
                            <td><@edit.label name='eetDicPoverujiciho' items=items /></td><td><@edit.place object=object name='eetDicPoverujiciho' items=items marker=marker /></td>
                            </#if>
                        </tr>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['eetProvozovna', 'eetLimit'])>
                        <tr>
                            <#if canEditAtLeastOne(items, object, ['eetProvozovna'])>
                            <td><@edit.label name='eetProvozovna' items=items /></td><td><@edit.place object=object name='eetProvozovna' items=items marker=marker /></td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['eetLimit'])>
                            <td><@edit.label name='eetLimit' items=items /></td><td><@edit.place object=object name='eetLimit' items=items marker=marker /></td>
                            </#if>
                        </tr>
                        </#if>
                    </table>
                    </#if>
                    </div>

                </div>
                </#if>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['zaokrJakDphK', 'zaokrNaDphK', 'zaokrJakSumK', 'zaokrNaSumK'])>
            <div id="edit-fragment-3" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['zaokrJakDphK', 'zaokrNaDphK', 'zaokrJakSumK', 'zaokrNaSumK'])>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <#if canEditAtLeastOne(items, object, ['zaokrJakDphK'])>
                    <tr>
                        <td><@edit.label name='zaokrJakDphK' items=items /></td><td><@edit.place object=object name='zaokrJakDphK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['zaokrNaDphK'])>
                    <tr>
                        <td><@edit.label name='zaokrNaDphK' items=items /></td><td><@edit.place object=object name='zaokrNaDphK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['zaokrJakSumK'])>
                    <tr>
                        <td><@edit.label name='zaokrJakSumK' items=items /></td><td><@edit.place object=object name='zaokrJakSumK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['zaokrNaSumK'])>
                    <tr>
                        <td><@edit.label name='zaokrNaSumK' items=items /></td><td><@edit.place object=object name='zaokrNaSumK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['tiskAutomat'])>
            <div id="edit-fragment-4" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['tiskAutomat'])>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <#if canEditAtLeastOne(items, object, ['tiskAutomat'])>
                    <tr>
                        <td><@edit.label name='tiskAutomat' items=items /></td><td><@edit.place object=object name='tiskAutomat' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['statOdesl', 'obchTrans', 'statUrc', 'dodPodm', 'statPuvod', 'druhDopr', 'krajUrc', 'zvlPoh'])>
            <div id="edit-fragment-5" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['statOdesl', 'obchTrans', 'statUrc', 'dodPodm', 'statPuvod', 'druhDopr', 'krajUrc', 'zvlPoh'])>
                <table class="flexibee-tbl-1">
                    <#if canEditAtLeastOne(items, object, ['statOdesl', 'obchTrans'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['statOdesl'])>
                        <td><@edit.label name='statOdesl' items=items /></td><td><@edit.place object=object name='statOdesl' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['obchTrans'])>
                        <td><@edit.label name='obchTrans' items=items /></td><td><@edit.place object=object name='obchTrans' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['statUrc', 'dodPodm'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['statUrc'])>
                        <td><@edit.label name='statUrc' items=items /></td><td><@edit.place object=object name='statUrc' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['dodPodm'])>
                        <td><@edit.label name='dodPodm' items=items /></td><td><@edit.place object=object name='dodPodm' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['statPuvod', 'druhDopr'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['statPuvod'])>
                        <td><@edit.label name='statPuvod' items=items /></td><td><@edit.place object=object name='statPuvod' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['druhDopr'])>
                        <td><@edit.label name='druhDopr' items=items /></td><td><@edit.place object=object name='druhDopr' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['krajUrc', 'zvlPoh'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['krajUrc'])>
                        <td><@edit.label name='krajUrc' items=items /></td><td><@edit.place object=object name='krajUrc' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['zvlPoh'])>
                        <td><@edit.label name='zvlPoh' items=items /></td><td><@edit.place object=object name='zvlPoh' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['popisDoklad', 'uvodTxt', 'zavTxt', 'emailTxt'])>
            <div id="edit-fragment-6" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['popisDoklad', 'uvodTxt', 'zavTxt', 'emailTxt'])>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <#if canEditAtLeastOne(items, object, ['popisDoklad'])>
                    <tr>
                        <td><@edit.label name='popisDoklad' items=items /></td><td><@edit.place object=object name='popisDoklad' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['uvodTxt'])>
                    <tr>
                        <td colspan="4"><@edit.label name='uvodTxt' items=items /><br><@edit.textarea object=object name='uvodTxt' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['zavTxt'])>
                    <tr>
                        <td colspan="4"><@edit.label name='zavTxt' items=items /><br><@edit.textarea object=object name='zavTxt' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['emailTxt'])>
                    <tr>
                        <td colspan="4"><@edit.label name='emailTxt' items=items /><br><@edit.textarea object=object name='emailTxt' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <div id="edit-fragment-7" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
                <table class="flexibee-tbl-1">
                    <#if canEditAtLeastOne(items, object, ['popis'])>
                    <tr>
                        <td><@edit.label name='popis' items=items /><br><@edit.textarea object=object name='popis' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['poznam'])>
                    <tr>
                        <td><@edit.label name='poznam' items=items /><br><@edit.textarea object=object name='poznam' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['ucetObdobiOd', 'ucetObdobiDo', 'razeniProTiskK', 'primarni'])>
            <div id="edit-fragment-8" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['ucetObdobiOd', 'ucetObdobiDo', 'razeniProTiskK', 'primarni'])>
                <table class="flexibee-tbl-1">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <tr>
                        <td colspan="4">Platnost se udává v účetních obdobích, pro neomezenou platnost nechte pole nevyplněné</td>
                    </tr>

                    <#if canEditAtLeastOne(items, object, ['ucetObdobiOd', 'ucetObdobiDo'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['ucetObdobiOd'])>
                        <td><@edit.label name='ucetObdobiOd' items=items /></td><td><@edit.place object=object name='ucetObdobiOd' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['ucetObdobiDo'])>
                        <td><@edit.label name='ucetObdobiDo' items=items /></td><td><@edit.place object=object name='ucetObdobiDo' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['razeniProTiskK'])>
                    <tr>
                        <td><@edit.label name='razeniProTiskK' items=items /></td><td colspan="3"><@edit.place object=object name='razeniProTiskK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['primarni'])>
                    <tr>
                        <td><@edit.label name='primarni' items=items /></td><td colspan="3"><@edit.place object=object name='primarni' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

    <br/>
    <@edit.listItems items=items object=object marker=marker />

    <@edit.saveButton />
<@edit.endForm />

<@edit.generateNaiveJavascript object=object it=it />

<#include "/parts/i-dalsi-informace.ftl" />

<#include "/i-footer-edit.ftl" />
</#escape>
