<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.${it.senchaName}.BaseList', {
    extend: 'Ext.container.Container',

    requires: [
        'FlexiBee.main.BackwardWindow', // TODO why?
        'FlexiBee.${it.senchaName}.Toolbar',
        'FlexiBee.${it.senchaName}.QuickFilter',
        'FlexiBee.${it.senchaName}.Grid'
    ],

    listUrl: null,
    showToolbar: true,
    showQuickFilter: true,

    layout: {
        type: 'border'
    },

    initComponent: function() {
        var me = this;

        var centerContainerItems = [];

        if (me.showToolbar) {
            centerContainerItems.push({
                xtype: 'fbToolbar${it.senchaName?cap_first}',
                isList: true
            });
        }

        if (me.showQuickFilter) {
            centerContainerItems.push({
                xtype: 'fbQuickFilter${it.senchaName?cap_first}'
            });
        }

        centerContainerItems.push({
            xtype: 'fbGrid${it.senchaName?cap_first}',
            listUrl: me.listUrl,
            flex: 1
        });

        me.items = [
            {
                xtype: 'container',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                region: 'center',
                items: centerContainerItems
            }
        ];

        var additionalItems = FlexiBee.Plugins.invoke(me, 'defineAdditionalItems');
        Ext.iterate(additionalItems, function(additionalItem) {
            if (Ext.isArray(additionalItem)) {
                me.items = me.items.concat(additionalItem);
            } else {
                me.items.push(additionalItem);
            }
        });

        me.callParent(arguments);
    }
});


<#import "/l-sencha.ftl" as sencha>
<@sencha.includePlugins path="${it.senchaName}.List"/>


</#escape>
