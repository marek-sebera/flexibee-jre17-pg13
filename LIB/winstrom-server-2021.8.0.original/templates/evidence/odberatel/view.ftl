<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-view.ftl" />

<#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canShowAtLeastOne(items, object, ['cenik', 'firma', 'skupFir', 'platiOdData', 'stredisko', 'platiDoData', 'rucneVybrat', 'prodejCena', 'kodIndi', 'poznam'], it)>
    <table class="flexibee-tbl-1">
        <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
        <#if canShowAtLeastOne(items, object, ['cenik'], it)>
        <tr>
            <td><@tool.label name='cenik' items=items />:</td><td><@tool.place object=object name='cenik' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['firma', 'skupFir'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['firma'], it)>
            <td><@tool.label name='firma' items=items />:</td><td><@tool.place object=object name='firma' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['skupFir'], it)>
            <td><@tool.label name='skupFir' items=items />:</td><td><@tool.place object=object name='skupFir' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['platiOdData', 'stredisko'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['platiOdData'], it)>
            <td><@tool.label name='platiOdData' items=items />:</td><td><@tool.place object=object name='platiOdData' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['stredisko'], it)>
            <td><@tool.label name='stredisko' items=items />:</td><td><@tool.place object=object name='stredisko' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['platiDoData', 'rucneVybrat'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['platiDoData'], it)>
            <td><@tool.label name='platiDoData' items=items />:</td><td><@tool.place object=object name='platiDoData' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['rucneVybrat'], it)>
            <td><@tool.label name='rucneVybrat' items=items />:</td><td><@tool.place object=object name='rucneVybrat' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['prodejCena'], it)>
        <tr>
            <td><@tool.label name='prodejCena' items=items />:</td><td><@tool.place object=object name='prodejCena' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['kodIndi'], it)>
        <tr>
            <td><@tool.label name='kodIndi' items=items />:</td><td><@tool.place object=object name='kodIndi' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['poznam'], it)>
        <tr>
            <td colspan="4"><@tool.label name='poznam' items=items />:<br><@tool.placeArea object=object name='poznam' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

<#--AUTOGEN END-->

<@tool.listItems items=items object=object marker=marker />

<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />

<#include "/i-footer-view.ftl" />
</#escape>