<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>

${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}

<@edit.beginForm />
    <#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canEditAtLeastOne(items, object, ['cenik', 'firma', 'skupFir', 'platiOdData', 'stredisko', 'platiDoData', 'rucneVybrat', 'prodejCena', 'kodIndi', 'poznam'])>
    <table class="flexibee-tbl-1">
        <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
        <#if canEditAtLeastOne(items, object, ['cenik'])>
        <tr>
            <td><@edit.label name='cenik' items=items /></td><td><@edit.place object=object name='cenik' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['firma', 'skupFir'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['firma'])>
            <td><@edit.label name='firma' items=items /></td><td><@edit.place object=object name='firma' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['skupFir'])>
            <td><@edit.label name='skupFir' items=items /></td><td><@edit.place object=object name='skupFir' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['platiOdData', 'stredisko'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['platiOdData'])>
            <td><@edit.label name='platiOdData' items=items /></td><td><@edit.place object=object name='platiOdData' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['stredisko'])>
            <td><@edit.label name='stredisko' items=items /></td><td><@edit.place object=object name='stredisko' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['platiDoData', 'rucneVybrat'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['platiDoData'])>
            <td><@edit.label name='platiDoData' items=items /></td><td><@edit.place object=object name='platiDoData' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['rucneVybrat'])>
            <td><@edit.label name='rucneVybrat' items=items /></td><td><@edit.place object=object name='rucneVybrat' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['prodejCena'])>
        <tr>
            <td><@edit.label name='prodejCena' items=items /></td><td><@edit.place object=object name='prodejCena' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['kodIndi'])>
        <tr>
            <td><@edit.label name='kodIndi' items=items /></td><td><@edit.place object=object name='kodIndi' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['poznam'])>
        <tr>
            <td colspan="4"><@edit.label name='poznam' items=items /><br><@edit.textarea object=object name='poznam' items=items marker=marker rows=3 /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

<#--AUTOGEN END-->

    <br/>
    <@edit.listItems items=items object=object marker=marker />

    <@edit.saveButton />
<@edit.endForm />

<@edit.generateNaiveJavascript object=object it=it />

<#include "/parts/i-dalsi-informace.ftl" />


<#include "/i-footer-edit.ftl" />
</#escape>
