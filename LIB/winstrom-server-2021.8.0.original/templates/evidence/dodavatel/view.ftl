<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-view.ftl" />

<#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canShowAtLeastOne(items, object, ['cenik', 'firma', 'primarni', 'nakupCena', 'kodIndi', 'stavMJ', 'poznam'], it)>
    <table class="flexibee-tbl-1">
        <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
        <#if canShowAtLeastOne(items, object, ['cenik'], it)>
        <tr>
            <td><@tool.label name='cenik' items=items />:</td><td colspan="3"><@tool.place object=object name='cenik' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['firma', 'primarni'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['firma'], it)>
            <td><@tool.label name='firma' items=items />:</td><td><@tool.place object=object name='firma' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['primarni'], it)>
            <td><@tool.label name='primarni' items=items />:</td><td><@tool.place object=object name='primarni' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['nakupCena'], it)>
        <tr>
            <td><@tool.label name='nakupCena' items=items />:</td><td colspan="3"><@tool.place object=object name='nakupCena' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['kodIndi'], it)>
        <tr>
            <td><@tool.label name='kodIndi' items=items />:</td><td colspan="3"><@tool.place object=object name='kodIndi' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['stavMJ'], it)>
        <tr>
            <td><@tool.label name='stavMJ' items=items />:</td><td colspan="3"><@tool.place object=object name='stavMJ' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['poznam'], it)>
        <tr>
            <td colspan="4"><@tool.label name='poznam' items=items />:<br><@tool.placeArea object=object name='poznam' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

<#--AUTOGEN END-->

<@tool.listItems items=items object=object marker=marker />

<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />

<#include "/i-footer-view.ftl" />
</#escape>