<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>

${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}

<@edit.beginForm />
    <#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canEditAtLeastOne(items, object, ['firma', 'nazev', 'tel', 'stat', 'mobil', 'mesto', 'psc', 'fax', 'ulice', 'email', 'eanKod', 'www', 'prijmeni', 'primarni', 'kontaktOsoba'])>
    <table class="flexibee-tbl-1">
        <col width="10%" /><col width="23%" /><col width="10%" /><col width="23%" /><col width="10%" /><col width="23%" />
        <#if canEditAtLeastOne(items, object, ['firma'])>
        <tr>
            <td><@edit.label name='firma' items=items /></td><td colspan="5"><@edit.place object=object name='firma' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['nazev', 'tel'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['nazev'])>
            <td><@edit.label name='nazev' items=items /></td><td colspan="3"><@edit.place object=object name='nazev' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['tel'])>
            <td><@edit.label name='tel' items=items /></td><td><@edit.place object=object name='tel' items=items marker=marker  type='tel'/></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['stat', 'mobil'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['stat'])>
            <td><@edit.label name='stat' items=items /></td><td colspan="3"><@edit.place object=object name='stat' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['mobil'])>
            <td><@edit.label name='mobil' items=items /></td><td><@edit.place object=object name='mobil' items=items marker=marker  type='tel'/></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['mesto', 'psc', 'fax'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['mesto'])>
            <td><@edit.label name='mesto' items=items /></td><td><@edit.place object=object name='mesto' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['psc'])>
            <td><@edit.label name='psc' items=items /></td><td><@edit.place object=object name='psc' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['fax'])>
            <td><@edit.label name='fax' items=items /></td><td><@edit.place object=object name='fax' items=items marker=marker  type='fax'/></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['ulice', 'email'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['ulice'])>
            <td><@edit.label name='ulice' items=items /></td><td colspan="3"><@edit.place object=object name='ulice' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['email'])>
            <td><@edit.label name='email' items=items /></td><td><@edit.place object=object name='email' items=items marker=marker  type='email'/></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['eanKod', 'www'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['eanKod'])>
            <td><@edit.label name='eanKod' items=items /></td><td colspan="3"><@edit.place object=object name='eanKod' items=items marker=marker  type='ean'/></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['www'])>
            <td><@edit.label name='www' items=items /></td><td><@edit.place object=object name='www' items=items marker=marker  type='www'/></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['prijmeni', 'primarni'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['prijmeni'])>
            <td><@edit.label name='prijmeni' items=items /></td><td colspan="3"><@edit.place object=object name='prijmeni' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['primarni'])>
            <td><@edit.label name='primarni' items=items /></td><td><@edit.place object=object name='primarni' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['kontaktOsoba'])>
        <tr>
            <td><@edit.label name='kontaktOsoba' items=items /></td><td><@edit.place object=object name='kontaktOsoba' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
    <h4>${lang('labels', 'textyPN')}</h4>
    <div id="edit-fragment-1" class="tab-pane">
        <div class="flexibee-table-border">
        <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
        <table class="flexibee-tbl-1">
            <#if canEditAtLeastOne(items, object, ['popis'])>
            <tr>
                <td><@edit.label name='popis' items=items /><br><@edit.textarea object=object name='popis' items=items marker=marker rows=3 /></td>
            </tr>
            </#if>
            <#if canEditAtLeastOne(items, object, ['poznam'])>
            <tr>
                <td><@edit.label name='poznam' items=items /><br><@edit.textarea object=object name='poznam' items=items marker=marker rows=3 /></td>
            </tr>
            </#if>
        </table>
        </#if>
        </div>

    </div>
    </#if>

<#--AUTOGEN END-->

    <br/>
    <@edit.listItems items=items object=object marker=marker />

    <@edit.saveButton />
<@edit.endForm />

<@edit.generateNaiveJavascript object=object it=it />

<#include "/parts/i-dalsi-informace.ftl" />

<#if isSlowMobile == false>
<script type="text/javascript">
    $(function() {
    <#assign pscUrl = formListUrl(it.companyResource, 'PSC')!"" />
    $("#flexibee-inp-psc, #flexibee-inp-mesto").suggestPsc('${pscUrl}', '#flexibee-inp-stat', function(data) {
            $("#flexibee-inp-mesto").val(data.nazev).change();
            $("#flexibee-inp-psc").val(data.kod).change();
    });
});

</script>
</#if>


<#include "/i-footer-edit.ftl" />
</#escape>
