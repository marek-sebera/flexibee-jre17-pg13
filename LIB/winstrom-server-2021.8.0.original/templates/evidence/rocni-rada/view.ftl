<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-view.ftl" />

${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}
${marker.mark('modul')}


<#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canShowAtLeastOne(items, object, ['rada', 'cisAkt', 'prefix', 'cisPoc', 'postfix', 'cisDelka', 'ucetObdobi', 'zobrazNuly'], it)>
    <table class="flexibee-tbl-1">
        <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
        <#if canShowAtLeastOne(items, object, ['rada'], it)>
        <tr>
            <td><@tool.label name='rada' items=items />:</td><td colspan="3"><@tool.place object=object name='rada' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['cisAkt', 'prefix'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['cisAkt'], it)>
            <td><@tool.label name='cisAkt' items=items />:</td><td><@tool.place object=object name='cisAkt' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['prefix'], it)>
            <td><@tool.label name='prefix' items=items />:</td><td><@tool.place object=object name='prefix' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['cisPoc', 'postfix'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['cisPoc'], it)>
            <td><@tool.label name='cisPoc' items=items />:</td><td><@tool.place object=object name='cisPoc' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['postfix'], it)>
            <td><@tool.label name='postfix' items=items />:</td><td><@tool.place object=object name='postfix' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['cisDelka', 'ucetObdobi'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['cisDelka'], it)>
            <td><@tool.label name='cisDelka' items=items />:</td><td><@tool.place object=object name='cisDelka' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['ucetObdobi'], it)>
            <td><@tool.label name='ucetObdobi' items=items />:</td><td><@tool.place object=object name='ucetObdobi' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['zobrazNuly'], it)>
        <tr>
            <td><@tool.label name='zobrazNuly' items=items />:</td><td><@tool.place object=object name='zobrazNuly' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

<#--AUTOGEN END-->

<@tool.listItems items=items object=object marker=marker />

<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />

<#include "/i-footer-view.ftl" />
</#escape>