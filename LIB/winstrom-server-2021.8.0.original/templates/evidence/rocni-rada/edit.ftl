<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>

${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}
${marker.mark('modul')}

<@edit.beginForm />
    <#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canEditAtLeastOne(items, object, ['rada', 'cisAkt', 'prefix', 'cisPoc', 'postfix', 'cisDelka', 'ucetObdobi', 'zobrazNuly'])>
    <table class="flexibee-tbl-1">
        <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
        <#if canEditAtLeastOne(items, object, ['rada'])>
        <tr>
            <td><@edit.label name='rada' items=items /></td><td colspan="3"><@edit.place object=object name='rada' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['cisAkt', 'prefix'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['cisAkt'])>
            <td><@edit.label name='cisAkt' items=items /></td><td><@edit.place object=object name='cisAkt' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['prefix'])>
            <td><@edit.label name='prefix' items=items /></td><td><@edit.place object=object name='prefix' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['cisPoc', 'postfix'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['cisPoc'])>
            <td><@edit.label name='cisPoc' items=items /></td><td><@edit.place object=object name='cisPoc' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['postfix'])>
            <td><@edit.label name='postfix' items=items /></td><td><@edit.place object=object name='postfix' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['cisDelka', 'ucetObdobi'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['cisDelka'])>
            <td><@edit.label name='cisDelka' items=items /></td><td><@edit.place object=object name='cisDelka' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['ucetObdobi'])>
            <td><@edit.label name='ucetObdobi' items=items /></td><td><@edit.place object=object name='ucetObdobi' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['zobrazNuly'])>
        <tr>
            <td><@edit.label name='zobrazNuly' items=items /></td><td><@edit.place object=object name='zobrazNuly' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

<#--AUTOGEN END-->

    <br/>
    <@edit.listItems items=items object=object marker=marker />

    <@edit.saveButton />
<@edit.endForm />

<@edit.generateNaiveJavascript object=object it=it />

<#include "/parts/i-dalsi-informace.ftl" />

<#include "/i-footer-edit.ftl" />
</#escape>
