<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#macro generateJavaScript it object polozkyPropName>

<#import "/l-prijemce-tools.ftl" as prijemce />
<@prijemce.placeCreditCheckJavaScript />

<script type="text/javascript">
    var symbolyMen = {
        "CZK": "Kč",
        "SKK": "Sk",
        "EUR": "&euro;",
        "GBP": "&pound;",
        "USD": "$",
        "JPY": "&yen;"
    };

    function polozkyDokladu_renderItem(polozka, editId, deleteId) {
        //console.log("rendering ", polozka);

        var mnozMj = polozka.mnozMj;
        if (mnozMj > 0) {
            if (parseInt(mnozMj) == parseFloat(mnozMj)) { // aby se 1.0 zobrazovalo jako 1
                mnozMj = parseInt(mnozMj, 10);
            }
        }

    var readOnly = false;

    if (polozka.$readonly || polozka.$autogen) {
            readOnly = true;
    }

        var result = ''
                + '<tr>\n';
        result = result
                + '  <td class="flexibee-small flexibee-grey">' + (polozka.kod ? polozka.kod : '') + '<'+'/td>\n';
    result = result
                + '  <td class="flexibee-small">' + (mnozMj > 0 ? mnozMj : '') + ' ' + (polozka.mj$nazev ? polozka.mj$nazev : '') + '<'+'/td>\n';
    if (polozka.$jeVMene) {
        result = result
                + '  <td class="flexibee-r"><strong>' + polozka.$sumZklMen + ' ' + polozka.mena$symbol + '<'+'/strong><'+'/td>\n';
    } else {
        result = result
                + '  <td class="flexibee-r"><strong>' + polozka.$sumZkl + ' ' + polozka.mena$symbol + '<'+'/strong><'+'/td>\n';
    }
    result = result
                + (readOnly ? '' : ('  <td><span class="flexibee-js-action flexibee-button" id="' + editId + '" <#if isMobile>data-role="button"</#if>>Upravit<'+'/span><'+'/td>\n'));
    result = result
                + '<'+'/tr>\n';
    result = result
                + '<tr>\n';
    result = result
                + '  <td colspan="2">' + (polozka.nazev ? polozka.nazev : '') + '<'+'/td>\n';
    if (polozka.$jeVMene) {
        if (polozka.$sumCelkemMen != polozka.$sumZklMen) {
            result = result
                    + '  <td class="flexibee-r"><small>' + polozka.$sumCelkemMen + ' ' + polozka.mena$symbol + ' s DPH<'+'/small><'+'/td>';
        } else {
            result = result
                    + '  <td class="flexibee-r"><'+'/td>';
        }
    } else {
        if (polozka.$sumCelkem != polozka.$sumZkl) {
            result = result
                    + '  <td class="flexibee-r"><small>' + polozka.$sumCelkem + ' ' + polozka.mena$symbol + ' s DPH<'+'/small><'+'/td>';
        } else {
            result = result
                    + '  <td class="flexibee-r"><'+'/td>';
        }
    }
    result = result
                + (readOnly ? '' : ('  <td><span class="flexibee-js-action flexibee-button negative" id="' + deleteId + '" <#if isMobile>data-role="button"</#if>>Smazat<'+'/span><'+'/td>\n'))
                + '<'+'/tr>\n';
    return result;
    }

    function polozkyDokladu_postConvertForm2Json(form, json, jsonFromServer) {
        <#-- TODO jsonFromServer je hack, chtělo by to vyřešit nějak jinak! viz ItemsHelper -->

        var mjInput = $("#flexibee-inp-polozkyDokladu__mj :selected", form);
        json.mj$nazev = mjInput.val() ? mjInput.text().replace(/.+?:\s*/, "") : "";

        var cenik = $("#flexibee-inp-polozkyDokladu__cenik", form);

        if (json['cenik'] && cenik && cenik.hasClass("flexibee-flexbox")) {
            if (jsonFromServer && jsonFromServer['cenik@internalId'] && jsonFromServer['cenik@showAs']) {
                json['cenik@internalId'] = jsonFromServer['cenik@internalId'];
                json['cenik@showAs'] = jsonFromServer['cenik@showAs'];
            }

        } else if (json['cenik'] && cenik && cenik.length) {
            json.cenik$nazev = cenik.children(':selected').text().replace(/.+?:\s*/, "").trim();
        }

        if ($("span#flexibee-inp-polozkyDokladu__sumZkl", form).length) {
            json.$sumZkl = $("span#flexibee-inp-polozkyDokladu__sumZkl", form).text();
            json.$sumDph = $("span#flexibee-inp-polozkyDokladu__sumDph", form).text();
            json.$sumCelkem = $("span#flexibee-inp-polozkyDokladu__sumCelkem", form).text();
        } else { // neplátce
            json.$sumZkl = $("span#flexibee-inp-polozkyDokladu__sumCelkem", form).text();
            json.$sumCelkem = $("span#flexibee-inp-polozkyDokladu__sumCelkem", form).text();
        }

        if ($("span#flexibee-inp-polozkyDokladu__sumZklMen", form).length) {
            json.$sumZklMen = $("span#flexibee-inp-polozkyDokladu__sumZklMen", form).text();
            json.$sumDphMen = $("span#flexibee-inp-polozkyDokladu__sumDphMen", form).text();
            json.$sumCelkemMen = $("span#flexibee-inp-polozkyDokladu__sumCelkemMen", form).text();
        } else { // neplátce
            json.$sumZklMen = $("span#flexibee-inp-polozkyDokladu__sumCelkemMen", form).text();
            json.$sumCelkemMen = $("span#flexibee-inp-polozkyDokladu__sumCelkemMen", form).text();
        }

        json.mena$symbol = "";
        if (jsonFromServer) {
            json.mena = jsonFromServer['mena@ref'].match(/(\d+)$/)[1];
            var kodMeny = jsonFromServer['mena@showAs'].match(/^(.*?):/)[1];
            json.mena$symbol = symbolyMen[kodMeny] || kodMeny;
        }

        json.$jeVMene = parseFloat(json.$sumCelkemMen) != 0;
    }

    function polozkyDokladu_postConvertJson2Form(json, form) {
        if (json.cenik$nazev && $("#flexibee-inp-polozkyDokladu__cenik-flexbox_input", form).length) {
            $("#flexibee-inp-polozkyDokladu__cenik-flexbox_input", form).val(json.cenik$nazev).removeClass('watermark');
        }
        $("#flexibee-inp-polozkyDokladu__sumZkl", form).text(json.$sumZkl);
        $("#flexibee-inp-polozkyDokladu__sumDph", form).text(json.$sumDph);
        $("#flexibee-inp-polozkyDokladu__sumCelkem", form).text(json.$sumCelkem);

        $("#flexibee-inp-polozkyDokladu__sumZklMen", form).text(json.$sumZklMen);
        $("#flexibee-inp-polozkyDokladu__sumDphMen", form).text(json.$sumDphMen);
        $("#flexibee-inp-polozkyDokladu__sumCelkemMen", form).text(json.$sumCelkemMen);
    }

    function polozkyDokladu_postItemDeleted(itemsHelper) {
        <#-- po smazání poslední položky je potřeba vynulovat celkové součty -->
        if (itemsHelper === undefined || itemsHelper.getAllItems().length == 0) {
            $.each(["sumOsv", "sumZklSniz", "sumDphSniz", "sumCelkSniz", "sumZklZakl", "sumDphZakl", "sumCelkZakl", "sumCelkem",
                "sumOsvMen", "sumZklSnizMen", "sumDphSnizMen", "sumCelkSnizMen", "sumZklZaklMen", "sumDphZaklMen", "sumCelkZaklMen", "sumCelkemMen",
                "sumZklSniz2", "sumDphSniz2", "sumCelkSniz2",
                "sumZklSniz2Men", "sumDphSniz2Men", "sumCelkSniz2Men"],
                    function(idx, value) {
                        $("#flexibee-inp-" + value).val("");
                    }
            );
        }
    }

    $(function() {
        <#if !object.bezPolozek>
            $("#flexibee-doklad-polozky").show();
        </#if>

        var initialItems = [
        <#if object.polozkyDokladu??>
        <#escape x as x?default("")?js_string?html>
            <#list object.polozkyDokladu as polozka>
            {
                id:          "${polozka.id?c}",
                $readonly:   <#if polozka.autogen || polozka.autoZaokr>true<#else>false</#if>,
                <#if polozka.cenik??>
                cenik: "${polozka.cenik.id?c}",
                cenik$nazev: "${polozka.cenik.nazev}",
                "cenik@internalId": "${polozka.cenik.id?c}",
                "cenik@showAs":     "${polozka.cenik.kod}: ${polozka.cenik.nazev}",
                </#if>
                <#if polozka.sklad??>
                sklad:       "${polozka.sklad.id?c}",
                sklad$nazev: "${polozka.sklad.nazev}",
                "sklad@internalId": "${polozka.sklad.id?c}",
                "sklad@showAs":     "${polozka.sklad.kod}: ${polozka.sklad.nazev}",
                </#if>
                kod:         "${polozka.kod}",
                nazev:       "${polozka.nazev}",
                mnozMj:      "${polozka.mnozMj?c}",
                <#if polozka.modul = 'FAV'|| polozka.modul = 'FAP'>sumVedlNaklIntrMen: "${polozka.sumVedlNaklIntrMen?c}",</#if>
                <#if polozka.mj??>
                mj:          "${polozka.mj.id?c}",
                mj$nazev:    "${polozka.mj.nazev}",
                "mj@internalId": "${polozka.mj.id?c}",
                "mj@showAs":     "${polozka.mj.kod}: ${polozka.mj.nazev}",
                </#if>
                cenaMj:      "${polozka.cenaMj?c}",
                typCenyDphK: "${polozka.typCenyDphK}",
                <#if polozka.typSzbDphK??>
                typSzbDphK:    "${polozka.typSzbDphK}",
                szbDph:      "${polozka.szbDph}",
                </#if>
                <#if polozka.mena??>
                mena:        "${polozka.mena.id?c}",
                mena$symbol: "${polozka.mena.symbol}",
                </#if>
                $sumZkl:     "${polozka.sumZkl?c}",
                $sumDph:     "${polozka.sumDph?c}",
                $sumCelkem:  "${polozka.sumCelkem?c}",
                $sumZklMen:  "${polozka.sumZklMen?c}",
                $sumDphMen:  "${polozka.sumDphMen?c}",
                $sumCelkemMen: "${polozka.sumCelkemMen?c}",
                $jeVMene:    <#if polozka.sumCelkemMen != 0>true<#else>false</#if>,
                kopClenKonVykDph: "${polozka.kopClenKonVykDph?string}"
                <#-- jeVMene je boolean, kopClenKonVykDph(taky boolean!) je string. jeVMene je totiž jen "lokální" pro využití v JS,
                zatímco kopClenKonVykDph je property VO. A server posílá a očekává boolean jako string a tak ho dále zpracováváme-->
            }<#if polozka_has_next>,</#if>
            </#list>
        </#escape>
        </#if>
        ];

        <#assign polozkyIt = it.getSubEvidence(it.evidenceName + "-polozka") />

        function haveItems(itemsHelper) {
            // brát v úvahu pouze "opravdové" položky, ne ty syntetické
            return $.grep(itemsHelper.getAllItems(), function(item) {
                return !item.$readonly;
            }).length > 0;
        }

        var items = new flexibee.ItemsHelper({
            evidence: {
                name: "${polozkyIt.evidenceResource.evidenceName}",
                props: <@edit.propsTypes restActionHelper=polozkyIt/>,
                prefix: "polozkyDokladu__",
                defaults: {
                    kopClenKonVykDph: "true",
                    <#if polozkyPropName == "polozkyIntDokladu" && object.modul = 'BAN'>
                    typPolozkyK: "typPolozky.ucetni"
                    <#else>
                    typPolozkyK: "typPolozky.obecny"
                    </#if>

                    //typCenyDphK: "typCeny.bezDph",
                    <#--<#if settings(object.datumDokladu).platceDph>-->
                    //typSzbDphK:  "typSzbDph.dphZakl"
                    <#--</#if>-->
                },
                hideOnStart: ["sklad"] // property budou při přechodu z listu na edit skryté, pokud nemají vybranou žádnou hodnotu
            },
            dependencies: {
                mnozMj:  ["sumZkl", "sumDph"],
                cenaMj:  ["sumZkl", "sumDph"],
                sumZkl:  ["sumDph", "cenaMj"],
                typSzbDphK: ["szbDph"]
            },
            page: {
                list: $("#flexibee-doklad-polozky-list"),
                edit: $("#flexibee-doklad-polozky-edit"),
                activateList: function() { $("#flexibee-doklad-polozky ul#tabs li:eq(0) a").onClick(); },
                activateEdit: function() { $("#flexibee-doklad-polozky ul#tabs li:eq(1) a").onClick(); },
                changeEditTitle: function(title) { $("#flexibee-doklad-polozky ul#tabs li:eq(1) a span").text(title); }
            },
            data: {
                renderItem: polozkyDokladu_renderItem,
                postConvertForm2Json: polozkyDokladu_postConvertForm2Json,
                postConvertJson2Form: polozkyDokladu_postConvertJson2Form,
                postItemDeleted: polozkyDokladu_postItemDeleted,
                itemTitle: function(item) { return item.kod; },
                <#-- rnovacek: Skladové položky mají vazbu na ceník povinnou, takže pokud není nastavena, lze prohlásit položku za prázdnou. -->
                <#--    Pokud to je právě editovaná položka, tak se nepřenese na server. -->
                <#if polozkyPropName == "skladovePolozky">
                    isItemEmpty: function(item) { return !item.cenik; },
                <#else>
                    <#-- kfridrich: Ignorujeme kopClenKonVykDph na položkách, pokud je to jediná zadaná property. -->
                    <#--    Je to checkbox a ten neumí být prázdný, vždy je true nebo false. Pokud někdy přidáme další checkbox -->
                    <#--    do editace položek, tak by se měla následující (a možná i předchozí) funkce upravit. -->
                    isItemEmpty: function(editedObj) {
                        var name;
                        for ( name in editedObj ) {
                            if ( name === "kopClenKonVykDph" ) {
                                continue;
                            }

                            return false;
                        }
                        return true;
                    },
                </#if>
                initialData: initialItems,
                haveSignificantItems: haveItems
            }
        });

        // ---

        var defaults = {
            <#if object['id']?? && object['id'] != -1>
            id: "${object['id']?c}",
            </#if>
            bezPolozek: function() {
                return !haveItems(this.items.${polozkyPropName});
            }
        };

        var form = new flexibee.FormHelper({
            evidence: {
                name: "${it.evidenceResource.evidenceName}",
                url: "${it.evidenceResource.listURI}",
                props: <@edit.propsTypes restActionHelper=it/>,
                filledProps: <@edit.filledProps object=object restActionHelper=it/>,
                defaults: defaults,
                stripDependenciesOfFieldsOnFirstAmend: ["banka", "pokladna", "sklad", "typDokl"]
            },
            dependencies: {
                firma:       ["nazFirmy", "ulice", "mesto", "psc", "stat", "ic", "dic", "vatId"],

                sumOsv:      ["sumCelkem"],

                sumZklSniz:  ["sumDphSniz", "sumCelkSniz", "sumCelkem"],
                sumDphSniz:  ["sumCelkSniz", "sumCelkem"],
                sumCelkSniz: ["sumZklSniz", "sumDphSniz", "sumCelkem"],

                sumZklSniz2:  [               "sumDphSniz2", "sumCelkSniz2", "sumCelkem"],
                sumDphSniz2:  [                              "sumCelkSniz2", "sumCelkem"],
                sumCelkSniz2: ["sumZklSniz2", "sumDphSniz2",                 "sumCelkem"],

                sumZklZakl:  ["sumDphZakl", "sumCelkZakl", "sumCelkem"],
                sumDphZakl:  ["sumCelkZakl", "sumCelkem"],
                sumCelkZakl: ["sumZklZakl", "sumDphZakl", "sumCelkem"],

                sumCelkem:   ["sumOsv", "sumZklSniz", "sumDphSniz", "sumCelkSniz", "sumZklSniz2", "sumDphSniz2", "sumCelkSniz2", "sumZklZakl", "sumDphZakl", "sumCelkZakl"],

                sumOsvMen:      ["sumCelkemMen",                                       "sumOsv", "sumCelkem"],

                sumZklSnizMen:  [                 "sumDphSnizMen", "sumCelkSnizMen", "sumCelkemMen", "sumZklSniz", "sumDphSniz", "sumCelkSniz"],
                sumDphSnizMen:  [                                  "sumCelkSnizMen", "sumCelkemMen", "sumZklSniz", "sumDphSniz", "sumCelkSniz"],
                sumCelkSnizMen: ["sumZklSnizMen", "sumDphSnizMen",                   "sumCelkemMen", "sumZklSniz", "sumDphSniz", "sumCelkSniz"],

                sumZklSniz2Men:  [                 "sumDphSniz2Men", "sumCelkSniz2Men", "sumCelkemMen", "sumZklSniz2", "sumDphSniz2", "sumCelkSniz2"],
                sumDphSniz2Men:  [                                   "sumCelkSniz2Men", "sumCelkemMen", "sumZklSniz2", "sumDphSniz2", "sumCelkSniz2"],
                sumCelkSniz2Men: ["sumZklSniz2Men", "sumDphSniz2Men",                   "sumCelkemMen", "sumZklSniz2", "sumDphSniz2", "sumCelkSniz2"],

                sumZklZaklMen:  [                  "sumDphZaklMen", "sumCelkZaklMen", "sumCelkemMen",  "sumZklZakl", "sumDphZakl", "sumCelkZakl"],
                sumDphZaklMen:  [                                   "sumCelkZaklMen", "sumCelkemMen",  "sumZklZakl", "sumDphZakl", "sumCelkZakl"],
                sumCelkZaklMen: ["sumZklZaklMen", "sumDphZaklMen",                    "sumCelkemMen",  "sumZklZakl", "sumDphZakl", "sumCelkZakl"],

                sumCelkemMen:   ["sumOsvMen", "sumZklSnizMen", "sumDphSnizMen", "sumCelkSnizMen", "sumZklZaklMen", "sumDphZaklMen", "sumCelkZaklMen",
                                 "sumOsv", "sumZklSniz", "sumDphSniz", "sumCelkSniz", "sumZklZakl", "sumDphZakl", "sumCelkZakl",
                                 "sumZklSniz2", "sumDphSniz2", "sumCelkSniz2", "sumZklSniz2Men", "sumDphSniz2Men", "sumCelkSniz2Men",],

                <#-- kód a variabilní symbol proto, že s datumem se může změnit rok -->
                <#-- sumy DPH/celkem (ne základy!) kvůli tomu, že se změnou kalendářního data se může změnit sazba DPH -->
                datVyst:  ["kod", "varSym", "datSplat",
                           "sumDphSniz", "sumCelkSniz", "sumDphSniz2", "sumCelkSniz2", "sumDphZakl", "sumCelkZakl", "sumCelkem", "sumDphSnizMen", "sumCelkSnizMen", "sumDphSniz2Men", "sumCelkSniz2Men", "sumDphZaklMen", "sumCelkZaklMen", "sumCelkemMen"],
                datSplat: ["sumDphSniz", "sumCelkSniz", "sumDphSniz2", "sumCelkSniz2", "sumDphZakl", "sumCelkZakl", "sumCelkem", "sumDphSnizMen", "sumCelkSnizMen", "sumDphSniz2Men", "sumCelkSniz2Men", "sumDphZaklMen", "sumCelkZaklMen", "sumCelkemMen"],
                duzpPuv:  ["sumDphSniz", "sumCelkSniz", "sumDphSniz2", "sumCelkSniz2", "sumDphZakl", "sumCelkZakl", "sumCelkem", "sumDphSnizMen", "sumCelkSnizMen", "sumDphSniz2Men", "sumCelkSniz2Men", "sumDphZaklMen", "sumCelkZaklMen", "sumCelkemMen"],

                mena: ["kurz", "kurzMnozstvi"],

                banka:    ["mena", "kurz", "kurzMnozstvi"],
                pokladna: ["kod", "mena", "kurz", "kurzMnozstvi", "sumOsvMen", "sumZklSnizMen", "sumDphSnizMen", "sumCelkSnizMen",  "sumZklSniz2Men", "sumDphSniz2Men", "sumCelkSniz2Men", "sumZklZaklMen", "sumDphZaklMen", "sumCelkZaklMen", "sumCelkemMen", "sumOsv", "sumZklSniz", "sumDphSniz", "sumCelkSniz", "sumZklSniz2", "sumDphSniz2", "sumCelkSniz2", "sumZklZakl", "sumDphZakl", "sumCelkZakl", "sumCelkem"],
                sklad:    ["mena", "kurz", "kurzMnozstvi"],

                typDokl: ["statDph", "clenDph", "duzpPuv", "duzpUcto", "dphSnizUcet", "dphZaklUcet", "primUcet", "protiUcet", "sumOsvMen", "sumZklSnizMen", "sumDphSnizMen", "sumCelkSnizMen", "sumZklSniz2Men", "sumDphSniz2Men", "sumCelkSniz2Men", "sumZklZaklMen", "sumDphZaklMen", "sumCelkZaklMen", "sumCelkemMen", "sumOsv", "sumZklSniz", "sumDphSniz", "sumCelkSniz", "sumZklSniz2", "sumDphSniz2", "sumCelkSniz2", "sumZklZakl", "sumDphZakl", "sumCelkZakl", "sumCelkem", "eetPokladniZarizeni", "eetProvozovna", "eetDicPoverujiciho", "eetTypK"]
            },
            page: {
                form: $("#flexibee-document-header")
            },
            items: {
                ${polozkyPropName}: items
            },
            itemsDependencies: {
                ${polozkyPropName}: ["sumOsv", "sumZklSniz2", "sumDphSniz2", "sumCelkSniz2", "sumZklSniz", "sumDphSniz", "sumCelkSniz", "sumZklZakl", "sumDphZakl", "sumCelkZakl", "sumCelkem",
                                     "sumOsvMen", "sumZklSniz2Men", "sumDphSniz2Men", "sumCelkSniz2Men", "sumZklSnizMen", "sumDphSnizMen", "sumCelkSnizMen", "sumZklZaklMen", "sumDphZaklMen", "sumCelkZaklMen", "sumCelkemMen"]
            },
            csrfToken: "${csrfToken}",
            onFieldChangedBeforeAmend: function(field, oldVal, newVal) {
                if (field == "mena") {
                    <#-- TODO tohle by ideálně měl zvládnout server, ale trochu se to mlátí s importními validacemi (v módu asGui je neprovádět?) -->
                    var tuz = "${settings(object.datumDokladu).mena.id}";
                    var fields = [];
                    if (newVal == tuz) { // měn -> tuz
                        fields = ["kurz", "kurzMnozstvi", "sumOsvMen", "sumZklSnizMen", "sumDphSnizMen", "sumCelkSnizMen", "sumZklSniz2Men", "sumDphSniz2Men", "sumCelkSniz2Men", "sumZklZaklMen", "sumDphZaklMen", "sumCelkZaklMen", "sumCelkemMen"];
                        // přepnout se na záložku s tuz. měnou
                        $('a[href="#edit-fragment-17"]').click();
                    } else if (oldVal == tuz) { // tuz -> měn
                        fields = ["sumOsv", "sumZklSniz", "sumDphSniz", "sumCelkSniz",  "sumZklSniz2", "sumDphSniz2", "sumCelkSniz2", "sumZklZakl", "sumDphZakl", "sumCelkZakl", "sumCelkem"];
                        // přepnout se na záložku s cizí měnou
                        $('a[href="#edit-fragment-22"]').click();
                    }
                    $.each(fields, function() {
                        $("#flexibee-inp-" + this).val("0"); // všechna políčka jsou type="number", proto ta nula!
                    });
                }
            },
            onAfterAmend: function(obj) {
                if (obj) {
                    var maPolozky = obj.${polozkyPropName} && obj.${polozkyPropName}.length > 0;
                    var jePlatceDph = obj["sumZklZakl@enabled"] != "false";
                    var jeVMene = obj["sumZklZaklMen@enabled"] != "false" || obj["sumOsvMen@enabled"] != "false";

                    //console.log("!!! maPolozky = ", maPolozky, ", jePlatceDph = ", jePlatceDph, ", jeVMene = ", jeVMene);

                    var zobrazitPolozky = maPolozky || obj.sumCelkem == "0.0";
                    var zobrazitSumyPlatce = jePlatceDph && !maPolozky;
                    var zobrazitSumyNeplatce = !jePlatceDph && !maPolozky;
                    var zobrazitSumyMenPlatce = jeVMene && jePlatceDph && !maPolozky;
                    var zobrazitSumyMenNeplatce = jeVMene && !jePlatceDph && !maPolozky;

                    //console.log("!!! zobrazitPolozky = ", zobrazitPolozky, ", zobrazitSumyPlatce = ", zobrazitSumyPlatce,
                    //            ", zobrazitSumyNeplatce = ", zobrazitSumyNeplatce, ", zobrazitSumyMenPlatce = ", zobrazitSumyMenPlatce,
                    //            ", zobrazitSumyMenNeplatce = ", zobrazitSumyMenNeplatce);

                    $("#flexibee-doklad-polozky").toggle(zobrazitPolozky);
                    $(".flexibee-doklad-sumy-platce-dph").toggle(zobrazitSumyPlatce);
                    $(".flexibee-doklad-sumy-neplatce-dph").toggle(zobrazitSumyNeplatce);
                    $(".flexibee-doklad-sumy-men-platce-dph").toggle(zobrazitSumyMenPlatce);
                    $(".flexibee-doklad-sumy-men-neplatce-dph").toggle(zobrazitSumyMenNeplatce);

                    handleCreditCheck(obj.stat, obj.ic, obj.dic);
                }
            }
        });
        form.start();

        var extraParams = { country: "<#if object.stat??>${object.stat.kod}<#else>${settings().statLegislativa.kod}</#if>", format: "json" };

        $("#flexibee-inp-stat-flexbox_input").change(function() {
            var stat = $(this).val();
        if (stat.length > 2) {
                stat = stat.substring(0, 2);
        }
        if (stat != '') {
            extraParams.country = stat;
        }
        });

        $("#flexibee-inp-nazFirmy").autocomplete("https://support.flexibee.eu/remote/suggest/", {
            width: 280,
            extraParams: extraParams,
            dataType: "json",
            selectFirst: false,
            minChars: 3,
            parse: function(data) {
                if (!data.winstrom.adresar) {
                    return [];
                }

                return $.map(data.winstrom.adresar, function(row) {
                    return {
                        data: row,
                        value: row.nazev,
                        result: row.nazev
                    }
                });
            },
            formatItem: function(item) {
                var nazev = item.nazev;
                if (nazev.length > 50) {
                    nazev = nazev.substring(0, 50) + "...";
                }
                return nazev + "<br/><small style=\"color:grey\">" + item.ulice + ", " + item.mesto + (item.ic ? " | IČO: " + item.ic : "") + "<"+"/small>";
            }
        }).result(function(e, item) {
            $("#flexibee-inp-nazFirmy").valWithAlert(item.nazev).change();
            $("#flexibee-inp-ic").valWithAlert(item.ic).change();
            $("#flexibee-inp-ulice").valWithAlert(item.ulice).change();

            $("#flexibee-inp-stat-flexbox").fillFlexBoxData('${formListUrl(it.companyResource, 'STAT')}', item.stat);

            $("#flexibee-inp-psc").valWithAlert(item.psc).change();
            $("#flexibee-inp-dic").valWithAlert(item.dic).change();
            $("#flexibee-inp-mesto").valWithAlert(item.mesto).change();

            $("#flexibee-inp-kod").valWithAlert("").change();
        });

        <#assign pscUrl = formListUrl(it.companyResource, 'PSC')!"" />
        $("#flexibee-inp-psc, #flexibee-inp-mesto").suggestPsc('${pscUrl}', '#flexibee-inp-stat', function(data) {
            $("#flexibee-inp-mesto").valWithAlert(data.nazev).change();
            $("#flexibee-inp-psc").valWithAlert(data.kod).change();
        });
        $("#flexibee-inp-faPsc, #flexibee-inp-faMesto").suggestPsc('${pscUrl}', '#flexibee-inp-faStat', function(data) {
            $("#flexibee-inp-faMesto").valWithAlert(data.nazev).change();
            $("#flexibee-inp-faPsc").valWithAlert(data.kod).change();
        });
    });
</script>

</#macro>

</#escape>
