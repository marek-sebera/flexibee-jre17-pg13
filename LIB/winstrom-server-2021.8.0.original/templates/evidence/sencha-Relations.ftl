<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.${it.senchaName}.Relations', {
    extend: 'Ext.tab.Panel',
    alias: 'widget.fbRelations${it.senchaName?cap_first}',

    requires: [
        'Ext.tab.Panel'
    ],

    height: 200,
    plain: true,

    // gets filled in the loadRecord method
    items: [],

    loadRecord: function(model) {
        var me = this;

        var id = model.data.id;

        var relations = [
        <#list it.evidenceResource.getSubEvidences(null).list as sub>
            Ext.create('FlexiBee.${sub.evidenceType.path?replace("-", "_")}.List', {
                title: '${sub.name}',
                listUrl: '/c/${companyId}/${it.evidenceResource.evidenceName}/' + id + '/${sub.url}',
                urlPart: '${sub.url}',
                showQuickFilter: false
            })<#if sub_has_next>,</#if>
        </#list>
        ];

        var additionalItems = FlexiBee.Plugins.invoke(me, 'defineAdditionalItems', model.data);
        Ext.iterate(additionalItems, function(additionalItem) {
            if (Ext.isArray(additionalItem)) {
                relations = relations.concat(additionalItem);
            } else {
                relations.push(additionalItem);
            }
        });

        me.removeAll();
        if (relations.length === 0) {
            me.hide(); // hide the whole panel
        } else {
            me.add(relations);
        }
    }
});

<#import "/l-sencha.ftl" as sencha>
<@sencha.includePlugins path=it.senchaName+".Relations"/>

</#escape>
