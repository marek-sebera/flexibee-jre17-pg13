<#ftl encoding="utf-8" />
<#-- @ftlvariable name="fakItByEt" type="cz.winstrom.net.server.evidence.BankaPokladnaEvidenceResource.FakItByEt" -->
<#-- @ftlvariable name="stav" type="cz.winstrom.api.BankaPokladnaEvidence.StavSparovani" -->
<#escape x as x?default("")?html>

<#assign myTitle = "Ruční párování" />
<#include "/i-header-edit.ftl"/>

<#if rucniParovaniOk?? && rucniParovaniOk>
    <div class="alert alert-success">
        <strong>Ruční párování úspěšně dokončeno.</strong>
    </div>
</#if>

<#if rucniParovaniNeuspesne??>
    <div class="alert alert-danger">
        <ul class="flexibee-errors">
            <li>${rucniParovaniNeuspesne}</li>
        </ul>
    </div>
</#if>

<#if stav.kandidatiNaUhradu?values?size == 0>
    <div class="alert alert-warning">
    Nenalezeny žádné faktury, které by bylo možné tímto dokladem uhradit.
    </div>
<#else>
    <@edit.beginForm/>
        <br>
        <#import "/evidence/l-pagination.ftl" as p/>
        <@p.paginate pagination=pagination/>

        <#import "/evidence/l-doklad-list.ftl" as my/>
        <table class="table flexibee-list flexibee-tbl-list table-striped table-hover table-condensed">
            <col width="5%"><col width="10%"><col width="85%">
            <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th>Částka</th>
                    <th>Doklad</th>
                </tr>
            </thead>
            <tbody>
                <#list stav.kandidatiNaUhradu?values as kandidat>
                    <tr class="flexibee-clickable">
                        <td>
                            <input type="checkbox" tabindex="${kandidat_index * 2 + 1}" class="flexibee-kandidat-checkbox" id="flexibee-inp-checkbox_${kandidat.id?c}" name="kandidat_${kandidat.id?c}" value="true" <#if (zadaneUhrady?? && zadaneUhrady[kandidat.id?c]??) || (!zadaneUhrady?? && stav.isUhrazenaFak(kandidat.id))>checked</#if>>
                            <#if isMobile><label for="flexibee-inp-checkbox_${kandidat.id?c}">&nbsp;</label></#if> <#-- bez labelu jQueryMobile vůbec checkbox nezobrazí -->
                        </td>
                        <td>
                            <#if errorKandidatId?? && errorKandidatId == kandidat.id>
                                <div class="flexibee-error-box">
                            </#if>
                            <input type="text" tabindex="${kandidat_index * 2 + 2}" class="flexibee-kandidat-castka form-control" id="flexibee-inp-castka_${kandidat.id?c}" name="castka_${kandidat.id?c}" style="width: 5em;" <#if zadaneUhrady?? && zadaneUhrady[kandidat.id?c]??>value="${zadaneUhrady[kandidat.id?c]}"<#elseif !zadaneUhrady?? && stav.isUhrazenaFak(kandidat.id)>value="${stav.getUhrazenaCastka(kandidat.id)?c}"</#if>>
                            <#if errorKandidatId?? && errorKandidatId == kandidat.id>
                                </div>
                            </#if>
                        </td>
                        <td class="flexibee-keynav">
                            <#assign oldIt = it />
                            <#global it = fakItByEt.getByEvidenceType(stav.getEvidenceProKandidata(kandidat.id)) />
                            <#compress>
                            <@my.dokladInList object=kandidat iteratorIndex=kandidat_index marker=marker items=it.devDocItemsForHtmlMap />
                            </#compress>
                            <#global it = oldIt />
                        </td>
                    </tr>
                </#list>
            </tbody>
        </table>
        <script type="text/javascript">
            $(function() {
                function checkboxChanged() {
                    var checked = $(this).is(":checked");
                    var id = $(this).attr("name").replace(/^kandidat_/, "");
                    if (checked) {
                        $("#flexibee-inp-castka_" + id).removeAttr("disabled").focus();
                    } else {
                        $("#flexibee-inp-castka_" + id).attr("disabled", "disabled");
                    }
                }
                $(".flexibee-kandidat-checkbox").each(function() {
                    $(this).onClick(checkboxChanged);
                    $(this).change(checkboxChanged);
                    checkboxChanged.call(this);
                });
            });
        </script>

        <br>
        <p>Použito pro úhradu: <strong><span id="flexibee-jiz-sparovano">${stav.uhrazujiciDoklJizSparovano}</span> <@tool.menaSymbol stav.uhrazujiciDokl.mena/></strong>
           z ${stav.uhrazujiciDoklCelkem} <@tool.menaSymbol stav.uhrazujiciDokl.mena/> celkem</p>
        <p>Zbytek: <span id="flexibee-rozdil">${stav.uhrazujiciDoklZbyvaSparovat}</span> <@tool.menaSymbol stav.uhrazujiciDokl.mena/>
            <span style="margin-left: 1em;">(<input type="checkbox" class="" name="zbytekZauctovat" id="flexibee-inp-zbytekZauctovat" value="true" <#if zbytekZauctovat?? && zbytekZauctovat>checked</#if>> <label for="flexibee-inp-zbytekZauctovat"> zaúčtovat</label>&nbsp;)</span></p>

        <@edit.okCancelButton />
    <@edit.endForm />
</#if>

<script type="text/javascript">
    $(function() {
        var celkem = ${stav.uhrazujiciDoklCelkem?c};

        function recompute() {
            <#assign pouzito = stav.uhrazujiciDoklJizSparovano/>
            <#list stav.kandidatiNaUhradu?values as kandidat>
                <#if stav.isUhrazenaFak(kandidat.id)>
                    <#assign pouzito = pouzito - stav.getUhrazenaCastka(kandidat.id) />
                </#if>
            </#list>
            var pouzito = ${pouzito};

            $(".flexibee-kandidat-castka:enabled").each(function() {
                var castka = parseFloat($(this).val().replace(/(\s)/, "").replace(",", "."));
                if (castka) {
                    pouzito += castka;
                }
            });

            $("#flexibee-jiz-sparovano").textNumber(pouzito);
            $("#flexibee-rozdil").textNumber(celkem - pouzito);
        }

        $(".flexibee-kandidat-castka").change(recompute);
        $(".flexibee-kandidat-checkbox").click(recompute);
        $(".flexibee-kandidat-checkbox").change(recompute);
        recompute();
    });
</script>

<#include "/i-footer-edit.ftl" />
</#escape>
