<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#include "/i-header-edit.ftl"/>

<#-- Obsluha chyby -->
<#if chyba??>
    <div class="alert alert-danger">
        <ul class="flexibee-errors">
            ${chyba}
        </ul>
    </div>

    <@edit.buttonsBegin/>
    <a class="flexibee-focus btn btn-primary flexibee-clickable"  data-dismiss="modal"  href="${it.goBackURI}"><span>${lang('labels', 'backToDocument', 'Zpět na doklad')}</span></a></li>
    <@edit.buttonsEnd/>
</#if>

<#-- Vyber cleneni dph pro zdd -->
<#if clenDphs??>
    <#import "/l-edits.ftl" as edit />
    <@edit.beginForm />

    <table class="table table-striped table-hover table-condensed flexibee-tbl-list">
    <thead>
        <tr>
            <th><label>${lang('labels', 'zaloha', 'Záloha')}</label></th>
            <th><label>${lang('labels', 'cleneniDph', 'Členění DPH')}</label></th>
        </tr>
    </thead>
    <tbody class="flexibee-tbl-list-body ">
        <#list zalohy as zaloha>
        <tr>
            <td><label>${zaloha.name}</label></td>
            <td>
                <select name="zalohaACleneni" size="10" class="flexibee-rounded-form-select flexibee-inp form-control">
                <#list clenDphs as cd>
                    <option value="${zaloha.name}^^^${cd.id}">${cd.name}</option>
                </#list>
                </select>
            </td>
        </tr>
        </#list>
    <tr>
    <#if prenasetCisDosle??>
        <td>
            <input type="checkbox" name="prenasetCisDosle" class="flexibee-inp" id="flexibee-inp-prenasetCisDosle" value="true" <#if prenasetCisDosle>checked</#if>>
        </td>
        <td><label for="flexibee-inp-prenasetCisDosle" class="control-label">${lang('messages', 'zalohyZddGenPrenosCisDosleLb', 'Přenést číslo došlé do vytvářeného ZDD')}</label></td>
    </#if>
    </tr>
    </tbody>
    </table>

    <@edit.okCancelButton showCancel=false />

    <@edit.endForm />
</#if>

<#-- Vysledek - vygenerovane zdd -->
<#if vygenerovaneZDDUrl??>
        <div class="alert alert-success">
            <p><strong>${generovanoMsg}<a href="${vygenerovaneZDDUrl}"><#if kodZdd??>${kodZdd}<#else>${lang('labels', 'vytvorZdd.faktury', 'Faktury')}</#if></a>.</strong></p>
        </div>
</#if>

<script type="text/javascript">
    $(function() {
        $("form").submit(function() {
            <#--
              - hack, ale ono se to pořádně řešit nedá; lepší hack používá cookies
              - http://geekswithblogs.net/GruffCode/archive/2010/10/28/detecting-the-file-download-dialog-in-the-browser.aspx
              -->
            setTimeout(function() {
                $.hideWait();
            }, 5000);
        });
    });
</script>

<#include "/i-footer-edit.ftl" />
</#escape>