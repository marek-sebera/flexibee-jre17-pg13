<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-view.ftl" />

<#--AUTOGEN BEGIN-->
    <#if canShowAtLeastOne(items, object, ['stat', 'typSzbDphK', 'szbDph', 'platiOdData', 'platiDoData', 'popis', 'poznam'], it)>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canShowAtLeastOne(items, object, ['stat', 'typSzbDphK', 'szbDph', 'platiOdData', 'platiDoData'], it)>
            <li class="active"><a href="#view-fragment-1" data-toggle="tab">Základní</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
            <li><a href="#view-fragment-2" data-toggle="tab">${lang('labels', 'textyPN')}</a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canShowAtLeastOne(items, object, ['stat', 'typSzbDphK', 'szbDph', 'platiOdData', 'platiDoData'], it)>
            <div id="view-fragment-1" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['stat', 'typSzbDphK', 'szbDph', 'platiOdData', 'platiDoData'], it)>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <#if canShowAtLeastOne(items, object, ['stat'], it)>
                    <tr>
                        <td><@tool.label name='stat' items=items />:</td><td><@tool.place object=object name='stat' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['typSzbDphK'], it)>
                    <tr>
                        <td><@tool.label name='typSzbDphK' items=items />:</td><td><@tool.place object=object name='typSzbDphK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['szbDph'], it)>
                    <tr>
                        <td><@tool.label name='szbDph' items=items />:</td><td><@tool.place object=object name='szbDph' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['platiOdData'], it)>
                    <tr>
                        <td><@tool.label name='platiOdData' items=items />:</td><td><@tool.place object=object name='platiOdData' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['platiDoData'], it)>
                    <tr>
                        <td><@tool.label name='platiDoData' items=items />:</td><td><@tool.place object=object name='platiDoData' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
            <div id="view-fragment-2" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
                <table class="flexibee-tbl-1">
                    <#if canShowAtLeastOne(items, object, ['popis'], it)>
                    <tr>
                        <td><@tool.label name='popis' items=items />:<br><@tool.placeArea object=object name='popis' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['poznam'], it)>
                    <tr>
                        <td><@tool.label name='poznam' items=items />:<br><@tool.placeArea object=object name='poznam' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

<@tool.listItems items=items object=object marker=marker />

<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />

<#include "/i-footer-view.ftl" />
</#escape>