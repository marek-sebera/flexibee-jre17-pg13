<#ftl encoding="utf-8"  strip_whitespace=true />
<#escape x as x?default("")?html>

<#include "/i-header-list.ftl" />
<#include "/evidence/i-list-begin.ftl" />

<#if it.rowCount &gt; 0>
<table class="table flexibee-tbl-list table-striped table-hover table-condensed">
    <thead>
    <tr>
        <th><@tool.listHeader name="jmeno"    items=it.devDocItemsForHtmlMap url=url urlSuffix=filterQueryParam orderProperty=orderProperty orderDirection=orderDirection/></th>
        <th><@tool.listHeader name="prijmeni" items=it.devDocItemsForHtmlMap url=url urlSuffix=filterQueryParam orderProperty=orderProperty orderDirection=orderDirection/></th>
        <th><@tool.listHeader name="firma"    items=it.devDocItemsForHtmlMap url=url urlSuffix=filterQueryParam orderProperty=orderProperty orderDirection=orderDirection/></th>
    </tr>
    </thead>
    <tbody class="flexibee-tbl-list-body">
    <#list it.list as object>
        <#assign dataUrl = "/c/" + it.companyResource.companyId + "/" + it.evidenceType.path + "/" + object.id?string('0') />
    <tr class="<#if object_index % 2 = 0>flexibee-even</#if> flexibee-clickable flexibee-keynav">
        <td colspan="3">
        <a href="${dataUrl}">
            <#if canShowAtLeastOne(items, object, ['prijmeni', 'jmeno'])>
            <big><big><strong><@tool.placeList marker=marker object=object name="prijmeni" items=it.devDocItemsForHtmlMap /></strong> <@tool.placeList marker=marker object=object name="jmeno" items=it.devDocItemsForHtmlMap /></big></big>
            <#else>
            (nezadáno)
            </#if>
        </a>


            <#if canShowAtLeastOne(items, object, ['firma', 'funkce'])>
        <a href="${dataUrl}" style="float:right;color:gray;text-align:right">
            <small><@tool.placeList marker=marker object=object name="firma" items=it.devDocItemsForHtmlMap /><br/>
                <#if canShowAtLeastOne(items, object, ['funkce'])>
            <em><@tool.placeList marker=marker object=object name="funkce" items=it.devDocItemsForHtmlMap /></em></small>
                    </#if>
        </a>
        </#if>
<br/>
            <#if canShowAtLeastOne(items, object, ['mobil', 'tel', 'email'])>
        <a href="${dataUrl}">
            <small><@tool.placeList marker=marker object=object name=[ "mobil", "tel", "email" ] items=it.devDocItemsForHtmlMap /></small>
        </a>
        </#if>
</td>
    </tr>
        </#list>
    </tbody>
</table>
</#if>

<#include "/evidence/i-list-end.ftl" />


<#include "/footer.ftl" />
</#escape>