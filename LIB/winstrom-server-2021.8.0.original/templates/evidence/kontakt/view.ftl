<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-view.ftl" />


${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}
${marker.mark('stitky')}
${marker.mark('primarni')}

<#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canShowAtLeastOne(items, object, ['firma', 'prijmeni', 'titul', 'jmeno', 'titulZa', 'osloveni', 'primarni'], it)>
    <table class="flexibee-tbl-1">
        <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
        <#if canShowAtLeastOne(items, object, ['firma'], it)>
        <tr>
            <td><@tool.label name='firma' items=items />:</td><td><@tool.place object=object name='firma' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['prijmeni', 'titul'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['prijmeni'], it)>
            <td><@tool.label name='prijmeni' items=items />:</td><td><@tool.place object=object name='prijmeni' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['titul'], it)>
            <td><@tool.label name='titul' items=items />:</td><td><@tool.place object=object name='titul' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['jmeno', 'titulZa'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['jmeno'], it)>
            <td><@tool.label name='jmeno' items=items />:</td><td><@tool.place object=object name='jmeno' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['titulZa'], it)>
            <td><@tool.label name='titulZa' items=items />:</td><td><@tool.place object=object name='titulZa' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['osloveni'], it)>
        <tr>
            <td><@tool.label name='osloveni' items=items />:</td><td><@tool.place object=object name='osloveni' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['primarni'], it)>
        <tr>
            <td><@tool.label name='primarni' items=items />:</td><td><@tool.place object=object name='primarni' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canShowAtLeastOne(items, object, ['funkce', 'oddeleni', 'ic', 'dic', 'ulice', 'mesto', 'psc', 'stat', 'tel', 'mobil', 'fax', 'email', 'www', 'odesilatFak', 'rodCis', 'odesilatObj', 'datNaroz', 'odesilatNab', 'odesilatPpt', 'odesilatPok', 'odesilatSkl', 'popis', 'poznam', 'platiOd', 'platiDo'], it)>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canShowAtLeastOne(items, object, ['funkce', 'oddeleni', 'ic', 'dic', 'ulice', 'mesto', 'psc', 'stat', 'tel', 'mobil', 'fax', 'email', 'www'], it)>
            <li class="active"><a href="#view-fragment-1" data-toggle="tab">${lang('labels', 'zaklInformacePN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['odesilatFak', 'rodCis', 'odesilatObj', 'datNaroz', 'odesilatNab', 'odesilatPpt', 'odesilatPok', 'odesilatSkl'], it)>
            <li><a href="#view-fragment-2" data-toggle="tab">${lang('labels', 'upresneniPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
            <li><a href="#view-fragment-3" data-toggle="tab">${lang('labels', 'textyPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['platiOd', 'platiDo'], it)>
            <li><a href="#view-fragment-4" data-toggle="tab">${lang('labels', 'spravaPN')}</a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canShowAtLeastOne(items, object, ['funkce', 'oddeleni', 'ic', 'dic', 'ulice', 'mesto', 'psc', 'stat', 'tel', 'mobil', 'fax', 'email', 'www'], it)>
            <div id="view-fragment-1" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['funkce', 'oddeleni', 'ic', 'dic', 'ulice', 'mesto', 'psc', 'stat', 'tel', 'mobil', 'fax', 'email', 'www'], it)>
                <table class="flexibee-tbl-1">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canShowAtLeastOne(items, object, ['funkce', 'oddeleni'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['funkce'], it)>
                        <td><@tool.label name='funkce' items=items />:</td><td><@tool.place object=object name='funkce' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['oddeleni'], it)>
                        <td><@tool.label name='oddeleni' items=items />:</td><td><@tool.place object=object name='oddeleni' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['ic', 'dic'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['ic'], it)>
                        <td><@tool.label name='ic' items=items />:</td><td><@tool.place object=object name='ic' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['dic'], it)>
                        <td><@tool.label name='dic' items=items />:</td><td><@tool.place object=object name='dic' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['ulice'], it)>
                    <tr>
                        <td><@tool.label name='ulice' items=items />:</td><td colspan="3"><@tool.place object=object name='ulice' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['mesto', 'psc'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['mesto'], it)>
                        <td><@tool.label name='mesto' items=items />:</td><td><@tool.place object=object name='mesto' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['psc'], it)>
                        <td><@tool.label name='psc' items=items />:</td><td><@tool.place object=object name='psc' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['stat'], it)>
                    <tr>
                        <td><@tool.label name='stat' items=items />:</td><td colspan="3"><@tool.place object=object name='stat' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['tel'], it)>
                    <tr>
                        <td><@tool.label name='tel' items=items />:</td><td><@tool.placeTel object=object name='tel' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['mobil', 'fax'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['mobil'], it)>
                        <td><@tool.label name='mobil' items=items />:</td><td><@tool.placeTel object=object name='mobil' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['fax'], it)>
                        <td><@tool.label name='fax' items=items />:</td><td><@tool.place object=object name='fax' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['email', 'www'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['email'], it)>
                        <td><@tool.label name='email' items=items />:</td><td><@tool.placeEmail object=object name='email' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['www'], it)>
                        <td><@tool.label name='www' items=items />:</td><td><@tool.placeWww object=object name='www' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['odesilatFak', 'rodCis', 'odesilatObj', 'datNaroz', 'odesilatNab', 'odesilatPpt', 'odesilatPok', 'odesilatSkl'], it)>
            <div id="view-fragment-2" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['odesilatFak', 'rodCis', 'odesilatObj', 'datNaroz', 'odesilatNab', 'odesilatPpt', 'odesilatPok', 'odesilatSkl'], it)>
                <table class="flexibee-tbl-1">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canShowAtLeastOne(items, object, ['odesilatFak', 'rodCis'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['odesilatFak'], it)>
                        <td><@tool.label name='odesilatFak' items=items />:</td><td><@tool.place object=object name='odesilatFak' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['rodCis'], it)>
                        <td><@tool.label name='rodCis' items=items />:</td><td><@tool.place object=object name='rodCis' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['odesilatObj', 'datNaroz'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['odesilatObj'], it)>
                        <td><@tool.label name='odesilatObj' items=items />:</td><td><@tool.place object=object name='odesilatObj' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['datNaroz'], it)>
                        <td><@tool.label name='datNaroz' items=items />:</td><td><@tool.place object=object name='datNaroz' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['odesilatNab'], it)>
                    <tr>
                        <td><@tool.label name='odesilatNab' items=items />:</td><td><@tool.place object=object name='odesilatNab' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['odesilatPpt'], it)>
                    <tr>
                        <td><@tool.label name='odesilatPpt' items=items />:</td><td><@tool.place object=object name='odesilatPpt' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['odesilatPok'], it)>
                    <tr>
                        <td><@tool.label name='odesilatPok' items=items />:</td><td><@tool.place object=object name='odesilatPok' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['odesilatSkl'], it)>
                    <tr>
                        <td><@tool.label name='odesilatSkl' items=items />:</td><td><@tool.place object=object name='odesilatSkl' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
            <div id="view-fragment-3" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
                <table class="flexibee-tbl-1">
                    <#if canShowAtLeastOne(items, object, ['popis'], it)>
                    <tr>
                        <td><@tool.label name='popis' items=items />:<br><@tool.placeArea object=object name='popis' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['poznam'], it)>
                    <tr>
                        <td><@tool.label name='poznam' items=items />:<br><@tool.placeArea object=object name='poznam' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['platiOd', 'platiDo'], it)>
            <div id="view-fragment-4" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['platiOd', 'platiDo'], it)>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <tr>
                        <td colspan="2">Platnost se udává v kalendářních rocích, pro neomezenou platnost zadejte 0-9999</td>
                    </tr>

                    <#if canShowAtLeastOne(items, object, ['platiOd'], it)>
                    <tr>
                        <td><@tool.label name='platiOd' items=items />:</td><td><@tool.place object=object name='platiOd' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['platiDo'], it)>
                    <tr>
                        <td><@tool.label name='platiDo' items=items />:</td><td><@tool.place object=object name='platiDo' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

<@tool.listItems items=items object=object marker=marker />

<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />


<#include "/i-footer-view.ftl" />
</#escape>