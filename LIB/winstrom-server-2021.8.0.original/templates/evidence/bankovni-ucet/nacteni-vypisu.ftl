<#ftl encoding="utf-8" />
<#-- @ftlvariable name="bankovniUcetIt" type="cz.winstrom.net.server.action.ActionList" -->
<#escape x as x?default("")?html>

<#--
  - tato šablona se používá jak pro evidenci banka, tak pro evidenci bankovní účet
  - rozpoznat to lze podle přítomnosti proměnné bankovniUcetIt, ta existuje jen při přístupu z evidence banka
  -->
<#assign isBanka = bankovniUcetIt??>

<#if isBanka>
    <#assign myTitle="Načtení bankovního výpisu" />
<#else>
    <#assign myTitle="Načtení bankovního výpisu pro " />
</#if>


<#include "/i-header-edit-file.ftl"/>

<#if vypisNactenOk?? && vypisNactenOk>
    <div class="alert alert-success">
        <strong>Výpis byl úspěšně naimportován.</strong>
    </div>
</#if>

<#if nelzeNacistVypis??>
    <div class="alert alert-danger">
        <ul class="flexibee-errors">
            <li>${nelzeNacistVypis}</li>
        </ul>
    </div>
</#if>

<#if nacteniVypisuNeuspesne??>
    <div class="alert alert-danger">
        <ul class="flexibee-errors">
            <#list nacteniVypisuNeuspesne as vypis>
                <li>
                    ${vypis.error}
                    <#if vypis.polozkyErrors??>
                        <ul class="flexibee-suberrors">
                            <#list vypis.polozkyErrors as polozkaError>
                                <li>${polozkaError}</li>
                            </#list>
                        </ul>
                    </#if>
                </li>
            </#list>
        </ul>
    </div>
</#if>

<#assign cnt = 1/>
    <div class="flexibee-steps">
    <#if isBanka>
         <h2 class="flexibee-step${cnt}<#assign cnt = cnt + 1/>">Bankovní účet:</h2>
                <div>
                    <#list bankovniUcetIt.list as ucet>
                         <div class="radio">
                             <label>
                                  <input type="radio" name="bankovniUcet" id="bankovniUcet-${ucet.id}" value="${ucet.id}" <#if (vybranyBankovniUcet?? && vybranyBankovniUcet == ucet.id) || (!vybranyBankovniUcet?? && ucet_index == 0)>checked</#if>>
                                ${ucet.nazev}
                            </label>
                        </div>
                </#list>
                </div>
    </#if>

<div class="removeStepsBorder">
    <h2 class="flexibee-step${cnt}<#assign cnt = cnt + 1/>">Načtení souboru</h2>
    <div>
                  <label for="file">Bankovní výpis:</label>
                <input type="file" name="file" id="file" class="flexibee-inp form-control" />
    </div>

        </div>
        </div> <#-- flexibee-steps -->
        <@edit.okCancelButton />

<#include "/i-footer-edit.ftl" />
</#escape>