<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>

${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}

<@edit.beginForm />
    <#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canEditAtLeastOne(items, object, ['kod', 'nazev', 'danEvidK', 'protiUcetPrijem', 'protiUcetVydej', 'dphSnizUcet', 'dphZaklUcet', 'kodPlneniK'])>
    <table class="flexibee-tbl-1">
        <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
        <#if canEditAtLeastOne(items, object, ['kod'])>
        <tr>
            <td><@edit.label name='kod' items=items /></td><td><@edit.place object=object name='kod' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['nazev'])>
        <tr>
            <td><@edit.label name='nazev' items=items /></td><td><@edit.place object=object name='nazev' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['danEvidK'])>
        <tr>
            <td><@edit.label name='danEvidK' items=items /></td><td><@edit.place object=object name='danEvidK' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['protiUcetPrijem', 'protiUcetVydej'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['protiUcetPrijem'])>
            <td><@edit.label name='protiUcetPrijem' items=items /></td><td><@edit.place object=object name='protiUcetPrijem' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['protiUcetVydej'])>
            <td><@edit.label name='protiUcetVydej' items=items /></td><td><@edit.place object=object name='protiUcetVydej' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['dphSnizUcet', 'dphZaklUcet'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['dphSnizUcet'])>
            <td><@edit.label name='dphSnizUcet' items=items /></td><td><@edit.place object=object name='dphSnizUcet' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['dphZaklUcet'])>
            <td><@edit.label name='dphZaklUcet' items=items /></td><td><@edit.place object=object name='dphZaklUcet' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['kodPlneniK'])>
        <tr>
            <td><@edit.label name='kodPlneniK' items=items /></td><td><@edit.place object=object name='kodPlneniK' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canEditAtLeastOne(items, object, ['modulFav', 'modulFap', 'modulPhl', 'modulZav', 'modulBanP', 'modulBanV', 'modulPokP', 'modulPokV', 'modulSklP', 'modulSklV', 'modulInt'])>
    <h4>${lang('labels', 'vybKliceViditelnost')}</h4>
    <div id="edit-fragment-1" class="tab-pane">
        <div class="flexibee-table-border">
        <#if canEditAtLeastOne(items, object, ['modulFav', 'modulFap', 'modulPhl', 'modulZav', 'modulBanP', 'modulBanV', 'modulPokP', 'modulPokV', 'modulSklP', 'modulSklV', 'modulInt'])>
        <table class="flexibee-tbl-1">
            <col width="20%" /><col width="20%" /><col width="20%" /><col width="20%" />
            <#if canEditAtLeastOne(items, object, ['modulFav', 'modulFap'])>
            <tr>
                <td>Faktury</td>
                <#if isMobile></tr><tr></#if>
                <#if canEditAtLeastOne(items, object, ['modulFav'])>
                <td><@edit.label name='modulFav' items=items /></td><td><@edit.place object=object name='modulFav' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canEditAtLeastOne(items, object, ['modulFap'])>
                <td><@edit.label name='modulFap' items=items /></td><td><@edit.place object=object name='modulFap' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
            <#if canEditAtLeastOne(items, object, ['modulPhl', 'modulZav'])>
            <tr>
                <td>Ost. zúčt. vztahy</td>
                <#if isMobile></tr><tr></#if>
                <#if canEditAtLeastOne(items, object, ['modulPhl'])>
                <td><@edit.label name='modulPhl' items=items /></td><td><@edit.place object=object name='modulPhl' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canEditAtLeastOne(items, object, ['modulZav'])>
                <td><@edit.label name='modulZav' items=items /></td><td><@edit.place object=object name='modulZav' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
            <#if canEditAtLeastOne(items, object, ['modulBanP', 'modulBanV'])>
            <tr>
                <td>Banka</td>
                <#if isMobile></tr><tr></#if>
                <#if canEditAtLeastOne(items, object, ['modulBanP'])>
                <td><@edit.label name='modulBanP' items=items /></td><td><@edit.place object=object name='modulBanP' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canEditAtLeastOne(items, object, ['modulBanV'])>
                <td><@edit.label name='modulBanV' items=items /></td><td><@edit.place object=object name='modulBanV' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
            <#if canEditAtLeastOne(items, object, ['modulPokP', 'modulPokV'])>
            <tr>
                <td>Pokladna</td>
                <#if isMobile></tr><tr></#if>
                <#if canEditAtLeastOne(items, object, ['modulPokP'])>
                <td><@edit.label name='modulPokP' items=items /></td><td><@edit.place object=object name='modulPokP' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canEditAtLeastOne(items, object, ['modulPokV'])>
                <td><@edit.label name='modulPokV' items=items /></td><td><@edit.place object=object name='modulPokV' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
            <#if canEditAtLeastOne(items, object, ['modulSklP', 'modulSklV'])>
            <tr>
                <td>Sklad</td>
                <#if isMobile></tr><tr></#if>
                <#if canEditAtLeastOne(items, object, ['modulSklP'])>
                <td><@edit.label name='modulSklP' items=items /></td><td><@edit.place object=object name='modulSklP' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canEditAtLeastOne(items, object, ['modulSklV'])>
                <td><@edit.label name='modulSklV' items=items /></td><td><@edit.place object=object name='modulSklV' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
            <#if canEditAtLeastOne(items, object, ['modulInt'])>
            <tr>
                <td><@edit.label name='modulInt' items=items /></td><td><@edit.place object=object name='modulInt' items=items marker=marker /></td>
            </tr>
            </#if>
        </table>
        </#if>
        </div>

    </div>
    </#if>

    <#if canEditAtLeastOne(items, object, ['popis', 'poznam', 'ucetObdobiOd', 'ucetObdobiDo'])>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <li class="active"><a href="#edit-fragment-2" data-toggle="tab"><span>${lang('labels', 'textyPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['ucetObdobiOd', 'ucetObdobiDo'])>
            <li><a href="#edit-fragment-3" data-toggle="tab"><span>${lang('labels', 'spravaPN')}</span></a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <div id="edit-fragment-2" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
                <table class="flexibee-tbl-1">
                    <#if canEditAtLeastOne(items, object, ['popis'])>
                    <tr>
                        <td><@edit.label name='popis' items=items /><br><@edit.textarea object=object name='popis' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['poznam'])>
                    <tr>
                        <td><@edit.label name='poznam' items=items /><br><@edit.textarea object=object name='poznam' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['ucetObdobiOd', 'ucetObdobiDo'])>
            <div id="edit-fragment-3" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['ucetObdobiOd', 'ucetObdobiDo'])>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <tr>
                        <td colspan="2">Platnost se udává v účetních obdobích, pro neomezenou platnost nechte pole nevyplněné</td>
                    </tr>

                    <#if canEditAtLeastOne(items, object, ['ucetObdobiOd'])>
                    <tr>
                        <td><@edit.label name='ucetObdobiOd' items=items /></td><td><@edit.place object=object name='ucetObdobiOd' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['ucetObdobiDo'])>
                    <tr>
                        <td><@edit.label name='ucetObdobiDo' items=items /></td><td><@edit.place object=object name='ucetObdobiDo' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

    <br/>
    <@edit.listItems items=items object=object marker=marker />

    <@edit.saveButton />
<@edit.endForm />

<@edit.generateNaiveJavascript object=object it=it />

<#include "/parts/i-dalsi-informace.ftl" />

<#include "/i-footer-edit.ftl" />
</#escape>
