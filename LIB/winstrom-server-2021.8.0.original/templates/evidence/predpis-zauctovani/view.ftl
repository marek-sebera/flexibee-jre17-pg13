<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-view.ftl" />

<#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canShowAtLeastOne(items, object, ['kod', 'nazev', 'danEvidK', 'protiUcetPrijem', 'protiUcetVydej', 'dphSnizUcet', 'dphZaklUcet', 'kodPlneniK'], it)>
    <table class="flexibee-tbl-1">
        <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
        <#if canShowAtLeastOne(items, object, ['kod'], it)>
        <tr>
            <td><@tool.label name='kod' items=items />:</td><td><@tool.place object=object name='kod' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['nazev'], it)>
        <tr>
            <td><@tool.label name='nazev' items=items />:</td><td><@tool.place object=object name='nazev' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['danEvidK'], it)>
        <tr>
            <td><@tool.label name='danEvidK' items=items />:</td><td><@tool.place object=object name='danEvidK' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['protiUcetPrijem', 'protiUcetVydej'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['protiUcetPrijem'], it)>
            <td><@tool.label name='protiUcetPrijem' items=items />:</td><td><@tool.place object=object name='protiUcetPrijem' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['protiUcetVydej'], it)>
            <td><@tool.label name='protiUcetVydej' items=items />:</td><td><@tool.place object=object name='protiUcetVydej' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['dphSnizUcet', 'dphZaklUcet'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['dphSnizUcet'], it)>
            <td><@tool.label name='dphSnizUcet' items=items />:</td><td><@tool.place object=object name='dphSnizUcet' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['dphZaklUcet'], it)>
            <td><@tool.label name='dphZaklUcet' items=items />:</td><td><@tool.place object=object name='dphZaklUcet' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['kodPlneniK'], it)>
        <tr>
            <td><@tool.label name='kodPlneniK' items=items />:</td><td><@tool.place object=object name='kodPlneniK' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canShowAtLeastOne(items, object, ['modulFav', 'modulFap', 'modulPhl', 'modulZav', 'modulBanP', 'modulBanV', 'modulPokP', 'modulPokV', 'modulSklP', 'modulSklV', 'modulInt'], it)>
    <h4>${lang('labels', 'vybKliceViditelnost')}</h4>
    <div id="view-fragment-1" class="tab-pane">
        <div class="flexibee-table-border">
        <#if canShowAtLeastOne(items, object, ['modulFav', 'modulFap', 'modulPhl', 'modulZav', 'modulBanP', 'modulBanV', 'modulPokP', 'modulPokV', 'modulSklP', 'modulSklV', 'modulInt'], it)>
        <table class="flexibee-tbl-1">
            <col width="20%" /><col width="20%" /><col width="20%" /><col width="20%" />
            <#if canShowAtLeastOne(items, object, ['modulFav', 'modulFap'], it)>
            <tr>
                <td>Faktury</td>
                <#if isMobile></tr><tr></#if>
                <#if canShowAtLeastOne(items, object, ['modulFav'], it)>
                <td><@tool.label name='modulFav' items=items />:</td><td><@tool.place object=object name='modulFav' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canShowAtLeastOne(items, object, ['modulFap'], it)>
                <td><@tool.label name='modulFap' items=items />:</td><td><@tool.place object=object name='modulFap' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
            <#if canShowAtLeastOne(items, object, ['modulPhl', 'modulZav'], it)>
            <tr>
                <td>Ost. zúčt. vztahy</td>
                <#if isMobile></tr><tr></#if>
                <#if canShowAtLeastOne(items, object, ['modulPhl'], it)>
                <td><@tool.label name='modulPhl' items=items />:</td><td><@tool.place object=object name='modulPhl' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canShowAtLeastOne(items, object, ['modulZav'], it)>
                <td><@tool.label name='modulZav' items=items />:</td><td><@tool.place object=object name='modulZav' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
            <#if canShowAtLeastOne(items, object, ['modulBanP', 'modulBanV'], it)>
            <tr>
                <td>Banka</td>
                <#if isMobile></tr><tr></#if>
                <#if canShowAtLeastOne(items, object, ['modulBanP'], it)>
                <td><@tool.label name='modulBanP' items=items />:</td><td><@tool.place object=object name='modulBanP' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canShowAtLeastOne(items, object, ['modulBanV'], it)>
                <td><@tool.label name='modulBanV' items=items />:</td><td><@tool.place object=object name='modulBanV' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
            <#if canShowAtLeastOne(items, object, ['modulPokP', 'modulPokV'], it)>
            <tr>
                <td>Pokladna</td>
                <#if isMobile></tr><tr></#if>
                <#if canShowAtLeastOne(items, object, ['modulPokP'], it)>
                <td><@tool.label name='modulPokP' items=items />:</td><td><@tool.place object=object name='modulPokP' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canShowAtLeastOne(items, object, ['modulPokV'], it)>
                <td><@tool.label name='modulPokV' items=items />:</td><td><@tool.place object=object name='modulPokV' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
            <#if canShowAtLeastOne(items, object, ['modulSklP', 'modulSklV'], it)>
            <tr>
                <td>Sklad</td>
                <#if isMobile></tr><tr></#if>
                <#if canShowAtLeastOne(items, object, ['modulSklP'], it)>
                <td><@tool.label name='modulSklP' items=items />:</td><td><@tool.place object=object name='modulSklP' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canShowAtLeastOne(items, object, ['modulSklV'], it)>
                <td><@tool.label name='modulSklV' items=items />:</td><td><@tool.place object=object name='modulSklV' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
            <#if canShowAtLeastOne(items, object, ['modulInt'], it)>
            <tr>
                <td><@tool.label name='modulInt' items=items />:</td><td><@tool.place object=object name='modulInt' items=items marker=marker /></td>
            </tr>
            </#if>
        </table>
        </#if>
        </div>

    </div>
    </#if>

    <#if canShowAtLeastOne(items, object, ['popis', 'poznam', 'ucetObdobiOd', 'ucetObdobiDo'], it)>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
            <li class="active"><a href="#view-fragment-2" data-toggle="tab">${lang('labels', 'textyPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['ucetObdobiOd', 'ucetObdobiDo'], it)>
            <li><a href="#view-fragment-3" data-toggle="tab">${lang('labels', 'spravaPN')}</a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
            <div id="view-fragment-2" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
                <table class="flexibee-tbl-1">
                    <#if canShowAtLeastOne(items, object, ['popis'], it)>
                    <tr>
                        <td><@tool.label name='popis' items=items />:<br><@tool.placeArea object=object name='popis' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['poznam'], it)>
                    <tr>
                        <td><@tool.label name='poznam' items=items />:<br><@tool.placeArea object=object name='poznam' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['ucetObdobiOd', 'ucetObdobiDo'], it)>
            <div id="view-fragment-3" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['ucetObdobiOd', 'ucetObdobiDo'], it)>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <tr>
                        <td colspan="2">Platnost se udává v účetních obdobích, pro neomezenou platnost nechte pole nevyplněné</td>
                    </tr>

                    <#if canShowAtLeastOne(items, object, ['ucetObdobiOd'], it)>
                    <tr>
                        <td><@tool.label name='ucetObdobiOd' items=items />:</td><td><@tool.place object=object name='ucetObdobiOd' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['ucetObdobiDo'], it)>
                    <tr>
                        <td><@tool.label name='ucetObdobiDo' items=items />:</td><td><@tool.place object=object name='ucetObdobiDo' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

<@tool.listItems items=items object=object marker=marker />

<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />

<#include "/i-footer-view.ftl" />
</#escape>