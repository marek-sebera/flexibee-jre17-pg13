<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.${it.senchaName}.ListController', {
    extend: 'FlexiBee.main.AbstractController',

    requires: [
        'FlexiBee.${it.senchaName}.Properties',
        'FlexiBee.${it.senchaName}.Store',
        'FlexiBee.${it.senchaName}.List'
    ],

    title: '',

    createView: function() {
        var tabTitle = this.title;
        return Ext.create('FlexiBee.${it.senchaName}.List', {
            closable: true,
            title: tabTitle
        });
    },

    setupControl: function() {
        this.controlOver({
            'grid': {
                select: this.onGridCellSelected,
                celldblclick: this.onGridCellEntered
            },
            'fbToolbar${it.senchaName?cap_first} button[itemId="new"]': {
                click: this.onToolbarNewClicked
            },
            'fbToolbar${it.senchaName?cap_first} button[itemId="edit"]': {
                click: this.onToolbarEditClicked
            },
            'fbToolbar${it.senchaName?cap_first} button[itemId="delete"]': {
                click: this.onToolbarDeleteClicked
            },
            'fbQuickFilter${it.senchaName?cap_first} combobox': {
                select: this.onQuickFilterChanged
            },
            'fbQuickFilter${it.senchaName?cap_first} button[itemId="cancelQuickFilter"]': {
                click: this.onQuickFilterCancel
            }
        });

        this.loadRecordListeners = [];
        var toolbar = this.view.query('fbToolbar${it.senchaName?cap_first}')[0];
        if (toolbar) {
            this.loadRecordListeners.push(toolbar);
        }
    },

    onGridCellSelected: function(row, record) {
        Ext.iterate(this.loadRecordListeners, function(listener) {
            listener.loadRecord(record);
        });
    },

    onGridCellEntered: function(table, td, cellIndex, record) {
        this.editRecord(record);
    },

    onToolbarNewClicked: function() {
        var me = this;

        var model = Ext.create('FlexiBee.${it.senchaName}.Model');

        FlexiBee.MessageBus.publish('status', { busy: true });
        Ext.require('FlexiBee.${it.senchaName}.DetailController', function() {
            me.application.newController('FlexiBee.${it.senchaName}.DetailController', {
                title: "nový záznam (${it.evidenceName})",
                model: model
            });

            FlexiBee.MessageBus.publish('status', { busy: false });
        });
    },

    onToolbarEditClicked: function() {
        var grid = this.view.query('grid')[0];
        var record = grid.getSelectionModel().getSelection()[0];
        this.editRecord(record);
    },

    onToolbarDeleteClicked: function() {
        var grid = this.view.query('grid')[0];
        var record = grid.getSelectionModel().getSelection()[0];
        var title = this.getRecordTitle(record);
        Ext.Msg.show({
            title: 'Smazat záznam?',
            msg: 'Opravdu chcete smazat záznam ' + title,
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(id) {
                if (id === 'yes') {
                    grid.getStore().remove(record);
                    record.destroy();
                }
            }
        });
    },

    onQuickFilterChanged: function(combobox, selected) {
        var key = selected[0].data.key;
        var isNeomezeno = key === combobox.quickFilterNeomezenoItem;
        var store = this.view.query('grid')[0].getStore();
        store.changeQuickFilter(combobox.quickFilterGroup, isNeomezeno ? null : key);
    },

    onQuickFilterCancel: function(button) {
        var store = this.view.query('grid')[0].getStore();
        store.clearQuickFilter();

        var quickFilterToolbar = button.findParentByType('fbQuickFilter${it.senchaName?cap_first}');
        Ext.iterate(quickFilterToolbar.query("combobox"), function(combobox) {
            combobox.setValue(combobox.quickFilterNeomezenoItem);
        });
    },

    editRecord: function(record) {
        var me = this;

        var title = this.getRecordTitle(record);

        FlexiBee.MessageBus.publish('status', { busy: true });
        Ext.require('FlexiBee.${it.senchaName}.DetailController', function() {
            me.application.newController('FlexiBee.${it.senchaName}.DetailController', {
                title: title,
                model: record
            });

            FlexiBee.MessageBus.publish('status', { busy: false });
        });
    },

    getRecordTitle: function(record) {
        if (record.data.nazev) {
            return Ext.String.ellipsis(record.data.nazev, 40, true) + ' (${it.evidenceName})';
        } else if (record.data.kod) {
            return Ext.String.ellipsis(record.data.kod, 40, true) + ' (${it.evidenceName})';
        } else {
            return '${it.evidenceName} [' + record.data.id + ']';
        }
    }
});

</#escape>
