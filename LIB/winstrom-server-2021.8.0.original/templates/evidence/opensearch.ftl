<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<?xml version="1.0" encoding="UTF-8"?>
 <OpenSearchDescription xmlns="http://a9.com/-/spec/opensearch/1.1/">
   <ShortName>ABRA Flexi: ${lang('dialogs', it.name, it.name)} - ${it.companyResource.databaseInfo.nazev}</ShortName>
   <Description>ABRA Flexi: ${lang('labels', 'search', 'Hledat')} ${lang('dialogs', it.name, it.name)} - ${it.companyResource.databaseInfo.nazev}</Description>
   <Url type="text/html"
        template="https://${serverHost}/c/${it.companyResource.companyId}/${it.evidenceName}?q={searchTerms}"/>
<#--   <Url type="application/atom+xml"
        template="https://${serverHost}/c/${it.companyResource.companyId}/${it.evidenceName}.xml?q={searchTerms}&amp;format=atom"/>
   <Url type="application/rss+xml"
        template="https://${serverHost}/c/${it.companyResource.companyId}/${it.evidenceName}.xml?q={searchTerms}&amp;format=atom"/> -->

   <Image height="64" width="64" type="image/png">https://${serverHost}${staticPrefix}/img/winstrom-flexibee-icon.png</Image>
   <Image height="16" width="16" type="image/vnd.microsoft.icon">https://${serverHost}${staticPrefix}/img/winstrom.ico</Image>
   <Tags>${it.evidenceName}</Tags>
   <Developer>ABRA Flexi s.r.o.</Developer>
   <OutputEncoding>UTF-8</OutputEncoding>
   <InputEncoding>UTF-8</InputEncoding>
 </OpenSearchDescription>

</#escape>
