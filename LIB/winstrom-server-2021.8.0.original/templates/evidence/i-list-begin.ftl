<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#if it.listParameters?? && it.listParameters.filterRules?exists>
    <div class="alert alert-info" role="alert">
        <a href="/c/${it.companyResource.companyId}/${it.evidenceName}" class="close"><span aria-hidden="true">&times;</span><span class="sr-only">${lang("actions", "zrusitFiltraci", "Zrušit filtraci")}</span></a>
    ${lang("messages", "filtrAplikovan", "Právě je aplikován filtr:")} <strong>${it.localizedFilter}</strong></div>
</#if>

<#include "i-page.ftl" />

<#include "/i-search.ftl" />

<#if it.rowCount?? && (it.rowCount == 0)>
    <h2 style="text-align: center;">${lang('messages', 'no.records', 'Žádné záznamy')}</h2>
</#if>

</#escape>