<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-view.ftl" />

${marker.mark('typOrganizace')}

<#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canShowAtLeastOne(items, object, ['nazev', 'kod'], it)>
    <table class="flexibee-tbl-1">
        <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
        <#if canShowAtLeastOne(items, object, ['nazev'], it)>
        <tr>
            <td><@tool.label name='nazev' items=items />:</td><td><@tool.place object=object name='nazev' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['kod'], it)>
        <tr>
            <td><@tool.label name='kod' items=items />:</td><td><@tool.place object=object name='kod' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canShowAtLeastOne(items, object, ['autoProdl', 'typDoklFak', 'autoZakazka', 'den', 'autoGen', 'mesic', 'autoMail', 'zpusFaktK', 'varSymFakt', 'dnyFakt', 'preplatky', 'genPenale', 'procPenale', 'popis', 'poznam', 'platiOd', 'platiDo'], it)>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canShowAtLeastOne(items, object, ['autoProdl', 'typDoklFak', 'autoZakazka', 'den', 'autoGen', 'mesic', 'autoMail', 'zpusFaktK', 'varSymFakt', 'dnyFakt', 'preplatky', 'genPenale', 'procPenale'], it)>
            <li class="active"><a href="#view-fragment-1" data-toggle="tab">Základní</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
            <li><a href="#view-fragment-2" data-toggle="tab">${lang('labels', 'textyPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['platiOd', 'platiDo'], it)>
            <li><a href="#view-fragment-3" data-toggle="tab">${lang('labels', 'spravaPN')}</a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canShowAtLeastOne(items, object, ['autoProdl', 'typDoklFak', 'autoZakazka', 'den', 'autoGen', 'mesic', 'autoMail', 'zpusFaktK', 'varSymFakt', 'dnyFakt', 'preplatky', 'genPenale', 'procPenale'], it)>
            <div id="view-fragment-1" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['autoProdl', 'typDoklFak', 'autoZakazka', 'den', 'autoGen', 'mesic', 'autoMail', 'zpusFaktK', 'varSymFakt', 'dnyFakt', 'preplatky', 'genPenale', 'procPenale'], it)>
                <table class="flexibee-tbl-1">
                    <#if canShowAtLeastOne(items, object, ['autoProdl', 'typDoklFak'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['autoProdl'], it)>
                        <td><@tool.label name='autoProdl' items=items />:</td><td><@tool.place object=object name='autoProdl' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['typDoklFak'], it)>
                        <td><@tool.label name='typDoklFak' items=items />:</td><td><@tool.place object=object name='typDoklFak' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['autoZakazka', 'den'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['autoZakazka'], it)>
                        <td><@tool.label name='autoZakazka' items=items />:</td><td><@tool.place object=object name='autoZakazka' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['den'], it)>
                        <td><@tool.label name='den' items=items />:</td><td><@tool.place object=object name='den' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['autoGen', 'mesic'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['autoGen'], it)>
                        <td><@tool.label name='autoGen' items=items />:</td><td><@tool.place object=object name='autoGen' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['mesic'], it)>
                        <td><@tool.label name='mesic' items=items />:</td><td><@tool.place object=object name='mesic' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['autoMail', 'zpusFaktK'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['autoMail'], it)>
                        <td><@tool.label name='autoMail' items=items />:</td><td><@tool.place object=object name='autoMail' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['zpusFaktK'], it)>
                        <td><@tool.label name='zpusFaktK' items=items />:</td><td><@tool.place object=object name='zpusFaktK' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['varSymFakt', 'dnyFakt'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['varSymFakt'], it)>
                        <td><@tool.label name='varSymFakt' items=items />:</td><td><@tool.place object=object name='varSymFakt' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['dnyFakt'], it)>
                        <td><@tool.label name='dnyFakt' items=items />:</td><td><@tool.place object=object name='dnyFakt' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['preplatky'], it)>
                    <tr>
                        <td><@tool.label name='preplatky' items=items />:</td><td><@tool.place object=object name='preplatky' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['genPenale', 'procPenale'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['genPenale'], it)>
                        <td><@tool.label name='genPenale' items=items />:</td><td><@tool.place object=object name='genPenale' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['procPenale'], it)>
                        <td><@tool.label name='procPenale' items=items />:</td><td><@tool.place object=object name='procPenale' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
            <div id="view-fragment-2" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
                <table class="flexibee-tbl-1">
                    <#if canShowAtLeastOne(items, object, ['popis'], it)>
                    <tr>
                        <td><@tool.label name='popis' items=items />:<br><@tool.placeArea object=object name='popis' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['poznam'], it)>
                    <tr>
                        <td><@tool.label name='poznam' items=items />:<br><@tool.placeArea object=object name='poznam' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['platiOd', 'platiDo'], it)>
            <div id="view-fragment-3" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['platiOd', 'platiDo'], it)>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <tr>
                        <td colspan="2">Platnost se udává v kalendářních rocích, pro neomezenou platnost zadejte 0-9999</td>
                    </tr>

                    <#if canShowAtLeastOne(items, object, ['platiOd'], it)>
                    <tr>
                        <td><@tool.label name='platiOd' items=items />:</td><td><@tool.place object=object name='platiOd' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['platiDo'], it)>
                    <tr>
                        <td><@tool.label name='platiDo' items=items />:</td><td><@tool.place object=object name='platiDo' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

<@tool.listItems items=items object=object marker=marker />

<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />

<#include "/i-footer-view.ftl" />
</#escape>