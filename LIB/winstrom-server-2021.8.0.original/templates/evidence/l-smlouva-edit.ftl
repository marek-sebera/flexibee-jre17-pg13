<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#macro polozkySmlouvyData object polozkyPropName>[
<#if object[polozkyPropName]??>
<#escape x as x?default("")?js_string?html>
    <#list object[polozkyPropName] as polozka>
    {
        id:             "${polozka.id?c}",
        kod:            "${polozka.kod}",
        nazev:          "${polozka.nazev}",
        <#if polozka.cenik??>
        cenik:          "${polozka.cenik.id?c}",
        cenik$kod:      "${polozka.cenik.kod}",
        </#if>
        <#if polozka.sklad??>
        sklad:          "${polozka.sklad.id?c}",
        sklad$kod:      "${polozka.sklad.kod}",
        </#if>
        mnozMj:         "${polozka.mnozMj?c}",
        cenaMj:         "${polozka.cenaMj?c}",
        cenaCelkem:     "${polozka.cenaCelkem?c}",
        typCenyDphK:    "${polozka.typCenyDphK}",
        typSzbDphK:     "${polozka.typSzbDphK}",
        platiOdData:    "${(polozka.platiOdData.time?date)!}",
        platiDoData:    "${(polozka.platiDoData.time?date)!}",
        frekFakt:       "${(polozka.frekFakt?c)!}",
        datPoslFaktObd: "${(polozka.datPoslFaktObd.time?date)!}",
        cisRad:         "${polozka.cisRad?c}"
    }<#if polozka_has_next>,</#if>
    </#list>
</#escape>
</#if>
]</#macro>

<#macro generateJavaScript it object>

<script type="text/javascript">
    function polozkySmlouvy_renderItem(polozka, editId, deleteId) {
        return '' +
                '<tbody>\n' +
                '    <tr>\n' +
                '        <td>Označení:</td><td>' + (polozka.kod || '') + '</td>\n' +
                '        <td>Název:</td><td>' + (polozka.nazev || '') + '</td>\n' +
                '        <td>Kód z ceníku:</td><td>' + (polozka.cenik$kod || '') + '</td>\n' +
                '        <td><span class="flexibee-js-action flexibee-button" id="' + editId + '" <#if isMobile>data-role="button"</#if>>Upravit<'+'/span><'+'/td>\n' +
                '    </tr>\n' +
                '    <tr>\n' +
                '        <td>Množství:</td><td>' + (polozka.mnozMj || '') + '</td>\n' +
                '        <td>Cena za MJ:</td><td>' + (polozka.cenaMj || '') + '</td>\n' +
                '        <td>Cena celkem:</td><td>' + (polozka.cenaCelkem || '') + '</td>\n' +
                '        <td><span class="flexibee-js-action flexibee-button negative" id="' + deleteId + '" <#if isMobile>data-role="button"</#if>>Smazat<'+'/span><'+'/td>\n' +
                '    </tr>' +
                '</tbody>';
    }

    function polozkySmlouvy_postConvertForm2Json(form, json, jsonFromServer) {
        if ($("#flexibee-inp-polozkySmlouvy__cenik-flexbox_input", form).length) {
            json.cenik$kod = $("#flexibee-inp-polozkySmlouvy__cenik-flexbox_input", form).val().replace(/:.+?$/, "");
        } else {
            json.cenik$kod = $("#flexibee-inp-polozkySmlouvy__cenik :selected", form).text();
        }

        if ($("#flexibee-inp-polozkySmlouvy__sklad-flexbox_input", form).length) {
            json.sklad$kod = $("#flexibee-inp-polozkySmlouvy__sklad-flexbox_input", form).val().replace(/:.+?$/, "");
        } else {
            json.sklad$kod = $("#flexibee-inp-polozkySmlouvy__sklad :selected", form).text();
        }
    }

    function polozkySmlouvy_postConvertJson2Form(json, form) {
        if (json.cenik$kod && $("#flexibee-inp-polozkySmlouvy__cenik-flexbox_input", form).length) {
            $("#flexibee-inp-polozkySmlouvy__cenik-flexbox_input", form).val(json.cenik$kod).removeClass('watermark');
        }

        if (json.sklad$kod && $("#flexibee-inp-polozkySmlouvy__sklad-flexbox_input", form).length) {
            $("#flexibee-inp-polozkySmlouvy__sklad-flexbox_input", form).val(json.sklad$kod).removeClass('watermark');
        }
    }

    $(function() {
        <#if object.polozkySmlouvyInt??>
            $("#flexibee-smlouva-polozky").show();
        </#if>
        <#if object.polozkySmlouvyExt??>
            $("#flexibee-smlouva-polozky-ext").show();
        </#if>

        <#assign polozkyIt = it.getSubEvidence("smlouva-polozka") />

        var items = new flexibee.ItemsHelper({
            evidence: {
                name: "${polozkyIt.evidenceResource.evidenceName}",
                props: <@edit.propsTypes restActionHelper=polozkyIt/>,
                prefix: "polozkySmlouvy__"
            },
            page: {
                list: $("#flexibee-smlouva-polozky-list"),
                edit: $("#flexibee-smlouva-polozky-edit"),
                activateList: function() { $("#flexibee-smlouva-polozky ul.ui-tabs-nav li:eq(0) a").click(); },
                activateEdit: function() { $("#flexibee-smlouva-polozky ul.ui-tabs-nav li:eq(1) a").click(); },
                changeEditTitle: function(title) { $("#flexibee-smlouva-polozky ul.ui-tabs-nav li:eq(1) a span").text(title); }
            },
            data: {
                renderItem: polozkySmlouvy_renderItem,
                postConvertForm2Json: polozkySmlouvy_postConvertForm2Json,
                postConvertJson2Form: polozkySmlouvy_postConvertJson2Form,
                itemTitle: function(item) { return item.kod; },
                initialData: <@polozkySmlouvyData object "polozkySmlouvyInt"/>
            }
        });

        var extItems = new flexibee.ItemsHelper({
            evidence: {
                name: "${polozkyIt.evidenceResource.evidenceName}",
                props: <@edit.propsTypes restActionHelper=polozkyIt/>,
                prefix: "polozkySmlouvyExt__"
            },
            page: {
                list: $("#flexibee-smlouva-polozky-ext-list"),
                edit: $("#flexibee-smlouva-polozky-ext-edit"),
                activateList: function() { $("#flexibee-smlouva-polozky-ext ul.ui-tabs-nav li:eq(0) a").click(); },
                activateEdit: function() { $("#flexibee-smlouva-polozky-ext ul.ui-tabs-nav li:eq(1) a").click(); },
                changeEditTitle: function(title) { $("#flexibee-smlouva-polozky-ext ul.ui-tabs-nav li:eq(1) a span").text(title); }
            },
            data: {
                renderItem: polozkySmlouvy_renderItem,
                postConvertForm2Json: polozkySmlouvy_postConvertForm2Json,
                postConvertJson2Form: polozkySmlouvy_postConvertJson2Form,
                itemTitle: function(item) { return item.kod; },
                initialData: <@polozkySmlouvyData object "polozkySmlouvyExt"/>
            }
        });

        // ---

        var defaults = {
            <#if object.id?? && object.id != -1>
            id: "${object.id?c}"
            </#if>
        };

        var form = new flexibee.FormHelper({
            evidence: {
                name: "${it.evidenceResource.evidenceName}",
                url: "${it.evidenceResource.listURI}",
                props: <@edit.propsTypes restActionHelper=it/>,
                filledProps: <@edit.filledProps object=object restActionHelper=it/>,
                defaults: defaults
            },
            page: {
                form: $("#flexibee-smlouva-hlavicka")
            },
            items: {
                polozkySmlouvy:    items,
                polozkySmlouvyExt: extItems
            },
            csrfToken: "${csrfToken}"
        });
        form.start();
    });
</script>

</#macro>

</#escape>