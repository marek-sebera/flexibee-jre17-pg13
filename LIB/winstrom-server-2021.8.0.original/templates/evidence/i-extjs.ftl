<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
    var winstrom${it.evidenceResource.tagName}Fields = [
<#list it.devDocItemsForHtml as item>        {    name: '${item.propertyName}', type: <#if item.type == 'logic'>'boolean'<#elseif item.type == 'numeric'>'int'<#elseif item.type == 'money'>'float'<#elseif item.type == 'datetime'>'date'<#elseif item.type == 'relation'>'auto'<#elseif item.type == 'select'>'auto'<#else>'string'</#if>,
<#if item.type == 'datetime'>            dateFormat: 'Y-m-dTh:i:s',</#if><#if item.type == 'date'>dateFormat: 'Y-m-d',</#if>},
</#list>            {    name    : 'id'    }
    ];

    var winstrom${it.evidenceResource.tagName}Columns = [
    <#list it.devDocItemsForHtml as item>
        <#if item.showToUser>
            {
                id              : '${item.propertyName}',
                header          : "${item.name}",
                dataIndex       : '${item.propertyName}',
                sortable        : <#if item.isSortable>true<#else>false</#if>,
                hidden          : <#if item.isVisible>false<#else>true</#if>,
                <#if item.decimal?exists         >decimalPrecision: '${item.decimal}',
                </#if><#if item.maxLength?exists >maxLength       : ${item. maxLength},
                </#if><#if item.minLength?exists >minLength       : ${item. minLength},
                </#if><#if item.minValue?exists  >minValue        : '${item.minValue}',
                </#if><#if item.maxValue?exists  >maxValue        : '${item.maxValue}',
                </#if><#if item.mandatory        >allowBlank      : false,
                </#if><#if item.width?exists     >width           : '${item.width}',
                </#if><#if item.type == 'logic'  >renderer        : renderBoolean,
                <#elseif item.type == 'numeric'  >renderer        : renderMoney,
                <#elseif item.type == 'date'     >renderer        : renderDate,
                <#elseif item.type == 'datetime' >renderer        : renderDateTime,
                <#elseif item.type == 'select'   >renderer        : renderSelect,
                <#elseif item.type == 'relation' >renderer        : renderRelation,
                </#if><#if item.type == 'select' >possibleValues  : {<#list item.possibleValues as val>
                    '${val.key}': '${val.name}',</#list>
                },
                </#if><#if item.type == 'string' >editor          : new Ext.form.TextField({}),
                </#if><#if item.type == 'date'   >editor          : new Ext.form.DateField({}),
                </#if><#if item.type == 'integer'>editor          : new Ext.form.NumberField({allowDecimals: false}),
                </#if><#if item.type == 'numeric'>editor          : new Ext.form.NumberField({allowDecimals: true}),
                </#if><#if item.type == 'logic'  >editor          : new Ext.form.Checkbox({}),
                </#if><#if item.type == 'select' >editor          : new Ext.form.ComboBox({typeAhead: true}),</#if>
            },
        </#if></#list>
    ];
</#escape>
