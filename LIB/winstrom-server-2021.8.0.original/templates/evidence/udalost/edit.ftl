<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>


    ${marker.mark('stitky')}


<@edit.beginForm />
<#if canShowAtLeastOne(items, object, ['typAkt'])>
    ${marker.mark('druhUdalK')}
</#if>


    <#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canEditAtLeastOne(items, object, ['druhUdalK', 'typAkt', 'zodpPrac', 'firma', 'zakazka', 'kontakt', 'zahajeni', 'volno', 'dokonceni', 'celodenni', 'stavUdalK', 'termin', 'predmet', 'umisteni', 'prioritaK'])>
    <table class="flexibee-tbl-1">
        <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
        <#if canEditAtLeastOne(items, object, ['druhUdalK'])>
        <tr>
            <td><@edit.label name='druhUdalK' items=items /></td><td colspan="3"><@edit.place object=object name='druhUdalK' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['typAkt', 'zodpPrac'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['typAkt'])>
            <td><@edit.label name='typAkt' items=items /></td><td><@edit.place object=object name='typAkt' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['zodpPrac'])>
            <td><@edit.label name='zodpPrac' items=items /></td><td><@edit.place object=object name='zodpPrac' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['firma', 'zakazka'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['firma'])>
            <td><@edit.label name='firma' items=items /></td><td><@edit.place object=object name='firma' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['zakazka'])>
            <td><@edit.label name='zakazka' items=items /></td><td><@edit.place object=object name='zakazka' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['kontakt'])>
        <tr>
            <td><@edit.label name='kontakt' items=items /></td><td><@edit.place object=object name='kontakt' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['zahajeni', 'volno'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['zahajeni'])>
            <td><@edit.label name='zahajeni' items=items /></td><td><@edit.place object=object name='zahajeni' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['volno'])>
            <td><@edit.label name='volno' items=items /></td><td><@edit.place object=object name='volno' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['dokonceni'])>
        <tr>
            <td><@edit.label name='dokonceni' items=items /></td><td><@edit.place object=object name='dokonceni' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['celodenni'])>
        <tr>
            <td><@edit.label name='celodenni' items=items /></td><td><@edit.place object=object name='celodenni' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['stavUdalK', 'termin'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['stavUdalK'])>
            <td><@edit.label name='stavUdalK' items=items /></td><td><@edit.place object=object name='stavUdalK' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['termin'])>
            <td><@edit.label name='termin' items=items /></td><td><@edit.place object=object name='termin' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['predmet'])>
        <tr>
            <td><@edit.label name='predmet' items=items level='0' /></td><td colspan="3"><@edit.place object=object name='predmet' items=items marker=marker  level='0'/></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['umisteni'])>
        <tr>
            <td><@edit.label name='umisteni' items=items /></td><td colspan="3"><@edit.place object=object name='umisteni' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['prioritaK'])>
        <tr>
            <td><@edit.label name='prioritaK' items=items /></td><td colspan="3"><@edit.place object=object name='prioritaK' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canEditAtLeastOne(items, object, ['popis', 'cenik', 'doklSklad', 'majetek', 'poznam'])>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canEditAtLeastOne(items, object, ['popis'])>
            <li class="active"><a href="#edit-fragment-1" data-toggle="tab"><span>Popis</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['cenik', 'doklSklad', 'majetek'])>
            <li><a href="#edit-fragment-2" data-toggle="tab"><span>Relace</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['poznam'])>
            <li><a href="#edit-fragment-3" data-toggle="tab"><span>${lang('labels', 'textyPN')}</span></a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canEditAtLeastOne(items, object, ['popis'])>
            <div id="edit-fragment-1" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['popis'])>
                <table class="flexibee-tbl-1">
                    <#if canEditAtLeastOne(items, object, ['popis'])>
                    <tr>
                        <td><@edit.label name='popis' items=items /><br><@edit.textarea object=object name='popis' items=items marker=marker rows=8 /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['cenik', 'doklSklad', 'majetek'])>
            <div id="edit-fragment-2" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['cenik', 'doklSklad', 'majetek'])>
                <table class="flexibee-tbl-1">
                    <#if canEditAtLeastOne(items, object, ['cenik'])>
                    <tr>
                        <td><@edit.label name='cenik' items=items /></td><td><@edit.place object=object name='cenik' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['doklSklad'])>
                    <tr>
                        <td><@edit.label name='doklSklad' items=items /></td><td><@edit.place object=object name='doklSklad' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['majetek'])>
                    <tr>
                        <td><@edit.label name='majetek' items=items /></td><td><@edit.place object=object name='majetek' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['poznam'])>
            <div id="edit-fragment-3" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['poznam'])>
                <table class="flexibee-tbl-1">
                    <#if canEditAtLeastOne(items, object, ['poznam'])>
                    <tr>
                        <td><@edit.label name='poznam' items=items /><br><@edit.textarea object=object name='poznam' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

    <br/>
    <@edit.listItems items=items object=object marker=marker />

    <@edit.saveButton />
<@edit.endForm />

<@edit.generateNaiveJavascript object=object it=it />

<#include "/parts/i-dalsi-informace.ftl" />

<#include "/i-footer-edit.ftl" />
</#escape>
