<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>

${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}
${marker.mark('modul')}

<@edit.beginForm />
    <#--AUTOGEN BEGIN-->
    <div id="flexibee-rada-hlavicka">

    <div class="flexibee-table-border">
    <#if canEditAtLeastOne(items, object, ['kod', 'nazev'])>
    <table class="flexibee-tbl-1">
        <col width="20%" /><col width="80%" />
        <#if canEditAtLeastOne(items, object, ['kod'])>
        <tr>
            <td><@edit.label name='kod' items=items /></td><td><@edit.place object=object name='kod' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['nazev'])>
        <tr>
            <td><@edit.label name='nazev' items=items /></td><td><@edit.place object=object name='nazev' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canEditAtLeastOne(items, object, ['popis', 'poznam', 'platiOd', 'platiDo'])>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <li class="active"><a href="#edit-fragment-1" data-toggle="tab"><span>Texty</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
            <li><a href="#edit-fragment-2" data-toggle="tab"><span>${lang('labels', 'spravaPN')}</span></a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <div id="edit-fragment-1" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
                <table class="flexibee-tbl-1">
                    <#if canEditAtLeastOne(items, object, ['popis'])>
                    <tr>
                        <td><@edit.label name='popis' items=items /><br><@edit.textarea object=object name='popis' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['poznam'])>
                    <tr>
                        <td><@edit.label name='poznam' items=items /><br><@edit.textarea object=object name='poznam' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
            <div id="edit-fragment-2" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <tr>
                        <td colspan="2">Platnost se udává v kalendářních rocích, pro neomezenou platnost zadejte 0-9999</td>
                    </tr>

                    <#if canEditAtLeastOne(items, object, ['platiOd'])>
                    <tr>
                        <td><@edit.label name='platiOd' items=items /></td><td><@edit.place object=object name='platiOd' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['platiDo'])>
                    <tr>
                        <td><@edit.label name='platiDo' items=items /></td><td><@edit.place object=object name='platiDo' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

    </div>

    <#assign oldIt=it, oldItems=items, oldObject=object, oldMarker=marker />
    <#global it=it.getSubEvidence("rocni-rada") />
    <#assign items=it.editHandler.propertiesMap, object=it.editHandler.object, marker=newMarker() />

    <div id="flexibee-rada-polozky" style="display:none">

    <br><br>

    <#if canEditAtLeastOne(items, object, ['cisAkt', 'prefix', 'cisPoc', 'postfix', 'cisDelka', 'ucetObdobi', 'zobrazNuly'])>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <li class="active"><a href="#edit-fragment-3" data-toggle="tab"><span>Roční položky číselné řady</span></a></li>

            <#if canEditAtLeastOne(items, object, ['cisAkt', 'prefix', 'cisPoc', 'postfix', 'cisDelka', 'ucetObdobi', 'zobrazNuly'])>
            <li><a href="#edit-fragment-4" data-toggle="tab"><span>+</span></a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <div id="edit-fragment-3" class="tab-pane active">
                <div id="flexibee-rada-polozky-list" class="flexibee-table-border">
                      <table class="table">
                        <col width="12.5%"/><col width="12.5%"/>
                        <col width="12.5%"/><col width="12.5%"/>
                        <col width="12.5%"/><col width="12.5%"/>
                        <col width="12.5%"/><col width="12.5%"/>
                      </table>
                    </div>

            </div>

            <#if canEditAtLeastOne(items, object, ['cisAkt', 'prefix', 'cisPoc', 'postfix', 'cisDelka', 'ucetObdobi', 'zobrazNuly'])>
            <div id="edit-fragment-4" class="tab-pane">
                <div id="flexibee-rada-polozky-edit">

                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['cisAkt', 'prefix', 'cisPoc', 'postfix', 'cisDelka', 'ucetObdobi', 'zobrazNuly'])>
                <table class="flexibee-tbl-1">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canEditAtLeastOne(items, object, ['cisAkt', 'prefix'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['cisAkt'])>
                        <td><@edit.label name='cisAkt' items=items prefix="polozkyRady__" /></td><td><@edit.place object=object name='cisAkt' items=items marker=marker prefix="polozkyRady__" /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['prefix'])>
                        <td><@edit.label name='prefix' items=items prefix="polozkyRady__" /></td><td><@edit.place object=object name='prefix' items=items marker=marker prefix="polozkyRady__" /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['cisPoc', 'postfix'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['cisPoc'])>
                        <td><@edit.label name='cisPoc' items=items prefix="polozkyRady__" /></td><td><@edit.place object=object name='cisPoc' items=items marker=marker prefix="polozkyRady__" /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['postfix'])>
                        <td><@edit.label name='postfix' items=items prefix="polozkyRady__" /></td><td><@edit.place object=object name='postfix' items=items marker=marker prefix="polozkyRady__" /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['cisDelka', 'ucetObdobi'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['cisDelka'])>
                        <td><@edit.label name='cisDelka' items=items prefix="polozkyRady__" /></td><td><@edit.place object=object name='cisDelka' items=items marker=marker prefix="polozkyRady__" /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['ucetObdobi'])>
                        <td><@edit.label name='ucetObdobi' items=items prefix="polozkyRady__" /></td><td><@edit.place object=object name='ucetObdobi' items=items marker=marker prefix="polozkyRady__" /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['zobrazNuly'])>
                    <tr>
                        <td><@edit.label name='zobrazNuly' items=items prefix="polozkyRady__" /></td><td><@edit.place object=object name='zobrazNuly' items=items marker=marker prefix="polozkyRady__" /></td>
                    </tr>
                    </#if>
                    <tr>
                        <td>Náhled:</td>
                        <#if isMobile></tr><tr></#if>
                        <td><strong><span id="flexibee-inp-polozkyRady__preview"></strong></td>
                    </tr>

                </table>
                </#if>
                </div>

                <div class="flexibee-buttons" <#if isMobile>data-role="controlgroup"  data-type="horizontal"</#if>>
                        <input type="button" name="ok" value="OK" class="flexibee-button" id="flexibee-rada-polozky-edit-ok" />
                        <input type="button" name="cancel" value="Zrušit" class="flexibee-button negative" id="flexibee-rada-polozky-edit-cancel" />
                      </div>
                    </div>

            </div>
            </#if>
        </div>
    </#if>

    </div>

    <#-- uklizení -->
    <#global it=oldIt />
    <#assign items=oldItems, object=oldObject, marker=oldMarker />

<#--AUTOGEN END-->

    <br/>
    <@edit.listItems items=items object=object marker=marker />

    <@edit.saveButton />
<@edit.endForm />

<#import "/evidence/l-rada-edit.ftl" as radaEdit />
<@radaEdit.generateJavaScript it=it object=object polozkyPropName="polozkyRady" />

<#include "/parts/i-dalsi-informace.ftl" />

<#include "/i-footer-edit.ftl" />
</#escape>
