<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

<#--
    <#list it.evidenceResource.getToolBars().list as item>
    <#if item.actionId != 'help'>
        {
            id: '${item.actionId}',
            name: '${item.actionName}',
            icon: '${staticPrefix}/img/skin/${skin}/icons/${item.actionId}.png',
            service: ${(item.isService == 'YES')?string},
            <#if item.actionId != 'pdf'>
            <#if item.prefixUrl??>
            url: '${item.prefixUrl}%%%${item.suffixUrl!""}',
            <#else>
            url: '${item.url}',
            </#if>
            download: ${item.download?string},
            <#else>
            items: [
            <#list it.evidenceResource.reports.list as report>
                {
                    id: 'pdf-${report.reportId}',
                    name: '${report.reportName}',
                    <#if item.prefixUrl??>
                    url: '${item.prefixUrl}%%%${item.suffixUrl!""}<#if item.urlHasQuery>&<#else>?</#if>report-name=${report.reportId}',
                    <#else>
                    url: '${item.url}<#if item.urlHasQuery>&<#else>?</#if>report-name=${report.reportId}',
                    </#if>
                    download: true
                }<#if report_has_next>,</#if>
            </#list>
            ],
            </#if>
            makesSense: {
                forList: ${item.actionMakesSense.forList?string},
                forRecordInList: ${item.actionMakesSense.forRecordInList?string},
                forDetailNew: ${item.actionMakesSense.forDetailNew?string},
                forDetailExisting: ${item.actionMakesSense.forDetailExisting?string}
            }
        }<#if item_has_next>,</#if>
    </#if>
    </#list>
-->

<#--
    services: {
        id: 'services',
        name: '${lang('actions', 'sluzby', 'Služby')}'
    },

    help: {
        id: 'help',
        name: '${lang('actions', 'contextHelp', 'Nápověda')}'
    },
-->

Ext.define('FlexiBee.${it.senchaName}.ToolbarStructure', {
    basicOperations: function() {
        <#if it.evidenceResource.allowToolbar>
        return [
            {
                id: 'new',
                name: '${lang('actions', 'add', 'Přidat')}',
                show: function(ctx) {
                    return ctx.isList;
                }
            },
            {
                id: 'copy',
                name: '${lang('actions', 'newWithCopy', 'Vytvořit kopii')}',
                show: function(ctx) {
                    return ctx.isRecordInList;
                },
                handler: {
                    type: 'custom',
                    action: function() {
                        window.alert('Not yet implemented!');
                    }
                }
            },
            {
                id: 'edit',
                name: '${lang('actions', 'edit', 'Změnit')}',
                show: function(ctx) {
                    return ctx.isRecordInList;
                }
            },
            {
                id: 'delete',
                name: '${lang('actions', 'delete', 'Smazat')}',
                show: function(ctx) {
                    return ctx.isRecordInList || ctx.isDetailExisting;
                }
            }
        ];
        <#else>
        return [];
        </#if>
    },

    outputFormats: function() {
        <#if it.evidenceResource.allowToolbar>
        return [
            <#assign reports = it.evidenceResource.reports.list/>
            <#if it.evidenceResource.hasReports && reports?size &gt; 0>
            {
                id: 'pdf-now',
                name: 'PDF',
                handler: {
                    type: 'download',
                    url: '/c/${it.companyId}/${it.tagName}%%%.pdf?limit=0'
                },
                show: function(ctx) {
                    return ctx.isList || ctx.isRecordInList || ctx.isDetailExisting;
                }
            }<#if reports?size &gt; 1>,</#if>
            <#if reports?size &gt; 1>
            {
                id: 'pdf',
                name: 'Sestavy',
                show: function(ctx) {
                    return ctx.isList || ctx.isRecordInList || ctx.isDetailExisting;
                },
                menu: [
                <#list reports as report>
                    {
                        id: 'pdf-${report.reportId}',
                        name: '${report.reportName}',
                        icon: false,
                        handler: {
                            type: 'download',
                            url: '/c/${it.companyId}/${it.tagName}%%%.pdf?limit=0&report-name=${report.reportId}'
                        }
                    }<#if report_has_next>,</#if>
                </#list>
                ]
            }
            </#if>
            </#if>
        ];
        <#else>
        return [];
        </#if>
    },

    services: function() {
        return [];
    },

    itemsMap: {},

    constructor: function() {
        var me = this;
        me.items = me.basicOperations().concat(me.outputFormats()).concat(me.services());
        Ext.iterate(me.items, function(item) {
            me.itemsMap[item.id] = item;
        });
    },

    _asToolbarItem: function(item, record, level) {
        var me = this;

        var result = {
            itemId: item.id,
            text: item.name
        };
        if (level === 0) {
            Ext.apply(result, {
                xtype: 'button',
                scale: 'large'
            });
        }

        if (item.icon !== false) { // explicitly setting to 'false' means 'no icon'
            if (item.icon === true || item.icon === null || item.icon === undefined) { // infer from 'id'
                result.icon = '${staticPrefix}/img/skin/${skin}/icons/' + item.id + '.png';
            } else { // otherwise, there has to be full URL to the icon
                result.icon = item.icon;
            }
        }

        if (item.handler) {
            switch (item.handler.type) {
                case 'download':
                    result.href = item.handler.url;
                    result.hrefTarget = '_blank';
                    break;
                case 'custom':
                    result.handler = function() { // TODO check for leaks here
                        item.handler.action(record);
                    };
                    break;
            }
        }

        if (item.menu) {
            result.menu = [];
            Ext.iterate(item.menu, function(subItem) {
                result.menu.push(me._asToolbarItem(subItem, level + 1));
            });
        }

        return result;
    },

    _replaceTokenByRecordId: function(toolbarItem, recordId) {
        var me = this;

        if (toolbarItem.href) {
            var replacement = "";
            if (recordId) {
                replacement = "/" + recordId;
            }

            toolbarItem.href = toolbarItem.href.replace("%%%", replacement);
        }

        if (toolbarItem.menu) {
            Ext.iterate(toolbarItem.menu, function(subItem) {
                me._replaceTokenByRecordId(subItem, recordId);
            });
        }
    },

    asToolbarItems: function(record, makesSenseContext) {
        var me = this;

        var toolbarItems = [];
        Ext.iterate(me.items, function(item) {
            var ok = true;
            if (item.show) {
                ok = item.show(makesSenseContext);
            }

            if (ok) {
                var toolbarItem = me._asToolbarItem(item, record, 0);
                if (toolbarItem) {
                    // if it makes sense both for list and selected record, list has priority
                    var recordId = null;
                    if (record && !makesSenseContext.isList) {
                        recordId = record.id;
                    }

                    me._replaceTokenByRecordId(toolbarItem, makesSenseContext.isList ? null : recordId);
                    toolbarItems.push(toolbarItem);
                }
            }
        });

        return toolbarItems;
    }
});

Ext.define('FlexiBee.${it.senchaName}.Toolbar', {
    extend: 'Ext.toolbar.Toolbar',
    alias: 'widget.fbToolbar${it.senchaName?cap_first}',

    requires: [
    ],

    layout: {
        overflowHandler: 'Menu'
    },

    items: [],

    isList: false,
    isDetail: false,

    structure: Ext.create('FlexiBee.${it.senchaName}.ToolbarStructure'),

    initComponent: function() {
        this.callParent(arguments);

        FlexiBee.Plugins.invoke(this, 'modifyToolbar', this.structure.items);

        this.buildItems(null);
    },

    buildItems: function(record) {
        var recordId = null;
        if (record && record.id) {
            recordId = record.id;
        }

        var makesSenseContext = {};
        if (this.isList) {
            makesSenseContext.isList = true;
            if (recordId) {
                makesSenseContext.isRecordInList = true;
            }
        } else if (this.isDetail) {
            if (recordId) {
                makesSenseContext.isDetailExisting = true;
            } else {
                makesSenseContext.isDetailNew = true;
            }
        }

        var items = this.structure.asToolbarItems(record, makesSenseContext);

        if (items.length == 0) {
            this.hide();
        } else {
            this.show();
            this.add(items);
        }
    },

    loadRecord: function(model) {
        this.removeAll();
        this.buildItems(model.data);
    }
});

<#-- TODO plugin example -->
<#import "/l-sencha.ftl" as sencha>
<@sencha.includePlugins path="${it.senchaName}.Toolbar"/>

</#escape>
