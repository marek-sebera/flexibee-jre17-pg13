<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-view.ftl" />

<#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canShowAtLeastOne(items, object, ['cenikOtec', 'cenik', 'mnozMj', 'poznam'], it)>
    <table class="flexibee-tbl-1">
        <col width="20%" /><col width="80%" />
        <#if canShowAtLeastOne(items, object, ['cenikOtec'], it)>
        <tr>
            <td><@tool.label name='cenikOtec' items=items />:</td><td colspan="3"><@tool.place object=object name='cenikOtec' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['cenik'], it)>
        <tr>
            <td><@tool.label name='cenik' items=items />:</td><td colspan="3"><@tool.place object=object name='cenik' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['mnozMj'], it)>
        <tr>
            <td><@tool.label name='mnozMj' items=items />:</td><td colspan="3"><@tool.place object=object name='mnozMj' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['poznam'], it)>
        <tr>
            <td colspan="4"><@tool.label name='poznam' items=items />:<br><@tool.placeArea object=object name='poznam' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

<#--AUTOGEN END-->

<@tool.listItems items=items object=object marker=marker />

<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />

<#include "/i-footer-view.ftl" />
</#escape>