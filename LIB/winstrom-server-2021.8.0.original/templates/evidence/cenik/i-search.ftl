<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#if !isSlowMobile>
<div id="flexibee-search-cenik" class="flexibee-search" style="display:none">
    <form class="form-inline">
        <div class="input-group">
            <div id="flexibee-search-cenik-flexbox"></div>
            <div class="input-group-btn">
                <button type="submit" class="btn btn-default btn-sm" title="${lang('labels', 'search', 'Hledat')} ${lang('global', 'cenik', 'Ceník')}"><span class="glyphicon glyphicon-search"></span></button>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    <#assign cenikUrl = formListUrl(it.companyResource, 'CENIK')!"" />
    function initSearchCenikFlexBox(url) {
        $('#flexibee-search-cenik-flexbox').flexbox(url + '.json?mode=suggest', {
            showArrow: false,
            autoCompleteFirstMatch: false,
            watermark: "${lang('labels', 'search', 'Hledat')} ${lang('global', 'cenik', 'Ceník')}",
            noResultsText: '${lang('messages', 'suggest.nothingFound', 'Nic nenalezeno')}',
            paging: {
                style: 'links',
                summaryTemplate: '${lang('messages', 'suggest.summary', '{start} až {end} z celkem {total}')}'
            },
            onSelect: function() {
                window.location = url + "/" + this.getAttribute('hiddenValue');
            },
            inputClass: "form-control input-sm"
        });
    }
    $(function() {
        setTimeout(function() {
            initSearchCenikFlexBox('${cenikUrl}');
        }, 100);

        $("#flexibee-search-cenik form").submit(function() {
            var query = $("#flexibee-search-cenik-flexbox_input").val();
            window.location = '${cenikUrl}?q=' + encodeURIComponent(query);
            return false;
        });
        $("#flexibee-search-cenik").show();
    });
</script>
</#if>

</#escape>