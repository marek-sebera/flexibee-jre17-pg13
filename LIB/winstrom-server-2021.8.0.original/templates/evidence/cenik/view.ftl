<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-view.ftl" />

${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}
${marker.mark('popisA')}
${marker.mark('popisB')}
${marker.mark('popisC')}
${marker.mark('stitky')}
${marker.mark('exportNaEshop')}

<#-- odpovídá základnímu ceníku -->
${marker.mark('procZakl')}
${marker.mark('individCena')}
${marker.mark('limMnoz2')}
${marker.mark('limMnoz3')}
${marker.mark('limMnoz4')}
${marker.mark('limMnoz5')}
${marker.mark('procento2')}
${marker.mark('procento3')}
${marker.mark('procento4')}
${marker.mark('procento5')}
${marker.mark('cena2')}
${marker.mark('cena3')}
${marker.mark('cena4')}
${marker.mark('cena5')}
${marker.mark('zaokrJakK')}
${marker.mark('zaokrNaK')}
${marker.mark('cenJednotka')}
${marker.mark('typCenyVychoziK')}
${marker.mark('typVypCenyK')}
${marker.mark('typCenyVychozi25K')}
${marker.mark('typVypCeny25K')}
${marker.mark('evidVyrCis')}
${marker.mark('mjKoef2')}
${marker.mark('mjKoef3')}
${marker.mark('prodejMj')}
${marker.mark('zatrid')}
${marker.mark('baleniNazev1')}
${marker.mark('baleniNazev2')}
${marker.mark('baleniNazev3')}
${marker.mark('baleniNazev4')}
${marker.mark('baleniNazev5')}
${marker.mark('baleniMj1')}
${marker.mark('baleniMj2')}
${marker.mark('baleniMj3')}
${marker.mark('baleniMj4')}
${marker.mark('baleniMj5')}
${marker.mark('baleniEan1')}
${marker.mark('baleniEan2')}
${marker.mark('baleniEan3')}
${marker.mark('baleniEan4')}
${marker.mark('baleniEan5')}
${marker.mark('cenaBezna')}
${marker.mark('mj2')}
${marker.mark('mj3')}
${marker.mark('vyrobce')}

<#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canShowAtLeastOne(items, object, ['nazev', 'kod', 'typZasobyK', 'cenaZakl', 'cenaZaklBezDph', 'cenaZaklVcDph', 'typCenyDphK', 'skladove', 'typSzbDphK', 'skupZboz', 'sumStavMj', 'sumRezerMj'], it)>
    <table class="flexibee-tbl-1">
        <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
        <#if canShowAtLeastOne(items, object, ['nazev'], it)>
        <tr>
            <td><@tool.label name='nazev' items=items level='0'/>:</td><td colspan="3"><@tool.place object=object level='0' name='nazev' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['kod', 'typZasobyK'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['kod'], it)>
            <td><@tool.label name='kod' items=items level='2'/>:</td><td><@tool.place object=object level='2' name='kod' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['typZasobyK'], it)>
            <td><@tool.label name='typZasobyK' items=items />:</td><td><@tool.place object=object name='typZasobyK' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['cenaZakl'], it)>
        <tr>
            <td><@tool.label name='cenaZakl' items=items />:</td><td colspan="3"><@tool.place object=object name='cenaZakl' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['cenaZaklBezDph', 'cenaZaklVcDph'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['cenaZaklBezDph'], it)>
            <td><@tool.label name='cenaZaklBezDph' items=items level='0'/>:</td><td><@tool.place object=object level='0' name='cenaZaklBezDph' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['cenaZaklVcDph'], it)>
            <td><@tool.label name='cenaZaklVcDph' items=items level='0'/>:</td><td><@tool.place object=object level='0' name='cenaZaklVcDph' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['typCenyDphK', 'skladove'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['typCenyDphK'], it)>
            <td><@tool.label name='typCenyDphK' items=items />:</td><td><@tool.place object=object name='typCenyDphK' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['skladove'], it)>
            <td><@tool.label name='skladove' items=items />:</td><td><@tool.place object=object name='skladove' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['typSzbDphK', 'skupZboz'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['typSzbDphK'], it)>
            <td><@tool.label name='typSzbDphK' items=items />:</td><td><@tool.place object=object name='typSzbDphK' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['skupZboz'], it)>
            <td><@tool.label name='skupZboz' items=items />:</td><td><@tool.place object=object name='skupZboz' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['sumStavMj', 'sumRezerMj'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['sumStavMj'], it)>
            <td><@tool.label name='sumStavMj' items=items />:</td><td><@tool.place object=object name='sumStavMj' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['sumRezerMj'], it)>
            <td><@tool.label name='sumRezerMj' items=items />:</td><td><@tool.place object=object name='sumRezerMj' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canShowAtLeastOne(items, object, ['popis', 'poznam', 'mj1', 'desetinMj', 'nakupCena', 'dodavatel', 'eanKod', 'kodPlu', 'zaruka', 'mjZarukyK', 'hmotMj', 'mjHmot', 'hmotObal', 'objem', 'mjObj', 'exportNaEshop', 'kratkyPopis', 'klicSlova', 'techParam', 'dodaciLhuta', 'mjDodaciLhuta', 'inEvid', 'inKoefMj', 'inKoefStat', 'inKodSled', 'nomen', 'stat', 'platiOd', 'platiDo'], it)>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
            <li class="active"><a href="#view-fragment-1" data-toggle="tab">${lang('labels', 'textyPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['mj1', 'desetinMj', 'nakupCena', 'dodavatel', 'eanKod', 'kodPlu', 'zaruka', 'mjZarukyK', 'hmotMj', 'mjHmot', 'hmotObal', 'objem', 'mjObj'], it)>
            <li><a href="#view-fragment-2" data-toggle="tab">${lang('labels', 'upresneniPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['exportNaEshop', 'kratkyPopis', 'klicSlova', 'techParam', 'dodaciLhuta', 'mjDodaciLhuta'], it)>
            <li><a href="#view-fragment-3" data-toggle="tab">${lang('labels', 'interObchPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['inEvid', 'inKoefMj', 'inKoefStat', 'inKodSled', 'nomen', 'stat'], it)>
            <li><a href="#view-fragment-4" data-toggle="tab">${lang('labels', 'intrastatPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['platiOd', 'platiDo'], it)>
            <li><a href="#view-fragment-5" data-toggle="tab">${lang('labels', 'spravaPN')}</a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
            <div id="view-fragment-1" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
                <table class="flexibee-tbl-1">
                    <#if canShowAtLeastOne(items, object, ['popis'], it)>
                    <tr>
                        <td><@tool.label name='popis' items=items />:<br><@tool.placeArea object=object name='popis' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['poznam'], it)>
                    <tr>
                        <td><@tool.label name='poznam' items=items />:<br><@tool.placeArea object=object name='poznam' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['mj1', 'desetinMj', 'nakupCena', 'dodavatel', 'eanKod', 'kodPlu', 'zaruka', 'mjZarukyK', 'hmotMj', 'mjHmot', 'hmotObal', 'objem', 'mjObj'], it)>
            <div id="view-fragment-2" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['mj1', 'desetinMj', 'nakupCena', 'dodavatel', 'eanKod', 'kodPlu', 'zaruka', 'mjZarukyK', 'hmotMj', 'mjHmot', 'hmotObal', 'objem', 'mjObj'], it)>
                <table class="flexibee-tbl-1">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canShowAtLeastOne(items, object, ['mj1', 'desetinMj'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['mj1'], it)>
                        <td><@tool.label name='mj1' items=items />:</td><td><@tool.place object=object name='mj1' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['desetinMj'], it)>
                        <td><@tool.label name='desetinMj' items=items />:</td><td><@tool.place object=object name='desetinMj' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['nakupCena', 'dodavatel'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['nakupCena'], it)>
                        <td><@tool.label name='nakupCena' items=items />:</td><td><@tool.place object=object name='nakupCena' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['dodavatel'], it)>
                        <td><@tool.label name='dodavatel' items=items />:</td><td><@tool.place object=object name='dodavatel' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['eanKod', 'kodPlu'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['eanKod'], it)>
                        <td><@tool.label name='eanKod' items=items level='3'/>:</td><td><@tool.place object=object level='3' name='eanKod' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['kodPlu'], it)>
                        <td><@tool.label name='kodPlu' items=items level='3'/>:</td><td><@tool.place object=object level='3' name='kodPlu' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['zaruka', 'mjZarukyK'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['zaruka'], it)>
                        <td><@tool.label name='zaruka' items=items />:</td><td><@tool.place object=object name='zaruka' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['mjZarukyK'], it)>
                        <td><@tool.place object=object name='mjZarukyK' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['hmotMj', 'mjHmot'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['hmotMj'], it)>
                        <td><@tool.label name='hmotMj' items=items />:</td><td><@tool.place object=object name='hmotMj' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['mjHmot'], it)>
                        <td><@tool.place object=object name='mjHmot' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['hmotObal'], it)>
                    <tr>
                        <td><@tool.label name='hmotObal' items=items />:</td><td><@tool.place object=object name='hmotObal' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['objem', 'mjObj'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['objem'], it)>
                        <td><@tool.label name='objem' items=items />:</td><td><@tool.place object=object name='objem' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['mjObj'], it)>
                        <td><@tool.place object=object name='mjObj' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['exportNaEshop', 'kratkyPopis', 'klicSlova', 'techParam', 'dodaciLhuta', 'mjDodaciLhuta'], it)>
            <div id="view-fragment-3" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['exportNaEshop', 'kratkyPopis', 'klicSlova', 'techParam', 'dodaciLhuta', 'mjDodaciLhuta'], it)>
                <table class="flexibee-tbl-1">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canShowAtLeastOne(items, object, ['exportNaEshop'], it)>
                    <tr>
                        <td><@tool.label name='exportNaEshop' items=items />:</td><td colspan="3"><@tool.place object=object name='exportNaEshop' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['kratkyPopis'], it)>
                    <tr>
                        <td><@tool.label name='kratkyPopis' items=items />:</td><td colspan="3"><@tool.place object=object name='kratkyPopis' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['klicSlova'], it)>
                    <tr>
                        <td><@tool.label name='klicSlova' items=items />:</td><td colspan="3"><@tool.place object=object name='klicSlova' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['techParam'], it)>
                    <tr>
                        <td><@tool.label name='techParam' items=items />:</td><td colspan="3"><@tool.place object=object name='techParam' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['dodaciLhuta', 'mjDodaciLhuta'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['dodaciLhuta'], it)>
                        <td><@tool.label name='dodaciLhuta' items=items />:</td><td><@tool.place object=object name='dodaciLhuta' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['mjDodaciLhuta'], it)>
                        <td><@tool.label name='mjDodaciLhuta' items=items />:</td><td><@tool.place object=object name='mjDodaciLhuta' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['inEvid', 'inKoefMj', 'inKoefStat', 'inKodSled', 'nomen', 'stat'], it)>
            <div id="view-fragment-4" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['inEvid', 'inKoefMj', 'inKoefStat', 'inKodSled', 'nomen', 'stat'], it)>
                <table class="flexibee-tbl-1">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canShowAtLeastOne(items, object, ['inEvid'], it)>
                    <tr>
                        <td><@tool.label name='inEvid' items=items />:</td><td colspan="3"><@tool.place object=object name='inEvid' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['inKoefMj', 'inKoefStat'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['inKoefMj'], it)>
                        <td><@tool.label name='inKoefMj' items=items />:</td><td><@tool.place object=object name='inKoefMj' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['inKoefStat'], it)>
                        <td><@tool.label name='inKoefStat' items=items />:</td><td><@tool.place object=object name='inKoefStat' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['inKodSled'], it)>
                    <tr>
                        <td><@tool.label name='inKodSled' items=items />:</td><td><@tool.place object=object name='inKodSled' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['nomen', 'stat'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['nomen'], it)>
                        <td><@tool.label name='nomen' items=items />:</td><td><@tool.place object=object name='nomen' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['stat'], it)>
                        <td><@tool.label name='stat' items=items />:</td><td><@tool.place object=object name='stat' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['platiOd', 'platiDo'], it)>
            <div id="view-fragment-5" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['platiOd', 'platiDo'], it)>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <tr>
                        <td colspan="2">Platnost se udává v kalendářních rocích, pro neomezenou platnost zadejte 0-9999</td>
                    </tr>

                    <#if canShowAtLeastOne(items, object, ['platiOd'], it)>
                    <tr>
                        <td><@tool.label name='platiOd' items=items />:</td><td><@tool.place object=object name='platiOd' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['platiDo'], it)>
                    <tr>
                        <td><@tool.label name='platiDo' items=items />:</td><td><@tool.place object=object name='platiDo' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

<@tool.listItems items=items object=object marker=marker />

<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />

<#include "/i-footer-view.ftl" />
</#escape>
