<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>

${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}
${marker.mark('popisA')}
${marker.mark('popisB')}
${marker.mark('popisC')}
${marker.mark('stitky')}
${marker.mark('exportNaEshop')}

<#-- odpovídá základnímu ceníku -->
${marker.mark('procZakl')}
${marker.mark('individCena')}
${marker.mark('limMnoz2')}
${marker.mark('limMnoz3')}
${marker.mark('limMnoz4')}
${marker.mark('limMnoz5')}
${marker.mark('procento2')}
${marker.mark('procento3')}
${marker.mark('procento4')}
${marker.mark('procento5')}
${marker.mark('cena2')}
${marker.mark('cena3')}
${marker.mark('cena4')}
${marker.mark('cena5')}
${marker.mark('zaokrJakK')}
${marker.mark('zaokrNaK')}
${marker.mark('cenJednotka')}
${marker.mark('typCenyVychoziK')}
${marker.mark('typVypCenyK')}
${marker.mark('typCenyVychozi25K')}
${marker.mark('typVypCeny25K')}
${marker.mark('evidVyrCis')}
${marker.mark('mjKoef2')}
${marker.mark('mjKoef3')}
${marker.mark('prodejMj')}
${marker.mark('zatrid')}
${marker.mark('baleniNazev1')}
${marker.mark('baleniNazev2')}
${marker.mark('baleniNazev3')}
${marker.mark('baleniNazev4')}
${marker.mark('baleniNazev5')}
${marker.mark('baleniMj1')}
${marker.mark('baleniMj2')}
${marker.mark('baleniMj3')}
${marker.mark('baleniMj4')}
${marker.mark('baleniMj5')}
${marker.mark('baleniEan1')}
${marker.mark('baleniEan2')}
${marker.mark('baleniEan3')}
${marker.mark('baleniEan4')}
${marker.mark('baleniEan5')}
${marker.mark('cenaBezna')}
${marker.mark('mj2')}
${marker.mark('mj3')}
${marker.mark('vyrobce')}

<@edit.beginForm />
    <#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canEditAtLeastOne(items, object, ['nazev', 'kod', 'typZasobyK', 'cenaZakl', 'cenaZaklBezDph', 'cenaZaklVcDph', 'typCenyDphK', 'skladove', 'typSzbDphK', 'skupZboz', 'sumStavMj', 'sumRezerMj'])>
    <table class="flexibee-tbl-1">
        <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
        <#if canEditAtLeastOne(items, object, ['nazev'])>
        <tr>
            <td><@edit.label name='nazev' items=items level='0' /></td><td colspan="3"><@edit.place object=object name='nazev' items=items marker=marker  level='0'/></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['kod', 'typZasobyK'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['kod'])>
            <td><@edit.label name='kod' items=items level='2' /></td><td><@edit.place object=object name='kod' items=items marker=marker  level='2'/></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['typZasobyK'])>
            <td><@edit.label name='typZasobyK' items=items /></td><td><@edit.place object=object name='typZasobyK' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['cenaZakl'])>
        <tr>
            <td><@edit.label name='cenaZakl' items=items /></td><td colspan="3"><@edit.place object=object name='cenaZakl' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['cenaZaklBezDph', 'cenaZaklVcDph'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['cenaZaklBezDph'])>
            <td><@edit.label name='cenaZaklBezDph' items=items level='0' /></td><td><@edit.place object=object name='cenaZaklBezDph' items=items marker=marker  level='0'/></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['cenaZaklVcDph'])>
            <td><@edit.label name='cenaZaklVcDph' items=items level='0' /></td><td><@edit.place object=object name='cenaZaklVcDph' items=items marker=marker  level='0'/></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['typCenyDphK', 'skladove'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['typCenyDphK'])>
            <td><@edit.label name='typCenyDphK' items=items /></td><td><@edit.place object=object name='typCenyDphK' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['skladove'])>
            <td><@edit.label name='skladove' items=items /></td><td><@edit.place object=object name='skladove' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['typSzbDphK', 'skupZboz'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['typSzbDphK'])>
            <td><@edit.label name='typSzbDphK' items=items /></td><td><@edit.place object=object name='typSzbDphK' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['skupZboz'])>
            <td><@edit.label name='skupZboz' items=items /></td><td><@edit.place object=object name='skupZboz' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['sumStavMj', 'sumRezerMj'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['sumStavMj'])>
            <td><@edit.label name='sumStavMj' items=items /></td><td><@edit.place object=object name='sumStavMj' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['sumRezerMj'])>
            <td><@edit.label name='sumRezerMj' items=items /></td><td><@edit.place object=object name='sumRezerMj' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canEditAtLeastOne(items, object, ['popis', 'poznam', 'mj1', 'desetinMj', 'nakupCena', 'dodavatel', 'eanKod', 'kodPlu', 'zaruka', 'mjZarukyK', 'hmotMj', 'mjHmot', 'hmotObal', 'objem', 'mjObj', 'exportNaEshop', 'kratkyPopis', 'klicSlova', 'techParam', 'dodaciLhuta', 'mjDodaciLhuta', 'inEvid', 'inKoefMj', 'inKoefStat', 'inKodSled', 'nomen', 'stat', 'platiOd', 'platiDo'])>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <li class="active"><a href="#edit-fragment-1" data-toggle="tab"><span>${lang('labels', 'textyPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['mj1', 'desetinMj', 'nakupCena', 'dodavatel', 'eanKod', 'kodPlu', 'zaruka', 'mjZarukyK', 'hmotMj', 'mjHmot', 'hmotObal', 'objem', 'mjObj'])>
            <li><a href="#edit-fragment-2" data-toggle="tab"><span>${lang('labels', 'upresneniPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['exportNaEshop', 'kratkyPopis', 'klicSlova', 'techParam', 'dodaciLhuta', 'mjDodaciLhuta'])>
            <li><a href="#edit-fragment-3" data-toggle="tab"><span>${lang('labels', 'interObchPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['inEvid', 'inKoefMj', 'inKoefStat', 'inKodSled', 'nomen', 'stat'])>
            <li><a href="#edit-fragment-4" data-toggle="tab"><span>${lang('labels', 'intrastatPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
            <li><a href="#edit-fragment-5" data-toggle="tab"><span>${lang('labels', 'spravaPN')}</span></a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <div id="edit-fragment-1" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
                <table class="flexibee-tbl-1">
                    <#if canEditAtLeastOne(items, object, ['popis'])>
                    <tr>
                        <td><@edit.label name='popis' items=items /><br><@edit.textarea object=object name='popis' items=items marker=marker rows=10 /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['poznam'])>
                    <tr>
                        <td><@edit.label name='poznam' items=items /><br><@edit.textarea object=object name='poznam' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['mj1', 'desetinMj', 'nakupCena', 'dodavatel', 'eanKod', 'kodPlu', 'zaruka', 'mjZarukyK', 'hmotMj', 'mjHmot', 'hmotObal', 'objem', 'mjObj'])>
            <div id="edit-fragment-2" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['mj1', 'desetinMj', 'nakupCena', 'dodavatel', 'eanKod', 'kodPlu', 'zaruka', 'mjZarukyK', 'hmotMj', 'mjHmot', 'hmotObal', 'objem', 'mjObj'])>
                <table class="flexibee-tbl-1">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canEditAtLeastOne(items, object, ['mj1', 'desetinMj'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['mj1'])>
                        <td><@edit.label name='mj1' items=items /></td><td><@edit.place object=object name='mj1' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['desetinMj'])>
                        <td><@edit.label name='desetinMj' items=items /></td><td><@edit.place object=object name='desetinMj' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['nakupCena', 'dodavatel'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['nakupCena'])>
                        <td><@edit.label name='nakupCena' items=items /></td><td><@edit.place object=object name='nakupCena' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['dodavatel'])>
                        <td><@edit.label name='dodavatel' items=items /></td><td><@edit.place object=object name='dodavatel' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['eanKod', 'kodPlu'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['eanKod'])>
                        <td><@edit.label name='eanKod' items=items level='3' /></td><td><@edit.place object=object name='eanKod' items=items marker=marker  level='3' type='ean'/></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['kodPlu'])>
                        <td><@edit.label name='kodPlu' items=items level='3' /></td><td><@edit.place object=object name='kodPlu' items=items marker=marker  level='3'/></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['zaruka', 'mjZarukyK'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['zaruka'])>
                        <td><@edit.label name='zaruka' items=items /></td><td><@edit.place object=object name='zaruka' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['mjZarukyK'])>
                        <td><@edit.place object=object name='mjZarukyK' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['hmotMj', 'mjHmot'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['hmotMj'])>
                        <td><@edit.label name='hmotMj' items=items /></td><td><@edit.place object=object name='hmotMj' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['mjHmot'])>
                        <td><@edit.place object=object name='mjHmot' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['hmotObal'])>
                    <tr>
                        <td><@edit.label name='hmotObal' items=items /></td><td><@edit.place object=object name='hmotObal' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['objem', 'mjObj'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['objem'])>
                        <td><@edit.label name='objem' items=items /></td><td><@edit.place object=object name='objem' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['mjObj'])>
                        <td><@edit.place object=object name='mjObj' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['exportNaEshop', 'kratkyPopis', 'klicSlova', 'techParam', 'dodaciLhuta', 'mjDodaciLhuta'])>
            <div id="edit-fragment-3" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['exportNaEshop', 'kratkyPopis', 'klicSlova', 'techParam', 'dodaciLhuta', 'mjDodaciLhuta'])>
                <table class="flexibee-tbl-1">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canEditAtLeastOne(items, object, ['exportNaEshop'])>
                    <tr>
                        <td><@edit.label name='exportNaEshop' items=items /></td><td colspan="3"><@edit.place object=object name='exportNaEshop' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['kratkyPopis'])>
                    <tr>
                        <td><@edit.label name='kratkyPopis' items=items /></td><td colspan="3"><@edit.place object=object name='kratkyPopis' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['klicSlova'])>
                    <tr>
                        <td><@edit.label name='klicSlova' items=items /></td><td colspan="3"><@edit.place object=object name='klicSlova' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['techParam'])>
                    <tr>
                        <td><@edit.label name='techParam' items=items /></td><td colspan="3"><@edit.place object=object name='techParam' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['dodaciLhuta', 'mjDodaciLhuta'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['dodaciLhuta'])>
                        <td><@edit.label name='dodaciLhuta' items=items /></td><td><@edit.place object=object name='dodaciLhuta' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['mjDodaciLhuta'])>
                        <td><@edit.label name='mjDodaciLhuta' items=items /></td><td><@edit.place object=object name='mjDodaciLhuta' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['inEvid', 'inKoefMj', 'inKoefStat', 'inKodSled', 'nomen', 'stat'])>
            <div id="edit-fragment-4" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['inEvid', 'inKoefMj', 'inKoefStat', 'inKodSled', 'nomen', 'stat'])>
                <table class="flexibee-tbl-1">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canEditAtLeastOne(items, object, ['inEvid'])>
                    <tr>
                        <td><@edit.label name='inEvid' items=items /></td><td colspan="3"><@edit.place object=object name='inEvid' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['inKoefMj', 'inKoefStat'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['inKoefMj'])>
                        <td><@edit.label name='inKoefMj' items=items /></td><td><@edit.place object=object name='inKoefMj' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['inKoefStat'])>
                        <td><@edit.label name='inKoefStat' items=items /></td><td><@edit.place object=object name='inKoefStat' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['inKodSled'])>
                    <tr>
                        <td><@edit.label name='inKodSled' items=items /></td><td><@edit.place object=object name='inKodSled' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['nomen', 'stat'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['nomen'])>
                        <td><@edit.label name='nomen' items=items /></td><td><@edit.place object=object name='nomen' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['stat'])>
                        <td><@edit.label name='stat' items=items /></td><td><@edit.place object=object name='stat' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
            <div id="edit-fragment-5" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <tr>
                        <td colspan="2">Platnost se udává v kalendářních rocích, pro neomezenou platnost zadejte 0-9999</td>
                    </tr>

                    <#if canEditAtLeastOne(items, object, ['platiOd'])>
                    <tr>
                        <td><@edit.label name='platiOd' items=items /></td><td><@edit.place object=object name='platiOd' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['platiDo'])>
                    <tr>
                        <td><@edit.label name='platiDo' items=items /></td><td><@edit.place object=object name='platiDo' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

    <br/>
    <@edit.listItems items=items object=object marker=marker />

    <@edit.saveButton />
<@edit.endForm />

<@edit.generateNaiveJavascript object=object it=it />

<#include "/parts/i-dalsi-informace.ftl" />


<#include "/i-footer-edit.ftl" />
</#escape>
