<#ftl encoding="utf-8" strip_whitespace=true/>
<#escape x as x?default("")?html>

<#include "/i-header-view.ftl" />

${marker.mark('platiOd')}
${marker.mark('platiDo')}
${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}
${marker.mark('stitky')}
${marker.mark('modul')}
${marker.mark('prodejka')}

<#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canShowAtLeastOne(items, object, ['kod', 'radaPrijem', 'radaVydej', 'nazev', 'typPohybuK'], it)>
    <table class="flexibee-tbl-1">
        <col width="15%" /><col width="18%" /><col width="15%" /><col width="18%" /><col width="15%" /><col width="18%" />
        <#if canShowAtLeastOne(items, object, ['kod', 'radaPrijem', 'radaVydej'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['kod'], it)>
            <td><@tool.label name='kod' items=items />:</td><td><@tool.place object=object name='kod' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['radaPrijem'], it)>
            <td><@tool.label name='radaPrijem' items=items />:</td><td><@tool.place object=object name='radaPrijem' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['radaVydej'], it)>
            <td><@tool.label name='radaVydej' items=items />:</td><td><@tool.place object=object name='radaVydej' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['nazev'], it)>
        <tr>
            <td><@tool.label name='nazev' items=items />:</td><td colspan="5"><@tool.place object=object name='nazev' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['typPohybuK'], it)>
        <tr>
            <td><@tool.label name='typPohybuK' items=items />:</td><td><@tool.place object=object name='typPohybuK' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canShowAtLeastOne(items, object, ['typDoklK', 'konSym', 'bspBan', 'bsp', 'splatDny', 'slevaDokl', 'formaUhradyCis', 'formaUhrK', 'formaDopravy', 'doprava', 'generovatSkl', 'typProtiDokladuPrijem', 'typProtiDokladuVydej', 'typPohybuSkladK', 'nekatalPolDoAnalyzy', 'ucetni', 'stredisko', 'cinnost', 'primUcet', 'mena', 'statDph', 'clenKonVykDph', 'typUcOpPrijem', 'typUcOpVydej', 'eetTypK', 'eetPokladniZarizeni', 'eetDicPoverujiciho', 'eetProvozovna', 'eetLimit', 'zaokrJakDphK', 'zaokrNaDphK', 'zaokrJakSumK', 'zaokrNaSumK', 'tiskAutomat', 'statOdesl', 'obchTrans', 'statUrc', 'dodPodm', 'statPuvod', 'druhDopr', 'krajUrc', 'zvlPoh', 'popisDoklad', 'uvodTxt', 'zavTxt', 'emailTxt', 'popis', 'poznam', 'ucetObdobiOd', 'ucetObdobiDo', 'razeniProTiskK', 'primarni'], it)>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canShowAtLeastOne(items, object, ['typDoklK', 'konSym', 'bspBan', 'bsp', 'splatDny', 'slevaDokl', 'formaUhradyCis', 'formaUhrK', 'formaDopravy', 'doprava', 'generovatSkl', 'typProtiDokladuPrijem', 'typProtiDokladuVydej', 'typPohybuSkladK', 'nekatalPolDoAnalyzy'], it)>
            <li class="active"><a href="#view-fragment-1" data-toggle="tab">Faktura</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['ucetni', 'stredisko', 'cinnost', 'primUcet', 'mena', 'statDph', 'clenKonVykDph', 'typUcOpPrijem', 'typUcOpVydej', 'eetTypK', 'eetPokladniZarizeni', 'eetDicPoverujiciho', 'eetProvozovna', 'eetLimit'], it)>
            <li><a href="#view-fragment-3" data-toggle="tab">Účtování</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['zaokrJakDphK', 'zaokrNaDphK', 'zaokrJakSumK', 'zaokrNaSumK'], it)>
            <li><a href="#view-fragment-4" data-toggle="tab">Zaokrouhlování</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['tiskAutomat'], it)>
            <li><a href="#view-fragment-5" data-toggle="tab">Tisk</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['statOdesl', 'obchTrans', 'statUrc', 'dodPodm', 'statPuvod', 'druhDopr', 'krajUrc', 'zvlPoh'], it)>
            <li><a href="#view-fragment-6" data-toggle="tab">Intrastat</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['popisDoklad', 'uvodTxt', 'zavTxt', 'emailTxt'], it)>
            <li><a href="#view-fragment-7" data-toggle="tab">Texty na doklad</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
            <li><a href="#view-fragment-8" data-toggle="tab">Texty</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['ucetObdobiOd', 'ucetObdobiDo', 'razeniProTiskK', 'primarni'], it)>
            <li><a href="#view-fragment-9" data-toggle="tab">Správa</a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canShowAtLeastOne(items, object, ['typDoklK', 'konSym', 'bspBan', 'bsp', 'splatDny', 'slevaDokl', 'formaUhradyCis', 'formaUhrK', 'formaDopravy', 'doprava', 'generovatSkl', 'typProtiDokladuPrijem', 'typProtiDokladuVydej', 'typPohybuSkladK', 'nekatalPolDoAnalyzy'], it)>
            <div id="view-fragment-1" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['typDoklK', 'konSym', 'bspBan', 'bsp', 'splatDny', 'slevaDokl', 'formaUhradyCis', 'formaUhrK', 'formaDopravy', 'doprava', 'generovatSkl', 'typProtiDokladuPrijem', 'typProtiDokladuVydej', 'typPohybuSkladK', 'nekatalPolDoAnalyzy'], it)>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <#if canShowAtLeastOne(items, object, ['typDoklK'], it)>
                    <tr>
                        <td><@tool.label name='typDoklK' items=items />:</td><td><@tool.place object=object name='typDoklK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['konSym'], it)>
                    <tr>
                        <td><@tool.label name='konSym' items=items />:</td><td><@tool.place object=object name='konSym' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['bspBan'], it)>
                    <tr>
                        <td><@tool.label name='bspBan' items=items />:</td><td><@tool.place object=object name='bspBan' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['bsp'], it)>
                    <tr>
                        <td><@tool.label name='bsp' items=items />:</td><td><@tool.place object=object name='bsp' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['splatDny'], it)>
                    <tr>
                        <td><@tool.label name='splatDny' items=items />:</td><td><@tool.place object=object name='splatDny' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['slevaDokl'], it)>
                    <tr>
                        <td><@tool.label name='slevaDokl' items=items />:</td><td><@tool.place object=object name='slevaDokl' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['formaUhradyCis'], it)>
                    <tr>
                        <td><@tool.label name='formaUhradyCis' items=items />:</td><td><@tool.place object=object name='formaUhradyCis' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['formaUhrK'], it)>
                    <tr>
                        <td><@tool.label name='formaUhrK' items=items />:</td><td><@tool.place object=object name='formaUhrK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['formaDopravy'], it)>
                    <tr>
                        <td><@tool.label name='formaDopravy' items=items />:</td><td><@tool.place object=object name='formaDopravy' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['doprava'], it)>
                    <tr>
                        <td><@tool.label name='doprava' items=items />:</td><td><@tool.place object=object name='doprava' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['generovatSkl'], it)>
                    <tr>
                        <td><@tool.label name='generovatSkl' items=items />:</td><td><@tool.place object=object name='generovatSkl' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['typProtiDokladuPrijem'], it)>
                    <tr>
                        <td><@tool.label name='typProtiDokladuPrijem' items=items />:</td><td><@tool.place object=object name='typProtiDokladuPrijem' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['typProtiDokladuVydej'], it)>
                    <tr>
                        <td><@tool.label name='typProtiDokladuVydej' items=items />:</td><td><@tool.place object=object name='typProtiDokladuVydej' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['typPohybuSkladK'], it)>
                    <tr>
                        <td><@tool.label name='typPohybuSkladK' items=items />:</td><td><@tool.place object=object name='typPohybuSkladK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['nekatalPolDoAnalyzy'], it)>
                    <tr>
                        <td><@tool.label name='nekatalPolDoAnalyzy' items=items />:</td><td><@tool.place object=object name='nekatalPolDoAnalyzy' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['ucetni', 'stredisko', 'cinnost', 'primUcet', 'mena', 'statDph', 'clenKonVykDph', 'typUcOpPrijem', 'typUcOpVydej', 'eetTypK', 'eetPokladniZarizeni', 'eetDicPoverujiciho', 'eetProvozovna', 'eetLimit'], it)>
            <div id="view-fragment-3" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['ucetni', 'stredisko', 'cinnost', 'primUcet', 'mena', 'statDph', 'clenKonVykDph', 'typUcOpPrijem', 'typUcOpVydej'], it)>
                <table class="flexibee-tbl-1">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canShowAtLeastOne(items, object, ['ucetni', 'stredisko'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['ucetni'], it)>
                        <td><@tool.label name='ucetni' items=items />:</td><td><@tool.place object=object name='ucetni' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['stredisko'], it)>
                        <td><@tool.label name='stredisko' items=items />:</td><td><@tool.place object=object name='stredisko' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['cinnost'], it)>
                    <tr>
                        <td><@tool.label name='cinnost' items=items />:</td><td><@tool.place object=object name='cinnost' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['primUcet', 'mena'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['primUcet'], it)>
                        <td><@tool.label name='primUcet' items=items />:</td><td><@tool.place object=object name='primUcet' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['mena'], it)>
                        <td><@tool.label name='mena' items=items />:</td><td><@tool.place object=object name='mena' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['statDph'], it)>
                    <tr>
                        <td><@tool.label name='statDph' items=items />:</td><td><@tool.place object=object name='statDph' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['clenKonVykDph'], it)>
                    <tr>
                        <td><@tool.label name='clenKonVykDph' items=items />:</td><td><@tool.place object=object name='clenKonVykDph' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <tr>
                        <td colspan="2">Předpis zaúčtování:</td>
                    </tr>

                    <#if canShowAtLeastOne(items, object, ['typUcOpPrijem'], it)>
                    <tr>
                        <td><@tool.label name='typUcOpPrijem' items=items />:</td><td><@tool.place object=object name='typUcOpPrijem' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['typUcOpVydej'], it)>
                    <tr>
                        <td><@tool.label name='typUcOpVydej' items=items />:</td><td><@tool.place object=object name='typUcOpVydej' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

                <#if canShowAtLeastOne(items, object, ['eetTypK', 'eetPokladniZarizeni', 'eetDicPoverujiciho', 'eetProvozovna', 'eetLimit'], it)>
                <h4>${lang('labels', 'dokladEBEetPN')}</h4>
                <div id="view-fragment-2" class="tab-pane">
                    <div class="flexibee-table-border">
                    <#if canShowAtLeastOne(items, object, ['eetTypK', 'eetPokladniZarizeni', 'eetDicPoverujiciho', 'eetProvozovna', 'eetLimit'], it)>
                    <table class="flexibee-tbl-1">
                        <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                        <#if canShowAtLeastOne(items, object, ['eetTypK'], it)>
                        <tr>
                            <td><@tool.label name='eetTypK' items=items />:</td><td><@tool.place object=object name='eetTypK' items=items marker=marker /></td>
                        </tr>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['eetPokladniZarizeni', 'eetDicPoverujiciho'], it)>
                        <tr>
                            <#if canShowAtLeastOne(items, object, ['eetPokladniZarizeni'], it)>
                            <td><@tool.label name='eetPokladniZarizeni' items=items />:</td><td><@tool.place object=object name='eetPokladniZarizeni' items=items marker=marker /></td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <#if canShowAtLeastOne(items, object, ['eetDicPoverujiciho'], it)>
                            <td><@tool.label name='eetDicPoverujiciho' items=items />:</td><td><@tool.place object=object name='eetDicPoverujiciho' items=items marker=marker /></td>
                            </#if>
                        </tr>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['eetProvozovna', 'eetLimit'], it)>
                        <tr>
                            <#if canShowAtLeastOne(items, object, ['eetProvozovna'], it)>
                            <td><@tool.label name='eetProvozovna' items=items />:</td><td><@tool.place object=object name='eetProvozovna' items=items marker=marker /></td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <#if canShowAtLeastOne(items, object, ['eetLimit'], it)>
                            <td><@tool.label name='eetLimit' items=items />:</td><td><@tool.place object=object name='eetLimit' items=items marker=marker /></td>
                            </#if>
                        </tr>
                        </#if>
                    </table>
                    </#if>
                    </div>

                </div>
                </#if>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['zaokrJakDphK', 'zaokrNaDphK', 'zaokrJakSumK', 'zaokrNaSumK'], it)>
            <div id="view-fragment-4" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['zaokrJakDphK', 'zaokrNaDphK', 'zaokrJakSumK', 'zaokrNaSumK'], it)>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <#if canShowAtLeastOne(items, object, ['zaokrJakDphK'], it)>
                    <tr>
                        <td><@tool.label name='zaokrJakDphK' items=items />:</td><td><@tool.place object=object name='zaokrJakDphK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['zaokrNaDphK'], it)>
                    <tr>
                        <td><@tool.label name='zaokrNaDphK' items=items />:</td><td><@tool.place object=object name='zaokrNaDphK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['zaokrJakSumK'], it)>
                    <tr>
                        <td><@tool.label name='zaokrJakSumK' items=items />:</td><td><@tool.place object=object name='zaokrJakSumK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['zaokrNaSumK'], it)>
                    <tr>
                        <td><@tool.label name='zaokrNaSumK' items=items />:</td><td><@tool.place object=object name='zaokrNaSumK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['tiskAutomat'], it)>
            <div id="view-fragment-5" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['tiskAutomat'], it)>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <#if canShowAtLeastOne(items, object, ['tiskAutomat'], it)>
                    <tr>
                        <td><@tool.label name='tiskAutomat' items=items />:</td><td><@tool.place object=object name='tiskAutomat' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['statOdesl', 'obchTrans', 'statUrc', 'dodPodm', 'statPuvod', 'druhDopr', 'krajUrc', 'zvlPoh'], it)>
            <div id="view-fragment-6" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['statOdesl', 'obchTrans', 'statUrc', 'dodPodm', 'statPuvod', 'druhDopr', 'krajUrc', 'zvlPoh'], it)>
                <table class="flexibee-tbl-1">
                    <#if canShowAtLeastOne(items, object, ['statOdesl', 'obchTrans'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['statOdesl'], it)>
                        <td><@tool.label name='statOdesl' items=items />:</td><td><@tool.place object=object name='statOdesl' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['obchTrans'], it)>
                        <td><@tool.label name='obchTrans' items=items />:</td><td><@tool.place object=object name='obchTrans' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['statUrc', 'dodPodm'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['statUrc'], it)>
                        <td><@tool.label name='statUrc' items=items />:</td><td><@tool.place object=object name='statUrc' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['dodPodm'], it)>
                        <td><@tool.label name='dodPodm' items=items />:</td><td><@tool.place object=object name='dodPodm' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['statPuvod', 'druhDopr'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['statPuvod'], it)>
                        <td><@tool.label name='statPuvod' items=items />:</td><td><@tool.place object=object name='statPuvod' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['druhDopr'], it)>
                        <td><@tool.label name='druhDopr' items=items />:</td><td><@tool.place object=object name='druhDopr' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['krajUrc', 'zvlPoh'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['krajUrc'], it)>
                        <td><@tool.label name='krajUrc' items=items />:</td><td><@tool.place object=object name='krajUrc' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['zvlPoh'], it)>
                        <td><@tool.label name='zvlPoh' items=items />:</td><td><@tool.place object=object name='zvlPoh' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['popisDoklad', 'uvodTxt', 'zavTxt', 'emailTxt'], it)>
            <div id="view-fragment-7" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['popisDoklad', 'uvodTxt', 'zavTxt', 'emailTxt'], it)>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <#if canShowAtLeastOne(items, object, ['popisDoklad'], it)>
                    <tr>
                        <td><@tool.label name='popisDoklad' items=items />:</td><td><@tool.place object=object name='popisDoklad' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['uvodTxt'], it)>
                    <tr>
                        <td colspan="4"><@tool.label name='uvodTxt' items=items />:<br><@tool.placeArea object=object name='uvodTxt' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['zavTxt'], it)>
                    <tr>
                        <td colspan="4"><@tool.label name='zavTxt' items=items />:<br><@tool.placeArea object=object name='zavTxt' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['emailTxt'], it)>
                    <tr>
                        <td colspan="4"><@tool.label name='emailTxt' items=items />:<br><@tool.placeArea object=object name='emailTxt' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
            <div id="view-fragment-8" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
                <table class="flexibee-tbl-1">
                    <#if canShowAtLeastOne(items, object, ['popis'], it)>
                    <tr>
                        <td><@tool.label name='popis' items=items />:<br><@tool.placeArea object=object name='popis' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['poznam'], it)>
                    <tr>
                        <td><@tool.label name='poznam' items=items />:<br><@tool.placeArea object=object name='poznam' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['ucetObdobiOd', 'ucetObdobiDo', 'razeniProTiskK', 'primarni'], it)>
            <div id="view-fragment-9" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['ucetObdobiOd', 'ucetObdobiDo', 'razeniProTiskK', 'primarni'], it)>
                <table class="flexibee-tbl-1">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <tr>
                        <td colspan="4">Platnost se udává v účetních obdobích, pro neomezenou platnost nechte pole nevyplněné</td>
                    </tr>

                    <#if canShowAtLeastOne(items, object, ['ucetObdobiOd', 'ucetObdobiDo'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['ucetObdobiOd'], it)>
                        <td><@tool.label name='ucetObdobiOd' items=items />:</td><td><@tool.place object=object name='ucetObdobiOd' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['ucetObdobiDo'], it)>
                        <td><@tool.label name='ucetObdobiDo' items=items />:</td><td><@tool.place object=object name='ucetObdobiDo' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['razeniProTiskK'], it)>
                    <tr>
                        <td><@tool.label name='razeniProTiskK' items=items />:</td><td colspan="3"><@tool.place object=object name='razeniProTiskK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['primarni'], it)>
                    <tr>
                        <td><@tool.label name='primarni' items=items />:</td><td colspan="3"><@tool.place object=object name='primarni' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

<@tool.listItems items=items object=object marker=marker />

<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />


<#include "/parts/i-dalsi-informace.ftl" />

<#include "/i-footer-view.ftl" />
</#escape>