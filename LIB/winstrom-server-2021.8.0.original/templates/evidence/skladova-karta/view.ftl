<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-view.ftl" />

<#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canShowAtLeastOne(items, object, ['cenik', 'sklad', 'ucetObdobi', 'stavMjSPozadavky'], it)>
    <table class="flexibee-tbl-1">
        <col width="20%" /><col width="80%" />
        <#if canShowAtLeastOne(items, object, ['cenik'], it)>
        <tr>
            <td><@tool.label name='cenik' items=items />:</td><td><@tool.place object=object name='cenik' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['sklad'], it)>
        <tr>
            <td><@tool.label name='sklad' items=items />:</td><td><@tool.place object=object name='sklad' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['ucetObdobi'], it)>
        <tr>
            <td><@tool.label name='ucetObdobi' items=items />:</td><td><@tool.place object=object name='ucetObdobi' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['stavMjSPozadavky'], it)>
        <tr>
            <td><@tool.label name='stavMjSPozadavky' items=items level='0'/>:</td><td><@tool.place object=object level='0' name='stavMjSPozadavky' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canShowAtLeastOne(items, object, ['nazev', 'cenPopis', 'vydExpir', 'stavMJ', 'minMJ', 'stavTuz', 'maxMJ', 'prumCenaTuz', 'rezervMJ', 'pocatMJ', 'datPosl', 'pocatTuz', 'poslCenaTuz', 'stavMen', 'mistnost', 'prumCenaMen', 'regal', 'pocatMen', 'police', 'poslCenaMen', 'poznamVelka', 'popis'], it)>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canShowAtLeastOne(items, object, ['nazev', 'cenPopis', 'vydExpir', 'stavMJ', 'minMJ', 'stavTuz', 'maxMJ', 'prumCenaTuz', 'rezervMJ', 'pocatMJ', 'datPosl', 'pocatTuz', 'poslCenaTuz'], it)>
            <li class="active"><a href="#view-fragment-1" data-toggle="tab">${lang('labels', 'hlavniPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['stavMen', 'mistnost', 'prumCenaMen', 'regal', 'pocatMen', 'police', 'poslCenaMen'], it)>
            <li><a href="#view-fragment-2" data-toggle="tab">Měna, Umístění</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['poznamVelka'], it)>
            <li><a href="#view-fragment-3" data-toggle="tab">${lang('labels', 'poznamkaPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['popis'], it)>
            <li><a href="#view-fragment-4" data-toggle="tab">${lang('labels', 'popisPN')}</a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canShowAtLeastOne(items, object, ['nazev', 'cenPopis', 'vydExpir', 'stavMJ', 'minMJ', 'stavTuz', 'maxMJ', 'prumCenaTuz', 'rezervMJ', 'pocatMJ', 'datPosl', 'pocatTuz', 'poslCenaTuz'], it)>
            <div id="view-fragment-1" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['nazev', 'cenPopis', 'vydExpir', 'stavMJ', 'minMJ', 'stavTuz', 'maxMJ', 'prumCenaTuz', 'rezervMJ', 'pocatMJ', 'datPosl', 'pocatTuz', 'poslCenaTuz'], it)>
                <table class="flexibee-tbl-1">
                    <#if canShowAtLeastOne(items, object, ['nazev'], it)>
                    <tr>
                        <td><@tool.label name='nazev' items=items />:</td><td colspan="3"><@tool.place object=object name='nazev' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['cenPopis'], it)>
                    <tr>
                        <td><@tool.label name='cenPopis' items=items />:</td><td colspan="3"><@tool.place object=object name='cenPopis' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['vydExpir'], it)>
                    <tr>
                        <td><@tool.label name='vydExpir' items=items />:</td><td colspan="3"><@tool.place object=object name='vydExpir' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['stavMJ', 'minMJ'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['stavMJ'], it)>
                        <td><@tool.label name='stavMJ' items=items />:</td><td><@tool.place object=object name='stavMJ' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['minMJ'], it)>
                        <td><@tool.label name='minMJ' items=items />:</td><td><@tool.place object=object name='minMJ' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['stavTuz', 'maxMJ'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['stavTuz'], it)>
                        <td><@tool.label name='stavTuz' items=items />:</td><td><@tool.place object=object name='stavTuz' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['maxMJ'], it)>
                        <td><@tool.label name='maxMJ' items=items />:</td><td><@tool.place object=object name='maxMJ' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['prumCenaTuz', 'rezervMJ'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['prumCenaTuz'], it)>
                        <td><@tool.label name='prumCenaTuz' items=items />:</td><td><@tool.place object=object name='prumCenaTuz' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['rezervMJ'], it)>
                        <td><@tool.label name='rezervMJ' items=items />:</td><td><@tool.place object=object name='rezervMJ' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['pocatMJ', 'datPosl'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['pocatMJ'], it)>
                        <td><@tool.label name='pocatMJ' items=items />:</td><td><@tool.place object=object name='pocatMJ' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['datPosl'], it)>
                        <td><@tool.label name='datPosl' items=items />:</td><td><@tool.place object=object name='datPosl' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['pocatTuz', 'poslCenaTuz'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['pocatTuz'], it)>
                        <td><@tool.label name='pocatTuz' items=items />:</td><td><@tool.place object=object name='pocatTuz' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['poslCenaTuz'], it)>
                        <td><@tool.label name='poslCenaTuz' items=items />:</td><td><@tool.place object=object name='poslCenaTuz' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['stavMen', 'mistnost', 'prumCenaMen', 'regal', 'pocatMen', 'police', 'poslCenaMen'], it)>
            <div id="view-fragment-2" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['stavMen', 'mistnost', 'prumCenaMen', 'regal', 'pocatMen', 'police', 'poslCenaMen'], it)>
                <table class="flexibee-tbl-1">
                    <#if canShowAtLeastOne(items, object, ['stavMen', 'mistnost'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['stavMen'], it)>
                        <td><@tool.label name='stavMen' items=items />:</td><td><@tool.place object=object name='stavMen' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['mistnost'], it)>
                        <td><@tool.label name='mistnost' items=items />:</td><td><@tool.place object=object name='mistnost' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['prumCenaMen', 'regal'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['prumCenaMen'], it)>
                        <td><@tool.label name='prumCenaMen' items=items />:</td><td><@tool.place object=object name='prumCenaMen' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['regal'], it)>
                        <td><@tool.label name='regal' items=items />:</td><td><@tool.place object=object name='regal' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['pocatMen', 'police'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['pocatMen'], it)>
                        <td><@tool.label name='pocatMen' items=items />:</td><td><@tool.place object=object name='pocatMen' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['police'], it)>
                        <td><@tool.label name='police' items=items />:</td><td><@tool.place object=object name='police' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['poslCenaMen'], it)>
                    <tr>
                        <td><@tool.label name='poslCenaMen' items=items />:</td><td colspan="3"><@tool.place object=object name='poslCenaMen' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['poznamVelka'], it)>
            <div id="view-fragment-3" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['poznamVelka'], it)>
                <table class="flexibee-tbl-1">
                    <#if canShowAtLeastOne(items, object, ['poznamVelka'], it)>
                    <tr>
                        <td><@tool.label name='poznamVelka' items=items />:</td><td><@tool.place object=object name='poznamVelka' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['popis'], it)>
            <div id="view-fragment-4" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['popis'], it)>
                <table class="flexibee-tbl-1">
                    <#if canShowAtLeastOne(items, object, ['popis'], it)>
                    <tr>
                        <td><@tool.label name='popis' items=items />:</td><td><@tool.place object=object name='popis' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

<@tool.listItems items=items object=object marker=marker />

<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />

<#include "/i-footer-view.ftl" />
</#escape>