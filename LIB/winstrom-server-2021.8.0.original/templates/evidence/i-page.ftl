<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#--
                                                 POZOR, ZNEČIŠTĚNÍ!

      Následující kód narve do globálního jmenného prostoru obrovské množství dementně pojmenovaných proměnných,
      se kterými lze velmi snadno kolidovat. Některé z nich se dokonce v jiných šablonách používají (pro řazení)!
      Autor by zasloužil zmrskat devítiocasou kočkou máčenou po tři úplňky ve slaném nálevu; do té doby je záhodno
      pojmenovávat proměnné nějak rozumně, aby se riziko kolize minimalizovalo.

-->

<!-- =============================== -->
<#assign url = it.requestPath />
<#assign filterQueryParam = "" />
<#if it.fullTextQuery?has_content>
    <#assign filterQueryParam = filterQueryParam + "&q=" + it.fullTextQuery?url />
</#if>
<#if it.quickFilter?has_content>
    <#list it.quickFilter?keys as filter>
        <#assign filterQueryParam = filterQueryParam + "&" + ("filter[" + filter + "]")?url + "=" + it.quickFilter[filter]?url />
    </#list>
</#if>

<#-- zjistime podle, ktereho sloupce je razeno a zda se radi vzestupne nebo sestupne -->
<#if it.listParameters?? && it.listParameters.order?exists && it.listParameters.order[0]?exists>
    <#assign orderProperty=it.listParameters.order[0].propertyName />
    <#assign orderDirection=it.listParameters.order[0].orderType />
<#else>
    <#assign orderProperty="" />
    <#assign orderDirection="" />
</#if>

<#if it.limit?exists && it.rowCount &gt; it.limit>
<ul class="pagination pagination-sm pull-right hidden-print">
<#assign minPos = 0 />
<#assign maxPos = it.maxPage-1 />
<#assign showMax = 5 />
<#if it.defaultLimit != it.limit>
<#assign limit="&limit="+it.limit />
<#assign limit2="&limit="+it.limit />
<#else>
<#assign limit="" />
<#assign limit2="" />
</#if>
<#-- zjistime podle, ktereho sloupce je razeno a zda se radi vzestupne nebo sestupne -->
<#if it.listParameters.order?exists && it.listParameters.order[0]?exists>
    <#if orderDirection == "ascending">
        <#assign desc="@A" />
    <#else>
        <#assign desc="@D" />
    </#if>
    <#assign order="&order=${orderProperty}${desc}" />
    <#assign orderOnly="&order=${orderProperty}${desc}" />
    <#if limit2=="">
        <#assign orderLimit2=orderOnly />
    <#else>
        <#assign orderLimit2=order />
        </#if>
<#else>
    <#assign order="" />
    <#assign orderOnly="" />
    <#assign orderLimit2="" />
</#if>

<#if it.page &gt; showMax + 1><#assign minPos = (it.page - showMax) /></#if>
<#if (it.maxPage - 2) - it.page &gt; showMax><#assign maxPos = it.page + showMax /></#if>

<#if it.page != 0><li><a href="${url}?start=0${orderOnly}<#if filterQueryParam != ''>${filterQueryParam}</#if>">|&lt;</a></li><#else><li class="disabled"><a href="#"><span class="glyphicon glyphicon-step-backward"></span></a></li></#if>
<#if it.page != 0><li><a href="${url}?start=${((it.page-1)*it.limit)?string("0")}<#if it.page != 1>${limit}${order}<#else>${limit2}${orderLimit2}</#if>${filterQueryParam}">&lt;&lt;</a></li><#else><li class="disabled"><a href="#"><span class="glyphicon glyphicon-backward"></span></a></li></#if>
<#if minPos &gt; 0><li><a href="${url}?start=0${orderOnly}${filterQueryParam}">1</a></li></#if>
<#if minPos != 0><li class="disabled"><a href="#">...</a></li></#if>
<#list minPos..maxPos as pos>
<#if it.page != pos><li><a href="${url}?start=${((pos*it.limit)?string("0"))}<#if pos != 0>${limit}${order}<#else>${limit2}${orderLimit2}</#if>${filterQueryParam}" <#if it.page = pos - 1>class="flexibee-page-next"</#if>>${pos+1}</a></li><#else><li class="active"><a href="#">${pos+1}</a></li></#if>
</#list>
<#if maxPos != it.maxPage-1><li class="disabled"><a href="#">...</a></li></#if>
<#if maxPos &lt; it.maxPage-1><li><a href="${url}?start=${((it.maxPage-1)*it.limit)?string("0")}${limit}${order}${filterQueryParam}">${it.maxPage}</a></li></#if>
<#if it.page != it.maxPage-1><li><a href="${url}?start=${((it.page+1)*it.limit)?string("0")}${limit}${order}${filterQueryParam}"><span class="glyphicon glyphicon-forward"></span></a></li><#else><li class="disabled"><a href="#"><span class="glyphicon glyphicon-forward"></span></a></li></#if>
<#if it.page != it.maxPage-1><li><a href="${url}?start=${((it.maxPage-1)*it.limit)?string("0")}${limit}${order}${filterQueryParam}"><span class="glyphicon glyphicon-step-forward"></span></a></li><#else><li class="disabled"><a href="#"><span class="glyphicon glyphicon-step-forward"></span></a></li></#if>
</ul>
<br clear="all"/>
</#if>
</#escape>
