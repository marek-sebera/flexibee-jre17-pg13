<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.${it.senchaName}.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.fbGrid${it.senchaName?cap_first}',

    requires: [
        'Ext.ux.grid.FiltersFeature'
    ],

    evidenceId: '${it.senchaName}',
    evidenceName: '${it.evidenceName}',

    /**
     * Umožňuje nadefinovat jiné URL pro výpis záznamů
     */
    listUrl: null,

    /**
     * Umožňuje doplnit další parametry do Proxy (např. pro analýzu nákupu doplnit parametr co analyzovat)
     */
    extraParams: undefined,

    /**
     * Umožňuje omezit seznam políček, které chceme zobrazovat. Vhodné např. pro dashboard.
     */
    defineGridColumns: undefined,

    /**
     * Umožňuje nastavit výchozí pořadí pro Store. Je ve formátu parametrů pro Sorter:
     *
     */
    defaultOrder: undefined,

    emptyText: 'Žádné záznamy',

    features: [
        { ftype: 'filters' }
    ],

    initComponent: function() {
        var me = this;

        // every grid needs its own instance of the store (because store is stateful!)
        me.store = Ext.create('FlexiBee.${it.senchaName}.Store', {
        });
        if (me.listUrl && me.listUrl !== '') {
            me.store.model.proxy.url = me.listUrl;
        }

        if (me.extraParams && me.extraParams !== '') {
            Ext.apply(me.store.model.proxy.extraParams, me.extraParams);
        }

        var gridColumns;
        if (me.defineGridColumns && typeof me.defineGridColumns == 'function') {
            gridColumns = me.defineGridColumns;
        } else {
            gridColumns = function(property) {
                var pluginGridColumns = FlexiBee.Plugins.invokeSingle(me, 'defineGridColumns', property);
                if (pluginGridColumns) {
                    return Ext.Array.filter(pluginGridColumns, function(pluginGridColumn) {
                        return Ext.isObject(pluginGridColumn);
                    });
                }
                return null;
            };
        }

        me.columns = Ext.create('FlexiBee.${it.senchaName}.Properties').gridColumns(gridColumns);

        if (me.defaultOrder) {
            for (var i = 0; i < me.defaultOrder.length; i++) {
                var s = me.defaultOrder[i];
                me.store.sorters.add(new Ext.util.Sorter(s));
            }
        }

        me.callParent(arguments);
    }
});

<#import "/l-sencha.ftl" as sencha>
<@sencha.includePlugins path="${it.senchaName}.Grid"/>

</#escape>
