<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>

${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}
${marker.mark('modul')}

<@edit.beginForm />
    <#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canEditAtLeastOne(items, object, ['mena', 'platiOdData', 'nbStred', 'kurzMnozstvi'])>
    <table class="flexibee-tbl-1">
        <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
        <#if canEditAtLeastOne(items, object, ['mena'])>
        <tr>
            <td><@edit.label name='mena' items=items /></td><td colspan="3"><@edit.place object=object name='mena' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['platiOdData'])>
        <tr>
            <td><@edit.label name='platiOdData' items=items /></td><td colspan="3"><@edit.place object=object name='platiOdData' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['nbStred', 'kurzMnozstvi'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['nbStred'])>
            <td><@edit.label name='nbStred' items=items /></td><td><@edit.place object=object name='nbStred' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['kurzMnozstvi'])>
            <td><label for="flexibee-inp-kurzMnozstvi">=:</label></td><td><@edit.place object=object name='kurzMnozstvi' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

<#--AUTOGEN END-->

    <br/>
    <@edit.listItems items=items object=object marker=marker />

    <@edit.saveButton />
<@edit.endForm />

<@edit.generateNaiveJavascript object=object it=it />

<#include "/parts/i-dalsi-informace.ftl" />

<#include "/i-footer-edit.ftl" />
</#escape>
