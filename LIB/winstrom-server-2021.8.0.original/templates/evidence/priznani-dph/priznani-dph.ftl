<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>


<#if chyba??>
    <div class="alert alert-danger">
        <ul class="flexibee-errors">
            ${chyba}
        </ul>
    </div>
</#if>

<@edit.beginForm />

    <div class="flexibee-steps">
    <h2 class="flexibee-step1">Rok</h2>
    <div>
        <div class="form-horizontal">
        <#list roky as rok>
            <div class="radio">
                        <label class="control-label"><input type="radio" name="rok" id="rok-${rok.id}"  class="flexibee-inp" value="${rok.id}" <#if (vybranyRok?? && vybranyRok?c == rok.id) || (!vybranyRok?? && rok_index == (roky?size-1))>checked</#if>>
            ${rok.name}</label>
            </div>
        </#list>
        </div>
    </div>

    <h2 class="flexibee-step2">Období přiznání</h2>
    <div>
        <div class="form-horizontal">
            <div class="form-group">
                <div class="col-xs-4">
                    <div class="radio">
                        <label class="control-label"><input type="radio" name="perioda" id="perioda-ctvrtleti" value="ctvrtleti"  class="flexibee-inp" <#if (perioda?? && perioda == "ctvrtleti" || !perioda??)>checked</#if>>
                        Čtvrtletní</label>
                    </div>
                </div>
                <div class="col-xs-8">
                    <select name="ctvrtleti" class="flexibee-rounded-form-select flexibee-inp form-control">
                        <#list ctvrtleti as mesic>
                            <option value="${mesic.id}" <#if (vybranyCvrtleti?? && vybranyCvrtleti == mesic.id) || (!vybranyCvrtleti?? && mesic_index == 0)>selected</#if>>${mesic.name}</option>
                            </#list>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-4">
                    <div class="radio">
                        <label class="control-label"><input type="radio" name="perioda" id="perioda-mesic" class="flexibee-inp" value="mesic" <#if (perioda?? && perioda == "mesic")>checked</#if>>
                        Měsíční</label>
                    </div>
                </div>
                <div class="col-xs-8">
                    <select name="mesic" class="flexibee-rounded-form-select flexibee-inp form-control">
                        <#list mesice as mesic>
                            <option value="${mesic.id}" <#if (vybranyMesic?? && vybranyMesic == mesic.id) || (!vybranyMesic?? && mesic_index == 0)>selected</#if>>${mesic.name}</option>
                           </#list>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <h2 class="flexibee-step3">Parametry</h2>
    <div>
    <p>Doplňující parametry do přiznání DPH.</p>
        <div class="form-horizontal">
            <div class="form-group">
                <div class="col-xs-4">
                    <label for="koeficient" class="control-label">Koeficient:</label>
                </div>
                <div class="col-xs-8">
                    <input type="text" id="koeficient" name="koeficient" class="flexibee-inp form-control" <#if vybranyKoeficient??>value="${vybranyKoeficient}"</#if>>
                </div>
            </div>
    <#if !slovenskaLegislativa>
            <div class="form-group">
                <div class="col-xs-4">
                    <label for="vyporadaciKoeficient" class="control-label">Vypořádací koeficient:</label>
                </div>
                <div class="col-xs-8">
                    <input type="text" id="vyporadaciKoeficient" name="vyporadaciKoeficient" class="flexibee-inp form-control" <#if vybranyVyporadaciKoeficient??>value="${vybranyVyporadaciKoeficient}"</#if>>
                </div>
            </div>
    </#if>
            <div class="form-group">
                <div class="col-xs-4">
                    <label for="zmenaOdpoctu" class="control-label">Změna odpočtu:</label>
                </div>
                <div class="col-xs-8">
                    <input type="text" id="zmenaOdpoctu" name="zmenaOdpoctu" class="flexibee-inp form-control" <#if vybranaZmenaOdpoctu??>value="${vybranaZmenaOdpoctu}"</#if>>
                </div>
            </div>

    <#if !slovenskaLegislativa>
            <div class="form-group" id="flexibee-kodZdanObd">
                <div class="col-xs-4">
                    <label for="kodZdanObd" class="control-label">Kód zdaňovacího období následujícího roku:</label>
                </div>
                <div class="col-xs-8">
                    <input type="text" id="kodZdanObd" name="kodZdanObd" class="flexibee-inp form-control" <#if vybranyKodZdanObd??>value="${vybranyKodZdanObd}"</#if>>
                </div>
            </div>
    </#if>
        </div>
    </div>

<div class="removeStepsBorder">
    <h2 class="flexibee-step4">Výstup:</h2>
    <div>
        <div class="form-horizontal">
            <div class="form-group">
                <div class="col-xs-4">
                    <div class="radio">
                        <label for="format-pdf" class="control-label">
                        <input type="radio" name="format"  class="flexibee-inp" id="format-pdf" value="pdf" <#if (vybranyFormat?? && vybranyFormat == "pdf")>checked</#if>>
                        PDF formulář:</label>
                    </div>
                </div>
                <div class="col-xs-8">
                    <select name="vystup" class="flexibee-rounded-form-select flexibee-inp form-control">
                        <#list reporty as report>
                            <option value="${report.reportId}" <#if (vybranyVystup?? && vybranyVystup == report.reportId) || (!vybranyVystup?? && report_index == 0)>selected</#if>>${report.reportName}</option>
                        </#list>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="radio">
                            <label for="format-xml" class="control-label">
                        <input type="radio" name="format" id="format-xml" class="flexibee-inp" value="xml" <#if (vybranyFormat?? && vybranyFormat == "xml")>checked</#if>>
                        Elektronické přiznání DPH ve formátu pro el. podatelnu (XML)</label>
                    </div>
                </div>
            </div>
        </div>
    </div>

   </div>
    </div> <#-- flexibee-steps -->
    <@edit.okCancelButton showCancel=false />
<@edit.endForm />


<#if !slovenskaLegislativa>
    <script type="text/javascript">
        function refreshKodZdanObd() {
            var ctvrtleti = $("select[name='ctvrtleti']").val();
            var mesic = $("select[name='mesic']").val();

            var showKodZdanObd = false;
            if ($('input[name="perioda"]')[1].checked) {
                showKodZdanObd = (mesic == "12");
            } else {
                showKodZdanObd = (ctvrtleti == "Q4");
            }

            if (showKodZdanObd) {
                var rok = $("input[name='rok']:checked").val();
                if (rok && mesic) {
                    $.get("${baseUrl}/kod-zdanovaciho-obdobi", { rok: rok, mesic: mesic }, function(data) {
                        $("input[name='kodZdanObd']").val(data);
                        $("#flexibee-kodZdanObd").show();
                    });
                }
            } else {
                $("input[name='kodZdanObd']").val("");
                $("#flexibee-kodZdanObd").hide();
            }
        }

        $(function() {
            $("select[name='mesic']").click(function() {
                $('input[name="perioda"]')[1].checked = true;
                refreshKodZdanObd();
            });
            $("select[name='mesic']").change(function() {
                $('input[name="perioda"]')[1].checked = true;
                refreshKodZdanObd();
            });
            $("select[name='ctvrtleti']").change(function() {
                $('input[name="perioda"]')[0].checked = true;
                refreshKodZdanObd();
            });
            $("select[name='ctvrtleti']").click(function() {
                $('input[name="perioda"]')[0].checked = true;
                refreshKodZdanObd();
            });
            $("select[name='vystup']").change(function() {
                $('input[name="format"]')[0].checked = true;
            });
            $("input[name='perioda']").change(refreshKodZdanObd);
            refreshKodZdanObd();
        });
    </script>
</#if>

<script type="text/javascript">
    $(function() {
        $("form").submit(function() {
            <#--
              - hack, ale ono se to pořádně řešit nedá; lepší hack používá cookies
              - http://geekswithblogs.net/GruffCode/archive/2010/10/28/detecting-the-file-download-dialog-in-the-browser.aspx
              -->
            setTimeout(function() {
                $.hideWait();
            }, 5000);
        });
    });
</script>

<#include "/i-footer-edit.ftl" />
</#escape>
