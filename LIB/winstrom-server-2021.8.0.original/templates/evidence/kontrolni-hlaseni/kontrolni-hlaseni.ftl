<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

    <#include "/i-header-edit.ftl"/>

    <#if chyba??>
    <div class="alert alert-danger">
        <ul class="flexibee-errors">
        ${chyba}
        </ul>
    </div>
    </#if>

    <@edit.beginForm />

<div class="flexibee-steps">
    <#if (staty?size gt 1)>
        <h2 class="flexibee-step1">Stát</h2>
        <div>
            <div class="form-horizontal">
                <#list staty as stat>
                    <div class="radio">
                        <label class="control-label"><input type="radio" name="stat" id="stat-${stat.id}" class="flexibee-inp" value="${stat.id}" <#if (vybranyStat?? && vybranyStat == stat.id) || (!vybranyStat?? && stat_index == (staty?size-1))>checked</#if>>
                        ${stat.name}</label>
                    </div>
                </#list>
            </div>
        </div>
    </#if>

    <h2 class="flexibee-step1">Rok</h2>
    <div>
        <div class="form-horizontal">
            <#list roky as rok>
                <div class="radio">
                    <label class="control-label"><input type="radio" name="rok" id="rok-${rok.id}"  class="flexibee-inp" value="${rok.id}" <#if (vybranyRok?? && vybranyRok?c == rok.id) || (!vybranyRok?? && rok_index == (roky?size-1))>checked</#if>>
                    ${rok.name}</label>
                </div>
            </#list>
        </div>
    </div>

    <h2 class="flexibee-step2">Období přiznání</h2>
    <div>
        <div class="form-horizontal">
            <div class="form-group">
                <div class="col-xs-4">
                    <div class="radio">
                        <label class="control-label"><input type="radio" name="perioda" id="perioda-ctvrtleti" value="ctvrtleti"  class="flexibee-inp" <#if (perioda?? && perioda == "ctvrtleti" || !perioda??)>checked</#if>>
                            Čtvrtletní</label>
                    </div>
                </div>
                <div class="col-xs-8">
                    <select name="ctvrtleti" class="flexibee-rounded-form-select flexibee-inp form-control">
                        <#list ctvrtleti as mesic>
                            <option value="${mesic.id}" <#if (vybranyCvrtleti?? && vybranyCvrtleti == mesic.id) || (!vybranyCvrtleti?? && mesic_index == 0)>selected</#if>>${mesic.name}</option>
                        </#list>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-4">
                    <div class="radio">
                        <label class="control-label"><input type="radio" name="perioda" id="perioda-mesic" class="flexibee-inp" value="mesic" <#if (perioda?? && perioda == "mesic")>checked</#if>>
                            Měsíční</label>
                    </div>
                </div>
                <div class="col-xs-8">
                    <select name="mesic" class="flexibee-rounded-form-select flexibee-inp form-control">
                        <#list mesice as mesic>
                            <option value="${mesic.id}" <#if (vybranyMesic?? && vybranyMesic == mesic.id) || (!vybranyMesic?? && mesic_index == 0)>selected</#if>>${mesic.name}</option>
                        </#list>
                    </select>
                </div>
            </div>
        </div>
    </div>

    <#if czSupported>
        <div id="divDruh">
            <h2 class="flexibee-step1">Druh</h2>
            <div>
                <div class="form-horizontal">
                    <#list druhy as druh>
                        <div class="radio">
                            <label class="control-label"><input type="radio" name="druh" id="druh-${druh.id}"  class="flexibee-inp" value="${druh.id}" <#if (vybranyDruh?? && vybranyDruh == druh.id) || (!vybranyDruh?? && druh_index == (druhy?size-1))>checked</#if>>
                            ${druh.name}</label>
                        </div>
                    </#list>
                </div>
            </div>

            <h2 class="flexibee-step3">Nepovinné parametry</h2>
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-xs-4">
                        <label for="datumZjisteni">${lang('labels', 'konVykDph.duvody', 'Důvody pro podání následného kontrolního hlášení zjištěny dne:')}</label>
                    </div>
                    <div class="col-xs-8">
                        <input type="text" format="dd.mm.yyyy" name="datumZjisteni" id="datumZjisteni" value="<#if datumZjisteni??>${datumZjisteni}</#if>" class="flexibee-edit-date flexibee-inp form-control">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-4">
                        <label for="vyzvaOdpovedB">${lang('labels', 'konVykDph.rychlaOdpoved', 'Rychlá odpověď na výzvu')}:</label>
                    </div>
                    <div class="col-xs-8">
                        <input type="checkbox" name="vyzvaOdpovedB" class="flexibee-inp" id="flexibee-inp-vyzvaOdpovedB" value="true" <#if vyzvaOdpovedB?? && vyzvaOdpovedB>checked</#if>>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-4">
                        <label for="vyzvaOdpovedB">${lang('labels', 'konVykDph.cisloJednaci', 'Číslo jednací výzvy')}</label>
                    </div>
                    <div class="col-xs-8">
                        <input type="text" name="cisloJednaciVyzvy" class="flexibee-inp form-control" <#if cisloJednaciVyzvy??>value="${cisloJednaciVyzvy}"</#if>>
                    </div>
                </div>

                <div class="form-horizontal">
                    <#list vyzvy as vyzva>
                        <div class="radio">
                            <label class="control-label"><input type="radio" name="vyzvaOdpoved" id="vyzva-${vyzva.id}"  class="flexibee-inp" value="${vyzva.id}" <#if (vyzvaOdpoved?? && vyzvaOdpoved == vyzva.id) || (!vyzvaOdpoved?? && vyzva_index == (vyzva?size-1))>checked</#if>>
                            ${vyzva.name}</label>
                        </div>
                    </#list>
                </div>

            </div>

        </div>
    </#if>

    <h2 class="flexibee-step1"></h2><#--Vertikalni mezera-->

</div>

    <#-- flexibee-steps -->
    <@edit.okCancelButton showCancel=false />
    <@edit.endForm />

<script type="text/javascript">

    $(function() {
        $("select[name='mesic']").click(function() {
            $('input[name="perioda"]')[1].checked = true;
        });
        $("select[name='mesic']").change(function() {
            $('input[name="perioda"]')[1].checked = true;
        });
        $("select[name='ctvrtleti']").change(function() {
            $('input[name="perioda"]')[0].checked = true;
        });
        $("select[name='ctvrtleti']").click(function() {
            $('input[name="perioda"]')[0].checked = true;
        });
        $("input[name='stat']").click(function() {
            showHideDruh();
        });
        $("input[name='stat']").change(function() {
            showHideDruh();
        });
    });

    function showHideDruh() {
        var statNotCZ = $('input[name="stat"]:checked').val() !== 'CZ';
        if (statNotCZ) {
            $('#divDruh').hide();
        } else {
            $('#divDruh').show();
        }
    }

    $(function() {
        $("form").submit(function() {
        <#--
          - hack, ale ono se to pořádně řešit nedá; lepší hack používá cookies
          - http://geekswithblogs.net/GruffCode/archive/2010/10/28/detecting-the-file-download-dialog-in-the-browser.aspx
          -->
            setTimeout(function() {
                $.hideWait();
            }, 5000);
        });
    });
</script>

<#include "/i-footer-edit.ftl" />
</#escape>
