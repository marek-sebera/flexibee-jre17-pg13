<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.${it.senchaName}.DetailController', {
    extend: 'FlexiBee.main.AbstractController',

    requires: [
        'FlexiBee.${it.senchaName}.Detail',
        'FlexiBee.${it.senchaName}.LiveFormEnhancer'
    ],

    title: '',
    model: null,
    formPanel: null,

    createView: function() {
        var tabTitle = this.title;
        return Ext.create('FlexiBee.${it.senchaName}.Detail', {
            closable: true,
            title: tabTitle,
            showRelations: !this.model.phantom // don't show relations for new objects
        });
    },

    setupControl: function() {
        this.controlOver({
            'fbEditForm${it.senchaName?cap_first} field': {
                change: Ext.create('FlexiBee.${it.senchaName}.LiveFormEnhancer').getHandler()
            },
            'fbEditForm${it.senchaName?cap_first} button[itemId="save"]': {
                click: this.onSaveButtonClicked
            },
            'fbEditForm${it.senchaName?cap_first} button[itemId="cancel"]': {
                click: this.onCancelButtonClicked
            },
            'fbToolbar${it.senchaName?cap_first} button[itemId="delete"]': {
                click: Ext.emptyFn // TODO
            }
        });

        this.formPanel = this.view.query('fbEditForm${it.senchaName?cap_first}')[0];

        var loadRecordListeners = [this.formPanel];
        var toolbar = this.view.query('fbToolbar${it.senchaName?cap_first}')[0];
        if (toolbar) {
            loadRecordListeners.push(toolbar);
        }
        var relations = this.view.query('fbRelations${it.senchaName?cap_first}')[0];
        if (relations) {
            loadRecordListeners.push(relations);
        }

        var me = this;
        Ext.iterate(loadRecordListeners, function(listener) {
            listener.loadRecord(me.model);
        });
    },

    onSaveButtonClicked: function() {
        var me = this;

        FlexiBee.MessageBus.publish('status', { busy: true });
        me.formPanel.getForm().updateRecord();
        me.model.save({
            callback: function() {
                // TODO check if succeeded
                me.closeTab();

                FlexiBee.MessageBus.publish('status', { busy: false });
            }
        });
    },

    onCancelButtonClicked: function() {
        this.closeTab();
    }
});

</#escape>
