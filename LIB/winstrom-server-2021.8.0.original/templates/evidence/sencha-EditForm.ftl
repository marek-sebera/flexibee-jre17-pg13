<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.${it.senchaName}.EditForm', {
    extend: 'FlexiBee.${it.senchaName}.BaseEditForm',
    alias: 'widget.fbEditForm${it.senchaName?cap_first}',

    requires: [
    ]
});

</#escape>
