<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>
<#import "/l-edits.ftl" as edit />

<#if chyba??>
    <div class="alert alert-danger">
        <ul class="flexibee-errors">
            ${chyba}
        </ul>
    </div>
</#if>

<@edit.beginForm />

    <div class="flexibee-steps">
<div class="removeStepsBorder">
    <h2 class="flexibee-step1">${lang('labels', 'vybraneDatumyUctovani', 'Datum účtování')}</h2>
    <div>
        <div class="form-horizontal">
            <div class="form-group">
                <div class="col-xs-4">
                       <label for="datUctoOd" class="control-label">${lang('labels', 'od', 'Od')}</label>
                </div>
                <div class="col-xs-8">
                    <input type="text" format="dd.mm.yyyy" name="datUctoOd" id="datUctoOd" value="<#if datUctoOd??>${datUctoOd}</#if>" class="flexibee-edit-date form-control flexibee-inp">
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-4">
                    <label for="datUctoDo" class="control-label">${lang('labels', 'do', 'Do')}</label>
                </div>
                <div class="col-xs-8">
                    <input type="text" format="dd.mm.yyyy" name="datUctoDo" id="datUctoDo" value="<#if datUctoDo??>${datUctoDo}</#if>" class="flexibee-edit-date form-control flexibee-inp">
                </div>
            </div>
        </div>
    </div> <#-- flexibee-steps -->
        <@edit.okCancelButton showCancel=false />
<@edit.endForm />

<script type="text/javascript">
    $(function() {
        $("form").submit(function() {
            <#--
              - hack, ale ono se to pořádně řešit nedá; lepší hack používá cookies
              - http://geekswithblogs.net/GruffCode/archive/2010/10/28/detecting-the-file-download-dialog-in-the-browser.aspx
              -->
            setTimeout(function() {
                $.hideWait();
            }, 5000);
        });
    });
</script>

<#include "/i-footer-edit.ftl" />
</#escape>