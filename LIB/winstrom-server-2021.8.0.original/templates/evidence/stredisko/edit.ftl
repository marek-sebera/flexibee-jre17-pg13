<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>

${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}

<@edit.beginForm />
    <#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canEditAtLeastOne(items, object, ['kod', 'nazev'])>
    <table class="flexibee-tbl-1">
        <col width="20%" /><col width="80%" />
        <#if canEditAtLeastOne(items, object, ['kod'])>
        <tr>
            <td><@edit.label name='kod' items=items /></td><td><@edit.place object=object name='kod' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['nazev'])>
        <tr>
            <td><@edit.label name='nazev' items=items /></td><td><@edit.place object=object name='nazev' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canEditAtLeastOne(items, object, ['ulice', 'mesto', 'psc', 'stat', 'tel', 'mobil', 'fax', 'email', 'www', 'eetProvozovna', 'eetLimit', 'popis', 'poznam', 'platiOd', 'platiDo'])>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canEditAtLeastOne(items, object, ['ulice', 'mesto', 'psc', 'stat', 'tel', 'mobil', 'fax', 'email', 'www'])>
            <li class="active"><a href="#edit-fragment-1" data-toggle="tab"><span>${lang('labels', 'adresaPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['eetProvozovna', 'eetLimit'])>
            <li><a href="#edit-fragment-2" data-toggle="tab"><span>${lang('labels', 'dokladEBEetPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <li><a href="#edit-fragment-3" data-toggle="tab"><span>${lang('labels', 'textyPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
            <li><a href="#edit-fragment-4" data-toggle="tab"><span>${lang('labels', 'spravaPN')}</span></a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canEditAtLeastOne(items, object, ['ulice', 'mesto', 'psc', 'stat', 'tel', 'mobil', 'fax', 'email', 'www'])>
            <div id="edit-fragment-1" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['ulice', 'mesto', 'psc', 'stat', 'tel', 'mobil', 'fax', 'email', 'www'])>
                <table class="flexibee-tbl-1">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canEditAtLeastOne(items, object, ['ulice'])>
                    <tr>
                        <td><@edit.label name='ulice' items=items /></td><td colspan="3"><@edit.place object=object name='ulice' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['mesto', 'psc'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['mesto'])>
                        <td><@edit.label name='mesto' items=items /></td><td><@edit.place object=object name='mesto' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['psc'])>
                        <td><@edit.label name='psc' items=items /></td><td><@edit.place object=object name='psc' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['stat'])>
                    <tr>
                        <td><@edit.label name='stat' items=items /></td><td colspan="3"><@edit.place object=object name='stat' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['tel'])>
                    <tr>
                        <td><@edit.label name='tel' items=items /></td><td><@edit.place object=object name='tel' items=items marker=marker  type='tel'/></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['mobil', 'fax'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['mobil'])>
                        <td><@edit.label name='mobil' items=items /></td><td><@edit.place object=object name='mobil' items=items marker=marker  type='tel'/></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['fax'])>
                        <td><@edit.label name='fax' items=items /></td><td><@edit.place object=object name='fax' items=items marker=marker  type='fax'/></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['email', 'www'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['email'])>
                        <td><@edit.label name='email' items=items /></td><td><@edit.place object=object name='email' items=items marker=marker  type='email'/></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['www'])>
                        <td><@edit.label name='www' items=items /></td><td><@edit.place object=object name='www' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['eetProvozovna', 'eetLimit'])>
            <div id="edit-fragment-2" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['eetProvozovna', 'eetLimit'])>
                <table class="flexibee-tbl-1">
                    <col width="35%" /><col width="65%" />
                    <#if canEditAtLeastOne(items, object, ['eetProvozovna'])>
                    <tr>
                        <td><@edit.label name='eetProvozovna' items=items /></td><td><@edit.place object=object name='eetProvozovna' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['eetLimit'])>
                    <tr>
                        <td><@edit.label name='eetLimit' items=items /></td><td><@edit.place object=object name='eetLimit' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <div id="edit-fragment-3" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
                <table class="flexibee-tbl-1">
                    <#if canEditAtLeastOne(items, object, ['popis'])>
                    <tr>
                        <td><@edit.label name='popis' items=items /><br><@edit.textarea object=object name='popis' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['poznam'])>
                    <tr>
                        <td><@edit.label name='poznam' items=items /><br><@edit.textarea object=object name='poznam' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
            <div id="edit-fragment-4" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <tr>
                        <td colspan="2">Platnost se udává v kalendářních rocích, pro neomezenou platnost zadejte 0-9999</td>
                    </tr>

                    <#if canEditAtLeastOne(items, object, ['platiOd'])>
                    <tr>
                        <td><@edit.label name='platiOd' items=items /></td><td><@edit.place object=object name='platiOd' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['platiDo'])>
                    <tr>
                        <td><@edit.label name='platiDo' items=items /></td><td><@edit.place object=object name='platiDo' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

    <br/>
    <@edit.listItems items=items object=object marker=marker />

    <@edit.saveButton />
<@edit.endForm />

<@edit.generateNaiveJavascript object=object it=it />

<#include "/parts/i-dalsi-informace.ftl" />

<#include "/i-footer-edit.ftl" />
</#escape>
