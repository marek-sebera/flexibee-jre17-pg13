<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-view.ftl" />

${marker.mark('chybaPreceneni')}
${marker.mark('zmenaZaver')}


<#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canShowAtLeastOne(items, object, ['kod', 'rokProRadu', 'platiOdData', 'platiDoData'], it)>
    <table class="flexibee-tbl-1">
        <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
        <#if canShowAtLeastOne(items, object, ['kod', 'rokProRadu'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['kod'], it)>
            <td><@tool.label name='kod' items=items />:</td><td><@tool.place object=object name='kod' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['rokProRadu'], it)>
            <td><@tool.label name='rokProRadu' items=items />:</td><td><@tool.place object=object name='rokProRadu' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['platiOdData'], it)>
        <tr>
            <td><@tool.label name='platiOdData' items=items />:</td><td><@tool.place object=object name='platiOdData' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['platiDoData'], it)>
        <tr>
            <td><@tool.label name='platiDoData' items=items />:</td><td><@tool.place object=object name='platiDoData' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
    <h4>${lang('labels', 'textyPN')}</h4>
    <div id="view-fragment-1" class="tab-pane">
        <div class="flexibee-table-border">
        <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
        <table class="flexibee-tbl-1">
            <#if canShowAtLeastOne(items, object, ['popis'], it)>
            <tr>
                <td><@tool.label name='popis' items=items />:<br><@tool.placeArea object=object name='popis' items=items marker=marker /></td>
            </tr>
            </#if>
            <#if canShowAtLeastOne(items, object, ['poznam'], it)>
            <tr>
                <td><@tool.label name='poznam' items=items />:<br><@tool.placeArea object=object name='poznam' items=items marker=marker /></td>
            </tr>
            </#if>
        </table>
        </#if>
        </div>

    </div>
    </#if>

<#--AUTOGEN END-->

<@tool.listItems items=items object=object marker=marker />

<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />

<#include "/i-footer-view.ftl" />
</#escape>