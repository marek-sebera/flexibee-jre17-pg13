<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-view.ftl" />

<#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canShowAtLeastOne(items, object, ['nazev', 'clenEu', 'mena', 'telPredvolba'], it)>
    <table class="flexibee-tbl-1">
        <col width="20%" /><col width="80%" />
        <#if canShowAtLeastOne(items, object, ['nazev'], it)>
        <tr>
            <td><@tool.label name='nazev' items=items />:</td><td><@tool.place object=object name='nazev' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['clenEu'], it)>
        <tr>
            <td><@tool.label name='clenEu' items=items />:</td><td><@tool.place object=object name='clenEu' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['mena'], it)>
        <tr>
            <td><@tool.label name='mena' items=items />:</td><td><@tool.place object=object name='mena' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['telPredvolba'], it)>
        <tr>
            <td><@tool.label name='telPredvolba' items=items />:</td><td><@tool.place object=object name='telPredvolba' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canShowAtLeastOne(items, object, ['kod', 'kodAlpha3', 'kodNum'], it)>
    <h4>${lang('labels', 'statKodyDleIsoLB')}</h4>
    <div id="view-fragment-1" class="tab-pane">
        <div class="flexibee-table-border">
        <#if canShowAtLeastOne(items, object, ['kod', 'kodAlpha3', 'kodNum'], it)>
        <table class="flexibee-tbl-1">
            <col width="10%" /><col width="20%" /><col width="15%" /><col width="20%" /><col width="15%" /><col width="20%" />
            <#if canShowAtLeastOne(items, object, ['kod', 'kodAlpha3', 'kodNum'], it)>
            <tr>
                <#if canShowAtLeastOne(items, object, ['kod'], it)>
                <td><@tool.label name='kod' items=items />:</td><td><@tool.place object=object name='kod' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canShowAtLeastOne(items, object, ['kodAlpha3'], it)>
                <td><@tool.label name='kodAlpha3' items=items />:</td><td><@tool.place object=object name='kodAlpha3' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canShowAtLeastOne(items, object, ['kodNum'], it)>
                <td><@tool.label name='kodNum' items=items />:</td><td><@tool.place object=object name='kodNum' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
        </table>
        </#if>
        </div>

    </div>
    </#if>

    <#if canShowAtLeastOne(items, object, ['nazZemeC25', 'kodDph'], it)>
    <h4>${lang('labels', 'statNastFinSpravaPN')}</h4>
    <div id="view-fragment-2" class="tab-pane">
        <div class="flexibee-table-border">
        <#if canShowAtLeastOne(items, object, ['nazZemeC25', 'kodDph'], it)>
        <table class="flexibee-tbl-1">
            <col width="20%" /><col width="80%" />
            <#if canShowAtLeastOne(items, object, ['nazZemeC25'], it)>
            <tr>
                <td><@tool.label name='nazZemeC25' items=items />:</td><td><@tool.place object=object name='nazZemeC25' items=items marker=marker /></td>
            </tr>
            </#if>
            <#if canShowAtLeastOne(items, object, ['kodDph'], it)>
            <tr>
                <td><@tool.label name='kodDph' items=items />:</td><td><@tool.place object=object name='kodDph' items=items marker=marker /></td>
            </tr>
            </#if>
        </table>
        </#if>
        </div>

    </div>
    </#if>

    <#if canShowAtLeastOne(items, object, ['vatId', 'vatIdMoss', 'fuKraj', 'fuUzPrac'], it)>
    <h4>${lang('labels', 'statNastProStatPN')}</h4>
    <div id="view-fragment-3" class="tab-pane">
        <div class="flexibee-table-border">
        <#if canShowAtLeastOne(items, object, ['vatId', 'vatIdMoss', 'fuKraj', 'fuUzPrac'], it)>
        <table class="flexibee-tbl-1">
            <col width="20%" /><col width="80%" />
            <#if canShowAtLeastOne(items, object, ['vatId'], it)>
            <tr>
                <td><@tool.label name='vatId' items=items />:</td><td><@tool.place object=object name='vatId' items=items marker=marker /></td>
            </tr>
            </#if>
            <#if canShowAtLeastOne(items, object, ['vatIdMoss'], it)>
            <tr>
                <td><@tool.label name='vatIdMoss' items=items />:</td><td><@tool.place object=object name='vatIdMoss' items=items marker=marker /></td>
            </tr>
            </#if>
            <#if canShowAtLeastOne(items, object, ['fuKraj'], it)>
            <tr>
                <td><@tool.label name='fuKraj' items=items />:</td><td><@tool.place object=object name='fuKraj' items=items marker=marker /></td>
            </tr>
            </#if>
            <#if canShowAtLeastOne(items, object, ['fuUzPrac'], it)>
            <tr>
                <td><@tool.label name='fuUzPrac' items=items />:</td><td><@tool.place object=object name='fuUzPrac' items=items marker=marker /></td>
            </tr>
            </#if>
        </table>
        </#if>
        </div>

    </div>
    </#if>

    <#if canShowAtLeastOne(items, object, ['popis', 'poznam', 'platiOd', 'platiDo'], it)>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
            <li class="active"><a href="#view-fragment-4" data-toggle="tab">${lang('labels', 'textyPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['platiOd', 'platiDo'], it)>
            <li><a href="#view-fragment-5" data-toggle="tab">${lang('labels', 'spravaPN')}</a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
            <div id="view-fragment-4" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
                <table class="flexibee-tbl-1">
                    <#if canShowAtLeastOne(items, object, ['popis'], it)>
                    <tr>
                        <td><@tool.label name='popis' items=items />:<br><@tool.placeArea object=object name='popis' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['poznam'], it)>
                    <tr>
                        <td><@tool.label name='poznam' items=items />:<br><@tool.placeArea object=object name='poznam' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['platiOd', 'platiDo'], it)>
            <div id="view-fragment-5" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['platiOd', 'platiDo'], it)>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <tr>
                        <td colspan="2">Platnost se udává v kalendářních rocích, pro neomezenou platnost zadejte 0-9999</td>
                    </tr>

                    <#if canShowAtLeastOne(items, object, ['platiOd'], it)>
                    <tr>
                        <td><@tool.label name='platiOd' items=items />:</td><td><@tool.place object=object name='platiOd' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['platiDo'], it)>
                    <tr>
                        <td><@tool.label name='platiDo' items=items />:</td><td><@tool.place object=object name='platiDo' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

<@tool.listItems items=items object=object marker=marker />

<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />

<#include "/i-footer-view.ftl" />
</#escape>