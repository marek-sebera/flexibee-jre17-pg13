<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#if it.object??>

<#assign si = it.evidenceResource.getSouhrnInfo(it.object) />
<#if !si.empty>
<#if si.nezaplacenoPoDatSplat?? && si.nezaplacenoPoDatSplat &gt; 0>
<div class="alert alert-danger" role="alert">Nezaplaceno po splatnosti:
    <a href="/c/${companyId}/faktura-vydana/${urlEncode("(firma=${it.object.id?c} and stavUhrK != 'stavUhr.uhrazeno' and stavUhrK != 'stavUhr.uhrazenoRucne' and datSplat lt now() and storno = false)")}" style="color:red">${si.nezaplacenoPoDatSplat?string(",##0.00")}</a></div>
</#if>
<div class="flexibee-table-border-info">
<table class="flexibee-tbl-1">
    <col width="20%"/>
    <col width="80%"/>
<#if si.limitFakt?? && si.limitFakt &gt; 0>
<tr><td>Limit fakturace:</td><td>${si.limitFakt?string(",##0.00")}</td></tr>
</#if>
<#if si.celkFakt??>
<tr><td>Celkem fakturováno:</td><td>
<a href="/c/${companyId}/faktura-vydana/${urlEncode("(firma=${it.object.id?c})")}">${si.celkFakt?string(",##0.00")}</a></td></tr>
</#if>
<#if si.nezaplaceno?? && si.nezaplaceno &gt; 0>
<tr><td><strong>Nezaplaceno:</strong></td><td><strong>
<a href="/c/${companyId}/faktura-vydana/${urlEncode("(firma=${it.object.id?c} and stavUhrK != 'stavUhr.uhrazeno' and stavUhrK != 'stavUhr.uhrazenoRucne' and storno = false)")}">${si.nezaplaceno?string(",##0.00")}</a>
</strong></td></tr>
</#if>
<#if si.splatText?? && si.splatText != '' && si.splatText != '0'>
<tr><td>Splatnost:</td><td>${si.splatText}</td></tr>
</#if>

<#if si.prumProdleva?? && si.prumProdleva &gt; 0>
<tr><td>Průměrná prodleva:</td><td>${si.prumProdleva?string(",##0.00")} dní</td></tr>
</#if>
<#if si.neuzPpp?? && si.neuzPpp &gt; 0>
<tr><td>Poptávky přijaté:</td><td>${si.neuzPpp?string(",##0.00")}</td></tr>
</#if>
<#if si.neuzObp?? && si.neuzObp &gt; 0>
<tr><td>Objednávky přijaté:</td><td>${si.neuzObp?string(",##0.00")}</td></tr>
</#if>
<#if si.neuzPpv?? && si.neuzPpv &gt; 0>
<tr><td>Vydáné poptávky:</td><td>${si.neuzPpv?string(",##0.00")}</td></tr>
</#if>
<#if si.neuzNap?? && si.neuzNap &gt; 0>
<tr><td>Nabídky přijaté:</td><td>${si.neuzNap?string(",##0.00")}</td></tr>
</#if>
<#if si.neuzObv?? && si.neuzObv &gt; 0>
<tr><td>Objednávky vydané:</td><td>${si.neuzObv?string(",##0.00")}</td></tr>
</#if>
</table>
</div>
</#if>

</#if>

</#escape>