<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>


<div id="flexibee-search-adresar" class="flexibee-search" style="display:none">
    <form class="form-inline">
        <div class="input-group">
            <div id="flexibee-search-adresar-flexbox"></div>
            <div class="input-group-btn">
                <button type="submit" class="btn btn-default btn-sm" title="${lang('labels', 'search', 'Hledat')} ${lang('formsInfo', 'adrAdresar', 'Adresy firem')}"><span class="glyphicon glyphicon-search"></span></button>
            </div>
         </div>
    </form>
</div>

<script type="text/javascript">
    <#assign adresarUrl = formListUrl(it.companyResource, 'ADRESAR')!"" />
    function initSearchAdresarFlexBox(url) {
        $('#flexibee-search-adresar-flexbox').flexbox(url + '.json?mode=suggest', {
            showArrow: false,
            autoCompleteFirstMatch: false,
            watermark: "${lang('labels', 'search', 'Hledat')} ${lang('formsInfo', 'adrAdresar', 'Adresy firem')}",
            noResultsText: '${lang('messages', 'suggest.nothingFound', 'Nic nenalezeno')}',
            paging: {
                style: 'links',
                summaryTemplate: '${lang('messages', 'suggest.summary', '{start} až {end} z celkem {total}')}'
            },
            onSelect: function() {
                window.location = url + "/" + this.getAttribute('hiddenValue');
            },
            inputClass: "form-control input-sm"
        });
    }
    $(function() {
        setTimeout(function() {
            initSearchAdresarFlexBox('${adresarUrl}');
        }, 100);

        $("#flexibee-search-adresar form").submit(function() {
            var query = $("#flexibee-search-adresar-flexbox_input").val();
            window.location = '${adresarUrl}?q=' + encodeURIComponent(query);
            return false;
        });
        $("#flexibee-search-adresar").show();
    });
</script>


</#escape>