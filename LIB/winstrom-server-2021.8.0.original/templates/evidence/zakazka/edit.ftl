<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>

${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}

<@edit.beginForm />
    <#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canEditAtLeastOne(items, object, ['nazev', 'kod'])>
    <table class="flexibee-tbl-1">
        <col width="20%" /><col width="80%" />
        <#if canEditAtLeastOne(items, object, ['nazev'])>
        <tr>
            <td><@edit.label name='nazev' items=items /></td><td><@edit.place object=object name='nazev' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['kod'])>
        <tr>
            <td><@edit.label name='kod' items=items /></td><td><@edit.place object=object name='kod' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canEditAtLeastOne(items, object, ['typZakazky', 'zodpPrac', 'stredisko', 'kontaktOsoba', 'firma', 'stavZakazky', 'datZahaj', 'datKonec', 'termin', 'splatDny', 'vyhZakazky', 'procVyh', 'rozsah', 'varSym', 'nakladyPredpoklad', 'mena', 'ziskPredpoklad', 'mistUrc', 'popis', 'poznam', 'platiOd', 'platiDo'])>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canEditAtLeastOne(items, object, ['typZakazky', 'zodpPrac', 'stredisko', 'kontaktOsoba', 'firma', 'stavZakazky', 'datZahaj', 'datKonec', 'termin', 'splatDny', 'vyhZakazky', 'procVyh', 'rozsah', 'varSym', 'nakladyPredpoklad', 'mena', 'ziskPredpoklad', 'mistUrc'])>
            <li class="active"><a href="#edit-fragment-1" data-toggle="tab"><span>${lang('labels', 'zaklInformacePN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <li><a href="#edit-fragment-2" data-toggle="tab"><span>${lang('labels', 'textyPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
            <li><a href="#edit-fragment-3" data-toggle="tab"><span>${lang('labels', 'spravaPN')}</span></a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canEditAtLeastOne(items, object, ['typZakazky', 'zodpPrac', 'stredisko', 'kontaktOsoba', 'firma', 'stavZakazky', 'datZahaj', 'datKonec', 'termin', 'splatDny', 'vyhZakazky', 'procVyh', 'rozsah', 'varSym', 'nakladyPredpoklad', 'mena', 'ziskPredpoklad', 'mistUrc'])>
            <div id="edit-fragment-1" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['typZakazky', 'zodpPrac', 'stredisko', 'kontaktOsoba', 'firma', 'stavZakazky', 'datZahaj', 'datKonec', 'termin', 'splatDny', 'vyhZakazky', 'procVyh', 'rozsah', 'varSym', 'nakladyPredpoklad', 'mena', 'ziskPredpoklad', 'mistUrc'])>
                <table class="flexibee-tbl-1">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canEditAtLeastOne(items, object, ['typZakazky', 'zodpPrac'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['typZakazky'])>
                        <td><@edit.label name='typZakazky' items=items /></td><td><@edit.place object=object name='typZakazky' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['zodpPrac'])>
                        <td><@edit.label name='zodpPrac' items=items /></td><td><@edit.place object=object name='zodpPrac' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['stredisko', 'kontaktOsoba'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['stredisko'])>
                        <td><@edit.label name='stredisko' items=items /></td><td><@edit.place object=object name='stredisko' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['kontaktOsoba'])>
                        <td><@edit.label name='kontaktOsoba' items=items /></td><td><@edit.place object=object name='kontaktOsoba' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['firma', 'stavZakazky'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['firma'])>
                        <td><@edit.label name='firma' items=items /></td><td><@edit.place object=object name='firma' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['stavZakazky'])>
                        <td><@edit.label name='stavZakazky' items=items /></td><td><@edit.place object=object name='stavZakazky' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['datZahaj', 'datKonec'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['datZahaj'])>
                        <td><@edit.label name='datZahaj' items=items /></td><td><@edit.place object=object name='datZahaj' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['datKonec'])>
                        <td><@edit.label name='datKonec' items=items /></td><td><@edit.place object=object name='datKonec' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['termin', 'splatDny'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['termin'])>
                        <td><@edit.label name='termin' items=items /></td><td><@edit.place object=object name='termin' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['splatDny'])>
                        <td><@edit.label name='splatDny' items=items /></td><td><@edit.place object=object name='splatDny' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['vyhZakazky', 'procVyh'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['vyhZakazky'])>
                        <td><@edit.label name='vyhZakazky' items=items /></td><td><@edit.place object=object name='vyhZakazky' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['procVyh'])>
                        <td><@edit.label name='procVyh' items=items /></td><td><@edit.place object=object name='procVyh' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['rozsah', 'varSym'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['rozsah'])>
                        <td><@edit.label name='rozsah' items=items /></td><td><@edit.place object=object name='rozsah' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['varSym'])>
                        <td><@edit.label name='varSym' items=items /></td><td><@edit.place object=object name='varSym' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['nakladyPredpoklad', 'mena'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['nakladyPredpoklad'])>
                        <td><@edit.label name='nakladyPredpoklad' items=items /></td><td><@edit.place object=object name='nakladyPredpoklad' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['mena'])>
                        <td><@edit.label name='mena' items=items /></td><td><@edit.place object=object name='mena' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['ziskPredpoklad', 'mistUrc'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['ziskPredpoklad'])>
                        <td><@edit.label name='ziskPredpoklad' items=items /></td><td><@edit.place object=object name='ziskPredpoklad' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['mistUrc'])>
                        <td><@edit.label name='mistUrc' items=items /></td><td><@edit.place object=object name='mistUrc' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <div id="edit-fragment-2" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
                <table class="flexibee-tbl-1">
                    <#if canEditAtLeastOne(items, object, ['popis'])>
                    <tr>
                        <td><@edit.label name='popis' items=items /><br><@edit.textarea object=object name='popis' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['poznam'])>
                    <tr>
                        <td><@edit.label name='poznam' items=items /><br><@edit.textarea object=object name='poznam' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
            <div id="edit-fragment-3" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <tr>
                        <td colspan="2">Platnost se udává v kalendářních rocích, pro neomezenou platnost zadejte 0-9999</td>
                    </tr>

                    <#if canEditAtLeastOne(items, object, ['platiOd'])>
                    <tr>
                        <td><@edit.label name='platiOd' items=items /></td><td><@edit.place object=object name='platiOd' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['platiDo'])>
                    <tr>
                        <td><@edit.label name='platiDo' items=items /></td><td><@edit.place object=object name='platiDo' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

    <br/>
    <@edit.listItems items=items object=object marker=marker />

    <@edit.saveButton />
<@edit.endForm />

<@edit.generateNaiveJavascript object=object it=it />

<#include "/parts/i-dalsi-informace.ftl" />

<#include "/i-footer-edit.ftl" />
</#escape>
