<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-view.ftl" />

${marker.mark('typOrganizace')}

<#--AUTOGEN BEGIN-->
    <div id="flexibee-smlouva-hlavicka">

    <div class="flexibee-table-border">
    <table class="flexibee-tbl-1">
        <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
        <tr>
            <td><@tool.label name='typSml' items=items />:</td><td><@tool.place object=object name='typSml' items=items marker=marker /></td>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['firma'], it)>
            <td><@tool.label name='firma' items=items />:</td><td><@tool.place object=object name='firma' items=items marker=marker /></td>
            </#if>
        </tr>

        <#if canShowAtLeastOne(items, object, ['kod'], it)>
        <tr>
            <td><@tool.label name='kod' items=items />:</td><td><@tool.place object=object name='kod' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['nazev'], it)>
        <tr>
            <td><@tool.label name='nazev' items=items />:</td><td colspan="3"><@tool.place object=object name='nazev' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>

    </div>

    <#if canShowAtLeastOne(items, object, ['smlouvaOd', 'smlouvaDo', 'zakazka', 'stredisko', 'stavSml', 'typDoklFak', 'frekFakt', 'den', 'mesic', 'varSym', 'konSym', 'popis', 'poznam'], it)>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canShowAtLeastOne(items, object, ['smlouvaOd', 'smlouvaDo', 'zakazka', 'stredisko', 'stavSml'], it)>
            <li class="active"><a href="#view-fragment-1" data-toggle="tab">Základní</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['typDoklFak', 'frekFakt', 'den', 'mesic', 'varSym', 'konSym'], it)>
            <li><a href="#view-fragment-2" data-toggle="tab">Nastavení generace</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
            <li><a href="#view-fragment-3" data-toggle="tab">${lang('labels', 'textyPN')}</a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canShowAtLeastOne(items, object, ['smlouvaOd', 'smlouvaDo', 'zakazka', 'stredisko', 'stavSml'], it)>
            <div id="view-fragment-1" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['smlouvaOd', 'smlouvaDo', 'zakazka', 'stredisko', 'stavSml'], it)>
                <table class="flexibee-tbl-1">
                    <#if canShowAtLeastOne(items, object, ['smlouvaOd', 'smlouvaDo'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['smlouvaOd'], it)>
                        <td><@tool.label name='smlouvaOd' items=items />:</td><td><@tool.place object=object name='smlouvaOd' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['smlouvaDo'], it)>
                        <td><@tool.label name='smlouvaDo' items=items />:</td><td><@tool.place object=object name='smlouvaDo' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['zakazka'], it)>
                    <tr>
                        <td><@tool.label name='zakazka' items=items />:</td><td><@tool.place object=object name='zakazka' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['stredisko'], it)>
                    <tr>
                        <td><@tool.label name='stredisko' items=items />:</td><td><@tool.place object=object name='stredisko' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['stavSml'], it)>
                    <tr>
                        <td><@tool.label name='stavSml' items=items />:</td><td><@tool.place object=object name='stavSml' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['typDoklFak', 'frekFakt', 'den', 'mesic', 'varSym', 'konSym'], it)>
            <div id="view-fragment-2" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['typDoklFak', 'frekFakt', 'den', 'mesic', 'varSym', 'konSym'], it)>
                <table class="flexibee-tbl-1">
                    <#if canShowAtLeastOne(items, object, ['typDoklFak'], it)>
                    <tr>
                        <td><@tool.label name='typDoklFak' items=items />:</td><td><@tool.place object=object name='typDoklFak' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['frekFakt'], it)>
                    <tr>
                        <td><@tool.label name='frekFakt' items=items level='2'/>:</td><td><@tool.place object=object level='2' name='frekFakt' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['den', 'mesic'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['den'], it)>
                        <td><@tool.label name='den' items=items />:</td><td><@tool.place object=object name='den' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['mesic'], it)>
                        <td><@tool.label name='mesic' items=items />:</td><td><@tool.place object=object name='mesic' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['varSym', 'konSym'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['varSym'], it)>
                        <td><@tool.label name='varSym' items=items />:</td><td><@tool.place object=object name='varSym' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['konSym'], it)>
                        <td><@tool.label name='konSym' items=items />:</td><td><@tool.place object=object name='konSym' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
            <div id="view-fragment-3" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
                <table class="flexibee-tbl-1">
                    <#if canShowAtLeastOne(items, object, ['popis'], it)>
                    <tr>
                        <td><@tool.label name='popis' items=items />:<br><@tool.placeArea object=object name='popis' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['poznam'], it)>
                    <tr>
                        <td><@tool.label name='poznam' items=items />:<br><@tool.placeArea object=object name='poznam' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

    </div>

    <#include "/parts/i-polozky-smlouvy.ftl" /><br><br>

<#--AUTOGEN END-->

<@tool.listItems items=items object=object marker=marker />

<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />

<#include "/i-footer-view.ftl" />
</#escape>