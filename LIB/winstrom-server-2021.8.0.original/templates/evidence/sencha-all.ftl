<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.${it.senchaName}.All', {
});


<#list [
"sencha-Properties",
"sencha-Model",
"sencha-Store",
"sencha-Toolbar",
"sencha-QuickFilter",
"sencha-BaseEditForm",
"sencha-Grid",
"sencha-LiveFormEnhancer",
"sencha-EditForm",
"sencha-Relations",
"sencha-BaseDetail",
"sencha-Detail",
"sencha-DetailController",
"sencha-BaseList",
"sencha-List",
"sencha-ListController"
] as i>
<#if resolveTemplate("/evidence/"  + it.evidenceResource.evidenceName + "/" + i)??>
<#include resolveTemplate("/evidence/"  + it.evidenceResource.evidenceName + "/" + i)/>
<#else>
<#include resolveTemplate("/evidence/" + i) />
</#if>
</#list>


</#escape>
