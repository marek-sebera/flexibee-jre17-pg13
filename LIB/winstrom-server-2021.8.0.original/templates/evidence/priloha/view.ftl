<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-view.ftl" />

<@tool.listItems items=items object=object marker=marker />

<a href="/c/${it.companyResource.companyId}/${it.evidenceType.path}/${object.id?c}/content">Stáhnout</a>

<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />


<#include "/footer.ftl" />
</#escape>