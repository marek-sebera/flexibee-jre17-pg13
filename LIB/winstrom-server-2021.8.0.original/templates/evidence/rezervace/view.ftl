<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-view.ftl" />

${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}
${marker.mark('visible')}

<#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canShowAtLeastOne(items, object, ['firma', 'cenik', 'uzivatel', 'sklad', 'mnozstvi', 'datumOd', 'datumDo', 'poznamka'], it)>
    <table class="flexibee-tbl-1">
        <col width="20%" /><col width="80%" />
        <#if canShowAtLeastOne(items, object, ['firma'], it)>
        <tr>
            <td><@tool.label name='firma' items=items />:</td><td><@tool.place object=object name='firma' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['cenik'], it)>
        <tr>
            <td><@tool.label name='cenik' items=items />:</td><td><@tool.place object=object name='cenik' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['uzivatel'], it)>
        <tr>
            <td><@tool.label name='uzivatel' items=items />:</td><td><@tool.place object=object name='uzivatel' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['sklad'], it)>
        <tr>
            <td><@tool.label name='sklad' items=items />:</td><td><@tool.place object=object name='sklad' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['mnozstvi'], it)>
        <tr>
            <td><@tool.label name='mnozstvi' items=items />:</td><td><@tool.place object=object name='mnozstvi' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['datumOd'], it)>
        <tr>
            <td><@tool.label name='datumOd' items=items />:</td><td><@tool.place object=object name='datumOd' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['datumDo'], it)>
        <tr>
            <td><@tool.label name='datumDo' items=items />:</td><td><@tool.place object=object name='datumDo' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['poznamka'], it)>
        <tr>
            <td><@tool.label name='poznamka' items=items />:<br><@tool.placeArea object=object name='poznamka' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

<#--AUTOGEN END-->

<@tool.listItems items=items object=object marker=marker />

<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />

<#include "/i-footer-view.ftl" />
</#escape>
