<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>

${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}

<@edit.beginForm />
    <#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canEditAtLeastOne(items, object, ['kod', 'nazev', 'docasnost', 'platiOdData', 'platiDoData', 'stredisko', 'rucneVybrat'])>
    <table class="flexibee-tbl-1">
        <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
        <#if canEditAtLeastOne(items, object, ['kod'])>
        <tr>
            <td><@edit.label name='kod' items=items /></td><td><@edit.place object=object name='kod' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['nazev'])>
        <tr>
            <td><@edit.label name='nazev' items=items /></td><td colspan="3"><@edit.place object=object name='nazev' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['docasnost'])>
        <tr>
            <td><@edit.label name='docasnost' items=items /></td><td colspan="3"><@edit.place object=object name='docasnost' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['platiOdData', 'platiDoData'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['platiOdData'])>
            <td><@edit.label name='platiOdData' items=items /></td><td><@edit.place object=object name='platiOdData' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['platiDoData'])>
            <td><@edit.label name='platiDoData' items=items /></td><td><@edit.place object=object name='platiDoData' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['stredisko', 'rucneVybrat'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['stredisko'])>
            <td><@edit.label name='stredisko' items=items /></td><td><@edit.place object=object name='stredisko' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['rucneVybrat'])>
            <td><@edit.label name='rucneVybrat' items=items /></td><td><@edit.place object=object name='rucneVybrat' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canEditAtLeastOne(items, object, ['zaokrJakK', 'zaokrNaK', 'procZakl', 'typCenyVychoziK', 'typVypCenyK', 'typCenyVychozi25K', 'typVypCeny25K', 'limMnoz2', 'procento2', 'limMnoz3', 'procento3', 'limMnoz4', 'procento4', 'limMnoz5', 'procento5', 'a', 'vsechnySkupZboz', 'vsechnySkupFir', 'popis', 'poznam', 'platiOd', 'platiDo'])>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canEditAtLeastOne(items, object, ['zaokrJakK', 'zaokrNaK', 'procZakl', 'typCenyVychoziK', 'typVypCenyK', 'typCenyVychozi25K', 'typVypCeny25K', 'limMnoz2', 'procento2', 'limMnoz3', 'procento3', 'limMnoz4', 'procento4', 'limMnoz5', 'procento5'])>
            <li class="active"><a href="#edit-fragment-1" data-toggle="tab"><span>${lang('labels', 'cenHladEBCenotvorbaPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['a', 'vsechnySkupZboz', 'vsechnySkupFir'])>
            <li><a href="#edit-fragment-4" data-toggle="tab"><span>${lang('labels', 'cenHladEBVybraneSkupinyPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <li><a href="#edit-fragment-5" data-toggle="tab"><span>${lang('labels', 'textyPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
            <li><a href="#edit-fragment-6" data-toggle="tab"><span>${lang('labels', 'spravaPN')}</span></a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canEditAtLeastOne(items, object, ['zaokrJakK', 'zaokrNaK', 'procZakl', 'typCenyVychoziK', 'typVypCenyK', 'typCenyVychozi25K', 'typVypCeny25K', 'limMnoz2', 'procento2', 'limMnoz3', 'procento3', 'limMnoz4', 'procento4', 'limMnoz5', 'procento5'])>
            <div id="edit-fragment-1" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['zaokrJakK', 'zaokrNaK', 'procZakl', 'typCenyVychoziK', 'typVypCenyK', 'typCenyVychozi25K', 'typVypCeny25K', 'limMnoz2', 'procento2', 'limMnoz3', 'procento3', 'limMnoz4', 'procento4', 'limMnoz5', 'procento5'])>
                <table class="flexibee-tbl-1">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canEditAtLeastOne(items, object, ['zaokrJakK'])>
                    <tr>
                        <td><@edit.label name='zaokrJakK' items=items /></td><td><@edit.place object=object name='zaokrJakK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['zaokrNaK', 'procZakl'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['zaokrNaK'])>
                        <td><@edit.label name='zaokrNaK' items=items /></td><td><@edit.place object=object name='zaokrNaK' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['procZakl'])>
                        <td><@edit.label name='procZakl' items=items /></td><td><@edit.place object=object name='procZakl' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['typCenyVychoziK', 'typVypCenyK'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['typCenyVychoziK'])>
                        <td><@edit.label name='typCenyVychoziK' items=items /></td><td><@edit.place object=object name='typCenyVychoziK' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['typVypCenyK'])>
                        <td><@edit.label name='typVypCenyK' items=items /></td><td><@edit.place object=object name='typVypCenyK' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['typCenyVychozi25K', 'typVypCeny25K'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['typCenyVychozi25K'])>
                        <td><@edit.label name='typCenyVychozi25K' items=items /></td><td><@edit.place object=object name='typCenyVychozi25K' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['typVypCeny25K'])>
                        <td><@edit.label name='typVypCeny25K' items=items /></td><td><@edit.place object=object name='typVypCeny25K' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['limMnoz2', 'procento2'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['limMnoz2'])>
                        <td><@edit.label name='limMnoz2' items=items /></td><td><@edit.place object=object name='limMnoz2' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['procento2'])>
                        <td><@edit.label name='procento2' items=items /></td><td><@edit.place object=object name='procento2' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['limMnoz3', 'procento3'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['limMnoz3'])>
                        <td><@edit.label name='limMnoz3' items=items /></td><td><@edit.place object=object name='limMnoz3' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['procento3'])>
                        <td><@edit.label name='procento3' items=items /></td><td><@edit.place object=object name='procento3' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['limMnoz4', 'procento4'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['limMnoz4'])>
                        <td><@edit.label name='limMnoz4' items=items /></td><td><@edit.place object=object name='limMnoz4' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['procento4'])>
                        <td><@edit.label name='procento4' items=items /></td><td><@edit.place object=object name='procento4' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['limMnoz5', 'procento5'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['limMnoz5'])>
                        <td><@edit.label name='limMnoz5' items=items /></td><td><@edit.place object=object name='limMnoz5' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['procento5'])>
                        <td><@edit.label name='procento5' items=items /></td><td><@edit.place object=object name='procento5' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['a', 'vsechnySkupZboz', 'vsechnySkupFir'])>
            <div id="edit-fragment-4" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['a'])>
                <table class="flexibee-tbl-1">
                    <#if canEditAtLeastOne(items, object, ['a'])>
                    <tr>
                        <td><@edit.label name='a' items=items /></td><td><@edit.place object=object name='a' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

                <#if canEditAtLeastOne(items, object, ['vsechnySkupZboz', 'vsechnySkupFir'])>
                    <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
                        <#if canEditAtLeastOne(items, object, ['vsechnySkupZboz'])>
                        <li class="active"><a href="#edit-fragment-2" data-toggle="tab"><span>Skupiny zboží</span></a></li>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['vsechnySkupFir'])>
                        <li><a href="#edit-fragment-3" data-toggle="tab"><span>Skupiny firem</span></a></li>
                        </#if>
                    </ul>
                    <div class="tab-content">
                        <#if canEditAtLeastOne(items, object, ['vsechnySkupZboz'])>
                        <div id="edit-fragment-2" class="tab-pane active">
                            <div class="flexibee-table-border">
                            <#if canEditAtLeastOne(items, object, ['vsechnySkupZboz'])>
                            <table class="flexibee-tbl-1">
                                <#if canEditAtLeastOne(items, object, ['vsechnySkupZboz'])>
                                <tr>
                                    <td><@edit.label name='vsechnySkupZboz' items=items /></td><td><@edit.place object=object name='vsechnySkupZboz' items=items marker=marker /></td>
                                </tr>
                                </#if>
                            </table>
                            </#if>
                            </div>

                        </div>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['vsechnySkupFir'])>
                        <div id="edit-fragment-3" class="tab-pane">
                            <div class="flexibee-table-border">
                            <#if canEditAtLeastOne(items, object, ['vsechnySkupFir'])>
                            <table class="flexibee-tbl-1">
                                <#if canEditAtLeastOne(items, object, ['vsechnySkupFir'])>
                                <tr>
                                    <td><@edit.label name='vsechnySkupFir' items=items /></td><td><@edit.place object=object name='vsechnySkupFir' items=items marker=marker /></td>
                                </tr>
                                </#if>
                            </table>
                            </#if>
                            </div>

                        </div>
                        </#if>
                    </div>
                </#if>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <div id="edit-fragment-5" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
                <table class="flexibee-tbl-1">
                    <#if canEditAtLeastOne(items, object, ['popis'])>
                    <tr>
                        <td><@edit.label name='popis' items=items /><br><@edit.textarea object=object name='popis' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['poznam'])>
                    <tr>
                        <td><@edit.label name='poznam' items=items /><br><@edit.textarea object=object name='poznam' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
            <div id="edit-fragment-6" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <tr>
                        <td colspan="2">Platnost se udává v kalendářních rocích, pro neomezenou platnost zadejte 0-9999</td>
                    </tr>

                    <#if canEditAtLeastOne(items, object, ['platiOd'])>
                    <tr>
                        <td><@edit.label name='platiOd' items=items /></td><td><@edit.place object=object name='platiOd' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['platiDo'])>
                    <tr>
                        <td><@edit.label name='platiDo' items=items /></td><td><@edit.place object=object name='platiDo' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

    <br/>
    <@edit.listItems items=items object=object marker=marker />

    <@edit.saveButton />
<@edit.endForm />

<@edit.generateNaiveJavascript object=object it=it />

<#include "/parts/i-dalsi-informace.ftl" />


<#include "/i-footer-edit.ftl" />
</#escape>
