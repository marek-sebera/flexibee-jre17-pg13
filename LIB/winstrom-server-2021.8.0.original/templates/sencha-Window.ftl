<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.main.Window', {
    extend: 'Ext.Viewport',

    requires: [
        'FlexiBee.main.Menu',
        'FlexiBee.main.Toolbar',
        'FlexiBee.main.StatusBar',
        <#if it.hasDashboard>
        'FlexiBee.main.dashboard.DashboardPanel',
        </#if>
        'Ext.ux.TabReorderer',
        'Ext.ux.TabCloseMenu'
    ],

    layout: 'border',
    cls: 'flexibee-background-dark',

    items: [
        {
            xtype: 'container',
            region: 'center',
            layout: {
                type: 'border'
            },
            cls: 'flexibee-background-dark',
            items: [
                {
                    xtype: 'container',
                    region: 'west',
                    layout: {
                        type: 'border'
                    },
                    width: 210,
                    items: [
                        {
                            xtype: 'container',
                            region: 'north',
                            border: 0,
                            cls: 'flexibee-background-dark',
                            height: 55,
                            html: '<img style="text-align: center; padding: 15px 30px" src="${staticPrefix}/img/skin/${skin}/logo.png">'
                        },
                        {
                            xtype: 'fbMainMenu',
                            region: 'center'
                        }
                    ]
                },
                {
                    xtype: 'container',
                    region: 'center',
                    layout: {
                        type: 'border'
                    },
                    border: 0,
                    cls: 'flexibee-background-dark',
                    items: [
                        {
                            xtype: 'fbMainToolbar',
                            id: 'maintoolbar',
                            height: 35,
                            region: 'north'
                        },
                        {
                            xtype: 'tabpanel',
                            region: 'center',
                            id: 'content',
                            stateful: true,
                            stateId: 'fbMainTabs',
                            layout: {
                                type: 'hbox',
                                align: 'center',
                                pack: 'center',
                                overflowHandler: 'Menu'
                            },
                            plugins: [
                                Ext.create('Ext.ux.TabReorderer'),
                                'tabclosemenu'
                            ],
                            plain: true,
                            bodyBorder: false,
                            resizeTabs: true,
                            enableTabScroll: true,
                            bodyCls: 'flexibee-background-light',
                            items: [
                                {
                                    xtype: 'panel',
                                    title: '<strong>&#8962;</strong>',
                                    bodyCls: 'flexibee-background-light',
<#if it.hasDashboard>
                                    padding: '10',
<#else>
                                    padding: '40 0 0 0',
</#if>
                                    items: [
                                        {
<#if it.hasDashboard>
                                            xtype: 'fbDashboardPanel'
<#else>
                                            xtype: 'container',
                                            html: '<center><img src="${staticPrefix}/img/logo-title.png"></center>'
</#if>
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'container',
            layout: 'hbox',
            region: 'south',
            cls: 'flexibee-background-light',
            padding: '0 10px 0 10px',
            height: 25,
            items: [
                {
                    xtype: 'component',
                    padding: '4 0 0 0',
                    html: '<small><#if customerNumber??><a href="https://www.flexibee.eu/muj/${licenseKey}/" target="_new">${lang('labels', 'statusBar.cisloZakaznika', '')} ${customerNumber}</a></#if> <span style="color: grey;padding-left:1em">${licenseUserName}</span></small>'
                },
                {
                    xtype: 'fbMainStatusBar',
                    id: 'mainstatusbar',
                    padding: '0 0 0 10',
                    flex: 1
                }
            ]
        }
    ]
});

</#escape>
