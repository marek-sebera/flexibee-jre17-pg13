<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.main.StatusBar', {
    extend: 'Ext.ux.statusbar.StatusBar',
    alias: 'widget.fbMainStatusBar',

    requires: [
    ],

    baseCls: 'flexibee-background-light',
    margin: '0 0 0 10px',
    statusAlign: 'left',

    defaultText: '&#160;',
    busyText: 'Čekejte...',

    items: [
        <#if isDevel>
        {
            xtype: 'splitbutton',
            text: '${lang('dials', 'jazyk.' + languageCode, '')}',
            menu: [
                <#list ["cs", "sk", "en", "de"] as l>
                    <#if l != languageCode>
                        {
                            xtype: 'menuitem',
                            lang: '${l}',
                            text: '${lang('dials', 'jazyk.'+l, '')}',
                            handler: function(item) {
                                Ext.util.Cookies.set('lang', item.lang);
                                window.location.reload();
                            }
                        }<#if l_has_next>,</#if>
                    </#if>
                </#list>
            ]
        },
        </#if>
        {
            xtype: 'container',
            html: '<small>1991 - 2014 &copy; <a href="http://www.flexibee.eu/" target="_new">FlexiBee Systems s.r.o.</a></small>',
            margin: '0 0 0 30'
        }
    ],

    initComponent: function() {
        var me = this;

        var additionalItems = FlexiBee.Plugins.invoke(me, 'defineAdditionalItems');
        Ext.iterate(additionalItems, function(additionalItem) {
            if (Ext.isArray(additionalItem)) {
                me.items = me.items.concat(additionalItem);
            } else {
                me.items.push(additionalItem);
            }
        });

        this.callParent(arguments);
    }
});

<#import "/l-sencha.ftl" as sencha>
<@sencha.includePlugins path="main.StatusBar"/>

</#escape>
