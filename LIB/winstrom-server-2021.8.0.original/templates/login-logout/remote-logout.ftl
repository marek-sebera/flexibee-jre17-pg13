<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="ABRA Flexi" />

<#include "/header-html-start.ftl" />

<@tool.showTitle title=title menu=false showLogoInTitle=true showPath=false>
</@tool.showTitle>
<@tool.showTitleFinish />


<#if cloudOverride.isTemplate("root", "content")>
<#include cloudOverride.include("root", "content") />
</#if>
<div class="vertical-center-parent maxwidth">
<div class="vertical-center">
<div class="panel panel-default">
    <div class="panel-heading">
        <h4>${lblText('remoteLogoutHeading')}</h4>
    </div>
    <div class="panel-body">
        <p id="message">${lblText('remoteLogoutQuestion')}</p>
        <form id="remote-logout-form" action="/login-logout/remote-logout" method="post">

            <#--
            <ul>
            <li>TODO: List ostatních sezení
            </ul>
            -->

            <input type="hidden" name="returnUrl" value="${returnUrl}" />

            <div id="progress" class="progress" style="display: none">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                </div>
            </div>

            <div id="buttons" style="text-align:right">
                <input id="submit" type="submit" name="submit" class="btn btn-primary btn-lg" value="${btnText('yes', 'Ano')}" />
                <input id="cancel" type="button" name="cancel" class="btn btn-primary btn-lg" value="${btnText('no', 'Ne')}" />
            </div>
        </form>
    </div>
</div>
</div>
</div>

<script>
$(document).ready(function() {
    $("#remote-logout-form").submit(function() {

        $("#message").text('${lblText('remoteLogoutAction')}');
        $("#buttons").hide('blind', 'fast');
        $("#progress").show('blind', 'fast');

        var refreshInterval;
        var ajaxSubmit = function() {
            clearInterval(refreshInterval);
            $.showWait();
            $.ajax({
                type: 'POST',
                url: '/login-logout/remote-logout?token=${csrfToken}',
                async: 'false',
                dataType: 'html',
                data: $(this).serialize(),
                success: function() {
                    window.location = '${returnUrl}';
                },
                error: function() {
                    $.hideWait();
                }
            });
        };

        var timeout = ${isSnapshot?default(false)?string("500", "10000")};
        $.ajax({
            type: 'POST',
            url: '/login-logout/prepare-remote-logout?token=${csrfToken}',
            async: false,
            dataType: 'json',
            success: function(data) {
                if (data.success && data.recoveryTime > 0) {
                    timeout = data.recoveryTime;
                }
            }
        });

        var val = 0;
        var refreshPeriod = 250;
        var increment = 100 / (timeout / refreshPeriod);

        var refresh = function() {
            val += increment;
            if (val >= 100) {
                val = 100;
                ajaxSubmit();
            }
            $("#progress > .progress-bar").css('width', val + '%').attr('aria-valuenow', val);
        };
        refresh();
        refreshInterval = setInterval(refresh, refreshPeriod);

        return false;
    });

    $("#cancel").click(function() {
        window.location = '/login-logout/logout';
    });
});
</script>

<#include "/footer.ftl" />
</#escape>
