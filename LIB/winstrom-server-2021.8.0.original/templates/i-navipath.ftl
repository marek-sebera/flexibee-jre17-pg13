<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

    <#list paths as path>
        <li class="flexibee-navi-item-back">
            <a href="${path.url}"><span>${lang('dialogs', path.name, path.name)}</span></a>
        </li>
    </#list>

</#escape>