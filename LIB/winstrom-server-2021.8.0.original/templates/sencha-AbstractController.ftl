<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.main.AbstractController', {
    extend: 'Ext.app.Controller',

    view: null,
    loadRecordListeners: [],

    constructor: function(cfg) {
        this.callParent(arguments);

        this.view = this.createView();
        this.setupControl();

        var tabs = Ext.getCmp('content');
        tabs.add(this.view);
        tabs.setActiveTab(this.view);
    },

    closeTab: function() {
        var tabs = Ext.getCmp('content');
        tabs.remove(this.view);
    },

    // designed to be called from setupControl, which is called after createView
    controlOver: function(map) {
        var rootViewId = this.view.id;
        var newMap = {};
        Ext.iterate(map, function(key, value) {
            newMap["#" + rootViewId + " " + key] = value;
        });
        this.control(newMap);
    },

    createView: Ext.emptyFn,
    setupControl: Ext.emptyFn
});

</#escape>
