<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#if hideFilter == false && it.evidenceResource?? && (it.evidenceResource.supportsSuggest() || it.evidenceResource.supportsFullText() || (it.filterGroup?? && it.filterGroup?size &gt; 0))>
<form id="flexibee-search-form" class="form-inline" role="form">

<div class="flexibee-search">
<#if (it.evidenceResource.supportsSuggest() || it.evidenceResource.supportsFullText())>
    <span id="flexibee-search" style="display:none">
        <div class="input-group">
            <div id="flexibee-search-flexbox"></div>
            <div class="input-group-btn">
                <button type="submit"  class="btn btn-default btn-sm"><span aria-hidden="true"><span class="glyphicon glyphicon-search"></span></span><span class="sr-only">${lang('labels', 'search', 'Hledat')}</span></button>
            </div>
        </div>
    </span>
</#if>

<#if it.filterGroups?? && it.filterGroups?size &gt; 0>
    <#assign showFilters = false/>
    <#assign someFilterSelected = false/>
    <#list it.filterGroups as group>
        <#if group.allItems?size &gt; 1><#assign showFilters = true/></#if>
        <#if group.selectedItem?? && group.selectedItem.key != group.neomezenoItem.key><#assign someFilterSelected = true/></#if>
    </#list>

    <span class="flexibee-filter">
    <#if showFilters>
    ${lang('labels', 'filter', 'Filtrace')}:
    </#if>
    <#list it.filterGroups as group>
        <#if group.allItems?size &gt; 1>
        <select name="filter[${group.name}]" class="form-control input-sm flexibee-filter-group <#if group.selectedItem?? && group.selectedItem.key != group.neomezenoItem.key>flexibee-filter-group-active btn-primary</#if>" autocomplete="off">
            <#list group.allItems as filter>
                <option value="<#if filter.key != group.neomezenoItem.key>${filter.key}</#if>" <#if group.selectedItem?? && group.selectedItem.key == filter.key>selected="selected"</#if> <#if filter.key?starts_with("separator")>disabled="disabled"</#if>>
                    <#if !filter.key?starts_with("separator")>${filter.title}</#if>
                </option>
            </#list>
        </select>
        </#if>
    </#list>
        <#if someFilterSelected || it.fullTextQuery?? && it.fullTextQuery != ''>
            <span class="flexibee-discard-filter close"><span aria-hidden="true">&times;</span><span class="sr-only">${lang("actions", "zrusitFiltraci", "Zrušit filtraci")}</span><span>
        </#if>

        <input type="submit" class="flexibee-button flexibee-filter-submit" value="${lang('labels', 'search', 'Hledat')}"/>
    </span>

<script type="text/javascript">
    $(function() {
        $("#flexibee-search-form").submit(function() {

            $("#flexibee-search-flexbox_hidden").remove();
        });
        $(".flexibee-filter-group").change(function() {
            $("#flexibee-search-form").submit();
        });
        $(".flexibee-filter-submit").hide();
        $(".flexibee-discard-filter").show().click(function() {
            $("#flexibee-search-flexbox_input").val("");
            $(".flexibee-filter-group").val("");
            $("#flexibee-search-form").submit();
        });
    });
</script>
</div>
</#if>
</form>


<#if (it.evidenceResource.supportsSuggest() || it.evidenceResource.supportsFullText())>
<#if it.evidenceResource.isDoklad>

<script type="text/javascript">
<#noescape><#escape x as x?default("")?js_string>
    <#assign adresarUrl = formListUrl(it.companyResource, 'ADRESAR')!"" />
    <#assign searchUrl = formListUrl(it.companyResource, it.evidenceType)!"" />
    function initSearchFlexBox(searchUrl) {
        $('#flexibee-search-flexbox').flexbox(searchUrl + '.json?mode=suggest', {
            showArrow: false,
            autoCompleteFirstMatch: false,
            initialValue: "${it.fullTextQuery}",
            watermark: "${lang('labels', 'search', 'Hledat')}",
            noResultsText: '${lang('messages', 'suggest.nothingFound', 'Nic nenalezeno')}',
            inputClass: 'form-control input-sm <#if it.fullTextQuery?? && it.fullTextQuery != ''>btn-primary</#if>',
            paging: {
                style: 'links',
                summaryTemplate: '${lang('messages', 'suggest.summary', '{start} až {end} z celkem {total}')}'
            },
            onSelect: function() {
                window.location = '${searchUrl}' + "/(firma=" + this.getAttribute('hiddenValue')+")";
            }
        });
    }
    $(function() {
        setTimeout(function() {
            initSearchFlexBox('${adresarUrl}');
            $("#flexibee-search-flexbox_input").attr("name", "q");
        }, 100);
        $("#flexibee-search").show();
    });
</#escape></#noescape>
</script>


<#elseif it.evidenceResource.supportsSuggest()>

<script type="text/javascript">
<#noescape><#escape x as x?default("")?js_string>
    <#assign searchUrl = formListUrl(it.companyResource, it.evidenceType)!"" />
    function initSearchFlexBox(searchUrl) {
        $('#flexibee-search-flexbox').flexbox(searchUrl + '.json?mode=suggest', {
            showArrow: false,
            autoCompleteFirstMatch: false,
            watermark: "${lang('labels', 'search', 'Hledat')}",
            initialValue: "${it.fullTextQuery}",
            inputClass: 'form-control input-sm <#if it.fullTextQuery?? && it.fullTextQuery != ''>btn-primary</#if>',
            noResultsText: '${lang('messages', 'suggest.nothingFound', 'Nic nenalezeno')}',
            paging: {
                style: 'links',
                summaryTemplate: '${lang('messages', 'suggest.summary', '{start} až {end} z celkem {total}')}'
            },
            onSelect: function() {
                window.location = searchUrl + "/" + this.getAttribute('hiddenValue');
            }
        });
    }
    $(function() {
        setTimeout(function() {
            initSearchFlexBox('${searchUrl}');
            $("#flexibee-search-flexbox_input").attr("name", "q");
        }, 100);
        $("#flexibee-search").show();
    });
</#escape></#noescape>
</script>

</#if>
</#if>
</#if>

</#escape>
