<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#if it.editHandler.hasError>
    <div class="flexibee-error-head">
    <ul class="flexibee-errors">
    <#list it.editHandler.errors as e>
        <li><#if e.for?exists && items[e.for]?exists><span>${items[e.for].name}:</span> </#if>${e.message}</li>
    </#list>
    </ul>
    </div>
</#if>

</#escape>