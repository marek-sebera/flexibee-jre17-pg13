<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#assign m = menuTool().root/>
<#if it.evidenceResource?? && it.object?? && hideRelations == false && false>
<!--FLEXIBEE:RIGHT-MENU:START-->
  <div class="flexibee-application-right hidden-xs column">
        <div class="" id="more-menu">
        <ul class="nav">
        <#assign l = it.evidenceResource.getSubEvidencesByName(it.object, ['prilohy', 'udalosti', 'uzivatelske-vazby', 'vazby'])/>
        <#list l.list as sub>
                      <#if marker??>
                      ${marker.mark(sub.url)}
                      </#if>
                      <li>
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapse-${sub.url}">
                          <span class="fa fa-picture-o fa-fw"></span> ${sub.name}
                      </a>
                      <div class="row images-list panel-collapse" id="collapse-${sub.url}" data-tab-url="">
                      </div>
                      </li>
                      <script>
                       $(document).ready(function() {
                            $('#collapse-${sub.url}').load('/c/${companyId}/${it.evidenceName}/${it.object.id?string("0")}/${sub.url}?isPanel=true&hideToolbar=true&hideFilter=true', function() {
                            });
                       });
                       </script>
        </#list>
        </ul>
        </div>
  </div>
 </#if>
<!--FLEXIBEE:RIGHT-MENU:END-->


</#escape>
