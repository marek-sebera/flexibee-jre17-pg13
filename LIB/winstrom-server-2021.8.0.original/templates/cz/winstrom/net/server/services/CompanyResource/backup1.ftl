<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=lang('formsInfo', 'zalohaDB', 'Záloha firmy')+" "+lang('formsInfo', it.name, it.name) />
<#assign titleId="root"/>
<#assign useNaviPath=false />
<#assign showMenuAtEnd=false />
<#include "/header.ftl" />

<div class="flexibee-dialog">
    <p class="flexibee-dialog-text">${lang('messages', 'backup.confirmQuestion', 'Opravdu zálohovat firmu?')}</p>
    <div class="flexibee-dialog-buttons">
        <a class="btn btn-link negative" href="/c/${it.companyId}">${lang('buttons', 'backup.confirmNo', 'Ne, zpět')}</a>
        <a class="btn btn-primary" href="/c/${it.companyId}/zaloha?confirm=1">${lang('buttons', 'backup.confirmYes', 'Ano, zálohovat')}</a>
    </div>
    <br/>
</div>

<#include "/footer.ftl" />
</#escape>