<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
        <#assign titleId="menu-${m.groupName}"/>

        <@tool.showTitle titleId="${titleId}" title=lang('formsInfo', it.name, it.name) showAsTitle=true logo=false>
            <@tool.showTitleLogo title=lang('formsInfo', it.name, it.name) />
        </@tool.showTitle>
        <@tool.showTitleFinish />
        <h3>${langMenu(menuTool().selectedMenu.groupName)}</h3>

            <#include "i-menu-items.ftl" />

        <@tool.showFooter />
</#escape>