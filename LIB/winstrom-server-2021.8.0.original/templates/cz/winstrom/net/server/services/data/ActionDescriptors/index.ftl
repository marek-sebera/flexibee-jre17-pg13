<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=lang('dialogs', 'actions.title', 'Přehled akcí') />
<#assign titleId="root"/>
<#include "/header.ftl"/>

<table class="table flexibee-tbl-list table-striped table-hover table-condensed" summary="${lang('dialogs', 'actions.title', 'Přehled akcí')}">
    <thead>
    <tr>
        <th>${lang('labels', 'identifier', 'Identifikátor')}</th>
        <th>${lang('labels', 'name', 'Název')}</th>
        <th>${lang('labels', 'actions.needInstance', 'Potřebuje instanci objektu')}</th>
    </tr>
    </thead>
    <tbody>
    <#list it.list as item>
        <tr>
        <th>${item.actionId}</a></th>
        <td>${item.actionName}</td>
        <td><#if item.needInstance>${lang('buttons', 'ano', 'ano')}<#else>${lang('buttons', 'ne', 'ne')}</#if></td>
        </tr>
    </#list>
    </tbody>
</table>



<#include "/footer.ftl"/>
</#escape>
