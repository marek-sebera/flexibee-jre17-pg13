<#-- @ftlvariable name="props" type="cz.winstrom.net.server.services.data.PropertyDescriptorsTree" -->
<#-- @ftlvariable name="isForImport" type="java.lang.Boolean" -->
<#escape x as (x!"")?xml>
<#--
  - tenhle soubor se pouziva jako knihovna vkladana prikazem <#include>, takze tu muze byt libovolny text, a ten
  - nema vubec zadny vliv (viz http://freemarker.org/docs/ref_directive_import.html)
  -
  - korenovy element "schema" tu je pro spravne chovani IDE, jiny duvod to nema
  -->
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

<#macro defineBasicTypes isForImport>
    <xs:simpleType name="fbId">
        <xs:union>
            <xs:simpleType>
                <xs:restriction base="xs:string">
                    <xs:pattern value="${idPattern}"/>
                </xs:restriction>
            </xs:simpleType>
            <xs:simpleType>
                <xs:restriction base="xs:string">
                    <xs:pattern value="${extIdPattern}"/>
                </xs:restriction>
            </xs:simpleType>
            <xs:simpleType>
                <xs:restriction base="xs:string">
                    <xs:pattern value="${moreIdsPattern}"/>
                </xs:restriction>
            </xs:simpleType>
        </xs:union>
    </xs:simpleType>

    <xs:simpleType name="fbBoolean">
        <#-- xs:boolean umoznuje i cisla 0, 1 -->
        <xs:restriction base="xs:string">
            <xs:enumeration value="true"/>
            <xs:enumeration value="false"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="fbImportMode">
        <xs:restriction base="xs:string">
            <xs:enumeration value="ok"/>
            <xs:enumeration value="ignore"/>
            <xs:enumeration value="fail"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="fbIfNotFoundMode">
        <xs:restriction base="xs:string">
            <xs:enumeration value="null"/>
            <xs:enumeration value="fail"/>
        </xs:restriction>
    </xs:simpleType>
</#macro>

<#macro wrapNonMandatory prop>
    <#-- bohuzel se pri exportu stava, ze se vygeneruji prazdne elementy -->
    <#if !prop.mandatory && !prop.inId>
        <xs:union>
            <#if prop.type == "numeric" || prop.type == "integer">
                <#-- nepovinne ciselne polozky mohou nabyvat i hodnoty 0 -->
                <xs:simpleType>
                    <xs:restriction base="xs:integer">
                        <xs:enumeration value="0"/>
                    </xs:restriction>
                </xs:simpleType>
            </#if>
            <xs:simpleType>
                <#nested>
            </xs:simpleType>
            <xs:simpleType>
                <xs:restriction base="xs:string">
                    <xs:enumeration value=""/>
                </xs:restriction>
            </xs:simpleType>
        </xs:union>
    <#else>
        <#nested>
    </#if>
</#macro>

<#macro defineTypes typeNamePrefix props isForImport>
    <#list props.descriptors.list as prop>
        <#if !isForImport || (prop.isWritable || prop.inId)>
            <#assign xsType = "">
            <#assign isSelect = false>
            <#assign isRelation = false>
            <#if prop.type == "string">
                <#if !prop.minLength?? && !prop.maxLength??>
                    <#assign xsType = "xs:string">
                </#if>
            <#elseif prop.type == "integer">
                <#if !prop.minValue?? && !prop.maxValue?? && !prop.digits?? && !prop.decimal??>
                    <#assign xsType = "xs:integer">
                </#if>
            <#elseif prop.type == "numeric">
                <#if !prop.minValue?? && !prop.maxValue?? && !prop.digits?? && !prop.decimal??>
                    <#assign xsType = "xs:decimal">
                </#if>
            <#elseif prop.type == "date">
                <#assign xsType = "xs:date">
            <#elseif prop.type == "datetime">
                <#assign xsType = "xs:dateTime">
            <#elseif prop.type == "logic">
                <#assign xsType = "fbBoolean">
            <#elseif prop.type == "select">
                <#if !prop.possibleValues?? || prop.possibleValues?size == 0>
                    <#assign xsType = "xs:string">
                </#if>
                <#assign isSelect = true>
            <#elseif prop.type == "relation">
                <#assign xsType = "fbId">
                <#assign isRelation = true>
            <#elseif prop.type == "other">
                <#if !prop.minLength?? && !prop.maxLength??>
                    <#assign xsType = "xs:string">
                </#if>
            </#if>

            <#if prop.inId>
                <#assign xsType = "fbId">
            </#if>

            <#assign xsPrevValType = xsType>

            <#assign propTypeName>${typeNamePrefix}${prop.propertyName?cap_first}TypeInner</#assign>
            <#if xsType == "">
                <xs:simpleType name="${propTypeName}">
                    <@wrapNonMandatory prop=prop>
                        <#if prop.type != "numeric" && prop.type != "integer">
                            <xs:restriction base="xs:string">
                                <#if prop.possibleValues?? && prop.possibleValues?size &gt; 0>
                                    <xs:enumeration value=""/>
                                    <#list prop.possibleValues as val>
                                        <xs:enumeration value="${val.key}">
                                            <xs:annotation>
                                                <xs:documentation>${val.name}</xs:documentation>
                                            </xs:annotation>
                                        </xs:enumeration>
                                    </#list>
                                <#elseif prop.minLength?? || prop.maxLength??>
                                    <#if prop.minLength??><xs:minLength value="${prop.minLength?c}"/></#if>
                                    <#if prop.maxLength??><xs:maxLength value="${prop.maxLength?c}"/></#if>
                                </#if>
                            </xs:restriction>
                            <#assign xsPrevValType = "xs:string">
                        <#elseif prop.minValue?? || prop.maxValue?? || prop.digits?? || prop.decimal??>
                            <#if prop.type == "numeric">
                                <xs:restriction base="xs:decimal">
                                    <#if prop.digits?? && prop.decimal??><xs:totalDigits value="${prop.digits?c}"/></#if>
                                    <#if prop.decimal??><xs:fractionDigits value="${prop.decimal?c}"/></#if>
                                    <#if prop.minValue??><xs:minInclusive value="${prop.minValue?c}"/></#if>
                                    <#if prop.maxValue??><xs:maxInclusive value="${prop.maxValue?c}"/></#if>
                                </xs:restriction>
                                <#assign xsPrevValType = "xs:decimal">
                            <#elseif prop.type == "integer">
                                <xs:restriction base="xs:integer">
                                    <#if prop.digits??><xs:totalDigits value="${prop.digits?c}"/></#if>
                                    <#if prop.minValue??><xs:minInclusive value="${prop.minValue?c}"/></#if>
                                    <#if prop.maxValue??><xs:maxInclusive value="${prop.maxValue?c}"/></#if>
                                </xs:restriction>
                                <#assign xsPrevValType = "xs:integer">
                            </#if>
                        </#if>
                    </@wrapNonMandatory>
                </xs:simpleType>
            <#else>
                <xs:simpleType name="${propTypeName}">
                    <@wrapNonMandatory prop=prop>
                        <xs:restriction base="${xsType}"/>
                    </@wrapNonMandatory>
                </xs:simpleType>
            </#if>

            <xs:complexType name="${typeNamePrefix}${prop.propertyName?cap_first}Type">
                <xs:simpleContent>
                    <xs:extension base="${typeNamePrefix}${prop.propertyName?cap_first}TypeInner">
                        <#if !isForImport>
                            <#if isSelect || isRelation>
                                <xs:attribute name="showAs" type="xs:string"/>
                            </#if>
                            <#if isRelation>
                                <xs:attribute name="ref" type="xs:string"/>
                                <xs:attribute name="internalId" type="xs:string"/>
                            </#if>
                            <xs:attribute name="link" type="xs:string"/>
                            <xs:attribute name="visible" type="xs:boolean"/>
                            <xs:attribute name="enabled" type="xs:boolean"/>
                            <xs:attribute name="editable" type="xs:boolean"/>
                        </#if>
                        <#if isRelation>
                            <xs:attribute name="if-not-found" type="fbIfNotFoundMode"/>
                        </#if>
                        <#if prop.isWritable>
                            <xs:attribute name="previousValue" type="${xsPrevValType}"/>
                        </#if>
                    </xs:extension>
                </xs:simpleContent>
            </xs:complexType>
        </#if>
    </#list>

    <#list props.children as childProps>
        <@defineTypes typeNamePrefix="${typeNamePrefix}${childProps.identifier?cap_first}" props=childProps isForImport=isForImport/>
    </#list>
</#macro>

<#macro buildSchema typeNamePrefix props isForImport>
    <xs:element name="${props.descriptors.tagName}" minOccurs="0" maxOccurs="unbounded">
        <xs:annotation>
            <xs:documentation>${props.descriptors.evidenceName}</xs:documentation>
        </xs:annotation>
        <xs:complexType>
            <xs:sequence> <#-- bohuzel nelze pouzit xs:all, pak nelze zadat maxOccurs > 1 -->
                <#list props.descriptors.list as prop>
                    <#if !isForImport || (prop.isWritable || prop.inId)>
                        <#-- u importu nelze kvuli inkrementalnim aktualizacim vynucovat povinnost elementu staticky -->
                        <xs:element name="${prop.propertyName}" type="${typeNamePrefix}${prop.propertyName?cap_first}Type"<#if !prop.mandatory || isForImport> minOccurs="0"</#if><#if prop.inId> maxOccurs="unbounded"</#if>>
                            <xs:annotation>
                                <#if devDocMsg(props.descriptors.tagName, prop.propertyName)??>
                                    <xs:documentation>${devDocMsg(props.descriptors.tagName, prop.propertyName)}</xs:documentation>
                                <#else>
                                    <xs:documentation>${prop.name}</xs:documentation>
                                </#if>
                            </xs:annotation>
                        </xs:element>
                    </#if>
                </#list>

                <#list props.children as propsChild>
                    <xs:element name="${propsChild.identifier}" minOccurs="0" maxOccurs="1">
                        <xs:complexType>
                            <xs:sequence>
                                <@buildSchema typeNamePrefix="${typeNamePrefix}${propsChild.identifier?cap_first}" props=propsChild isForImport=isForImport/>
                            </xs:sequence>
                            <xs:attribute name="removeAll" type="fbBoolean"/>
                        </xs:complexType>
                    </xs:element>
                </#list>
            </xs:sequence>

            <xs:attribute name="create" type="fbImportMode"/>
            <xs:attribute name="update" type="fbImportMode"/>
        </xs:complexType>
    </xs:element>
</#macro>

</xs:schema>
</#escape>
