<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Stav replikace CouchDB"/>

<#include "/header-html-start.ftl" />

<@tool.showTitle title=title leftMenu="/i-left-menu-admin.ftl">
</@tool.showTitle>
<@tool.showTitleFinish />
<table class="table flexibee-tbl-list table-striped table-hover table-condensed">
<thead>
    <tr>
        <th>URL</th>
        <th>poslední synchronizace</th>
        <th>zpoždění</th>
    </tr>
</thead>
<#if it.totalCount &gt; 0>
<tbody>
<#list it.list as l>
    <tr>
        <td style="white-space:nowrap;">${t.url}</td>
        <td style="white-space:nowrap; text-align:right;">${t.lastSyncTime}</td>
        <td style="white-space:nowrap; text-align:right;"><strong>${t.delayAsString}</strong></td>
    </tr>
</#list>
</tbody>
</#if>
<tfoot>
    <tr>
        <th>#${it.totalCount}</th>
        <th colspan="3">&nbsp;</th>
    </tr>
</tfoot>
</table>

<#include "/footer.ftl" />
</#escape>