<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=lang('dialogs', 'properties.title', 'Přehled položek')/>
<#assign titleId="root"/>
<#include "/header.ftl"/>

<p><#noescape>${lang('labels', 'properties.description', 'Tento přehled slouží k&nbsp;výpisu položek, které tato evidence může obsahovat. Je možné jej převést do formátu XML či do JSON a následně strojově zpracovávat.')}</#noescape></p>

<#include "/i-devdoc-footer.ftl" />

<#include "/footer.ftl"/>
</#escape>
