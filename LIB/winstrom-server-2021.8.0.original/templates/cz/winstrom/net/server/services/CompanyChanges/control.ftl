<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=lang('formsInfo', 'status.title', 'Status') />
<#assign titleId="root"/>
<#assign useNaviPath=false />
<#assign showMenuAtEnd=false />
<#include "/header.ftl" />

<h2>Changes API</h2>

<#assign action = it.pageInfo.action />
<#assign success = it.pageInfo.success />
<#assign message>${it.pageInfo.message}</#assign>

<#if message?has_content>
<div id="message" class="flexibee-note-${success?string("ok", "error")}">${message}</div>
</#if>

<div class="flexibee-dialog">
    <p class="flexibee-dialog-text">${msgText('changesApi.' + action + 'Question', 'Changes API (sledování změn) nad aktuální firmou není zapnuté. Chcete jej aktivovat?')}</p>
    <div class="flexibee-dialog-buttons">
        <a class="btn btn-link negative" href="${goBackUrl(uriInfo)}">${btnText('backup.confirmNo', 'Ne, zpět')}</a>
        <a class="btn btn-${(action == "enable")?string("success", "danger")}" href="${baseUrl}/?action=${action}">${btnText(action, 'Aktivovat')}</a>
    </div>
    <br/>
</div>

<#include "/footer.ftl" />
</#escape>