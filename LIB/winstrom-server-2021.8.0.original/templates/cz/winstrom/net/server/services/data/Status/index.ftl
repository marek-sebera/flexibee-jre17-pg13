<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Status ABRA Flexi Server" />
<#assign titleId="root"/>

<#include "/header-html-start.ftl" />

<@tool.showTitle title=title leftMenu="/i-left-menu-admin.ftl">
</@tool.showTitle>
<@tool.showTitleFinish />


        <div>
        <table class="table table-striped table-hover table-condensed">
            <tr><th>${lang('labels', 'root.version', 'Verze')}:</th><td>${it.version}</td></tr>
            <tr><th>Metoda licencování:</th><td>${it.licencingMethod}</td></tr>
            <tr><th>Přihlášení uživatelé:</th><td>${it.sessions} <#if it.sessionsRO &gt; 0>(pro čtení ${it.sessionsRO})</#if></td></tr>
            <tr><th>Připojení uživatelé:</th><td>${it.loggedUser} <#if it.loggedUserRO &gt; 0>(pro čtení ${it.loggedUserRO})</#if></td></tr>
            <tr><th>Datum spuštění:</th><td>${it.startupTime?datetime}</td></tr>
            <tr><th>Java:</th><td>${it.javaVersion}</td></tr>
            <tr><th>OS:</th><td>${it.operatingSystem}</td></tr>
            <tr><th>Databáze centrálního serveru:</th><td>${it.centralServerDatabaseVersion}</td></tr>
        </table>
        </div>

        <div class="list-group">
            <a href="/status/session" class="list-group-item">Přihlášení uživatelé (autentizace)</a>
            <a href="/status/connection" class="list-group-item">Připojení uživatelé &mdash; spojení</a>
            <a href="/status/user" class="list-group-item">Připojení uživatelé &mdash; otevřené firmy</a>
            <#if loggedUser.permissions.canAccessThreadPool()>
            <a href="/status/thread-pool" class="list-group-item">Seznam vláken</a>
            </#if>
            <#if loggedUser.permissions.canAccessConnPool()>
            <a href="/status/conn-pool" class="list-group-item">Seznam databázových spojení</a>
            </#if>
            <#if loggedUser.permissions.canAccessClusterStatus()>
            <a href="/status/cluster" class="list-group-item">${lang('dialogs', 'cluster.list', 'Clusterový provoz')}</a>
            </#if>
            <#if loggedUser.isInstanceAdmin()>
            <a href="/status/web-hooks" class="list-group-item">${lang('dialogs', 'webHooksInfo.status.title', 'Stav zpracování Web Hooks')}</a>
            </#if>
        </div>

<#include "/footer.ftl"/>
</#escape>
