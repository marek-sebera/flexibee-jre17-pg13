<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=it.userAccess.username/>

<#include "/header-html-start.ftl" />

<@tool.showTitle title=title leftMenu="/i-left-menu-admin.ftl">
</@tool.showTitle>
<@tool.showTitleFinish />

    <table>
        <tr><th>Username</th><td>${it.userAccess.username}</td></tr>
        <tr><th>Fullname</th><td>${it.userAccess.fullname}</td></tr>
    </table>

<#include "/footer.ftl" />
</#escape>
