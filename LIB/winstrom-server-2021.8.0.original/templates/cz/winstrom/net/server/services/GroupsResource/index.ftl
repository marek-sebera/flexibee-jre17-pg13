<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=it.bundleAccessor.getMessage('groups', 'Skupiny')/>

<#include "/header-html-start.ftl" />

<@tool.showTitle title=title leftMenu="/i-left-menu-admin.ftl">
</@tool.showTitle>
<@tool.showTitleFinish />
<table class="table flexibee-tbl-list table-striped table-hover table-condensed">
<thead>
    <tr>
        <th>Název skupiny</th>
        <th>ID Skupiny</th>
        <th>Licenční skupina</th>
    </tr>
</thead>
<tbody>
     <#list it.list as l>
        <tr>
            <td>${l.name}</td>
            <td>${l.id}</td>
            <td><a href="/g/${l.id}/license">${l.licenseName}</a></td>
        </tr>
    </#list>
</tbody>
</table>

<#include "/footer.ftl" />
</#escape>
