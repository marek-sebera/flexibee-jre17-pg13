<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=lang('dialogs', 'evidences.title', 'Přehled evidencí')+" "+lang('dialogs', it.name, 'Přehled evidencí')/>
<#assign titleId="root"/>
<#include "/header.ftl"/>

<p><#noescape>${lang('labels', 'evidences.description', 'Tento přehled slouží k&nbsp;výpisu evidencí, ke kterým lze v této firmě přistupovat. Je možné jej převést do formátu XML či do JSON a následně strojově zpracovávat.')}</#noescape></p>

<#include "/i-devdoc-footer.ftl" />

<#include "/footer.ftl"/>
</#escape>
