<#ftl encoding="utf-8" />
<#escape x as (x)?default("")>
<#assign title=dlgText('webHooksInfo.status')/>


<#include "/header-html-start.ftl" />

<@tool.showTitle title=title leftMenu="/i-left-menu-admin.ftl">
</@tool.showTitle>
<@tool.showTitleFinish />

<style>
.key {
    min-width: 13em;
    display: inline-block;
}
</style>

<div class="row">
<form action="/status/web-hooks/control" method="post" class="form-inline" role="form">
    <div class="form-group">
        <label for="filter">Filtrace:</label>
        <a id="filter" href="/status/web-hooks?mode=${it.showAll?string('active','normal')}" class="btn btn-md btn-${it.showAll?string('danger','success')}">Active</a>
    </div>
    <div class="form-group">
        <label for="restart">Ovládání:</label>
        <input type="hidden" name="token" value="${csrfToken}">
        <input id="elect" type="submit" name="elect" class="btn btn-md btn-danger" title="${lblText('restartChangesWatcher.description')}" value="${btnText('newElection')}">
        <input id="restart" type="submit" name="restart" class="btn btn-md btn-danger" title="${lblText('restartWebHooks.description')}" value="${btnText('restart')}">
    </div>
</form>
</div>

<br/>

<table class="table flexibee-tbl-list table-striped table-hover table-condensed">
<#list it.list as i>
<thead>
    <tr>
        <th colspan="2"><h2>${i.server}</h2></th>
    </tr>
    <tr>
        <th>${lblText('status')}</th>
        <th>${lblText('webHooksInfo.status.watchedCompanies')}</th>
    </tr>
</thead>
<tbody>
    <tr>
        <td>
            <ul>
            <#list i.status?keys as property>
                <#assign value=i.status[property]>

                <#if value?is_hash_ex>
                <li><em>${lblText(property)}</em>:
                <ul>
                    <#list value?keys as subprop>
                    <#assign subval=value[subprop]>
                    <#assign clazz>
                        <#compress>
                        <#if subprop?ends_with('ActiveThreads')>flexibee-bold<#if subval &gt; 0> flexibee-green</#if></#if>
                        <#if subprop?ends_with('WaitingTasks')>flexibee-bold<#if subval &gt; 0> flexibee-red</#if></#if>
                        </#compress>
                    </#assign>
                    <li class="${clazz}"><span class="key">${lblText(subprop)}:</span><#if subval?is_sequence>[${subval?size}]
                        <ul>
                        <#list subval as subitem>
                        <li>${subitem}</li>
                        </#list>
                        </ul><#compress>
                    </#compress><#else><#compress>
                        ${subval}
                    </#compress></#if></li>
                    </#list>
                </ul>
                </li>
                <#else>
                <#assign clazz>
                    <#compress>
                    <#if property?ends_with('.enabled') && !value>
                        flexibee-bold flexibee-red
                    <#elseif property?ends_with('.watchingState')>
                        flexibee-bold <#if value == 'RUNNING'>flexibee-green<#elseif value == 'STOPPED'>flexibee-red</#if>
                    <#elseif property?ends_with('State')>
                        flexibee-bold
                    </#if>
                    </#compress>
                </#assign>
                <li class="${clazz}"><span class="key">${lblText(property)}:</span>${value?string}</li><#compress>
                </#compress>
                </#if>
            </#list>
            </ul>
        </td>
        <td>
            <ul>
            <#list i.watchedResources as r>
<#--                <li><a href="/status/hooks/${r.instanceId}/${r.companyId}">${r}</a></li>-->
                <li>${r}</li>
            </#list>
            </ul>
        </td>
    </tr>
    <#if i.currentlyNotifyingCompanies?has_content>
    <tr>
        <th colspan="2">${lblText('webHooksInfo.status.notifiedCompanies')}</th>
    </tr>
    <tr>
        <td colspan="2">
            <ul>
            <#list i.currentlyNotifyingCompanies as c>
                <li>${c}</li>
            </#list>
            </ul>
        </td>
    </tr>
    </#if>
    <#if i.currentlyScheduledHooks?has_content>
    <tr>
        <th colspan="2">${lblText('webHooksInfo.status.scheduledHooks')}</th>
    </tr>
    <tr>
        <td colspan="2">
            <ul>
            <#list i.currentlyScheduledHooks as h>
                <li>${h}</li>
            </#list>
            </ul>
        </td>
    </tr>
    </#if>
    <#if i.currentlyFiringHooks?has_content>
    <tr>
        <th colspan="2">${lblText('webHooksInfo.status.firingHooks')}</th>
    </tr>
    <tr>
        <td colspan="2">
            <ul>
            <#list i.currentlyFiringHooks as h>
                <li>${h}</li>
            </#list>
            </ul>
        </td>
    </tr>
    </#if>
</tbody>
</#list>
</table>

<#include "/footer.ftl" />
</#escape>