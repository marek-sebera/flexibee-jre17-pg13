<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=lang('dialogs', 'relations.title', 'Přehled relací')/>
<#assign titleId="root"/>
<#include "/header.ftl"/>

<table class="table flexibee-tbl-list table-striped table-hover table-condensed">
    <thead>
    <tr>
        <th>${lang('labels', 'identifier', 'Identifikátor')}</th>
        <th>${lang('labels', 'name', 'Název')}</th>
        <th>Typ evidence</th>
    </tr>
    </thead>
    <tbody>
    <#list it.list as item>
        <tr>
        <td>${item.url}</td>
        <td>${item.name}</td>
        <td><#if item.evidenceType?exists>${item.evidenceType}</#if></td>
        </tr>
    </#list>
    </tbody>
</table>



<#include "/footer.ftl"/>
</#escape>
