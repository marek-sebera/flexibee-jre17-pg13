<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=lang('dialogs', 'workflows.title', 'Přehled definovaných workflow') />
<#assign titleId="root"/>
<#include "/header.ftl"/>

<table class="table flexibee-tbl-list table-striped table-hover table-condensed">
    <thead>
    <tr>
        <th>${lang('labels', 'identifier', 'Identifikátor')}</th>
        <th>${lang('labels', 'name', 'Název')}</th>
    </tr>
    </thead>
    <tbody>
    <#list it.list as item>
        <tr>
        <th><a href="/c/${it.evidenceResource.companyResource.companyId}/${it.evidenceResource.evidenceName}.pdf?report-name=${item.key}">${item.key}</a></th>
        <td>${item.name}</td>
        </tr>
    </#list>
    </tbody>
</table>



<#include "/footer.ftl"/>
</#escape>
