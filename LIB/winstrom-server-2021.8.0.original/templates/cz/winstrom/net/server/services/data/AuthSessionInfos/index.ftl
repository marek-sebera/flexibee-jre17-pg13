<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=lang('dialogs', 'authentications.list', 'Přehled přihlášených uživatelů') />

<#include "/header-html-start.ftl" />

<#if inDesktopApp>
    <#-- zadne menu -->
<#else>
    <@tool.showTitle title=title leftMenu="/i-left-menu-admin.ftl">
    </@tool.showTitle>
    <@tool.showTitleFinish />
</#if>

<#if it.totalCount &gt; 0>
<table class="table flexibee-tbl-list table-striped table-hover table-condensed">
    <thead>
    <tr>
        <th>Instance <i class="fa fa-sort-desc"></th>
        <th>${lang('labels', 'loginDialogUzivatel', 'Uživatel')}</th>
        <th>Client ID</th>
        <th>Poslední obnovení</th>
        <th>Doba přihlášení</th>
        <th>Počet využití</th>
        <th>Typ klienta</th>
        <th>Instance admin</th>
        <th>Použita licence</th>
        <#if inDesktopApp && it.userInfo.isChangePassword()>
        <th></th>
        </#if>
    </tr>
    </thead>
    <tbody>
    <#list it.list as l>
    <#assign isLive=l.getConsumesLicense()/>
    <#assign isDead=!isLive/>
    <#macro td title="">
       <td<#if title?has_content> title="${title}"</#if>><#if isDead><s></#if><#nested><#if isDead></s></#if></td>
    </#macro>
    <tr>
        <@td>${l.instanceId}</@td>
        <@td>${l.username}</@td>
        <@td>
            <#if inDesktopApp>
                ${l.authSessionId}
            <#else>
                <a href="/status/session/${l.authSessionId}">${l.authSessionId}</a></td>
            </#if>
        </@td>
        <@td title=l.keepAlive?datetime>${l.keepAliveAsString}</@td>
        <@td title=l.createDate?datetime>${l.createDateAsString}</@td>
        <@td>${l.usageCount}</@td>
        <@td>${l.clientType}</@td>
        <@td>${l.instanceAdmin?string("Ano", "Ne")}</@td>
        <td>${l.ignoreForLicense?string("Vyloučeno" , l.consumesLicense?string("Ano", "Ne"))}${l.obsolete?string(" (odloženo)", "")}${l.loggedOut?string(" (odhlášeno)", "")}</td>
        <#if inDesktopApp && it.userInfo.isChangePassword()>
        <td>
         <form action="/status/user/${l.username}/logout" method="post">
                <input type="hidden" name="token" value="${csrfToken}">
                <input type="hidden" name="authSessionId" value="${it.authSessionId.sessionId}">
                <input type="hidden" name="instance" value="${l.instanceId}">
                <input type="hidden" name="inDesktopApp" value="true">
                <input type="hidden" name="clientType" value="${l.clientType}">
                <input type="submit" value="Odhlásit" class="btn btn-danger">
            </form>
        </td>
        </#if>
    </tr>
    </#list>
    </tbody>
    <tbody>
    <tr>
        <th colspan="8"># ${it.totalCount}</th>
        <#if inDesktopApp && it.userInfo.isChangePassword()>
        <th/>
        </#if>
    </tr>
    </tbody>
</table>
<#else>
<p>Není přihlášen žádný uživatel.</p>
</#if>

<#if inDesktopApp>
    <#-- zadna paticka -->

    <#if isDevel>
        <#-- Firebug lite pro debugovani -->
        <a href="javascript:(function(F,i,r,e,b,u,g,L,I,T,E){if(F.getElementById(b))return;E=F[i+'NS']&&F.documentElement.namespaceURI;E=E?F[i+'NS'](E,'script'):F[i]('script');E[r]('id',b);E[r]('src',I+g+T);E[r](b,u);(F[e]('head')[0]||F[e]('body')[0]).appendChild(E);E=new Image;E[r]('src',I+L);})(document,'createElement','setAttribute','getElementsByTagName','FirebugLite','4','firebug-lite.js','releases/lite/latest/skin/xp/sprite.png','https://getfirebug.com/','#startOpened');">Firebug Lite</a></td>
    </#if>
<#else>
    <#include "/footer.ftl" />
</#if>
</#escape>