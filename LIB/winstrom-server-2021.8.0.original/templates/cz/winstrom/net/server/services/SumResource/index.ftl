<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=lang('actions', 'sum', 'Sumace')/>
<#assign titleId="root"/>
<#include "/header.ftl" />

<#if isModal>
<div class="modal-header">
 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
 ${title}
 </div>
<div class="modal-body">
</#if>

<#if it.sumace??>
<table class="table flexibee-tbl-list table-striped table-hover table-condensed">
<#list it.sumace as sum>
    <#list sum.headers as header>
    <tr><td colspan="2"><strong>${header.value}</strong></td></tr>
    </#list>
    <#list sum.values as val>
    <#assign incomeOutcome="">
    <#if val.groupedValue.valueIncome??>
        <#assign incomeOutcome>${val.groupedValue.valueIncome?string("#,##0.00")}</#assign>
    </#if>
    <#if val.groupedValue.valueOutcome??>
        <#assign incomeOutcome>${incomeOutcome}-</#assign>
        <#assign incomeOutcome>${incomeOutcome}${val.groupedValue.valueOutcome?string("#,##0.00")}</#assign>
     </#if>
     <#if incomeOutcome?length &gt; 0>
        <#assign incomeOutcome>( ${incomeOutcome})</#assign>
     </#if>
    <tr><td style="padding-left:2em">${val.msg}</td><td class="r">${val.groupedValue.value?string("#,##0.00")} ${val.mena} ${incomeOutcome}</td></tr>
    </#list>
</#list>
</table>
</#if>

<#if isModal>
</div>
<div class="modal-footer">
    <input type="button" name="cancel" data-dismiss="modal" <#if isMobile>data-role="none"</#if> value="${actText('ok')}" class="btn btn-primary flexibee-cancel-button"/>
</div>
</#if>

<#include "/footer.ftl" />
</#escape>