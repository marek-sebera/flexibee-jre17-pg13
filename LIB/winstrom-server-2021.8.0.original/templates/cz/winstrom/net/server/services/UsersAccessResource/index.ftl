<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#include "/i-localization.ftl"/>
<#assign title><@dlg "users_access.title"/></#assign>
<#include "/header-html-start.ftl" />

<@tool.showTitle title=title leftMenu="/i-left-menu-admin.ftl">
</@tool.showTitle>
<@tool.showTitleFinish />

<table class="table flexibee-tbl-list table-striped table-hover table-condensed">
<thead>
    <tr>
        <th>Přihlašovací jméno</th>
        <th>Licenční skupina</th>
        <th>Přístupné firmy</th>
    </tr>
</thead>
<tbody>
    <#list it.list as l>
        <tr>
            <td>${l.username}</td>
            <td>${l.groupId}</td>
            <td><#list l.accessList as a><#if a_index != 0>, </#if><code>${a}</code></#list></td>
        </tr>
    </#list>
</tbody>
</table>

<#include "/footer.ftl" />
</#escape>
