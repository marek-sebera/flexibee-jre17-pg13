<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=lang('formsInfo', 'licence', 'Licence aplikace')/>
<#assign titleId="root"/>

<#include "/header-html-start.ftl" />

<@tool.showTitle title=title leftMenu="/i-left-menu-admin.ftl">
</@tool.showTitle>
<@tool.showTitleFinish />

<table class="table flexibee-tbl-list table-striped table-hover table-condensed">
    <tr>
        <th>id:</th>
    <td>${it.license.id}</td>
    </tr>
    <tr>
    <th>name:</th>
    <td>${it.license.name}</td>
    </tr>
    <tr>
    <th>variant:</th>
    <td>${it.license.variant}</td>
    </tr>
</table>

<#include "/footer.ftl"/>
</#escape>
