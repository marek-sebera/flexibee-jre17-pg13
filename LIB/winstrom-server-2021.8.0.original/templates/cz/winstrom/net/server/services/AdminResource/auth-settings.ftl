<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=dlgText('admin.authorization.title')/>
<#assign titleId="root"/>

<#include "/header-html-start.ftl" />

<@tool.showTitle title=title leftMenu="/i-left-menu-admin.ftl">
</@tool.showTitle>
<@tool.showTitleFinish />

<div id="panel"></div>

<script type="text/javascript" charset="utf-8" src="${staticPrefix}/js/sencha/ext-all.js?v=${serverVersion}"></script>
<link rel="stylesheet" type="text/css" href="${staticPrefix}/css/ext-all-flexibee.css?v=${serverVersion}" />
<script type="text/javascript" charset="utf-8" src="/sencha/classes.js?v=${serverVersion}"></script>
<script type="text/javascript" charset="utf-8" src="${staticPrefix}/js/sencha/ext-lang-cs.js?v=${serverVersion}"></script>

<script>
Ext.require([
    'Ext.tip.QuickTipManager',
    'FlexiBee.root.AuthPanel'
]);

Ext.onReady(function() {

    Ext.QuickTips.init();

    Ext.create('FlexiBee.root.AuthPanel', {
        renderTo: 'panel'
    });

});

</script>

<#include "/footer.ftl" />
</#escape>