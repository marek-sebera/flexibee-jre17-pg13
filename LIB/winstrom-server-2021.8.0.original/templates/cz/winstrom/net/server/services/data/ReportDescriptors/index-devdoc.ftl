<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=lang('dialogs', 'reports.title', 'Přehled tiskových sestav') />
<#assign titleId="root"/>
<#include "/header.ftl"/>

<p>Tento přehled slouží k&nbsp;výpisu tiskových výstupů.</p>

<#include "/i-devdoc-footer.ftl" />

<#include "/footer.ftl"/>
</#escape>
