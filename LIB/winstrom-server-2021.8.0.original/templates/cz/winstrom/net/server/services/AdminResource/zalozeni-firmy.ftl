<#ftl encoding="utf-8" />
<#-- @ftlvariable name="it" type="cz.winstrom.net.server.services.AdminResource" -->
<#escape x as x?default("")?html>
<#assign title=lang('formsInfo', 'novaFirmaWiz', 'Založení nové firmy')/>
<#assign titleId="root"/>

<#include "/header-html-start.ftl" />

<@tool.showTitle title=title showPath=false>
</@tool.showTitle>
<@tool.showTitleFinish />

<#import "/l-tools.ftl" as tool />
<#import "/l-edits.ftl" as edit />

<#if chyba??>
    <div class="alert alert-danger">
        <ul class="flexibee-errors">
            ${chyba}
        </ul>
    </div>
</#if>

<#if cloudOverride.isTemplate("create-company", "content")>
<#include cloudOverride.include("create-company", "content") />
</#if>

<#macro typyOrganizaci stat licenseHelper>
        <#list stat.getDostupneOrg(licenseHelper) as typOrg>
            <div class="radio">
                <label for="org-type-${typOrg.uniqueCode?replace("+", "_")}">
                <input type="radio" name="org-type" value="${typOrg.uniqueCode}" id="org-type-${typOrg.uniqueCode?replace("+", "_")}" <#if (vybranyTypOrg?? && vybranyTypOrg == typOrg.uniqueCode) || (!vybranyTypOrg?? && typOrg_index == 0)>checked="checked"</#if>/>
                        ${typOrg}</label>
                        <#if typOrg.description?? && typOrg.description != ''>
                        <span class="help-block">${typOrg.description}</span>
                        </#if>
            </div>
        </#list>
</#macro>

<div class="maxwidth2">
<div class="desktop-login desktop-content">

    <form action="?token=${csrfToken}" method="post" class="form-horizontal" role="form">
        <h2 class="flexibee-step1">Stát legislativy</h2>
        <div id="flexibee-select-stat">
                <#list it.staty as stat>
                    <div class="radio">
                        <label for="stat-${stat.kod}"><input type="radio" name="country" value="${stat.kod}"  id="stat-${stat.kod}" <#if (vybranyStat?? && vybranyStat == stat.kod) || (!vybranyStat?? && stat_index == 0)>checked="checked"</#if>/>
                        ${stat}</label>
                    </div>
                </#list>
        </div>

        <h2 class="flexibee-step2">Informace o firmě</h2>
            <div class="form-group">
                <label for="company-name" class="control-label col-sm-4">Název firmy:</label>
                <div class="col-sm-8 input-group">
                    <input type="text" name="name" id="company-name" maxlength="${maxlenNazev}" value="<#if vybranyNazev??>${vybranyNazev}</#if>" class="form-control" />
                </div>
            </div>
            <div class="form-group">
                <label for="company-ic" class="control-label col-sm-4">IČO:</label>
                <div class="input-group col-sm-8">
                    <input type="text" name="ic" placeholder="IČO" id="company-ic" maxlength="${maxlenIc}" value="<#if vybraneIc??>${vybraneIc}</#if>" class="form-control" />
                </div>
            </div>
            <div class="form-group">
                <label for="company-vatid" class="control-label col-sm-4">DIČ:</label>
                <div class="input-group col-sm-8">
                    <input type="text" name="vatid" id="company-vatid" maxlength="${maxlenDic}" value="<#if vybraneVatid??>${vybraneVatid}</#if>" class="form-control" />
                </div>
            </div>

        <h2 class="flexibee-step3">Typ organizace</h2>
        <div id="flexibee-select-typorg" class="flexibee-choose-list">
            <@typyOrganizaci stat=it.staty[0] licenseHelper=it.licenseHelper/>
        </div>


<#--        <h2 class="flexibee-step4">Testovací data</h2>
        <div>
            <label>Nahrát demo data: <input type="checkbox" name="use-demo" value="true" class="form-control" /></label>
        </div>
-->

        <div class="flexibee-dialog-buttons">
            <input type="button" name="cancel" value="${lang('buttons', 'cancel', 'Zrušit')}" class="btn btn-link negative flexibee-cancel-button" style="display: none;"/>
            <input type="submit" name="submit" value="${btnText('vyberFirmyNovaFirmaBT')}" class="btn btn-primary flexibee-show-wait"/>
        </div>

    <script>
        $(function() {
            $(".flexibee-cancel-button").show().click(function() {
                window.location = "/c";
            });
        });
    </script>

    </div><!-- flexibee-steps -->
    </form>

</div>
</div> <!-- flexibee-dialog -->

<script type="text/javascript">
$(function() {
    $(".body").flexiBeeWallpaper();

    var extraParams = { country: "CZ", format: "json" };

    $("#flexibee-select-stat input").change(function() {
        var stat = $(this).val();
        extraParams.country = stat;

    if (stat == 'CZ') {
            $("label[for='company-vatid']").html("DIČ:");
    } else {
            $("label[for='company-vatid']").html("IČ-DPH:");
    }

        <#list it.staty as stat>
            if ("${stat.kod}" == stat) {
                var html = '<#noescape><#assign code><@typyOrganizaci stat=stat licenseHelper=it.licenseHelper/></#assign><#escape x as x?replace("</", "<'+'/")><#escape x as x?js_string>${code}</#escape></#escape></#noescape>';
                $("#flexibee-select-typorg").html(html);
            }
        </#list>
    });

    $("#flexibee-select-stat input:checked").change();

    function handleTypOrganizace(typ_organizace) {
    var enableAll = true;
    var enabledTypOrgs = {};
    if (typ_organizace && typ_organizace.length > 0) {
        for(org in typ_organizace) {
                enabledTypOrgs[typ_organizace[org]] = true;
        }

        /* označíme první jako vybraný */
        $("input[name='org-type']").filter("[value='"+typ_organizace[0]+"']").attr("checked", "checked");

        enableAll = false;
    }

    <#list it.staty as stat>
        <#list stat.getDostupneOrg(licenseHelper) as typOrg>
        if (enableAll || enabledTypOrgs['${typOrg.uniqueCode}'] == true) {
            $("#org-type-${typOrg.uniqueCode?replace("+", "_")}").removeAttr('disabled');
        } else {
            $("#org-type-${typOrg.uniqueCode?replace("+", "_")}").attr('disabled', true);
        }

        </#list>
    </#list>

    }

    function updateFromInternet(isIc) {
    var ic = '';
    var dic = '';

    if (isIc) {
            ic = $("#company-ic").val();
    } else {
            dic = $("#company-vatid").val();
    }
        var stat = extraParams.country;

        if (ic != '' || dic != '') {
        var dicName = 'dic';
        if (stat == 'SK') {
        dicName = 'ic-dph';
        }
            $.getJSON('https://support.flexibee.eu/remote/company-info/' + stat + '/?ic=' + ic + '&' + dicName + '=' + dic + '&format=json', function(data) {
                if (data && data.winstrom && data.winstrom.adresar) {
                    $("#company-name").valWithAlert(data.winstrom.adresar.nazev);
                    $("#company-ic").valWithAlert(data.winstrom.adresar.ic);
                    $("#company-vatid").valWithAlert(data.winstrom.adresar.dic);

                    handleTypOrganizace(data.winstrom.adresar.typ_organizace);
                } else {
                    alert("${lang('messages', 'adresarAktInternetNotFound', 'Na internetu se nepodařilo nalézt informace o vybrané firmě.')}");
                }
            });
        } else {
            alert("${lang('messages', 'internetUpdate.ic_dic_empty', 'Není vyplněné IČO ani DIČ.')}");
        }
    }

    var html = $('<span class="input-group-btn"><button class="btn btn-default" type="button" title="${lang('actions', 'adresarAktInternet', 'Aktualizovat z internetu')}"><i class="fa fa-search fa-fw"></i></button></div>').click(function() {
            updateFromInternet(true);
            return false;
    });
    $("#company-ic").after(html);

    html = $('<span class="input-group-btn"><button class="btn btn-default" type="button" title="${lang('actions', 'adresarAktInternet', 'Aktualizovat z internetu')}"><i class="fa fa-search fa-fw"></i></button></div>').click(function() {
            updateFromInternet(false);
            return false;
    });
    $("#company-vatid").after(html);

    $("#company-name").autocomplete("https://support.flexibee.eu/remote/suggest/", {
        width: 280,
        extraParams: extraParams,
        dataType: "json",
        selectFirst: false,
        minChars: 3,
        parse: function(data) {
            return $.map(data.winstrom.adresar, function(row) {
                return {
                    data: row,
                    value: row.nazev,
                    result: row.nazev
                }
            });
        },
        formatItem: function(item) {
            var nazev = item.nazev;
            if (nazev.length > 50) {
                nazev = nazev.substring(0, 50) + "...";
            }
            return nazev + "<br/><small style=\"color:gray\">" + item.ulice + ", " + item.mesto + (item.ic ? " | IČO: " + item.ic : "") + "<"+"/small>";
        }
    }).result(function(e, item) {
        $("#company-ic").valWithAlert(item.ic);
        $("#company-vatid").valWithAlert(item.dic);
        handleTypOrganizace(item.typ_organizace);
    });
});
</script>

<#include "/footer.ftl" />
</#escape>