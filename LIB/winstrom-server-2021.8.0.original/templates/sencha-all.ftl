<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.main.All', {
});

<#list [
"root/sencha-MessageBus",
"root/sencha-Plugins",
"sencha-AbstractController",
"sencha-StatusBar",
"sencha-Toolbar",
"sencha-BackwardWindow",
"sencha-Controller",
"sencha-Evidences",
"sencha-MenuStructure",
"sencha-Menu",
"sencha-Window",
"sencha-Application"
] as i>
<#include resolveTemplate("/" + i) />
</#list>


</#escape>
