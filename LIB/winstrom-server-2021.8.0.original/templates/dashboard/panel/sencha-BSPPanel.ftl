<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.main.dashboard.panel.BSPPanel', {
    extend: 'FlexiBee.main.dashboard.panel.BasePanel',
    alias: 'widget.fbDashboardBSPPanel',

    requires: [
        'Ext.tip.QuickTipManager',
        'FlexiBee.main.dashboard.panel.BSPModel',
        'FlexiBee.main.dashboard.panel.BSPGrid',
        'FlexiBee.main.dashboard.panel.BasePanel'
    ],

    grid: 'FlexiBee.main.dashboard.panel.BSPGrid',

    processValues: function(content, prevContent, data, dataDefinition) {
        var self = this;
        var name;

        var year = new Date().getFullYear();

        for (var key in content['bsp']) {
            name = key;
            break;
        }

        data.push({
           group: dataDefinition.title,
           date: year - 1,
           pocatek: prevContent['bsp'][name].values['sumDoklPocatekKDatu'].value,
           prijmy: prevContent['sumDoklPrijmy'].values['sumDoklCelkem'].value,
           vydaje: -1 * prevContent['sumDoklVydaje'].values['sumDoklCelkem'].value,
           zustatek: prevContent['bsp'][name].values['sumDoklStavKDatu'].value,
           currency: prevContent['bsp'][name].values['sumDoklPocatekKDatu'].currency
        });

        data.push({
           group: dataDefinition.title,
           date: year,
           pocatek: content['bsp'][name].values['sumDoklPocatekKDatu'].value,
           prijmy: content['sumDoklPrijmy'].values['sumDoklCelkem'].value,
           vydaje: -1 * content['sumDoklVydaje'].values['sumDoklCelkem'].value,
           zustatek: content['bsp'][name].values['sumDoklStavKDatu'].value,
           currency: content['bsp'][name].values['sumDoklPocatekKDatu'].currency
        });

        return data;
    }
});
</#escape>
