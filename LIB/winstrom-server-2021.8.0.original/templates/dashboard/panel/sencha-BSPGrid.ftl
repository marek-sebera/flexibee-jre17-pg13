<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.main.dashboard.panel.BSPGrid', {
    extend: 'Ext.grid.Panel',

    requries: [
        'FlexiBee.main.dashboard.panel.BSPModel',
        'Ext.data.Store'
    ],

    store: undefined,

    bodyCls: 'flexibee-grid-no-background',

    border: 0,
    columns: [
        {
            text: '',
            dataIndex: 'date',
            flex: 1
        },
        {
            xtype: 'numbercolumn',
            text: 'Počátek',
            dataIndex: 'pocatek',
            format: '0,000.00',
            align: 'right',
        },
        {
            text: 'Příjmy',
            dataIndex: 'prijmy',
            format: '0,000.00',
            align: 'right',
            xtype: 'numbercolumn',
        },
        {
            text: 'Výdaje',
            dataIndex: 'vydaje',
            align: 'right',
            xtype: 'numbercolumn',
            format: '0,000.00',
        },
        {
            text: 'Zůstatek',
            dataIndex: 'zustatek',
            align: 'right',
            xtype: 'numbercolumn',
            format: '0,000.00',
        }
    ],
    features: [{
             id: 'group',
             ftype: 'groupingsummary',
             groupHeaderTpl: '{name}',
             hideGroupedHeader: false,
             enableGroupingMenu: false
    }],

    initComponent: function() {
        var self = this;

        self.store = Ext.create('Ext.data.Store', {
            model: 'FlexiBee.main.dashboard.panel.BSPModel',
            groupField: 'group',
            data: { 'items': [] },
            proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    root: 'items'
                }
            }
        });

        self.callParent(arguments);
    }
});


</#escape>
