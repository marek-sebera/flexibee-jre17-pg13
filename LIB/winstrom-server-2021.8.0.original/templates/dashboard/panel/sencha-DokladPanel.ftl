<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.main.dashboard.panel.DokladPanel', {
    extend: 'FlexiBee.main.dashboard.panel.BasePanel',
    alias: 'widget.fbDashboardDokladPanel',

    requires: [
        'Ext.tip.QuickTipManager',
        'FlexiBee.main.dashboard.panel.DokladModel',
        'FlexiBee.main.dashboard.panel.DokladGrid',
        'FlexiBee.main.dashboard.panel.BasePanel'
    ],

    grid: 'FlexiBee.main.dashboard.panel.DokladGrid',

    processValues: function(content, prevContent, data, dataDefinition) {
        var self = this;

        var year = new Date().getFullYear();


        data.push({
           group: dataDefinition.title,
           date: year - 1,
           obrat: prevContent['sumDoklUcetni'].values['sumDoklCelkem'].value,
           naklady: prevContent['sumDoklUcetni'].values['sumNaklady'] ? prevContent['sumDoklUcetni'].values['sumNaklady'].value : 0,
           uhradit: prevContent['sumDoklUcetni'].values['sumDoklZbyvaUh'].value,
           currency: prevContent['sumDoklUcetni'].values['sumDoklCelkem'].currency
        });

        data.push({
           group: dataDefinition.title,
           date: year,
           obrat: content['sumDoklUcetni'].values['sumDoklCelkem'].value,
           naklady: content['sumDoklUcetni'].values['sumNaklady'] ? content['sumDoklUcetni'].values['sumNaklady'].value : 0,
           uhradit: content['sumDoklUcetni'].values['sumDoklZbyvaUh'].value,
           currency: content['sumDoklUcetni'].values['sumDoklZbyvaUh'].currency
        });

        return data;
    }
});




</#escape>
