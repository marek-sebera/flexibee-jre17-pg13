<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>



Ext.define('FlexiBee.main.dashboard.AnalyzaProdejeDashboard', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.fbAnalyzaProdejeDashboard',

    requires: [
        'Ext.tip.QuickTipManager',
        'Ext.panel.Panel',
        'Ext.JSON',
        'FlexiBee.main.dashboard.panel.BasePanel',
        'FlexiBee.main.dashboard.BaseDashboard',
        'FlexiBee.analyza_prodeje.Properties',
        'FlexiBee.analyza_prodeje.Store',
        'FlexiBee.analyza_prodeje.Grid'
    ],

    title: 'Nejprodávanější zboží',

    items: [
        {
            xtype: 'container',
            region: 'center',
            items: [
                {
                    xtype: 'fbGridAnalyza_prodeje',
                    height: 300,
                    padding: 15,
                    border: 0,
                    defineGridColumns: function(property) {
                        return [
                            property('cenik', {
                                flex: 1
                            }),
                            property('sumSumZkl'),
                            property('sumMnozMj'),
                            property('sumObchodniRozpeti')
                        ];
                    },
                    extraParams: {
                        analyzovat: 'cenik',
                    },
                    defaultOrder: [
                        {
                            property : 'sumObchodniRozpeti',
                            direction: 'DESC'
                        }
                    ]
                }
            ]
        }
    ]
});

</#escape>
