<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.main.dashboard.BaseDashboard', {
    extend: 'Ext.panel.Panel',

    requires: [
        'Ext.tip.QuickTipManager',
        'Ext.panel.Panel',
        'Ext.JSON'
    ],

    // initComponent: function(args) {
    //     var self = this;
    //
    //     // self.items.push(
    //     //     {
    //     //         xtype: 'cycle',
    //     //         region: 'north',
    //     //         prependText: 'Frekvence ',
    //     //         showText: true,
    //     //         menu: {
    //     //             items: [
    //     //                 {
    //     //                     text: 'Ročně'
    //     //                 },
    //     //                 {
    //     //                     text: 'Pololetně'
    //     //                 },
    //     //                 {
    //     //                     text: 'Čvrtletně'
    //     //                 },
    //     //                 {
    //     //                     text: 'Měsíčně',
    //     //                     checked: true
    //     //                 },
    //     //                 {
    //     //                     text: 'Týdně'
    //     //                 },
    //     //             ]
    //     //         }
    //     //
    //     //     }
    //     // );
    //
    //     self.callParent(args);
    // }
});

</#escape>
