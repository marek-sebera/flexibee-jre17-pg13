<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>



Ext.define('FlexiBee.main.dashboard.MoneyDashboard', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.fbMoneyDashboard',

    requires: [
        'Ext.tip.QuickTipManager',
        'Ext.panel.Panel',
        'Ext.JSON',
        'FlexiBee.main.dashboard.BaseDashboard',
        'FlexiBee.main.dashboard.panel.BasePanel',
        'FlexiBee.main.dashboard.panel.BSPPanel'
    ],

//    title: '${lblText('penizePN')}',

    items: [
        {
            xtype: 'container',
            region: 'north',
            layout: 'hbox',
            padding: '10 15 0 15',
            items: [
                {
                    xtype: 'container',
                    flex: 1,
                    html: '<h2>${lblText('penizePN')}</h2>'
                },
                {
                    xtype: 'cycle',
                    region: 'north',
                    cls: 'flexibee-combobox-hidden',
                    prependText: 'Frekvence ',
                    showText: true,
                    menu: {
                        items: [
                            {
                                text: 'Ročně'
                            },
                            {
                                text: 'Pololetně'
                            },
                            {
                                text: 'Čvrtletně'
                            },
                            {
                                text: 'Měsíčně',
                                checked: true
                            },
                            {
                                text: 'Týdně'
                            }
                        ]
                    }
                }
            ]
        },
        {
            xtype: 'container',
            region: 'center',
            items: [
                {
                    xtype: 'fbDashboardBSPPanel',
                    data: [
                        {
                            title: '${actText('openDoklady$$POK')}',
                            evidenceName: 'pokladni-pohyb'
                        },
                        {
                            title: '${actText('openDoklady$$BAN')}',
                            evidenceName: 'banka'
                        }
                    ]
                }
            ]
        }
    ]
});

</#escape>
