<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Řazení záznamů"/>
<#include "/i-devdoc-header.ftl" />

<p>Záznamy lze samozřejmě řadit. Seznam hodnot, podle kterých lze řadit, je závislé na konkrétní evidenci.</p>
<p>Řazení je řízeno parametrem URL <code>order</code>. Tento parameter lze uvést opakovaně.</p>

<p>Ukázka: <code>/c/firma/adresar?order=nazev</code></p>
<p>Lze řídit, zda řadit sestupně (<code>order=nazev@A</code>) nebo vzestupně (<code>/c/firma/adresar?order=nazev@D</code>).</p>

<p>Kvůli kompatibilitě s některými implementacemi, lze také použít parametr <code>sort</code> a <code>dir</code> (nabývá hodnot <code>ASC</code> a <code>DESC</code>).</p>


<#include "/i-devdoc-footer.ftl" />
</#escape>