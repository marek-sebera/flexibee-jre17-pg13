<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Práce s uživateli"/>
<#include "/i-devdoc-header.ftl" />

<p>ABRA Flexi XML umožňuje i práci s uživateli v systému. Kromě obvyklé struktury, kterou získáte, když si uživatele vyexportujete, je třeba doplnit ještě heslo, a to do elementů <code>password</code> a <code>passwordAgain</code> (navrženo pro použití v uživatelském rozhraní, oba elementy jsou povinné a jejich hodnoty musí být totožné). XML pro založení uživatele tak může vypadat např. takto:</p>

<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;uzivatel&gt;
    &lt;id&gt;code:einstein&lt;/id&gt;
    &lt;kod&gt;einstein&lt;/kod&gt; &lt;!-- přihlašovací jméno --&gt;
    &lt;jmeno&gt;Albert&lt;/jmeno&gt;
    &lt;prijmeni&gt;Einstein&lt;/prijmeni&gt;
    &lt;password&gt;E=mc2&lt;/password&gt; &lt;!-- heslo --&gt;
    &lt;passwordAgain&gt;E=mc2&lt;/passwordAgain&gt; &lt;!-- heslo znovu --&gt;
    &lt;role&gt;code:JENCIST&lt;/role&gt; &lt;!-- uživatelská role --&gt;
  &lt;/uzivatel&gt;
&lt;/winstrom&gt;
</pre>

<#include "/i-devdoc-footer.ftl" />
</#escape>
