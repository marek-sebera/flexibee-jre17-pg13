<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Odpočet záloh a ZDD"/>
<#include "/i-devdoc-header.ftl" />

<p>Při vytváření dokladů (např. faktur) je potřeba umožnit odpočet jiným dokladem (např. zálohovou platbou).</p>

<h3>Odpočet zálohy (vystavení daňového dokladu k platbě)</h3>
<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;faktura-vydana&gt;

    &lt;id&gt;code:ZDD-12/2014&lt;/id&gt;

    &lt;!-- Typ faktury (objekt) - max. délka: 20 --&gt;
    &lt;typDokl&gt;code:ZDD&lt;/typDokl&gt;

    &lt;odpocty-zaloh&gt;
        &lt;odpocet&gt;  &lt;!-- odpočet zálohy --&gt;
            &lt;castkaMen&gt;1600.0&lt;/castkaMen&gt; &lt;!-- částka v měně určená pro odpočet --&gt;
            &lt;doklad&gt;code:ZALOHA0001/2010&lt;/doklad&gt; &lt;!-- identifikátor dokladu pro odpočet --&gt;
            &lt;id&gt;ext:odpocet1&lt;/id&gt; &lt;!-- externí identifikátor vytvořené odpočtové položky --&gt;
        &lt;/odpocet&gt;
    &lt;/odpocty-zaloh&gt;
  &lt;/faktura-vydana&gt;
&lt;/winstrom&gt;</pre>

<h3>Odpočet ZDD</h3>
<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;faktura-vydana&gt;

    &lt;id&gt;code:VF-12/2014&lt;/id&gt;

    &lt;!-- Typ faktury (objekt) - max. délka: 20 --&gt;
    &lt;typDokl&gt;code:FAKTURA&lt;/typDokl&gt;

    &lt;odpocty-zaloh&gt;
        &lt;odpocet&gt;  &lt;!-- odpočet zdd --&gt;
            &lt;castkaZaklMen&gt;400.0&lt;/castkaZaklMen&gt; &lt;!-- částka základní sazby v měně určená pro odpočet --&gt;
            &lt;castkaSnizMen&gt;400.0&lt;/castkaSnizMen&gt; &lt;!-- částka snížené sazby v měně určená pro odpočet --&gt;
            &lt;castkaSniz2Men&gt;400.0&lt;/castkaSniz2Men&gt; &lt;!-- částka 2. snížené sazby v měně určená pro odpočet --&gt;
            &lt;castkaOsvMen&gt;400.0&lt;/castkaOsvMen&gt; &lt;!-- částka osvobozená od DPH v měně určená pro odpočet --&gt;
            &lt;doklad&gt;code:ZDD0001/2010&lt;/doklad&gt; &lt;!-- identifikátor dokladu pro odpočet --&gt;
            &lt;id&gt;ext:odpocet1&lt;/id&gt; &lt;!-- externí identifikátor vytvořených odpočtových položek --&gt;
                   &lt;!-- identifikátor jednotlivých položek je doplněn o posfix -ZAKL, -SNIZ, -SNIZ2, -OSV --&gt;
        &lt;/odpocet&gt;
    &lt;/odpocty-zaloh&gt;
  &lt;/faktura-vydana&gt;
&lt;/winstrom&gt;</pre>

<p>Při vytváření dokladů je možné využít i automatický odpočet zálohových dokladů a zálohových daňových dokladů.</p>
<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;faktura-vydana&gt;

    &lt;!-- Typ faktury (objekt) - max. délka: 20 --&gt;
    &lt;typDokl&gt;code:FAKTURA&lt;/typDokl&gt;

    &lt;odpocty-zaloh&gt;
        &lt;!-- automatický odpočet zálohy a zdd, které mají automatický odpočet povolen v typu dokladu --&gt;
        &lt;automaticky-odpocet&gt;true&lt;/automaticky-odpocet&gt;
    &lt;/odpocty-zaloh&gt;
  &lt;/faktura-vydana&gt;
&lt;/winstrom&gt;</pre>
<p>Zálohové faktury a ZDD musí mít v typu dokladu zaškrtnuto zaškrtávátko Povolit automatické odpočtení při vytvoření nového dokladu.<br/>
Pokud v jednom XML importujete více faktur s automatickým odpočtem, použijte mód atomic="false".</p>
<p>V případě, že importujete fakturu s nastaveným zaokrouhlováním, vytvořte nejdříve fakturu a až následně odpočítávejte zálohové doklady.</p>
<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;faktura-vydana atomic="false"&gt;

    &lt;!-- Vytvoření faktury se zaokrouhlením --&gt;
    &lt;id&gt;ext:mojeId&lt;/id&gt;
    &lt;typDokl&gt;code:FAKTURA&lt;/typDokl&gt;

  &lt;/faktura-vydana&gt;
  &lt;faktura-vydana&gt;
    &lt;id&gt;ext:mojeId&lt;/id&gt;
    &lt;odpocty-zaloh&gt;
        &lt;!-- automatický odpočet zálohy a zdd, které mají automatický odpočet povolen v typu dokladu --&gt;
        &lt;automaticky-odpocet&gt;true&lt;/automaticky-odpocet&gt;
    &lt;/odpocty-zaloh&gt;
  &lt;/faktura-vydana&gt;
&lt;/winstrom&gt;</pre>

<#include "/i-devdoc-footer.ftl" />
</#escape>