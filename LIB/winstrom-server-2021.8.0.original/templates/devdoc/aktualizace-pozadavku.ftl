<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Aktualizace požadavků na výdej"/>
<#include "/i-devdoc-header.ftl" />

<p>V případě, že je v <a href="/help/pruvodce_nastaveni_firmy_-_nastaveni_firmy">nastavení firmy</a> povolené <a href="/help/pruvodce_nastaveni_firmy_-_nastaveni_firmy#Z.C3.A1lo.C5.BEka_.22Zbo.C5.BE.C3.AD.22">generování požadavků na výdej</a>, tak lze pomocí REST API volat službu <em>Aktualizace skladových požadavků na výdej</em>.</p>


<h3>Způsob volání</h3>

<p>Lze využít <a href="http-operations">HTTP metodu</a>: <code>PUT</code> nebo <code>POST</code>.</p>

<p>Služba je dostupná na adrese: <code>/c/{firma}/sklad/aktualizace-pozadavku</code>, kde <i>{firma}</i> je databázový idenfitikátor firmy.</p>

<p>Jsou podporovány <a href="format-types">výstupní formáty</a>: <code>XML</code> nebo <code>JSON</code>.</p>


<h3>Parametry</h3>

Služba neočekává parametry.


<h3>Výsledek</h3>

<p>Pro rozpoznání, zda byla služba vykonána úspěšně, lze kontrolovat HTTP status odpovědi nebo vlastnost <code>success</code> v získaném dokumentu.</p>

<h4>Úspěšné volání</h4>

<p>V případě úspěšného vykonání služby je vracen HTTP status 200 a dokument odpovídající požadovanému formátu:</p>

<h5>XML</h5>

<pre class="brush:xml">
&lt;winstrom version="1.0"&gt;
  &lt;success&gt;true&lt;/success&gt;
&lt;/winstrom&gt;
</pre>

<h5>JSON</h5>

<pre class="brush:javascript">
{
  "winstrom": {
    "@version": "1.0",
    "success": "true"
  }
}
</pre>

<h4>Neúspěšné volání</h4>

<p>HTTP status odpovědi je 4xx nebo 5xx. A například v případě, že firma nemá povolené generování požadavků na výdej, získáme výsledný dokument:</p>

<h5>XML</h5>

<pre class="brush:xml">
&lt;winstrom version="1.0"&gt;
  &lt;success&gt;false&lt;/success&gt;
  &lt;message&gt;Není povoleno generování požadavků na výdej.&lt;/message&gt;
&lt;/winstrom&gt;
</pre>

<h5>JSON</h5>

<pre class="brush:javascript">
{
  "winstrom": {
    "@version": "1.0",
    "success": "false",
    "message": "Není povoleno generování požadavků na výdej."
  }
}
</pre>


<h3>Ukázky volání</h3>

<ul>
    <li><code>PUT /c/demo/sklad/aktualizace-pozadavku.xml</code></li>
    <li><code>PUT /c/demo/sklad/aktualizace-pozadavku.json</code></li>
    <li><code>PUT /c/demo/sklad/aktualizace-pozadavku</code> (s hlavičkou <code>Accept: application/xml</code> nebo <code>Accept: application/json</code>)</li>
    <li><code>POST /c/demo/sklad/aktualizace-pozadavku.xml</code></li>
    <li><code>POST /c/demo/sklad/aktualizace-pozadavku.json</code></li>
    <li><code>POST /c/demo/sklad/aktualizace-pozadavku</code> (s hlavičkou <code>Accept: application/xml</code> nebo <code>Accept: application/json</code>)</li>
</ul>

<#include "/i-devdoc-footer.ftl" />
</#escape>
