<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Automatický start jádra"/>
<#include "/i-devdoc-header.ftl" />

<p>Standardní nastavení serveru je, aby se aplikační část spouštěla při prvním požadavku. Tím se snižují nároky na start při běžném provozu (trvá cca 0,5 s).</p>

<p>V&nbsp;některých případech ale není toto chování vhodné &ndash; první požadavek pak trvá cca 20 vteřin. Proto je možné nastavit, aby se po startu serveru nastartovalo účetní jádro automaticky.</p>

<p>Stačí jen upravit soubor <code>flexibee-server.xml</code> (např. <code>/etc/flexibee/flexibee-server.xml</code>) a přidat <code>&lt;entry key="startKernel"&gt;true&lt;/entry&gt;</code>.</p>

<#include "/i-devdoc-footer.ftl" />
</#escape>