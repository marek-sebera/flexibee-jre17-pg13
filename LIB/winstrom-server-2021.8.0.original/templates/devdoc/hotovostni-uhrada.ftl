<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Hotovostní úhrada"/>
<#include "/i-devdoc-header.ftl" />

<p>Fakturu vydanou nebo přijatou lze přes XML hotovostně uhradit následujícím způsobem:</p>

<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;faktura-vydana&gt; &lt;!-- uhrazovaný doklad; může být i "faktura-prijata" --&gt;
    &lt;id&gt;code:FAKTURA1&lt;/id&gt;
    &lt;!-- lze normálně uvést další vlastnosti dokladu jako při běžném importu --&gt;

    &lt;hotovostni-uhrada&gt;
      &lt;uhrazujiciDokl&gt;code:UHRADA1&lt;/uhrazujiciDokl&gt; &lt;!-- nepovinné --&gt;
      &lt;pokladna&gt;code:POKLADNA KČ&lt;/pokladna&gt;
      &lt;!-- nepovinné, pokud je vyplněné a je true, použije se kurz platný k datu úhrady, jinak se použije kurz převzatý z uhrazované faktury --&gt;
      &lt;kurzKDatuUhrady&gt;true/false&lt;/kurzKDatuUhrady&gt;
      &lt;typDokl&gt;code:STANDARD&lt;/typDokl&gt; &lt;!-- typ vytvářeného pokladního dokladu --&gt;
      &lt;castka&gt;1000&lt;/castka&gt;
      &lt;datumUhrady&gt;2011-01-01&lt;/datumUhrady&gt;
    &lt;/hotovostni-uhrada&gt;
  &lt;/faktura-vydana&gt;
&lt;/winstrom&gt;</pre>

<p>Lze uvést ID uhrazujícího pokladního dokladu (tag <code>&lt;uhrazujiciDokl&gt;</code>) podle <a href="identifiers">obvyklých pravidel</a> a funguje následovně:
<ol>
    <li><strong>Nová úhrada</strong>: Pokud uvedeno není, vždy se vytvoří nový pokladní doklad, takže pokud naimportujete XML dvakrát, vytvoří se dvě úhrady. Pokud je uvedeno a doklad s daným ID neexistuje, založí se.</li>
    <li><strong>Aktualizace</strong>: Pokud existuje, musí uhrazovat danou fakturu (jinak jde o chybu) a znamená to, že se má úhrada pouze zaktualizovat dostupnou částkou na uhrazujícím dokladě. Ostatní parametry úhrady se ignorují.</p>
</ol>
</p>

<p>V tagu <code>&lt;hotovostni-uhrada&gt;</code> lze uvést ještě dokladovou řadu pro vytvářený pokladní doklad. Není to povinné a standardně se bere z vybraného typu dokladu, případně z vybrané pokladny.</p>

<pre class="brush: xml">
  &lt;rada&gt;code:POKLADNA+&lt;/rada&gt;
</pre>

<#include "/i-devdoc-footer.ftl" />
</#escape>