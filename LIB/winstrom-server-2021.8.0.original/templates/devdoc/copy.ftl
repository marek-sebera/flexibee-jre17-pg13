<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Kopie záznamu"/>
<#include "/i-devdoc-header.ftl" />


<p>Při importu lze namísto běžného vytvoření založit nový záznam kopií již existujícího záznamu. K tomu slouží atribut <code>sourceId</code>:</p>

<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;ucet sourceId="code:132001"&gt;
    &lt;id&gt;code:132002&lt;/id&gt;
    &lt;nazev&gt;Zboží na hlavním skladě&lt;/nazev&gt;
  &lt;/faktura-vydana&gt;
&lt;/winstrom&gt;
</pre>

<p>Pokud <strong>je uveden</strong> identifikátor nového záznamu, je volání idempotentní. Opakované odeslání požadavku způsobí pouze běžnou aktualizaci již existujícího záznamu.</p>
<p>Pokud identifikátor <strong>není uveden</strong> nebo takový záznam dosud neexistuje, vytvoří se kopie záznamu odkazovaného atributem <code>sourceId</code>.</p>
<p>Vytváření kopie záznamu se obvykle musí postarat o zajištění unikátnosti vlastností (např. pole <em>Zkratka</em> u číselníků), ale v některých případech je to již zajištěno automaticky (např. při kopírování dokladů).</p> 

<pre class="brush: js">
{
    "winstrom": {
        "@version": "1.0",
        "faktura-vydana": [{
            "@sourceId": "123",
            "firma": "code:JINY_ODBERATEL"
        }]
    }
}
</pre>

<#include "/i-devdoc-footer.ftl" />
</#escape>
