<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Obratová předvaha"/>
<#include "/i-devdoc-header.ftl" />

<h3>Způsob volání</h3>

<p>Dostupná <a href="http-operations">HTTP metoda</a>: <code>GET</code>.</p>

<p>Služba je dostupná na adrese: <code>/c/{firma}/obratova-predvaha.pdf</code>, kde <i>{firma}</i> je databázový idenfitikátor firmy.</p>

<p>Výsledkem je požadovaný PDF dokument.</p>


<h3>Parametry</h3>

Služba vyžaduje parametry:
<ul>
  <li><code>report-name</code>, 2 možné hodnoty, <b>Predvaha</b> - Obratová předvaha a <b>PredvahaPort</b> - Obratová předvaha (na výšku)</li>
  <li><code>ucetniObdobi</code> - kód účetního období</li>
  <li><code>koncovyMesicRok</code> - pouze obraty do zadaného měsíce ve formátu "měsíc/rok" - např. 06/2019</li>
  <li><code>pocetMesicu</code> - počet měsíců v obratu</li>
  <li><code>stredisko</code> - id objektu z evidence /stredisko (lze použít více - např. stredisko=code:C&stredisko=code:DE)</li>
  <li><code>mena</code> - kód měny (lze použít více těchto parametrů - např. mena=CZK&mena=EUR)</li>
  <li><code>filtrUcty</code> - kódy účtů, případně jejich prefix nebo rozsah, oddělené čárkou (např. 311000,32,3-4)</li>
  <li><code>ucet</code> - kód účtu (lze použít více těchto parametrů - např. ucet=211001&ucet=112001)</li>
</ul>

<b>Pokud nejsou uvedeny</b>, tak je výchozí Obratová předvaha (na výšku) za aktuální účetní období.

<h3>Ukázky volání</h3>

<code>GET https://demo.flexibee.eu/c/demo/obratova-predvaha.pdf?report-name=Predvaha&ucetniObdobi=code:2021&koncovyMesicRok=06/2020&pocetMesicu=12&stredisko=code:C&mena=code:CZK&filtrUcty=3&ucet=code:701000</code>

Výsledkem je požadovaný PDF dokument.

<#include "/i-devdoc-footer.ftl" />
</#escape>