<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Režim pro založení / změnu"/>
<#include "/i-devdoc-header.ftl" />

<p>Při importu záznamu lze určit chování podle toho, zda záznam již existuje. Lze tak zajistit, aby související záznam byl importován jen pokud ještě neexistuje. Pokud již existuje, je možné, že jej již uživatel změnil a proto jej nebudeme importovat znovu a přepisovat hodnoty.</p>
<p>Režim lze určit pro každý záznam zvlášť a také pro každou operaci (založení/změna).</p>

<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;faktura-vydana update="ignore"&gt;
    &lt;id&gt;123&lt;/id&gt;
    ...
  &lt;/faktura-vydana&gt;
&lt;/winstrom&gt;
</pre>


<table class="table table-striped table-condensed">
    <tr>
    <th>Operace</th>
    <th>Režim</th>
    <th>Popis</th>
    </tr>

    <tr>
    <th rowspan="3">Založení<br/>
(<code>create</code>)</th>
        <td><code>ignore</code></td>
    <td>Pokud záznam neexistuje, ignoruj požadavek na jeho založení.</td>
    </tr>

    <tr>
        <td><code>fail</code></td>
    <td>Pokud záznam neexistuje, selži.</td>
    </tr>

    <tr>
        <td><code>ok</code></td>
    <td>Pokud záznam neexistuje, normálně jej založ. <em>Výchozí hodnota.</em></td>
    </tr>

    <tr>
    <th rowspan="3">Změna<br/>
(<code>update</code>)</th>
        <td><code>ignore</code></td>
    <td>Pokud záznam již existuje, ignoruj požadavek na jeho změnu.</td>
    </tr>

    <tr>
        <td><code>fail</code></td>
    <td>Pokud záznam již existuje, selži.</td>
    </tr>

    <tr>
        <td><code>ok</code></td>
    <td>Pokud záznam již existuje, normálně jej změň. <em>Výchozí hodnota.</em></td>
    </tr>

</table>

<p>Podobného mechanizmu jako je u přeskakování řádků, je možné použít i u relací. Příklad:</p>
<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;faktura-vydana&gt;
    &lt;firma if-not-found="null"&gt;code:FIRMA&lt;/firma&gt;
    ...
    &lt;/faktura-vydana&gt;
&lt;/winstrom&gt;
</pre>

<p>V tomto případě pokud nebude nalezena firma s&nbsp;kódem <code>FIRMA</code>, bude místo ní nastavena vazba na <code>null</code> (tj. prázdná).</p>

<table class="table table-striped table-condensed">
    <tr>
        <th>Operace</th>
        <th>Popis</th>
    </tr>
    <tr>
        <td><code>null</code></td>
        <td>Pokud záznam neexistuje nenastavuj vazbu.</td>
    </tr>
    <tr>
        <td><code>nearest-invalid</code></td>
        <td>Pokud záznam odkazovaný kódem v současnosti neexistuje, ale v minulosti existoval, je vazba provedena na nejmladší již neplatný záznam. Jako datum se uvažuje datum doklad, datum události, .... Použití je například při importu historických dokladů s&nbsp;vazbou na již neplatné čísla účtů.</td>
    </tr>
    <tr>
        <td><code>create</code></td>
        <td>Pokud záznam odkazovaný kódem neexistuje, dojde k jeho automatickému založení. Pro záznamy číselníkového typu se vyplní vlastnosti kód / název dle hodnoty odkazu (např. <code>FIRMA</code> / <code>FIRMA - Vyplněno při importu</code>). Takto nelze zakládat záznamy s dalšími povinnými vlastnostmi.</td>
    </tr>
</table>

<#include "/i-devdoc-footer.ftl" />
</#escape>