<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Založení firmy"/>
<#include "/i-devdoc-header.ftl" />

<p>Přes REST API je možné i zakládat firmy či <a href="restore-backup">obnovovat ze zálohy</a>.</p>

<p><code>PUT /admin/zalozeni-firmy</code></p>

<p>Při zakládání firmy lze uvést tyto parametry:</p>

<table class="table table-striped table-condensed">
    <tr><th><code>?name=Firma</code></th><td> Název nově zakládané firmy</td></tr>
    <tr><th><code>?use-demo=true</code></th><td> Nově založená firma má být naplněna demo daty (pouze pro kombinaci <code>CZ</code> a <code>PODNIKATELE+PU</code>)</td></tr>
    <tr><th><code>?country=CZ</code></th><td> Legislativa nově založené společnosti (viz níže)</td></tr>
    <tr><th><code>?org-type=PODNIKATELE+PU</code></th><td> Typ organizace nově založené společnosti (viz níže)</td></tr>
    <tr><th><code>?ic=123</code></th><td> IČO společnosti, které má být použito pro základní nastavení (plátce DPH, sídlo, spisová značka, ...)</td></tr>
    <tr><th><code>?vatid=CZ456</code></th><td>daňové identifikační číslo společnosti</td></tr>
</table>

<p>Povinným údajem je pouze <code>name</code>. Pokud nevyhovují výchozí hodnoty, je třeba uvést i <code>country</code> a <code>org-type</code>. Pokud je uvedeno i <code>ic</code>, při zakládání firmy se doplní některé další údaje z ARESu.</p>

<p>Možné hodnoty parametrů jsou:</p>

<table class="table table-striped table-condensed">
    <tr><th><code>?country=</code></th><td> <code>CZ</code> <td>Česká republika</td></tr>
    <tr><th>&nbsp;</th><td> <code>SK</code> <td>Slovenská republika</td></tr>

    <tr><td colspan="3">Pro <code>country=CZ</code>:</td></tr>
    <tr><th><code>?org-type=</code></th><td> <code>PODNIKATELE+PU</code> </td> <td>Podnikatelé &ndash; podvojné účetnictví</td></tr>
    <tr><th>&nbsp;</th><td> <code>PODNIKATELE+DE</code> </td> <td>Podnikatelé &ndash; daňová evidence</td></tr>
    <tr><th>&nbsp;</th><td> <code>NEZISKOVE</code> </td> <td>Neziskové organizace</td></tr>
    <tr><th>&nbsp;</th><td> <code>ROZPOCTOVE</code> </td> <td>Příspěvkové organizace</td></tr>

    <tr><td colspan="3">Pro <code>country=SK</code>:</td></tr>
    <tr><th><code>?org-type=</code></th><td> <code>PODNIKATELIA+PU</code> </td> <td>Podnikatelé &ndash; podvojné účetnictví</td></tr>
    <tr><th><code>?org-type=</code></th><td> <code>PODNIKATELIA+DE</code> </td> <td>Podnikatelé &ndash; daňová evidence (jednoduché účtovnictvo)</td></tr>
</table>

<p>Jako obvykle, pozor na to, že hodnoty musí být v URL správně zakódovány. Odpověď v případě úspěšně založené firmy je 201 a URL firmy v hlavičce <code>Location</code>.</p>

<ul>
    <li><a href="company-identifier">Identifikátor firmy</a></li>
    <li><a href="restore-backup">Obnovení firmy ze zálohy</a></li>
</ul>


<#include "/i-devdoc-footer.ftl" />
</#escape>
