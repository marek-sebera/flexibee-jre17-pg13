<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Předchozí hodnota - způsob reakce na změnu"/>
<#include "/i-devdoc-header.ftl" />


<p>Do systému je zapracována schopnost reakce na změnu a tu můžete použít i z&nbsp;jiného systému. Tato funkce se obvykle používá v&nbsp;kombinaci s&nbsp;<a href="dry-run"><code>?dry-run=true</code></a> v&nbsp;případech, kdy zpřístupňujete data uživateli k&nbsp;přímé editaci. Uživatel tak očekává, že pokud na faktuře změní kód firmy, dojde k&nbsp;přepisu názvu firmy. Nicméně ve formuláři musí být uveden i název firmy, aby jej uživatel mohl změnit. Pomocí předchozí hodnoty se dá určit reakce na tuto změnu. Předchozích hodnot lze v&nbsp;jednom požadavku použít více.</p>
<p>Ukázka:</p>

<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;faktura-vydana id="123"&gt;
    &lt;firma previousValue="code:JINA FIRMA"&gt;code:FIRMA&lt;/firma&gt;
    &lt;nazFirmy&gt;Jiná firma&lt;/nazFirmy&gt;
  &lt;/faktura-vydana&gt;
&lt;/winstrom&gt;
</pre>


<p>Odpovědí pak je:</p>
<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;faktura-vydana id="123"&gt;
    &lt;firma&gt;code:FIRMA&lt;/firma&gt;
    &lt;nazFirmy&gt;Firma&lt;/nazFirmy&gt;
  &lt;/faktura-vydana&gt;
&lt;/winstrom&gt;
</pre>

<p>V&nbsp;případě, že by nebyla uvedena předchozí hodnota, vypadala by odpověď takto:</p>
<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;faktura-vydana id="123"&gt;
    &lt;firma&gt;code:FIRMA&lt;/firma&gt;
    &lt;nazFirmy&gt;Jiná firma&lt;/nazFirmy&gt;
  &lt;/faktura-vydana&gt;
&lt;/winstrom&gt;
</pre>

<p>Pokud byste chtěli dosáhnout stejného efektu i bez uvedení předchozí hodnoty, museli byste vynechat atributy, které souvisí se měněným atributem. Museli byste tak znát veškeré vazby, které závisí i na aktuálních datech.</p>

<p>Pokud například změníte typ dokladu, který ale nezmění předpis zaúčtování, nedojde k&nbsp;přepsání účtů (má dáti a dal). Jinými slovy, proces reakce na změnu se spouští jen pokud došlo k&nbsp;opravdové změně.</p>

<p>Ve formátu JSON se hodnota uvádí jako další tag s&nbsp;názvem <code>firma-previousValue</code>.</p>

<#include "/i-devdoc-footer.ftl" />
</#escape>