<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Identifikátor firmy"/>
<#include "/i-devdoc-header.ftl" />


<p>Při komunikaci se serverem je nutné uvádět identifikátor firmy. Ten obvykle vychází z&nbsp;názvu firmy, ale není to podmínkou. Identifikátor musí být unikátní pro firmu na daném serveru. Povolené znaky identifikátoru jsou malá písmena, číslice a podtržítko. Pokud již existuje firma se stejným názvem, přidá se na konec číslo. Pokud tedy firmu zazálohujete a obnovíte, dostane jiný identifikátor. Identifikátor zůstává neměný, pokud firmu přejmenujete. Pokud firmu smažete a znovu vytvoříte, bude pod stejným identifikátorem jiná firma.</p>



<#include "/i-devdoc-footer.ftl" />
</#escape>