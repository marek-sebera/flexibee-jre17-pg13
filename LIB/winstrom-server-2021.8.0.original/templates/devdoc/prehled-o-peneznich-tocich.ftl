<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Přehled o peněžních tocích"/>
<#include "/i-devdoc-header.ftl" />

<h3>Způsob volání</h3>

<p>Dostupná <a href="http-operations">HTTP metoda</a>: <code>GET</code>.</p>

<p>Služba je dostupná na adrese: <code>/c/{firma}/sestava.pdf</code>, kde <i>{firma}</i> je databázový idenfitikátor firmy.</p>

<p>Výsledkem je požadovaný PDF dokument.</p>


<h3>Parametry</h3>

Služba vyžaduje povinný parametr <code>report-name</code>, kterým zvolíte požadovanou tiskovou sestavu.

Možné varianty Přehledu o peněžních tocích:
<ul>
  <li>cashFlow$$NES - Přehled o peněžních tocích</li>
  <li>cashFlow$$MIN_OBDOBI - Přehled o peněžních tocích (včetně údajů za minulé období)</li>
</ul>

<h3>Ukázky volání</h3>

<ul>
  <li><code>GET https://demo.flexibee.eu/c/demo/sestava.pdf?report-name=cashFlow$$NES</code></li>
  <li><code>GET https://demo.flexibee.eu/c/demo/sestava.pdf?report-name=cashFlow$$MIN_OBDOBI</code></li>
</ul>

<p>V obou případech je odpovědí HTTP 200 a požadovaný dokument ve formátu PDF.</p>

<#include "/i-devdoc-footer.ftl" />
</#escape>