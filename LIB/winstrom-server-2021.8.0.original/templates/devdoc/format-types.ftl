<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Podporované formáty"/>
<#include "/i-devdoc-header.ftl" />

<p>REST API ABRA Flexi dokáže exportovat a importovat data v&nbsp;různých formátech. Typ výstupního formátu může být určen příponou (<code>.xml</code>, <code>.json</code>) nebo pomocí hlavičky v&nbsp;HTTP požadavku (<code>Accept</code> / <code>Content-Type</code>). Formát jako přípona je nepovinný a má přednost před HTTP hlavičkou. Pokud je uvedeno <code>Accept: */*</code> je vrácena HTML forma.</p>

<p>ABRA Flexi podporuje tyto formáty:</p>

<table class="table table-striped table-condensed" summary="Přehled podporovaných formátů">
    <thead>
    <tr>
        <th>Název</th>
        <th>Poznámka</th>
        <th>Přípona</th>
        <th>Content-Type</th>
        <th>Import?</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th>HTML</th>
        <td>HTML stránka pro zobrazení informací na webové stránce.</td>
        <td><code>.html</code></td>
        <td><code>text/html</code></td>
        <td>ne</td>
    </tr>
    <tr>
        <th><a href="http://cs.wikipedia.org/wiki/XML">XML</a></th>
        <td>Strojově čitelná struktura ve formátu XML.</td>
        <td><code>.xml</code></td>
        <td><code>application/xml</code></td>
        <td><strong>ano</strong></td>
    </tr>
    <tr>
        <th><a href="http://cs.wikipedia.org/wiki/JSON">JSON</a></th>
        <td>Strojově čitelná struktura ve formátu JSON.<!-- Podporuje i JSONP pomocí parametru <code>callback</code>.--></td>
        <td><code>.js</code><br/><code>.json<br/></code></td>
        <td><code>text/javascript</code><br/><code>application/json</code></td>
        <td><strong>ano</strong></td>
    </tr>
    <tr>
        <th><a href="http://cs.wikipedia.org/wiki/CSV">CSV</a></th>
        <td>Tabulkový výstup do formátu <abbr title="Comma-Separated Values">CSV</abbr>.</td>
        <td><code>.csv</code><br/><code>.csv?encoding=iso-8859-2</code></code></td>
        <td><code>text/csv</code></td>
        <td><strong>ano</strong></td>
    </tr>
    <tr>
        <th><a href="http://cs.wikipedia.org/wiki/DBase">DBF</a></th>
        <td>Databázový výstup ve formátu DBF (dBase).</td>
        <td><code>.dbf</code><br/><code>.dbf?encoding=iso-8859-2</code></code></td>
        <td><code>application/dbf</code></td>
        <td><strong>ano</strong></td>
    </tr>
    <tr>
        <th>XLS</th>
        <td>Tabulkový výstup ve formátu Excel.</td>
        <td><code>.xls</code></code></td>
        <td><code>application/ms-excel</code></td>
        <td><strong>ano</strong></td>
    </tr>
    <tr>
        <th><a href="http://cs.wikipedia.org/wiki/ISDOC">ISDOC</a></th>
        <td>Formát e-fakturace - ISDOC.<br/>Parametry importu: <code>typDokl</code> - <em>Typ faktury</em> (povinný), <code>typUcOp</code> - <em>Předpis zaúčtování</em>. Oba typu <a href="identifiers">identifikátor</a>.</td>
        <td><code>.isdoc</code><br/><code>.isdocx</code><br/><code>?typDokl=code:FAKTURA</code><br/><code>&amp;typUcOp=code:NÁKUP%20SLUŽBY</code></td>
        <td><code>application/x-isdoc</code><br/><code>application/x-isdocx</code></td>
        <td><strong>ano</strong></td>
    </tr>
    <tr>
        <th><a href="http://cs.wikipedia.org/wiki/ISDOC">EDI</a></th>
        <td>Elektronická výměna dat (EDI) ve formátu INHOUSE.</td>
        <td><code>.edi</code></td>
        <td><code>application/x-edi-inhouse</code></td>
        <td><strong>ano</strong></td>
    </tr>
    <tr>
        <th><a href="http://cs.wikipedia.org/wiki/Portable_Document_Format">PDF</a></th>
        <td>Generování tiskového reportu. Jedná se o stejnou funkci, která je dostupná v&nbsp;aplikaci. <a href="pdf">Export do PDF</a>.</td>
        <td><code>.pdf</code><br/><code>.pdf?report-name=vydejka</code></td>
        <td><code>application/pdf</code></td>
        <td>ne</td>
    </tr>
    <tr>
        <th><a href="http://en.wikipedia.org/wiki/VCard">vCard</a></th>
        <td>Výstup adresáře do formátu elektronické vizitky vCard.</td>
        <td><code>.vcf</code></td>
        <td><code>text/vcard</code></td>
        <td>ne</td>
    </tr>
    <tr>
        <th><a href="http://cs.wikipedia.org/wiki/ICalendar">iCalendar</a></th>
        <td>Výstup do kalendáře ve formátu iCalendar. Lze takto exportovat události, ale také třeba splatnosti u přijatých či vydaných faktur.</td>
        <td><code>.ical</code></td>
        <td><code>text/calendar</code></td>
        <td>ne</td>
    </tr>
    </tbody>
</table>

<#include "/i-devdoc-footer.ftl" />
</#escape>
