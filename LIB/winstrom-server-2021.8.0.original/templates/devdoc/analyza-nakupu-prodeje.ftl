<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Analýza nákupu / prodeje"/>
<#include "/i-devdoc-header.ftl" />

<p>Pro dotazy na analýzu nákupu a prodeje přes REST API slouží tato dvě URL:</p>
<ol>
    <li>Analýza nákupu - <code>/c/{firma}/analyza-nakupu</code></li>
    <li>Analýza prodeje - <code>/c/{firma}/analyza-prodeje</code></li>
</ol>
<p>Parametr <i>{firma}</i> označuje databázový identifikátor firmy.</p>


<h3>Parametry</h3>

<p>Parametry jsou až na drobné odchylky pro obě analýzy shodné:</p>

<table class="table table-striped table-condensed">
    <tr>
        <th>Parametr</th>
        <th>*</th>
        <th>Popis</th>
        <th>Možnosti</th>
        <th>Výchozí hodnota</th>
    </tr>
    <tr>
        <th><code>analyzovat</code></th>
        <td>&times;</td>
        <td>Určuje co je cílem prováděné analýzy:</td>
        <td>
            <ul>
                <li><code>cenik</code> - pohyb zboží</li>
                <li><code>skupZboz</code> - pohyb sumárně dle skupin zboží</li>
                <li><code>sklad</code> - obraty skladů</li>
                <li><code>stredisko</code> - obraty středisek</li>
                <li><code>zakazka</code> - obraty na zakázky</li>
                <li><code>adresar</code> - obraty podle firem</li>
                <li><code>skupFir</code> - obraty sumárně dle skupin firem</li>
                <li><code>mistUrc</code> - obraty dle míst určení (pouze pro analýzu prodeje)</li>
            </ul>
        </td>
        <td />
    </tr>
    <tr>
        <th><code>clenit</code></th>
        <td />
        <td>Stanovuje další členění analyzovaných dat:</td>
        <td>
            <ul>
                <li><code>cenik</code> - podle zboží</li>
                <li><code>skupZboz</code> - podle skupin zboží</li>
                <li><code>sklad</code> - podle skladů</li>
                <li><code>stredisko</code> - dělení podle středisek</li>
                <li><code>zakazka</code> - dělení na zakázky</li>
                <li><code>adresar</code> - kterým firmám</li>
                <li><code>skupFir</code> - kterým skupinám firem</li>
                <li><code>mistUrc</code> - dle míst určení (pouze pro analýzu prodeje)</li>
            </ul>
        </td>
        <td>Žádné další členění.</td>
    </tr>
        <th><code>datumOd</code></th>
        <td />
        <td>Spodní hranice data (zaúčtování) analyzovaných dokladů.</td>
        <td />
        <td>Žádné omezení data nejstaršího dokladu.</td>
    </tr>
    <tr>
        <th><code>datumDo</code></th>
        <td />
        <td>Horní hranice data (zaúčtování) analyzovaných dokladů.</td>
        <td />
        <td>Žádné omezení data nejnovějšího dokladu.</td>
    </tr>
    <tr>
        <th><code>datumyUcetne</code></th>
        <td />
        <td>Určuje zda se zadaná data dokladů berou jako datumy zaúčtování. Při <code>false</code> hodnotě se berou datumy jako data vystavení.</td>
        <td><code>true</code>/<code>false</code></td>
        <td><code>true</code> (doklady hledat dle data zaúčtování)</td>
    </tr>
    <tr>
        <th><code>neucetniDoklady</code></th>
        <td />
        <td>Ovlivňuje zda se budou do analýzy zahrnovat neúčetní doklady.</td>
        <td><code>true</code>/<code>false</code></td>
        <td><code>true</code> (zahrnout i neúčetní doklady)</td>
    </tr>
    <tr>
        <th><code>mesicniSumace</code></th>
        <td />
        <td>Nastavuje zda se mají provádět měsíční sumace obratů.</td>
        <td><code>true</code>/<code>false</code></td>
        <td><code>true</code> (provádět měsíční sumace)</td>
    </tr>
    <tr>
        <th><code>stredisko</code></th>
        <td />
        <td>Omezuje analýzu pouze na vybrané středisko. Pro více středisek lze parametr dotazu uvést vícekrát.</td>
        <td>Identifikátor střediska (celočíselné ID případně kód).</td>
        <td>Analýza všech středisek bez omezení.</td>
    </tr>
    <tr>
        <th colspan="5">Platí pouze pro <code>analyzovat=adresar</code> nebo <code>analyzovat=skupFir</code></th>
    </tr>
    <tr>
        <th><code>nekatalogovePolozky</code></th>
        <td />
        <td>Určuje zda se mají do analýzy zahrnout i nekatalogové položky. Má význam pouze při analýze dle firem nebo dle skupin firem.</td>
        <td><code>true</code>/<code>false</code></td>
        <td><code>false</code> (zahrnovat pouze katalogové položky)</td>
    </tr>
    <tr>
        <th colspan="5">Platí pouze pro <code>analyzovat=cenik</code></th>
    </tr>
    <tr>
        <th><code>cenik</code></th>
        <td />
        <td>Omezí analýzu zboží na vybranou položku.</td>
        <td>Identifikátor požadované ceníkové položky.</td>
        <td>Analýza všech ceníkových položek bez omezení.</td>
    </tr>
    <tr>
        <th colspan="5">Platí pouze pro <code>analyzovat=adresar</code></th>
    </tr>
    <tr>
        <th><code>firma</code></th>
        <td />
        <td>Omezí analýzu podle firem na vybranou společnost.</td>
        <td>Identifikátor požadované společnosti.</td>
        <td>Analyzovat podle všech firem bez omezení.</td>
    </tr>
</table>

<p><small>* = povinný parametr</small></p>


<h3>Ukázky</h3>

<ul>
    <li><code>GET /c/demo/analyza-prodeje?analyzovat=cenik</code></li>
    <li><code>GET /c/demo/analyza-nakupu?analyzovat=adresar&amp;datumOd=2012-01-01&amp;datumDo=2012-12-31&amp;datumyUcetne=false</code></li>
    <li><code>GET /c/demo/analyza-prodeje?analyzovat=skupFir&amp;nekatalogovePolozky=false&amp;stredisko=code:A&amp;stredisko=code:B</code></li>
    <li><code>GET /c/demo/analyza-nakupu?analyzovat=adresar&amp;clenit=skupZboz&amp;datumOd=2012-01-01&amp;mesicniSumace=true</code></li>
</ul>

<#include "/i-devdoc-footer.ftl" />
</#escape>
