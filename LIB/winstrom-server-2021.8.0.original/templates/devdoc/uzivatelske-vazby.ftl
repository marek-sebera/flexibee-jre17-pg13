<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Uživatelské vazby"/>
<#include "/i-devdoc-header.ftl" />

<p>Uživatelské vazby umožňují provázat libovolný objekt s jiným. U každé vazby je nutné uvést typ vazby a provázané objekty.</p>

<p>Vazby mohou být dvou druhů:</p>
<ul>
    <li><strong>ruční</strong> - provázané v&nbsp;aplikaci nebo importem</li>
    <li><strong>automatické</strong> &ndash; objekty jsou automaticky zařazovány podle konfigurace typu vazby (filtrem)</li>
</ul>


<p>Výpis objektů ve vazbě:</p>
<pre>https://demo.flexibee.eu/c/demo/faktura-vydana/1/uzivatelske-vazby</pre>

<p>Je také možné udělat, aby navázaný objekt byl přímou součástí exportu vazeb:</p>
<pre>https://demo.flexibee.eu/c/demo/faktura-vydana/1/uzivatelske-vazby.xml?detail=full&amp;includes=/winstrom/uzivatelska-vazba/object</pre>

<p>Pro výpis je také možné využít konfiguraci <a href="detail-levels">relací</a> a exportovat je jako součást objektu:</p>
<pre>https://demo.flexibee.eu/c/demo/faktura-vydana/1.xml?relations=uzivatelske-vazby</pre>

<p>Ukázka importování ruční vazby:</p>
<pre class="brush: xml">
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;winstrom version="1.0"&gt;
  &lt;adresar&gt;
    &lt;id&gt;109&lt;/id&gt;

    &lt;uzivatelske-vazby&gt;
      &lt;uzivatelska-vazba&gt;
        &lt;id&gt;ext:VAZBA:TESTEXTID-CEN&lt;/id&gt;
        &lt;evidenceType&gt;cenik&lt;/evidenceType&gt;
        &lt;object&gt;code:SKL&lt;/object&gt;
        &lt;popis&gt;popisek skl&lt;/popis&gt;
        &lt;poznam&gt;poznam skl&lt;/poznam&gt;
        &lt;vazbaTyp&gt;code:ADRCEN&lt;/vazbaTyp&gt;
      &lt;/uzivatelska-vazba&gt;
    &lt;/uzivatelske-vazby&gt;
  &lt;/adresar&gt;
&lt;/winstrom&gt;
</pre>

<h2>Mazání vazby</h2>
<p>Ukázka mazání ruční vazby:</p>
<pre class="brush: xml">
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;winstrom version="1.0"&gt;
  &lt;uzivatelska-vazba action="delete"&gt;
    &lt;id&gt;ext:VAZBA:TESTEXTID-CEN&lt;/id&gt;
  &lt;/uzivatelska-vazba&gt;
&lt;/winstrom&gt;
</pre>

<#include "/i-devdoc-footer.ftl" />
</#escape>