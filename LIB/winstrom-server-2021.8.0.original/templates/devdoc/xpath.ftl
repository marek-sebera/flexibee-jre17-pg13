<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="XPath"/>
<#include "/i-devdoc-header.ftl" />

<p>Pro zjednoduššení práce z&nbsp;příkazové řádky či jiných systémů, je možné na výstupní XML aplikovat výraz <a href="http://cs.wikipedia.org/wiki/XPath">XPath</a>. Lze tak snadno získat jen podmnožinu výstupních dat.</p>

<p>XPath výraz lze použít pouze v&nbsp;kombinaci s&nbsp;XML a je aplikován až na vytvořenou XML strukturu. Je tedy nejdříve aplikován <a href="filters">filtr</a>, <a href="paging">stránkování</a> a <a href="detail-levels">úroveň detailu</a> a pak až je aplikován výraz XPath.</p>

<p>Ukázky:</p>
<pre class="brush: xml">
/c/demo/adresar.xml?detail=full&xpath=//winstrom/adresar/email/text()
</pre>

<!-- /c/demo/adresar.xml?detail=full&xpath=concat(//winstrom/adresar/ic/text(),'|',//winstrom/adresar/dic/text()) -->


<p><a href="http://interval.cz/clanky/zaklady-jazyka-xpath/">Základy jazyka XPath...</a></p>

<#include "/i-devdoc-footer.ftl" />
</#escape>