<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Podpora pro Ruby On Rails"/>
<#include "/i-devdoc-header.ftl" />

<p>Naší snahou je, aby bylo ABRA Flexi REST API snadno použitelné pro vývojáře. Proto jsme vytvořili režim, který zjednodušší práci při použití Ruby On Rails (resp. Active Resource).</p>

<p>Pokud uvedete v parametru URL <code>?mode=ruby</code>, budete dostávat správné výsledky ve výstupním XML a bude také zapnuta podpora pro pluralizaci (Invoice &rarr; Invoices).</p>

<pre class="brush: ruby">
# GET /invoices.xml?mode=ruby
Invoice.find(:all, :params => { :mode => 'ruby' })
</pre>

<p class="background-marker-info">Podpora Ruby On Rails je zatím experimentální. Narazili jste na problém nebo jen chcete zlepšit rozhraní tak, aby se vám pracovalo lépe? Kontaktujte nás.</p>

<#include "/i-devdoc-footer.ftl" />
</#escape>
