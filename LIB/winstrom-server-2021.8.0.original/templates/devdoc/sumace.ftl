<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Sumace"/>
<#include "/i-devdoc-header.ftl" />

<p>Pokud potřebujete získat základní sumace o dané evidenci, použijte sumaci:</p>
<pre>/c/<font style="color:green">&lt;identifikátor firmy&gt;</font>/<font style="color:red">&lt;evidence&gt;</font>/$sum</pre>
<p>Je také možné kombinovat <a href="filters">filtraci</a> a sumaci:</p>
<pre>/c/<font style="color:green">&lt;identifikátor firmy&gt;</font>/<font style="color:red">&lt;evidence&gt;</font>/(<font style="color:orange">&lt;filtr&gt;</font>)/$sum</pre>

<p>Sumaci lze exportovat do těchto formátů:</p>
<ul>
    <li>XML: <code>/c/<font style="color:green">&lt;identifikátor firmy&gt;</font>/<font style="color:red">&lt;evidence&gt;</font>/$sum.xml</code></li>
    <li>JSON: <code>/c/<font style="color:green">&lt;identifikátor firmy&gt;</font>/<font style="color:red">&lt;evidence&gt;</font>/$sum.json</code></li>
</ul>

<p><i>Poznámka:</i> sumace lze provádět jen nad doklady (faktury, objednávky, poptávky, pokladní pohyby, skladové pohyby, ...).</p>


<#include "/i-devdoc-footer.ftl" />
</#escape>