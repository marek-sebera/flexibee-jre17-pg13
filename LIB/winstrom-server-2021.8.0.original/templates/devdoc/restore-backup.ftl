<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Obnovení ze zálohy"/>
<#include "/i-devdoc-header.ftl" />

<p>Pomocí REST API lze získat zálohu firmy takto:</p>

<p><code>GET /c/db_nazev/backup</code>
<ul>
    <li>uvedete-li hlavičku <code>Accept</code>, musí nabývat hodnoty <strong><tt>application/x-winstrom-backup</tt></strong> (případně <tt>application/octet-stream</tt>).
</ul>
</p>

<p>Firmu lze ze zálohy pomocí REST API i obnovit:</p>

<p><code>PUT /c/db_nazev/restore?name=Firma</code>
<ul>
    <li>parametr <code>name</code> je volitelný,
    <li>v&nbsp;těle požadavku musí být záloha firmy,
    <li>uvedete-li hlavičku <code>Content-Type</code>, musí nabývat hodnoty <strong><tt>application/x-winstrom-backup</tt></strong> (případně <tt>application/octet-stream</tt>).
</ul>
</p>

<p><strong>Pozor:</strong> obnovovaná firma nesmí existovat, při obnovení bude založena. Pokud již firma existuje, server vrátí výsledek <code>Company 'restored_company' already exists</code>. Pokud uvedete v <a href="company-identifier">identifikátoru firmy</a> nepovolený znak, dojde k přesměrování na URL s platným identifikátorem.</p>

<ul>
    <li><a href="company-identifier">Identifikátor firmy</a></li>
    <li><a href="create-company">Založení nové společnosti</a></li>
</ul>

<p>Při obnovení ze zálohy je také možné firmu oznčit jako testovací a vypnout funkce, které nejsou při testování žádoucí.</p>


<h3>Obnovení testovací firmy</h3>

<p>Pokud chceme obnovit firmu jako testovací, je nutné do URL adresy přidat parametr <code>forTesting=1</code> (např. <code>PUT /c/db_nazev/restore?name=Firma&forTesting=1</code>).</p>
<p>V tomto režimu je navíc možné specifikovat, které funkce mají být vypnuty. Implicitně (při neuvedení) mají parametry hodnotu <code>1</code> při níž dochází k vypnutí odpovídající funkce. Pokud nemá k vypnutí dojít, uveďte parametr s hodnotou <code>0</code>.</p>
<p>Funkce, které je možné vypnout:</p>
<table class="table table-striped table-condensed">
    <tr><th><code>disableEet</code></th><td>Dojde k vypnutí odesílání zpráv do EET. Ve všech platnostech nastavení firmy dojde ke změně vlastnosti <i>Mód komunikace</i> (na záložce <i>Automatické operace</i> &mdash; <i>EET</i>) na hodnotu <code>Vypnuto</code>.</td></tr>
    <tr><th><code>disableAutoSendMail</code></th><td>Vypne <a href="odesilani-mailem">automatické odesílání mailů</a>. Ve všech platnostech nastavení firmy dojde ke změně vlastnosti <i>Automaticky odesílat doklady mailem</i> (na záložce <i>Ostatní</i>) na hodnotu <code>Neodesílat</code>.</td></tr>
    <tr><th><code>disableWebHooks</code></th><td>Odregistruje všechny <a href="web-hooks">Web Hooky</a>, které jsou ve firmě registrovány.</td></tr>
</table>
<p>URL pak může vypadat například <code>/c/db_nazev/restore?name=Firma&forTesting=1&disableAutoSendMail=0</code>.</p>

<#include "/i-devdoc-footer.ftl" />
</#escape>
