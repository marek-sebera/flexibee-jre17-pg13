<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Přepočet skladu"/>
<#include "/i-devdoc-header.ftl" />

<h3>Způsob volání</h3>

<p>Lze využít <a href="http-operations">HTTP metodu</a>: <code>PUT</code> nebo <code>POST</code>.</p>

<p>Služba je dostupná na adrese: <code>/c/{firma}/sklad/prepocet</code>, kde <i>{firma}</i> je databázový idenfitikátor firmy.</p>

<p>Alternativně na adrese: <code>/c/{firma}/skladova-karta/{karta}/prepocet</code>, kde <i>{karta}</i> je konkrétní záznam.</p>

<p>V případě volání z evidence <i>skladova-karta</i> lze místo <i>{karta}</i> použít <a href="filters">filtraci</a> pro získání záznamu.</p>

<p>Jsou podporovány <a href="format-types">výstupní formáty</a>: <code>XML</code> nebo <code>JSON</code>.</p>


<h3>Parametry</h3>

Služba pro evidenci <code>sklad</code> očekává povinně parametr <code>ucetniObdobi</code>, aby bylo možno identifikovat časové období, za které chcete sklad přepočítat.
Pro evidenci <code>skladova-karta</code> parametr není třeba, karta jednoznačně podléhá konkrétnímu období.


<h3>Výsledek</h3>

<p>Pro rozpoznání, zda byla služba vykonána úspěšně, lze kontrolovat HTTP status odpovědi nebo vlastnost <code>success</code> v získaném dokumentu.</p>

<p>V případě úspěšného vykonání služby je vracen HTTP status 200 a dokument odpovídající standardnímu formátu, viz. <a href="https://www.flexibee.eu/navratove-hodnoty/">návratové hodnoty</a>.</p>

<p>V případě neúspěchu je vracen status 4xx/5xx a zpráva o důvodu neúspěchu.</p>


<h3>Ukázky volání</h3>

<ul>
    <li><code>PUT /c/demo/sklad/prepocet.xml?ucetniObdobi=code:2020</code></li>
    <li><code>PUT /c/demo/sklad/prepocet.json?ucetniObdobi=code:2020</code></li>
    <li><code>PUT /c/demo/sklad/prepocet?ucetniObdobi=code:2020</code> (s hlavičkou <code>Accept: application/xml</code> nebo <code>Accept: application/json</code>)</li>

    <li><code>POST /c/demo/skladova-karta/1/prepocet.xml</code></li>
    <li><code>POST /c/demo/skladova-karta/(nazev='ZBOZI')/prepocet.json</code></li>
    <li><code>POST /c/demo/skladova-karta/1/prepocet</code> (s hlavičkou <code>Accept: application/xml</code> nebo <code>Accept: application/json</code>)</li>
</ul>

<p>V příkladech výše se používá <a href="filters">filtrace</a>.</p>

<#include "/i-devdoc-footer.ftl" />
</#escape>
