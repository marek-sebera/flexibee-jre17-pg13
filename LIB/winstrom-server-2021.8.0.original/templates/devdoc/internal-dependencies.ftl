<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Vnitřní vazby při ukládání"/>
<#include "/i-devdoc-header.ftl" />

<p>Při vytváření záznamů dochází k&nbsp;vyplnění výchozích hodnot. Stačí tedy vyplňovat jen ty údaje, které chcete měnit a u ostatních zachovat výchozí hodnoty. Některé položky ovšem závisí na položkách jiných (např. typ dokladu nebo stát DPH určuje sazby DPH). Proto se v&nbsp;aplikaci vytvoří strom závislých položek a hodnoty jsou aplikovány tak, aby hodnota, která ovlivňuje jinou byla nastavena první. Pak dojde k&nbsp;nastavení závislé položky. V&nbsp;opačném pořadí by totiž došlo k&nbsp;přepsání hodnoty.</p>

<p>Díky této vlastnosti nezáleží na pořadí v&nbsp;jakém jsou atributy uvedeny v&nbsp;XML souboru. Na pořadí jednotlivých záznamů záleží (např. založení firmy do adresáře a následné objednávky).</p>

<p>Efekt ovlivňování položek také může mít za následek, že dojde i ke změně hodnoty, která není při <a href="partial-updates">inkrementální aktualizaci</a> explicitně měněna.</p>

<p>Při ukládání záznamu je provedena <a href="validations">validace</a> záznamu a na případné chyby, upozornění nebo jen informační okna je volající upozorněn.</p>

<#include "/i-devdoc-footer.ftl" />
</#escape>
