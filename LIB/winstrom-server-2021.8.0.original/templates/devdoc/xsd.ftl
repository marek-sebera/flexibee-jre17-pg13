<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="XML Schéma (XSD)"/>
<#include "/i-devdoc-header.ftl" />

<p>Základem komunikace se systémem ABRA Flexi je XML (resp. jeho JSON varianta). Struktura XML je ovšem variabilní a jeho obsah závisí na zvolené variantě, typu organizace (ČR vs SR, daňová evidence vs podvojné účetnictví, ...) a také na přístupových právech. Proto je součástí systému i popis aktuální struktury XML (tzv. samodokumentace). Každé XML obsahuje u prvního záznamu popis dat. Lze také získat seznam položek (tzv. <code>properties</code>).</p>

<p>Abychom zjednodušili práci, připravili jsme i strojový popis struktury ve formátu XSD. Rozdělili jsem formát pro export (tj. to co ze systému vystupuje) a pro import (needitovatelné položky jsou vynechány). Mělo by platit, že schéma pro export je nadmnožinou schématu pro import.</p>

<p>XSD soubory plně neodrážejí možnosti ABRA Flexi API a je možné, že systém vytvoří XML, které nesplňuje tuto strutkturu.</p>

<p>Pro získání XML schéma konkrétní evidence je nutné sestavit URL následovně:</p>
<ul>
    <li><code>/c/<font style="color:green">&lt;identifikátor firmy&gt;</font>/<font style="color:red">&lt;evidence&gt;</font>/schema-export.xsd</code></li>
    <li><code>/c/<font style="color:green">&lt;identifikátor firmy&gt;</font>/<font style="color:red">&lt;evidence&gt;</font>/schema-import.xsd</code></li>
</ul>

<p>Schéma pro adresář:</p>
<ul>
        <li><a href="https://demo.flexibee.eu/c/demo/adresar/schema-export.xsd">XSD pro export</a></li>
        <li><a href="https://demo.flexibee.eu/c/demo/adresar/schema-import.xsd">XSD pro import</a></li>

</ul>

<#include "/i-devdoc-footer.ftl" />
</#escape>
