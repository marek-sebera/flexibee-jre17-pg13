<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Přístupová práva"/>
<#include "/i-devdoc-header.ftl" />

<p>Webové rozhraní automaticky zpracovává přístupová práva. V&nbsp;seznamu položek, v&nbsp;XML exportech a na webové stránce se zobrazují jen záznamy a sloupečky, ke kterým má přistupující uživatel právo.
Při importech je možné modifikovat jen taková položky, ke kterým má uživatel práva.</p>

<p>Práva mohou být omezena takto:</p>
<ul>
    <li>uživatel nemůže vůbec přistupovat do dané evidence a nebo ji má jen pro čtení</li>
    <li>uživatel vidí jen některé záznamy (např. z&nbsp;mého střediska) a nebo jen některé záznamy může měnit</li>
    <li>uživatel vidí jen některé sloupečky (např. nevidí nákupní ceny) a jen některé sloupečky mohu editovat</li>
    <li>licence aplikace neumožňuje používat danou položku</li>
</ul>

<p>V&nbsp;seznamu položek uživatel vidí vždy jen položky, ke kterým má práva včetně informace o tom, zda je smí měnit. Je také možné, že obecně smí danou položku měnit, ale konkrétní řádek ne (např. je zamčený). Pokud jsou omezeny řádky, které vidí, vypadá to jako by data v&nbsp;aplikaci vůbec nebyly.</p>

<p>Pokud uživatel modifikuje i položky na které nemá právo, je tento fakt ignorován a jsou změněny jen položky, které měnit smí. Pokud by chtěl modifikovat i celé záznamy a nemá na to právo, je vyvolána chyba a celá operace je zamítnuta.</p>

<#include "/i-devdoc-footer.ftl" />
</#escape>