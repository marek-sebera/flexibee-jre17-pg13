<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Atributy ceníku"/>
<#include "/i-devdoc-header.ftl" />

<p>Atributy umožňují ukládat další informace o položkách ceníků pro lepší porovnávání zboží.</p>

<p>Export atributů:</p>

<pre>https://demo.flexibee.eu/c/demo/cenik/42/atributy.xml</pre>

<p>Import atributů:</p>

<pre class="brush: xml">
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;winstrom version="1.0"&gt;
  &lt;atribut&gt;
    &lt;id&gt;1&lt;/id&gt;
    &lt;hodnota&gt;150&lt;/hodnota&gt;
    &lt;cenik&gt;code:KUFR&lt;/cenik&gt;
    &lt;typAtributu&gt;code:ROZLISENI&lt;/typAtributu&gt;
  &lt;/atribut&gt;
&lt;/winstrom&gt;
</pre>

<p>Zatím není možné položky ceníku filtrovat podle atributů. Nicméně je to v&nbsp;plánu.</p>

<#include "/i-devdoc-footer.ftl" />
</#escape>