<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Požadované položky při importu"/>
<#include "/i-devdoc-header.ftl" />

<p>Při exportu XML ze systému se vyexportuje mnoho polí. Nicméně mnoho z nich je jen pro čtení (např. aktuální průměrná cena skladu) a tak je nelze modifikovat. Současně není nutné uvádět spoustu hodnot, protože jsou buď výchozí a nebo jsou brány z&nbsp;typu dokladu.</p>

<p>Při tvorbě XML se řiďte těmito pravidly:</p>
<ul>
    <li>obvykle platí, že je nutné vyplňovat jen ty údaje, které musíte zadat v aplikaci při ručním zakládání záznamu.</li>
    <li>některé položky je ovšem potřeba uvést, protože aplikace nabídne jejich výchozí hodnotu, ale v importech je nutné je uvést.</li>
    <li>téměř vždy je povinná položka typ dokladu (<code>&lt;typDokl/&gt;</code>). Ta určuje, jakého typu daný doklad je (např. faktura, záloha apod.).</li>
    <li>je nutné uvádět jen hodnotu v tagu (bez atributů). Atributy <code>ref</code> a <code>showAs</code> jsou ignorovány a v&nbsp;exportu jsou jen pro snažší vizualizaci uživateli</li>
    <li>při navázání na další systém je potřeba uvádět <a href="identifiers">identifikátory</a>. Ty mohou být různých typů (<code>code:</code>, <code>ext:</code>, <code>ean:</code>, ...)</li>
    <li>v závislosti na typu dokladu se Vám může stát, že je jednou nutné nějaký atribut uvádět a jindy ne</li>
</ul>



<#include "/i-devdoc-footer.ftl" />
</#escape>