<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Režim individuálních kódů ceníkových položek"/>
<#include "/i-devdoc-header.ftl" />

<p>V případě importu dokladů s položkami s vazbou na ceník lze aktivovat režim <i>individuálních kódů odběratele / dodavatele</i>. V tomto režimu lze na všech položkách dokladu ve vazbách na ceník použít místo obvyklého kódu zadaného prefixem "<code>code:</code>" také individuální kód odběratele (resp. dodavatele).</p>

<p>Individuální kód je třeba také uvádět s prefixem "<code>code:</code>" a musí být pro daného odběratele / dodavatele unikátní.</p>

<pre class="brush:xml">
&lt;winstrom version="1.0" article-individual-codes="true"&gt;
  &lt;objednavka-prijata&gt;
    &lt;kod&gt;OBP0004/2014&lt;/kod&gt;
    &lt;datVyst&gt;2014-02-17&lt;/datVyst&gt;
    &lt;firma&gt;code:FLEXIBEE&lt;/firma&gt;
    &vellip;
    &lt;polozkyObchDokladu&gt;
      &lt;objednavka-prijata-polozka&gt;
        &lt;typPolozkyK&gt;typPolozky.katalog&lt;/typPolozkyK&gt;
        &lt;mnozMj&gt;1.0&lt;/mnozMj&gt;
        &lt;sklad&gt;code:SKLAD&lt;/sklad&gt;
        &lt;cenik&gt;code:S001&lt;/cenik&gt;&lt;!-- vyhledávání primárně dle kódu v individuálním ceníku odběratele --&gt;
      &lt;/objednavka-prijata-polozka&gt;
    &lt;/polozkyObchDokladu&gt;
  &lt;/objednavka-prijata&gt;
&lt;/winstrom&gt;
</pre>

<p>Při vytváření objednávky se vyhledává položka nejprve v individuálním ceníku odběratele "<code>FLEXIBEE</code>" podle kódu "<strong><code>S001</code></strong>". Pokud v individuálním ceníku takový záznam neexistuje, pokračuje vyhledání ceníkové položky standardním způsobem.</p>

<#include "/i-devdoc-footer.ftl" />
</#escape>
