<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Výpis záznamů"/>
<#include "/i-devdoc-header.ftl" />

<p>V ABRA Flexi je možné vypisovat základní data z&nbsp;aplikace. Obecně rozlišujeme výpis a detail.</p>

<h2>Výpis</h2>
<p>Jedná se o výpis více záznamů. Lze jej <a href="paging">stránkovat</a>, <a href="filters">filtrovat</a>, <a href="ordering">řadit</a> a určovat <a href="detail-levels">úroveň detailu</a>. Výstup lze vypisovat ve <a href="format-types">více formátech</a>.</p>

<h2>Detail</h2>
<p>Detail vždy reprezentuje jen jeden záznam, který může být <a href="identifiers">identifikován různými způsoby</a> a zapsán ve <a href="format-types">více formátech</a>.</p>

<h2>Výpis mnoha konkrétních záznamů</h2>
<p>Pokud potřebujete získat více konkrétních záznamů, u nichž znáte <a href="identifiers">identifikátor</a> (typicky externí ID), máte několik možností.</p>

<ol>
    <li>
        <p>Můžete každý záznam získat samostatně, takto:</p>
        <pre>GET /c/firma/faktura-vydana/1
GET /c/firma/faktura-vydana/code:2
GET /c/firma/faktura-vydana/ext:SYS:3</pre>
        <p>Pozor na to, že v&nbsp;tomto příkladu bude první odpovědí na druhý a&nbsp;třetí požadavek přesměrování na adresu, která obsahuje číselné ID. Na tu je pak třeba poslat nový požadavek. Knihovny pro práci s&nbsp;HTTP obvykle umí následovat přesměrování automaticky, ale může být potřeba to zapnout.</p>
    </li>
    <li>
        <p>Můžete použít <a href="filters">filtraci</a>, takto:</p>
        <pre>GET /c/firma/faktura-vydana/(id in (1, 'code:2', 'ext:SYS:3'))</pre>
        <p>Pozor na to, že příliš dlouhá URL mohou způsobovat problémy (ať už ve webových prohlížečích nebo na nejrůznějších proxy serverech).</p>
    </li>
    <li>
        <p>Můžete použít funkci hromadného získání záznamů podle ID zadaných v XML dokumentu.</p>
        <pre>POST /c/firma/faktura-vydana/<b>get</b>.xml
Content-Type: application/xml

&lt;winstrom&gt;
  &lt;id&gt;1&lt;/id&gt;
  &lt;id&gt;code:2&lt;/id&gt;
  &lt;id&gt;ext:SYS:3&lt;/id&gt;
&lt;/winstrom&gt;</pre>
        <p>Zde není problém s délkou URL, takže takto lze získat i několik set či tisíc záznamů zároveň.</p>
        <p>Pokud je uveden neexistující identifikátor, bude ignorován. Pokud je uveden jeden identifikátor vícekrát, příp. pokud je uvedeno více identifikátorů, které ukazují na stejný záznam, budou na výstupu duplicity.</p>
        <p>Kromě XML lze použít i JSON, kromě metody POST lze i PUT:</p>
        <pre>PUT /c/firma/faktura-vydana/<b>get</b>.json
Content-Type: application/json

{
  "winstrom": {
    "id": [1, "code:2", "ext:SYS:3"]
  }
}</pre>
    </li>
</ol>

<#include "/i-devdoc-footer.ftl" />
</#escape>
