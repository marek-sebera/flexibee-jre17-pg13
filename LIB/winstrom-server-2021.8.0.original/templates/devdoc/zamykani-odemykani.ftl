<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Zamykání a odemykání záznamů"/>
<#include "/i-devdoc-header.ftl" />

<p>Jednou z funkcí ABRA Flexi je možnost zamknout nebo odemknout záznam. Po zamčení záznamu není možné tento záznam měnit. Změny jsou umožněny až po odemknutí.</p>

<p>K <b>zamknutí</b> záznamu slouží akce <code>lock</code> takto:</p>

<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;faktura-vydana action="lock"&gt;
    &lt;id&gt;1&lt;/id&gt;
  &lt;/faktura-vydana&gt;
&lt;/winstrom&gt;</pre>


<p>K <b>zamknutí</b> záznamu <b>pro účetní</b> slouží akce <code>lock-for-ucetni</code> takto:</p>

<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;faktura-vydana action="lock-for-ucetni"&gt;
    &lt;id&gt;1&lt;/id&gt;
  &lt;/faktura-vydana&gt;
&lt;/winstrom&gt;</pre>


<p>K <b>odemknutí</b> záznamu slouží akce <code>lock</code> takto:</p>

<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;faktura-vydana action="unlock"&gt;
    &lt;id&gt;1&lt;/id&gt;
  &lt;/faktura-vydana&gt;
&lt;/winstrom&gt;</pre>


<p>Akce lze vyvolat i dávkově nad skupinou záznamů pomocí <a href="filters">filtru</a>:</p>
<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;faktura-vydana action="lock" filter="stavUhrK = 'stavUhr.uhrazeno' and typDokl = 'code:INTERNET'"&gt;
  &lt;/faktura-vydana&gt;
&lt;/winstrom&gt;</pre>



<#include "/i-devdoc-footer.ftl" />
</#escape>
