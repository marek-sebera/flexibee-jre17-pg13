<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Realizace objednávky"/>
<#include "/i-devdoc-header.ftl" />

<p>Přes REST API lze realizovat objednávku a to takto:</p>
<ul>
    <li>faktura vydaná  (<code>faktura-vydana</code>)</li>
    <li>faktura přijatá (<code>faktura-prijata</code>)</li>
    <li>skladový pohyb  (<code>skladovy-pohyb</code>)</li>
    <li>prodejka (<code>prodejka</code>)</li>
</ul>

<p>Lze určit typ dokladu (<code>typDokl</code>), datum vystavení (<code>datVyst</code>) a sklad (<code>sklad</code>). Pokud nejsou uvedeny, použije se výchozí typ dokladu z&nbsp;objednávky a aktuální datum.</p>

<p>Můžete také určit zda se mají při nedostatku zboží generovat požadavky na výdej (<code>&lt;generovatPozadavky&gt;true&lt;/generovatPozadavky&gt;</code>)</p>

<p>Objednávku lze realizovat na několikrát (např. dle dostupnosti zboží). Informace o stavu realizace je možné získat z&nbsp;položek objednávky.</p>

<p>Realizace objednávky byla navrhovaná především pro čtečky čárového kódu. Proto je jedno zda je zadáno výrobní číslo nebo zboží (pouze EAN). EAN se uvádí buď přímo a nebo s&nbsp;prefixem <code>ean:</code>. Doporučujeme používat tento prefix při zadání EANu.</p>

<p>Vytvářenému dokladu je možné přiřadit externí identifikátor, pomocí kterého je možné ho ještě v průběhu stejného importu modifikovat. V případě <strong>realizace do faktury</strong> vydané / přijaté je <strong>možné uvést již existující doklad</strong>, ke kterému budou přidány nově realizované položky. Pro ostatní typy realizací dochází vždy k vytvoření nového dokladu a uvedení existujícího dokladu způsobí chybu importu.</p>

<h2>Příklady XML</h2>
<p>Základní jednoduchá realizace dle identifikace zboží:</p>
<pre class="brush:xml">&lt;objednavka-prijata&gt;
  &lt;id&gt;code:OBP0004/2012&lt;/id&gt;
  &lt;realizaceObj type="faktura-vydana"&gt; 
    &lt;polozkyObchDokladu&gt;
      &lt;polozka&gt;
        &lt;cenikNeboVyrobniCislo&gt;123skl&lt;/cenikNeboVyrobniCislo&gt; 
        
        &lt;mj&gt;1&lt;/mj&gt;   
      &lt;/polozka&gt;
    &lt;/polozkyObchDokladu&gt;
  &lt;/realizaceObj&gt;
&lt;/objednavka-prijata&gt;</pre>

<p>Realizace dle identifikace řádku v objednávce:</p> 
<pre class="brush:xml">&lt;objednavka-prijata&gt;
  &lt;id&gt;code:OBP0004/2012&lt;/id&gt;
  &lt;realizaceObj type="faktura-vydana"&gt; 
    &lt;polozkyObchDokladu&gt;
      &lt;polozka&gt;
        &lt;cisRad&gt;1&lt;/cisRad&gt; &lt;!-- číslo řádky v&nbsp;objednávce --&gt;
        &lt;!-- lze také použít externí identifikátor řádku &lt;id&gt;extId...&lt;/id&gt; --&gt;
        &lt;mj&gt;1&lt;/mj&gt;
      &lt;/polozka&gt;
    &lt;/polozkyObchDokladu&gt;
  &lt;/realizaceObj&gt;
&lt;/objednavka-prijata&gt;</pre>

<p>Kompletní struktura XML:</p>
<pre class="brush:xml">&lt;objednavka-prijata&gt;
  &lt;id&gt;code:OBP0004/2012&lt;/id&gt;
  &lt;realizaceObj type="faktura-vydana"&gt; &lt;!-- faktura-vydana/prodejka/faktura-prijata/skladovy-pohyb--&gt;
    &lt;id&gt;ext:...&lt;/id&gt; &lt;!-- nepovinné externí id vytvořeného dokladu --&gt;
    &lt;typDokl&gt;...&lt;/typDokl&gt;
    &lt;sklad&gt;...&lt;/sklad&gt;
    &lt;generovatPozadavky&gt;true&lt;/generovatPozadavky&gt; &lt;!-- nepovinné --&gt;
    &lt;odpocetZaloh&gt;true&lt;/odpocetZaloh&gt; &lt;!-- u vydané faktury říká zda se mají odpočítat navázané zálohy --&gt;
    &lt;cisDosle&gt;...&lt;/cisDosle&gt; &lt;!-- u přijaté faktury povinné --&gt;
    &lt;datSplat&gt;2013-01-01&lt;/datSplat&gt; &lt;!-- u přijaté faktury povinné --&gt;
    &lt;polozkyObchDokladu&gt;
      &lt;polozka&gt;
        &lt;!-- nasleduji tri zpusoby identifikace polozky --&gt;
        &lt;!-- &lt;id&gt;extId...&lt;/id&gt; --&gt;
        &lt;!-- &lt;cisRad&gt;1&lt;/cisRad&gt; --&gt;
        &lt;cenikNeboVyrobniCislo&gt;123skl&lt;/cenikNeboVyrobniCislo&gt; &lt;!-- EAN ceníku nebo výrobní číslo --&gt;
        
        &lt;mj&gt;1&lt;/mj&gt; &lt;!-- pokud se najde podle EAN nebo výrobního čísla z předcházející řádky, je to vždy 1; nemusí se uvádět, pokud je uveden seznam výrobních čísel --&gt;
        &lt;vyrobniCisla&gt; &lt;!-- použito pokud se nenajde podle výrobního čisla - tj. u přijatých faktur je nutné zadat výrobní čísla vždy pokud jsou vyžadovány --&gt;
          &lt;vyrobniCislo&gt;123&lt;/vyrobniCislo&gt;
          &lt;vyrobniCislo&gt;456&lt;/vyrobniCislo&gt;
        &lt;/vyrobniCisla&gt;
      &lt;/polozka&gt;
    &lt;/polozkyObchDokladu&gt;
  &lt;/realizaceObj&gt;
&lt;/objednavka-prijata&gt;</pre>

<p>Ukázka použití JSON:</p>
<pre class="brush:js">{  
  "flexibee":{  
    "objednavka-prijata":{  
      "id":"code:OBP0004/2012",
      "realizaceObj@type":"faktura-vydana",
      "realizaceObj":{  
        "typDokl":"code:FAKTURA",
        "polozkyObchDokladu":[  
          {  
              "cisRad":1,
              "mj":"1",
              "vyrobniCisla": [ 123, 456 ]
          }
        ]
      }
    }
  }
}</pre>

<h2>Tvorba zálohy</h2>
<p>Dále lze z objednávky přijaté vytvořit zálohovou fakturu.</p>

<pre class="brush:xml">&lt;objednavka-prijata&gt;
  &lt;id&gt;code:OBP0004/2012&lt;/id&gt;
  &lt;tvorbaZalohy&gt; 
    &lt;id&gt;ext:...&lt;/id&gt; &lt;!-- nepovinné externí id vytvořeného dokladu --&gt;
    &lt;typDokl&gt;...&lt;/typDokl&gt;
    &lt;castka&gt;...&lt;/castka&gt;   &lt;!-- částka vytvářené zálohy --&gt;
    &lt;procent&gt;...&lt;/procent&gt; &lt;!-- alternativní zadání částky zálohy zadáním procent z celkové částky objednávky --&gt;
  &lt;/tvorbaZalohy&gt;
&lt;/objednavka-prijata&gt;</pre>

<#include "/i-devdoc-footer.ftl" />
</#escape>