<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="QR kód dokladu"/>
<#include "/i-devdoc-header.ftl" />

<h2>QR kód dokladu</h2>

<p>ABRA Flexi umí vygenerovat <a href="https://support.flexibee.eu/redirect/qrcode/">QR kód dle standardu schváleným Českou Bankovní Asociací</a> a kterou podporují mobilní klienti mnoha bank. Tento kód lze snímnout i z&nbsp;obrazovky pomocí mobilního zařízení.</p>

<p>Podporujeme QR kódy přijatých dokladů (pro vlastní platbu) tak i vydaných dokladů (platba klientem).</p>

<p>URL pro získání QR kódu:</p>
<pre>/c/<font style="color:green">&lt;identifikátor firmy&gt;</font>/<font style="color:red">&lt;evidence&gt;</font>/<font style="color:blue">&lt;ID záznamu&gt;</font>/<font style="color:red"><strong>qrcode.png?size=200</strong></font></pre>

<p>Velikost určuje rozměr obrázku od 1 do 1500 pixelů. Systém se snaží vytvořit takový okraj obrázku, aby na něm byly možné ostré přechody bílé a černé.</p>

<#include "/i-devdoc-footer.ftl" />
</#escape>
