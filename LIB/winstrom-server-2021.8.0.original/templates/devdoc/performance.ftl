<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Výkonnostní optimalizace"/>
<#include "/i-devdoc-header.ftl" />

<p>Snahou při implementaci ABRA Flexi Serveru bylo, aby byl co nejvýkonnější. Nicméně existuje několik faktorů, které mohou výkon ovlivňovat.</p>

<h2>První spuštění</h2>
<p>První požadavek, který ABRA Flexi obslouží, trvá delší dobu (až 20 sekund). Je totiž nutné, aby nastartovalo účetní jádro. Další požadavky jsou již výrazně rychlejší. Nicméně i tak je potřeba nechat ABRA Flexi trošku "zahřát", protože využívá technologii Just-In-Time překladače, který optimalizuje kód podle způsobu, jakým s&nbsp;ním komunikujete.</p>

<h2>Softwarové optimalizace</h2>
<p>Když komunikujete s&nbsp;ekonomickým systémem, je vhodné používat různé optimalizace:</p>

<ul>
    <li>Každý požadavek je autorizovaný. Proto rovnou posílejte autorizační údaje a nečekejte až ABRA Flexi vrátí informaci o nutnosti autorizovat a v&nbsp;dalším požadavku zašlete autorizační údaje.</li>
    <li>Když <a href="list">vypisujete</a> data, můžete určovat <a href="detail-levels">úroveň detailu</a>. Nejvyšší rychlost získáte pomocí <code>?detail=custom:...</code>. Uveďte pouze to co potřebujete..</li>
    <li>Pokud vypisujete velké množství dat a nepotřebujete externí identifikátory, vypňete je pomocí <code>?no-ext-ids=true</code>.</li>
    <li>Nepoužívejte výpis všech relací (<code>?relations=all</code>). Vždy si vypište pouze ty, které opravdu potřebujete.</li>
    <li>Pokud potřebujete vypsat např. všechny přílohy pro položky ceníku, použijte výpis relace příloh. Můžete tím získat vyšší rychlost.</li>
    <li>Zkontrolujte si, že nevoláte stejné URL vícekrát v&nbsp;rámci jednoho požadavku (je to překvapivý, ale velmi častý jev).</li>
    <li>Pokud potřebujete zjistit počet záznamů, použijte <code>?add-row-count=true</code> a nenačítejte všechny záznamy.</li>
    <li>Seskupujte podobné požadavky do jednoho. Pokud potřebujete načíst tři položky ceníku, použijte jeden požadavek a načtěte si všechny záznamy najednou.</li>
</ul>

<h2>Hardwarové optimalizace</h2>
<p>Pokud začínáte narážet na výkonnostní problémy a aplikace je již maximálně optimalizovaná, doporučujeme tyto kroky:</p>
<ul>
    <li><strong>Především kontaktujte naší technickou podporu, abychom Vám poradili, které kroky realizovat.</strong></li>
    <li>Mějte databázi a ABRA Flexi Server na jednom fyzickém stroji. Síťová komunikace s&nbsp;databází snižuje výkon.</li>
    <li>Zkontrolujte, že systém má dostatek paměti.</li>
    <li>Optimalizujte databázi PostgreSQL.</li>
    <li>Zvyšte výkon disků.</li>
    <li>Přidejte další procesory.</li>
</ul>

<#include "/i-devdoc-footer.ftl" />
</#escape>
