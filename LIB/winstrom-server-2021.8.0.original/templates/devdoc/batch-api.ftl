<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Dávkové operace"/>
<#include "/i-devdoc-header.ftl" />

<p>Pro partnerská řešení, která potřebují spravovat instance ABRA Flexi v cloudu, jsme připravili API, které to umožňuje. Jeho stav je zatím beta a tento dokument popisuje plánovaný stav. Vše si důkladně před použitím ověřte.</p>

<h2>Autentifikace požadavků vůči ABRA Flexi</h2>
<p>ABRA Flexi podporuje několik způsobů tzv. serverové autorizace:</p>
<ul>
    <li>serverovým jménem a heslem - při zadání speciálního jméne a hesla je akceptována serverová autorizace. Toto řešení není použitelné v cloudu.</li>
    <li>klientským certifikátem - umožňuje, abychom pro každého partnera nebo instanci měli certifikát, který umožňuje administraci. Toto řešení zatím není použitelné mimo cloud.</li>
</ul>


<h3>Serverové jméno a heslo</h3>
<p>Pro tento typ autorizace je nutné upravit <code>/etc/flexibee/server-auth.xml</code> a doplnit:
<pre class="brush: xml">&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;!DOCTYPE properties SYSTEM "http://java.sun.com/dtd/properties.dtd"&gt;
&lt;properties&gt;
    &lt;comment&gt;WinStrom server configuration&lt;/comment&gt;
    &lt;entry key="username"&gt;winstrom-server-admin&lt;/entry&gt;
    &lt;entry key="password"&gt;velmi-tajne-a-hodne-dlouhe-heslo&lt;/entry&gt;
&lt;/properties&gt;</pre>
<p>Po restartu serveru se již jméno a heslo akceptuje.</p>

<p>Poznámka: heslo musí být <strong>minimálně 15 znaků</strong> dlouhé. Toto heslo umožní přístup ke všem datům na dané instalaci. Bezpečně jej ukládejte!</p>

<h3>Klientským certifikátem</h3>
<p>Bezpečnějším způsobem je použití certifikátu. Lze se jím autorizovat pro získání administrátorského přístupu. Pro komunikaci je nutné použít protokol HTTPS a připojovat se na port 7000. Na serveru je uložen pouze otisk certifikátu (SHA1 fingerprint).</p>
<p>Otisk certifikátu lze získat takto:</p>
<code>openssl x509 -noout -in cert.pem -fingerprint</code>

<p>Tento otisk je nutné zaslat na podporu a bude nastaven do dané instance resp. stromu instancí (jejich seskupení).</p>

<h3>Impersonifikace uživatele</h3>
<p>V některých případech je nutné, aby se server přihlásil a následně se prohlásil za jednoho z uživatelů. To lze provést doplněním hlavičky:</p>
<code>X-FlexiBee-Authorization: jmeno</code>.
<p>Od té chvíle budou všechny změny včetně práv provedeny pod tímto uživatelem.</p>

<h2>Scénáře použití API</h2>
<p>Celé ABRA Flexi REST API vždy pracuje nad jednou databází s daty firmy. Vyjímkou je správa uživatelů a firem. Ta je z firmy vyjmuta a uložena jiným způsobem. Informace o uživatelích jsou u běžné instalace v databázi <code>centralServer</code>. Firmy jsou v jednotlivých databázích. V případě cloudového provozu jsou informace ve vysoce replikované databázi CouchDB. Pokud by se zpracování dělalo ve více požadavcích, je možné, že při přidělování práv uživatele do firmy nebude obsluhující server vědět, že je firma nebo uživatel již založen. Proto vzniklo toto dávkové API, kdy se předá seznam operací a vykoná je jeden server a tím je zajištěna konzistence.</p>
<p>Protože požadavek může selhat (ať už např. při dočasně nedostupnosti jedné z komponent), je možné požadavek zopakovat. Pak se provede vše tak jak je uvedeno znovu. Pokud již nějaká změna byla provedena, operace se přeskočí (operace jsou idempotentní).</p>

<h3>Založení účtu administrátora ABRA Flexi instance a výchozí účetní firmy</h3>
<p>Se založením ADMIN účtu v ABRA Flexi se založí výchozí účetní firma podle údajů z objednávky (budeme mít název firmy, IČO).</p>
<p>Není možné založit firmu bez administrátorského uživatele.</p>

<pre class="brush: xml">&lt;flexibee-batch id=&quot;abc-1&quot;&gt;
           &lt;user action=&quot;create-update&quot;&gt;
                 &lt;username&gt;admin&lt;/username&gt;
                 &lt;password hash=&quot;sha256&quot; salt=&rdquo;123&rdquo;&gt;f86vs6vds66&lt;/password&gt;
                 &lt;email&gt;admin@firma.cz&lt;/email&gt;
                 &lt;givenName&gt;Roman&lt;/givenName&gt;
                 &lt;familyName&gt;Skamene&lt;/familyName&gt;
                 &lt;ssoIdentifier&gt;admin@firma.cz&lt;/ssoIdentifier&gt;
                 &lt;defaultRole&gt;ADMIN&lt;/defaultRole&gt;
                 &lt;permissions&gt;
                         &lt;manageAll&gt;true&lt;/manageAll&gt;
                         &lt;createCompany&gt;false&lt;/createCompany&gt;
                         &lt;deleteCompany&gt;false&lt;/deleteCompany&gt;
                         &lt;createUser&gt;false&lt;/createUser&gt;
                         &lt;changePassword&gt;false&lt;/changePassword&gt;
                         &lt;grantPermission&gt;false&lt;/grantPermission&gt;
                         &lt;licenseManagement&gt;false&lt;/licenseManagement&gt;
                 &lt;/permissions&gt;
            &lt;/user&gt;
            &lt;company action=&quot;create-update&quot;&gt;
                 &lt;id&gt;digitalni_media_s_r_o_&lt;/id&gt;
                 &lt;name&gt;Digitalní media s.r.o.&lt;/name&gt;
                 &lt;country&gt;CZ&lt;/country&gt;
                 &lt;regNo&gt;966664322&lt;/regNo&gt;&lt;!-- IČO --&gt;
                 &lt;type&gt;PODNIKATELE&lt;/type&gt;
                 &lt;adminUser role=&quot;ADMIN&quot;&gt;admin&lt;/adminUser&gt;
         &lt;/company&gt;
 &lt;/flexibee-batch&gt;</pre>

<h4>Standardní uživatelské role ve ABRA Flexi</h4>
<ul>
    <li>ADMIN</li>
    <li>SUPERUZIVATEL</li>
    <li>MZDOVYUCETNI</li>
    <li>UCETNI</li>
    <li>OBCHODNIK</li>
    <li>SKLADNIK</li>
    <li>SKLADNIKSPOKLADNOU</li>
    <li>UZIVATEL</li>
    <li>JENCIST</li>
    <li>ZABLOKOVAN</li>
</ul>

<h4>Typ organizace + evidence</h4>
<p>Podvojné účetnictví je výchozí pro všechny typy organizací.</p>
<ul><li>PODNIKATELE</li></ul>
<ul><li>PODNIKATELE+PU (podvojné účetnictví)</li><li>PODNIKATELE+DE (daňová evidence)</li></ul><ul ><li >ROZPOCTOVE</li><li >NEZISKOVE</li><li >PODNIKATELIA (pouze pro Slovensko)</li></ul>
<h4>Podporované hash funkce pro ukládání hesel</h4>
<ul>
    <li>sha256 (výchozí pro ukládání hesel poslaných v plain textu)</li>
    <li>sha512</li>
    <li>sha1</li>
    <li>md5</li>
    <li>pbkdf2 (nejbezpečnější, ale také nejpomalejší metoda - bez používání session u REST API je pak nepoužitelná - je záměrně příliš <a href="http://en.wikipedia.org/wiki/PBKDF2">pomalá</a>).</li>
</ul>

<p>Ukázka programu pro výpočet otisku hesla pro ABRA Flexi.</p>
<pre class="brush: java">import org.apache.commons.codec.binary.Hex;
    public static final String SHA256 = &quot;sha256&quot;;
    public static final String SEPARATOR = &quot;:&quot;;

    protected static String encryptPasswordSHA256(String plain, String salt) {
        String toDigest = salt + SEPARATOR + plain;
        try {
            MessageDigest md = MessageDigest.getInstance(&quot;SHA-256&quot;);
            byte[] result = md.digest(toDigest.getBytes(&quot;utf-8&quot;));

            return SHA256 + SEPARATOR + salt + SEPARATOR + new String(Hex.encodeHex(result));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        } catch (GeneralSecurityException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getRandomSalt() {
        byte[] b = new byte[10];
        new Random().nextBytes(b);
        return new String(Hex.encodeHex(b)).substring(10);
    }

   String plain = &quot;moje heslo&quot;;
   String result = encryptPasswordSHA256(plain, getRandomSalt());
</pre>


<h3>Založení dalších &bdquo;běžných&ldquo; uživatelů</h3>
<p>Tito uživatelé se budou zakládat již z Admin portálu jako noví uživatelé správcem tenanta. Bude voláno i hromadně pro více uživatelů při změně profilu na Admin portálu.</p>

<pre class="brush: xml">&lt;flexibee-batch id=&quot;abc-12&quot;&gt;
        &lt;user action=&quot;create-update&quot;&gt;
                    &lt;username&gt;anna.mlada&lt;/username&gt;
                    &lt;password hash=&quot;sha256&quot; salt=&quot;123&quot;&gt;f86vs6vds66&lt;/password&gt;
                    &lt;email&gt;anna.mlada@firma.cz&lt;/email&gt;
                    &lt;givenName&gt;Anna&lt;/givenName&gt;
                    &lt;familyName&gt;Mladá&lt;/familyName&gt;
                    &lt;defaultRole&gt;UZIVATEL&lt;/defaultRole&gt;
                    &lt;permissions&gt;
                        &lt;manageAll&gt;true&lt;/manageAll&gt;
                    &lt;/permissions&gt;
        &lt;/user&gt;
&lt;/flexibee-batch&gt;</pre>

<p>Lze uložit i heslo bez saltu, ale důrazně to nedoporučujeme!</p>

<h3>Změna hesla existujícího uživatele</h3>
<pre class="brush: xml">&lt;flexibee-batch id=&quot;abc-123&quot;&gt;
        &lt;user action=&quot;create-update&quot;&gt;
                    &lt;username&gt;anna.mlada&lt;/username&gt;
                    &lt;password hash=&quot;sha256&quot; salt=&quot;123&quot;&gt;f86vs6vds66&lt;/password&gt;
        &lt;/user&gt;
&lt;/flexibee-batch&gt;</pre>

<h3>Smazání existujícího uživatele</h3>
<p>Smazání uživatele z Admin portálu =  uvolnění licence. Bude voláno i hromadně pro více uživatelů při změně profilu.</p>

<pre class="brush: xml">&lt;flexibee-batch id=&quot;abc-12345&quot;&gt;
        &lt;user action=&quot;delete&quot;&gt;
                    &lt;username&gt;anna.mlada&lt;/username&gt;
        &lt;/user&gt;
&lt;/flexibee-batch&gt;</pre>

<p>Poznámka: pokud byl uživatel smazán a později nastavím create-update, měl by se znovu obnovit.</p>

<h4>Obnovení uživatele</h4>
<pre class="brush: xml">&lt;flexibee-batch id=&quot;abc-123456&quot;&gt;
        &lt;user action=&quot;create-update&quot;&gt;
                   &lt;username&gt;anna.mlada&lt;/username&gt;
        &lt;/user&gt;
&lt;/flexibee-batch&gt;</pre>

<p>Poznámka: při create-update uživatele se nejprve zkouší recyklovat záznamy s příznakem &ldquo;vymazáno&rdquo;. Veškeré nastavení přístupů a rolí zůstane zachováno.</p>

<h3>Blokace existujícího uživatele</h3>
<p>Zablokování uživatele z Admin portálu = nemůže se přihlásit.</p>

<pre class="brush: xml">&lt;flexibee-batch id=&quot;abc-123456&quot;&gt;
        &lt;user action=&quot;create-update&quot;&gt;
                   &lt;username&gt;anna.mlada&lt;/username&gt;
                   &lt;blocked message=&quot;Blocked from external system&quot;&gt;true&lt;/blocked&gt;
        &lt;/user&gt;
&lt;/flexibee-batch&gt;</pre>

<h4>Odblokování uživatele</h4>
<pre class="brush: xml">&lt;flexibee-batch id=&quot;abc-123456&quot;&gt;
        &lt;user action=&quot;create-update&quot;&gt;
                 &lt;username&gt;anna.mlada&lt;/username&gt;
                 &lt;blocked&gt;false&lt;/blocked&gt;
        &lt;/user&gt;
&lt;/flexibee-batch&gt;</pre>

<h3>Přidání role ADMIN existujícímu uživateli</h3>
<p>Přidání atomické role &bdquo;ERP administrátor&ldquo; na Admin portálu. Může být voláno i hromadně pro více uživatelů při změně profilu na Admin portálu.</p>

<pre class="brush: xml">&lt;flexibee-batch id=&quot;abc-1234567&quot;&gt;
       &lt;user action=&quot;create-update&quot;&gt;
                &lt;username&gt;anna.mlada&lt;/username&gt;
                &lt;defaultRole&gt;ADMIN&lt;/defaultRole&gt;
       &lt;/user&gt;
       &lt;access role=&quot;ADMIN&quot;&gt;&lt;/access&gt;
&lt;/flexibee-batch&gt;</pre>

<h3>Odebrání role ADMIN existujícímu uživateli</h3>
<p>Odebrání atomické role &bdquo;ERP administrátor&ldquo; na Admin portálu. Může voláno i hromadně pro více uživatelů při změně profilu na Admin portálu.</p>

<pre class="brush: xml">&lt;flexibee-batch id=&quot;abc-12345678&quot;&gt;
       &lt;user action=&quot;create-update&quot;&gt;
                   &lt;username&gt;anna.mlada&lt;/username&gt;
                   &lt;defaultRole&gt;UZIVATEL&lt;/defaultRole&gt;
       &lt;/user&gt;
       &lt;access role=&quot;UZIVATEL&quot;&gt;&lt;/access&gt;
&lt;/flexibee-batch&gt;</pre>

<h3>Změna jména, příjmení existujícího uživatele</h3>
<pre class="brush: xml">&lt;flexibee-batch id=&quot;abc-123456789&quot;&gt;
        &lt;user action=&quot;create-update&quot;&gt;
                    &lt;username&gt;anna.mlada&lt;/username&gt;
                    &lt;givenName&gt;Anička&lt;/givenName&gt;
                    &lt;familyName&gt;Starší&lt;/familyName&gt;
        &lt;/user&gt;
&lt;/flexibee-batch&gt;</pre>


<h3>Skrytí / zobrazení firmy (odpojení)</h3>
<pre class="brush: xml">&lt;flexibee-batch id=&quot;abc-123&quot;&gt;
    &lt;company action=&quot;create-update&quot;&gt;
        &lt;id&gt;digitalni_media_s_r_o_&lt;/id&gt;
        &lt;show&gt;false&lt;/show&gt;&lt;!-- true pro zobrazení --&gt;
    &lt;/company&gt;
&lt;/flexibee-batch&gt;</pre>


<h3>Smazání firmy</h3>
<pre class="brush: xml">&lt;flexibee-batch id=&quot;abc-123&quot;&gt;
    &lt;company action=&quot;delete&quot;&gt;
        &lt;id&gt;digitalni_media_s_r_o_&lt;/id&gt;
        &lt;/company&gt;
&lt;/flexibee-batch&gt;</pre>



<h2>Použití API</h2>
<p>Platné pro první zvěřejněnou testovací verzi s funkčním zakládáním uživatelů v centrální databázi.</p>

<h3>Volání</h3>
<p>HTTP PUT na URL = <a  href="https://server:7000/admin/batch">http://server:7000/admin/batch</a></p>
<p>Požadavek musí být autorizován.</p>
<p>XML s požadavkem lze odeslat pomocí curl takto:</p>
<p><code>curl -3 'https://server:7000/admin/batch' -T batch.xml -E cert.pem</code></p>


<h3>Ukázkový požadavek</h3>
<p>batch.xml</p>
<pre class="brush: xml">&lt;flexibee-batch id=&quot;abc-123&quot;&gt;&lt;!-- ID by mělo unikátně označovat dávku --&gt;
    &lt;user&gt;
        &lt;username&gt;anna.mlada&lt;/username&gt;
        &lt;password hash=&quot;sha256&quot; salt=&quot;xyz987&quot;&gt;af123bd35&lt;/password&gt;
        &lt;email&gt;anna.mlada@firma.cz&lt;/email&gt;
        &lt;givenName&gt;Anna&lt;/givenName&gt;
        &lt;familyName&gt;Mladá&lt;/familyName&gt;
        &lt;permissions&gt;
            &lt;manageAll&gt;true&lt;/manageAll&gt;
            &lt;createCompany&gt;true&lt;/createCompany&gt;
            &lt;deleteCompany&gt;true&lt;/deleteCompany&gt;
            &lt;createUser&gt;false&lt;/createUser&gt;
            &lt;changePassword&gt;false&lt;/changePassword&gt;
            &lt;grantPermission&gt;true&lt;/grantPermission&gt;
            &lt;licenseManagement&gt;false&lt;/licenseManagement&gt;
        &lt;/permissions&gt;
        &lt;defaultRole&gt;UZIVATEL&lt;/defaultRole&gt;
    &lt;/user&gt;

    &lt;company action=&quot;create-update&quot;&gt;&lt;!-- action=&quot;create-update&quot; je výchozí akce --&gt;
        &lt;id&gt;moje_firma_s_r_o_&lt;/id&gt;
        &lt;name&gt;Moje Firma s.r.o.&lt;/name&gt;
        &lt;country&gt;CZ&lt;/country&gt;&lt;!-- aktuálně podporované jsou CZ a SK --&gt;
        &lt;regNo&gt;123&lt;/regNo&gt;&lt;!-- IČO --&gt;
        &lt;vatId&gt;CZ123&lt;/vatId&gt;&lt;!-- DIČ --&gt;
        &lt;type&gt;PODNIKATELE&lt;/type&gt;
        &lt;adminUser&gt;anna.mlada&lt;/adminUser&gt;
    &lt;/company&gt;

    &lt;company action=&quot;delete&quot;&gt;
        &lt;id&gt;demo_a_s_&lt;/id&gt;
    &lt;/company&gt;

    &lt;access role=&quot;ADMIN&quot;&gt;anna_mlada_s_r_o_&lt;/access&gt;
    &lt;access role=&quot;UZIVATEL&quot;&gt;moje_firma_s_r_o_&lt;/access&gt;
    &lt;access&gt;demo_a_s_&lt;/access&gt;
    &lt;access role=&quot;ADMIN&quot;/&gt;
    &lt;access&gt;demo_a_s_&lt;/access&gt;
&lt;/flexibee-batch&gt;</pre>


<h3>Odpověď</h3>
<p>Při úspěchu (HTTP 200) je odesláno XML v tomto formátu:</p>
<pre class="brush: xml">&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;
&lt;flexibee-batch-result id=&quot;abc-12345678&quot;&gt;
           &lt;entry&gt;
                 &lt;id&gt;moje_firma_s_r_o_&lt;/id&gt;
                 &lt;entity&gt;COMPANY&lt;/entity&gt;
                 &lt;action&gt;DELETE&lt;/action&gt;
                 &lt;result&gt;
                        &lt;status&gt;SKIPPED&lt;/status&gt;
                        &lt;message&gt;Not implemented yet.&lt;/message&gt;
                 &lt;/result&gt;
           &lt;/entry&gt;
           &lt;entry&gt;
                 &lt;id&gt;anna.mlada@firma.cz&lt;/id&gt;
                 &lt;entity&gt;USER&lt;/entity&gt;
                 &lt;action&gt;CREATE_UPDATE&lt;/action&gt;
                 &lt;result&gt;
                        &lt;status&gt;CREATED&lt;/status&gt;
                 &lt;/result&gt;
           &lt;/entry&gt;
&lt;/flexibee-batch-result&gt;</pre>

<p>Výčet typu entit</p>
<ul>
<li>USER</li>
<li>COMPANY</li>
<li>ACCESS_LIST</li>
</ul>

<p>Výčet typu akcí</p>
<ul>
    <li>CREATE_UPDATE</li>
    <li>DELETE</li>
</ul>

<p>Výčet stavových kódů</p>
<ul>
<li>CREATED</li>
<li>UPDATED</li>
<li>DELETED</li>
<li>FAILED (popis důvodu chyby bude v elementu &lt;message&gt;)</li>
<li>SKIPPED (může být doplněno elementem &lt;message&gt;)</li>
</ul>

<#include "/i-devdoc-footer.ftl" />
</#escape>
