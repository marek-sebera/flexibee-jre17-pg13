<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Přepočet stavů účtů"/>
<#include "/i-devdoc-header.ftl" />

<h3>Způsob volání</h3>

<p>Lze využít <a href="http-operations">HTTP metodu</a>: <code>PUT</code> nebo <code>POST</code>.</p>

<p>Služba je dostupná na adrese: <code>/c/{firma}/stav-uctu/prepocet</code>, kde <i>{firma}</i> je databázový idenfitikátor firmy.</p>

<p>Jsou podporovány <a href="format-types">výstupní formáty</a>: <code>XML</code> nebo <code>JSON</code>.</p>


<h3>Parametry</h3>

Služba očekává povinně parametr <code>ucetniObdobi</code>, aby bylo možno identifikovat časové období, za které chcete účty přepočítat.


<h3>Výsledek</h3>

<p>Pro rozpoznání, zda byla služba vykonána úspěšně, lze kontrolovat HTTP status odpovědi nebo vlastnost <code>success</code> v získaném dokumentu.</p>

<p>V případě úspěšného vykonání služby je vracen HTTP status 200 a dokument odpovídající standardnímu formátu, viz. <a href="https://www.flexibee.eu/navratove-hodnoty/">návratové hodnoty</a>.</p>

<p>V případě neúspěchu je vracen status 4xx/5xx a zpráva o důvodu neúspěchu.</p>

<h3>Ukázky volání</h3>

<ul>
    <li><code>PUT /c/demo/stav-uctu/prepocet.xml?ucetniObdobi=code:2020</code></li>
    <li><code>PUT /c/demo/stav-uctu/prepocet.json?ucetniObdobi=code:2020</code></li>
    <li><code>PUT /c/demo/stav-uctu/prepocet?ucetniObdobi=code:2020</code> (s hlavičkou <code>Accept: application/xml</code> nebo <code>Accept: application/json</code>)</li>
</ul>

<#include "/i-devdoc-footer.ftl" />
</#escape>
