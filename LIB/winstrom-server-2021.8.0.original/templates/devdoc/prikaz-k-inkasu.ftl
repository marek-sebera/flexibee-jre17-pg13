<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Příkaz k inkasu"/>
<#include "/i-devdoc-header.ftl" />

<p>Příkaz k inkasu lze vytvářet přes REST API jednoduchým voláním metodou <code>PUT</code> nebo <code>POST</code>:</p>

<table class="table table-striped table-condensed">
    <tr>
        <th><code>/c/{firma}/bankovni-ucet/{id}/prikaz-k-inkasu</code></th>
        <td>Pro zadaný bankovní účet vytvoří příkaz k inkasu ze všech dosud neuhrazených dokladů.</td>
    </tr>
    <tr>
        <td><code>/c/{firma}/bankovni-ucet/{id}/prikaz-k-inkasu?<b>splatnost</b>=2014-10-14</b>&amp;<b>datVystOd</b>=2014-09-01&amp;<b>datVystDo</b>=2014-09-31</code></td>
        <td>Pro zadaný bankovní účet vytvoří příkaz k inkasu se zadanou splatností ze všech dosud neuhrazených dokladů v uvedeném období.</td>
    </tr>
</table>

<h3>Vstupy</h3>

<p>Zpracování poadavku se řídí těmito parametry:</p>

<table class="table table-striped table-condensed">
    <tr><th>parametr</th><th>typ</th><th>popis</th><th>výchozí hodnota</th></tr>
    <tr>
        <th><code>splatnost</code></th>
        <td>datum <code>yyyy-mm-dd</code></td>
        <td>datum splatnosti příkazu</td>
        <td>následující všední den</td>
    </tr>
    <tr>
        <th><code>datVystOd</code></th>
        <td>datum <code>yyyy-mm-dd</code></td>
        <td>datum vystavení nejstaršího hrazeného dokladu</td>
        <td>neomezeno</td>
    </tr>
    <tr>
        <th><code>datVystDo</code></th>
        <td>datum <code>yyyy-mm-dd</code></td>
        <td>datum vystavení nejmladšího hrazeného dokladu</td>
        <td>aktuální datum</td>
    </tr>
</table>

<p>Do vytvářeného příkazu k inkasu jsou zahrnuty doklady splňující tyto podmínky:</p>
<ol>
    <li>doklad je typu vydaná faktura nebo pohledávka s kladnou (nenulovou) hodnotou,</li>
    <li>doklad je typu přijatá faktura nebo závazek se zápornou (nenulovou) hodnotou,</li>
    <li>nejedná se o zálohový daňový doklad,</li>
    <li>forma úhrady dokladu je "převodem",</li>
    <li>doklad má vyplněné bankovní spojení (číslo účtu),</li>
    <li>měna dokladu je shodná s měnou účtu,</li>
    <li>doklad ještě není zcela uhrazen (může být částečně),</li>
    <li>doklad se dosud nevyskytuje na příkazu k inkasu,</li>
    <li>na dokladu není nastaven zákaz proplacení (faktura přijatá),</li>
    <li>doklad je schválen k platbě, je-li vyžadován podpis před vystavením příkazu k úhradě,</li>
    <li>datum vystavení dokladu vyhovuje zadaným kritériím (viz parametry <code>datVystOd</code> a <code>datVystDo</code>).</li>
</ol>
</p>

<h3>Výstupy</h3>

<p>Úspěšné zpracování požadavku může vracet tyto stavy:</p>

<table class="table table-striped table-condensed">
    <tr><th>200</th><td>Nebyly nalezený žádné vhodné doklady pro vytvoření příkazu.</td></tr>
    <tr><th>201</th><td>Příkaz k inkasu byl vytvořen, URL je uvedeno v hlavičce <i>Location</i>.</td></tr>
</table>

<#include "/i-devdoc-footer.ftl" />
</#escape>