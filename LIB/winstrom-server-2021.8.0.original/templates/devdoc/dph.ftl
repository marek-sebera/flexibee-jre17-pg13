<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Výpočet Daně z přidané hodnoty (DPH)"/>
<#include "/i-devdoc-header.ftl" />

<p>DPH je jednou z věcí, kterou ABRA Flexi dokáže vypočítat sám. Nicméně existuje několik metod výpočtu (tzv. shora a zdola) a také jej ovlivňuje zaokrouhlování. Proto má ABRA Flexi několik metod pro ovlivňování DPH.</p>

<p>Vždy platí, že DPH u položkového dokladu je součtem DPH jednotlivých položek a na výsledek je aplikováno zaokrouhlení.</p>

<p>To zda se má DPH výpočítat shora nebo zdola se řídí tím, které parametry uvedete (např. základ &rarr; dojde k výpočtu DPH a výsledku). Můžete také uvést všechny parametry a ABRA Flexi uvede jejich kontrolu.</p>

<h2>Kontrolní výpočty</h2>

<p>ABRA Flexi při výpočtech kontroluje vzájemné vazby mezi položkami. Musí platit tato pravidla (mohou se lišit o zaokrouhlování):</p>
<ul>
<li><code>sumDphCelkem = sumDphSniz + sumDphZakl</code></li>
<li><code>sumCelkem = sumOsv + sumZklSniz * sazbaDphSniz + sumZklZakl * sumDphZakl</code></li>
<li><code>sumCelkZakl = sumZklZakl * sumDphZakl</code></li>
<li><code>sumCelkSniz = sumZklSniz * sumDphSniz</code></li>
<li><code>sumZklCelkem = sumOsv + sumZklSniz + sumZklZakl</code></li>
</ul>

<p>Pokud tedy ABRA Flexi vypíše chybu: <code>Zadaná hodnota [9999.99] vlastnosti [sumCelkem] se liší od vypočtené hodnoty [9999.99]</code> znamená to, že jste tyto kontrolní mechanizmy nedodržely. Obvykle se jedná o chybný výpočet DPH a nebo chybu vstupních dat.</p>

<p>Obvykle stačí uvádět pouze základy a nechat vše ostatní ABRA Flexi dopočítat.</p>

<p>To samé pak platí o měně, kde navíc musí odpovídat i hodnoty mezi základní měnou a měnou. ABRA Flexi v těchto případech lehce upraví kurz, aby se zamezilo zaokrouhlovací chybě.</p>

<ul>
<li><code>sumDphCelkemMen = sumDphSnizMen + sumDphZaklMen</code></li>
<li><code>sumCelkemMen = sumOsvMen + sumZklSnizMen * sazbaDphSniz + sumZklZaklMen * sumDphZakl</code></li>
<li><code>sumCelkZaklMen = sumZklZaklMen * sumDphZakl</code></li>
<li><code>sumCelkSnizMen = sumZklSnizMen * sumDphSniz</code></li>
<li><code>sumZklCelkemMen = sumOsvMen + sumZklSnizMen + sumZklZaklMen</code></li>
</ul>

<#include "/i-devdoc-footer.ftl" />
</#escape>
