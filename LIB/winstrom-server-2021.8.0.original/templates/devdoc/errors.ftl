<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Obsluha chyb"/>
<#include "/i-devdoc-header.ftl" />

<p>O chybách je uživatel informován pomocí stavového kódu HTTP.</p>

<table class="table table-striped table-condensed">
    <thead>
        <th>HTTP kód</th>
        <th>Význam</th>
    </thead>
    <tbody>
    <tr>
        <td>200 OK</td>
        <td>Operace byla provedena úspěšně.</td>
    </tr>
    <tr>
        <td>201 Created</td>
        <td>Záznam byl úspěšně vytvořen. V hlavičce <code>Location</code> je URL nově vytvořeného záznamu a v&nbsp;těle odpovědi je i identifikátor.</td>
    </tr>
    <tr>
        <td>304 Not Modified</td>
        <td>Záznam nebyl modifikován (v&nbsp;kombinaci s&nbsp;hlavičkou <code>If-Modified-Since</code>).</td>
    </tr>
    <tr>
        <td>400 Bad Request</td>
        <td>Špatný požadavek. Obvykle nastává při operaci PUT, která selhala (např. odkazujete na objekt, který neexistuje).</td>
    </tr>
    <tr>
        <td>401 Unauthorized</td>
        <td>Uživatel se musí pro provedení dané operace přihlásit.</td>
    </tr>
    <tr>
        <td>402 Payment Required</td>
        <td>Cílový systém nemá aktivované REST API pro zápis. U čtecích operací se vrací 404.</td>
    </tr>
    <tr>
        <td>403 Forbidden</td>
        <td>Uživatel na tuto operaci nemá oprávnění. Tato chyba se zobrazí i v&nbsp;případě, že danou operaci neumožňuje licence.</td>
    </tr>
    <tr>
        <td>404 Not Found</td>
        <td>Záznam nenalezen. Může se jednat o evidenci nebo konkrétní záznam (např. proto, že byl smazán).</td>
    </tr>
    <tr>
        <td>406 Not Acceptable</td>
        <td>Cílový formát není nad konkrétním zdrojem podporovaný (např. export adresáře jako ISDOC).</td>
    </tr>
    <tr>
        <td>409 Conflict</td>
        <td>Probíhá stejná (nebo konfliktní) operace na serveru. Přepočet skladu, Aktualizace požadavků na výdej a další. Vyčkejte, než skončí běžící proces.</td>
    </tr>
    <tr>
        <td>500 Internal Server Error</td>
        <td>Při zpracování požadavku nastala vnitřní chyba serveru. Toto by za normálních okolností nemělo nastat a vždy se jedná o chybu v&nbsp;kódu ABRA Flexi. Informujte nás.</td>
    </tr>
    <tr>
        <td>503 Maintenance</td>
        <td>Na serveru probíhá údržba. Vyčkejte prosím. Pokud přetrvává, kontaktujte nás.</td>
    </tr>
    </tbody>
</table>

<p>Součástí odpovědi, pokud nejde o&nbsp;chybu 500, je obvykle i popis ve strojově čitelné podobě (XML, JSON). Formát je shodný s&nbsp;<a href="validations">validačními zprávami</a>.</p>

<p>Ukázka chybové odpovědi:</p>

<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;success&gt;false&lt;/success&gt;
  &lt;result&gt;
    &lt;id&gt;105&lt;/id&gt;
    &lt;error&gt;Při ukládání došlo k neznámé chybě.&lt;/error&gt;
  &lt;/result&gt;
&lt;/winstrom&gt;
</pre>


<#include "/i-devdoc-footer.ftl" />
</#escape>
