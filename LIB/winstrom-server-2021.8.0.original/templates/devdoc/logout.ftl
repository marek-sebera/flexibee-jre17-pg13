<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Odhlašování uživatelů"/>
<#include "/i-devdoc-header.ftl" />

<p>Po úspěšném přihlášení uživatele do ABRA Flexi vzniká <em>autentizační sezení</em>. Následně po výběru firmy si systém zakládá ke každému uživateli <em>kontextové informace</em> o připojení k dané firmě. Kontext obsahuje užitečná data, která se nemění příliš často. Například základní měnu, nastavení firmy nebo oprávnění uživatele. Případná změna těchto údajů ovlivní pouze nové přihlášení uživatele. Potřebujete-li zajistit propagaci změn i k dlouhodobě přihlášeným uživatelům, nezbývá než vynutit ukončení jejich sezení. K tomu lze využít službu na adrese:</p>

<pre><strong>POST</strong> /status/user/<font style="color:red">&lt;uživatelské jméno&gt;</font>/logout</pre>
<p>Pozn.: Stejná metoda je využívána i pro <abbr title="webové rozhraní">WUI</abbr>, proto v případě API <a href="format-types">požadujte formát</a> <code>JSON</code> nebo <code>XML</code>, čímž zabráníte finálnímu přesměrování.</p>

<p><strong>Aktuálně je možné tímto způsobem ukončovat sezení uživatelů přihlášených prostřednictvím webového rozhraní nebo API sezení vytvořená <a href="login#json">JSON autentizací</a>.</strong> Pro API požadavky využívající HTTP autentizaci sezení nevzniká.</p>

<p>Požadavek lze poslat metodou <code>POST</code>, přičemž existují 2 možnosti použití:</p>
<ol>
    <li>Odhlášení uživatele ze všech firem + zrušení všech <em>autentizačních sezení</em></li>
    <li>Odhlášení uživatele z konkrétní firmy (všechna <em>autentizačních sezení</em> zůstávají v platnosti)
        <ul><li>k URL doplňte parametr: <code>company=<font style="color:green">&lt;databázové jméno firmy&gt;</font></code></li></ul>
    </li>
</ol>

<p>Odhlášení uživatele z firmy provede pouze zahození <em>kontextových informací</em>. K obnovení kontextu a jeho opětovnému nakešování dojde automaticky s příštím požadavkem uživatele.</p>

<p>Úspěšné volání vrací HTTP status 200 (API volání), případně přesměrování (HTTP status 303) na URL <code>/status/user</code> (HTML volání).</p>

<p>Pokud nechcete dostávat HTTP chybu 404 při odhlašování uživatele, který není přihlášen, tak připojte volitelný parametr <code>?ignore-not-found=true</code>. Chybový status je vracen i v případě, že došlo k zahození <em>kontextových informací</em>, ale neexistovalo autentizační sezení (API volání s HTTP autentizací).</p>

<p>Využití služby je umožněno uživateli s právem přístupu do všech firem a současně právem spravovat licenci. Případně lze využít <a href="server-auth">serverovou autorizaci</a> nebo také může odhlásit uživatel sám sebe.</p>

<h3>Příklady</h3>

<p>Odhlášení uživatele <code>novak</code> ze všech firem a ukončení platnosti autentizace:</p>
<pre><strong>POST</strong> /status/user/<font style="color:red">novak</font>/logout</pre>

<p>Odhlášení uživatele <code>franta</code> z firmy <code>nejlepsi_firma_a_s_</code> (bez zrušení autentizace):</p>
<pre><strong>POST</strong> /status/user/<font style="color:red">franta</font>/logout?company=<font style="color:green">nejlepsi_firma_a_s_</font></pre>

<#include "/i-devdoc-footer.ftl" />
</#escape>
