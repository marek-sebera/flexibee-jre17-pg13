<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Rozvaha zisku a ztráty v XML"/>
<#include "/i-devdoc-header.ftl" />

<h3>Způsob volání</h3>

<p>Dostupná <a href="http-operations">HTTP metoda</a>: <code>GET</code>.</p>

<p>Služba je dostupná na adrese: <code>/c/{firma}/rozvaha-vykaz.xml</code>, kde <i>{firma}</i> je databázový idenfitikátor firmy.</p>

<p>Jsou podporovány <a href="format-types">výstupní formáty</a>: <code>XML</code></p>


<h3>Parametry</h3>

Služba vyžaduje 3 povinné parametry - <code>druh</code>, <code>rozsah</code> a <code>typ</code> (účetní jednotky).
<p>Možné hodnoty těchto parametrů:</p>

<code>druh</code>:
<ul>
    <li>B - řádné</li>
    <li>O - řádné - opravné</li>
    <li>D - dodatečné</li>
    <li>E - dodatečné - opravné</li>
</ul>

<code>rozsah</code>:
<ul>
    <li>P - plný</li>
    <li>Z - zjednodušený</li>
</ul>

<code>typ</code>:
<ul>
    <li>M - mikro</li>
    <li>L - malá</li>
    <li>S - střední</li>
    <li>V - velká</li>
</ul>

<h3>Výsledek</h3>

Příklad chybného volání:

<code>GET https://demo.flexibee.eu/c/demo/rozvaha-vykaz.xml?druh=B&rozsah=P&typ=M</code>

<pre class="brush: xml">
&lt;?xml version="1.0" ?&gt;
&lt;winstrom version="1.0"&gt;
  &lt;success&gt;false&lt;/success&gt;
  &lt;message&gt;Pro vygenerování Rozvaha a výkaz zisku a ztráty v XML musí být vypočtené sestavy Rozvaha v plném rozsahu od r. 2018 a Výkaz zisku a ztráty - druhové členění od r.2016.&lt;/message&gt;
&lt;/winstrom&gt;
</pre>

Příklad úspěšného volání:

<code>GET https://demo.flexibee.eu/c/demo/rozvaha-vykaz.xml?druh=B&rozsah=P&typ=M</code>

<pre class="brush: xml">
&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 &lt;Pisemnost nazevSW=&quot;ABRA Flexi&quot; verzeSW=&quot;2020.3.0.3-SNAPSHOT-20201118-r29393&quot;&gt;
  &lt;DPPDP8 verzePis=&quot;05.01.01&quot;&gt;
   &lt;VetaD c_nace=&quot;582000&quot; c_ufo_cil=&quot;451&quot; dapdpp_forma=&quot;B&quot; dokument=&quot;DP8&quot; k_uladis=&quot;DPP&quot; kat_uj=&quot;M&quot; typ_dapdpp=&quot;A&quot; typ_popldpp=&quot;1&quot; typ_zo=&quot;A&quot; uv_vyhl=&quot;500&quot; zdobd_do=&quot;30.06.2019&quot; zdobd_od=&quot;01.07.2018&quot;/&gt;
   &lt;VetaP c_orient=&quot;7b&quot; c_pop=&quot;1422&quot; c_pracufo=&quot;2005&quot; c_telef=&quot;371124340&quot; dic=&quot;28019920&quot; naz_obce=&quot;Praha 15&quot; opr_jmeno=&quot;Stanislav&quot; opr_postaveni=&quot;Jednatel&quot; opr_prijmeni=&quot;Kurinec&quot; psc=&quot;15500&quot; ulice=&quot;Jeremi&aacute;&scaron;ova 1422/7b&quot; zkrobchjm=&quot;ABRA Flexi s.r.o.&quot;/&gt;
   &lt;VetaO/&gt;
   &lt;VetaUA c_radku=&quot;1&quot; kc_brutto=&quot;0&quot; kc_korekce=&quot;0&quot; kc_netto=&quot;0&quot; kc_netto_min=&quot;0&quot;/&gt;
   &lt;VetaUA c_radku=&quot;2&quot; kc_brutto=&quot;0&quot; kc_korekce=&quot;0&quot; kc_netto=&quot;0&quot; kc_netto_min=&quot;0&quot;/&gt;
   &lt;VetaUA c_radku=&quot;3&quot; kc_brutto=&quot;0&quot; kc_korekce=&quot;0&quot; kc_netto=&quot;0&quot; kc_netto_min=&quot;0&quot;/&gt;
...
   &lt;VetaUB c_radku=&quot;1&quot; kc_min=&quot;13817245.31&quot; kc_sled=&quot;3788007.63&quot;/&gt;
   &lt;VetaUB c_radku=&quot;2&quot; kc_min=&quot;0&quot; kc_sled=&quot;0&quot;/&gt;
   &lt;VetaUB c_radku=&quot;3&quot; kc_min=&quot;2987294.06&quot; kc_sled=&quot;664191.27&quot;/&gt;
...
   &lt;VetaUD c_radku=&quot;64&quot; kc_min=&quot;0&quot; kc_sled=&quot;0&quot;/&gt;
   &lt;VetaUD c_radku=&quot;65&quot; kc_min=&quot;0&quot; kc_sled=&quot;0&quot;/&gt;
   &lt;VetaUD c_radku=&quot;66&quot; kc_min=&quot;0&quot; kc_sled=&quot;0&quot;/&gt;
   &lt;/DPPDP8&gt;
   &lt;/Pisemnost&gt;
</pre>

<#include "/i-devdoc-footer.ftl" />
</#escape>



