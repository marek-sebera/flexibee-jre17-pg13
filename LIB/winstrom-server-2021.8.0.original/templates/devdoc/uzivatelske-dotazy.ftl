<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Uživatelské dotazy"/>
<#include "/i-devdoc-header.ftl" />

<p>Přes REST API lze běžným způsobem pracovat s uživatelskými dotazy (<code>/c/firma/uzivatelsky-dotaz</code>). Kromě toho je možné uživatelský dotaz také zavolat.</p>

<p>Nejjednodušší dotaz, tj. ten, který nemá parametry (nebo mají všechny parametry postačující výchozí hodnoty), lze zavolat takto: <code>/c/firma/uzivatelsky-dotaz/1/<strong>call</strong>.xml</code> (lze i <code>.json</code>). Fungují obvyklé parametry pro <a href="detail-levels">úroveň detailu</a> nebo <a href="paging">stránkování</a>.</p>

<p>Pokud má dotaz parametry, je potřeba pro každý parametr dotazu uvést parametr v URL. Tj. např. pokud má dotaz parametr <code>datum</code>, jeho volání vypadá takto: <code>/c/firma/uzivatelsky-dotaz/1/<strong>call</strong>.xml?<strong>datum=2012-01-01</strong></code>.</p>

<p>Pokud má parametr dotazu mohutnost N, tj. lze zadat více hodnot, stačí parametr v URL zopakovat. Např. pro výběr několika firem: <code>/c/firma/uzivatelsky-dotaz/1/<strong>call</strong>.xml?<strong>firma</strong>=code:FIRMA1&<strong>firma</strong>=code:FIRMA2</code>. V uloženém dotazu pak použijte operátor IN (&lt;&lt;firma&gt;&gt;).</p>

<p>Hodnoty parametrů se zapisují obvyklým způsobem, neuzavírají se do uvozovek. Nezapomeňte na správné zakódování v&nbsp;URL.</p>

<p>Pro zavolání dotazu lze použít kromě GET metody i metodu POST, která umožňuje předat i velký počet parametrů v těle požadavku.</p>

<p>Uložené dotazy lze i tisknout. Je nutné pro každý uložený dotaz vytvořit vlastní tisk a u uloženého dotazu jej vybrat. Tisk lze pak volat i přes REST API.</p>

<ul>
    <li><a href="objednavka-z-ulozeneho-dotazu">Objednávka z uloženého dotazu</a></li>
</ul>

<#include "/i-devdoc-footer.ftl" />
</#escape>
