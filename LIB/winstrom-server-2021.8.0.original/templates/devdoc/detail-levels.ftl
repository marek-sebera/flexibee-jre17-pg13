<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Úrovně detailu"/>
<#include "/i-devdoc-header.ftl" />

<p>Kvůli zvýšení rychlosti nejsou implicitně vraceny všechny údaje, které evidujeme. Úroveň detailnosti údajů lze řídit. Tento seznam má význam jen u formátů XML, JSON, XLS a CSV. Další formáty jako je PDF či ISDOC to nijak neovlivňuje. Lze vybrat vždy jen jednu z&nbsp;úrovní.</p>

<p>Úroveň detailu je určena parametrem <code>detail</code>. Pro výpis seznamu záznamů je výchozí úroveň <code>summary</code>, pro detail záznamu je to <code>full</code>.</p>

<p>Ukázka: <code>/c/firma/adresar?detail=summary</code></p>

<table class="table table-striped table-condensed">
    <thead>
    <tr>
        <th>Kód</th>
        <th>Název</th>
        <th>Popis</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th><code>id</code></th>
        <td>Identifikátor</td>
        <td>Jen primární klíč záznamu <code>id</code> a externí identifikátory</td>
    </tr>
    <tr>
        <th><code>summary</code></th>
        <td>Přehled</td>
        <td>Základní přehled jako je <code>id</code>, <code>lastUpdate</code>, <code>kod</code> nebo <code>nazev</code>.</td>
    </tr>
    <tr>
        <th><code>full</code></th>
        <td>Plný detail</td>
        <td>Přehled všech položek, které základní záznam obsahuje.</td>
    </tr>
    <tr>
        <th><code>custom:...</code></th>
        <td>Uživatelský detail</td>
        <td>Pouze <code>id</code> a vyjmenované položky (viz níže).</td>
    </tr>
    </tbody>
</table>

<p>Uživatelská úroveň detailu umožňuje vybrat libovolné položky k zobrazení. Je vhodné, pokud chcete pouze některé údaje, ale <code>summary</code> neobsahuje to, co potřebujete. Zápis v URL je speciální: <code>/c/firma/adresar?detail=custom:nazev,ic</code> způsobí, že se vyexportuje jen ID, název a IČO. Po řetězci <code>custom</code> následuje dvojtečka a za ní čárkou oddělený seznam položek, které se mají vyexportovat. Položka <code>id</code> se exportuje vždy, takže ji není třeba uvádět. Neznámé vlastnosti se ignorují.</p>

<p>Je možné specifikovat uživatelskou úroveň detailu i pro kolekce objektů. Za jméno kolekce stačí uvést seznam vlastností v závorkách, takto: <code>/c/firma/cenik?detail=custom:kod,sady-a-komplety(cenik,cenikSada)</code>. To znamená, že z ceníku se vyexportuje pouze kód a kolekce sad/kompletů, přičemž pro každou sadu/komplet se vyexportují pouze vlastnosti <code>cenik</code> a <code>cenikSada</code> (a samozřejmě ID). Pokud je potřeba, tento zápis lze i vnořovat (např. <code>/c/abc/cenik.xml?detail=custom:kod,sady-a-komplety(cenik(nazev),cenikSada)&includes=/cenik/sady-a-komplety/sady-a-komplety/cenik</code>).</p>

<p>Kromě základní úrovně detailu lze parametrem <code>relations</code> řídit i další informace. Ty lze kombinovat oddělením čárkou:</p>
<p>Ukázka: <code>/c/firma/adresar?relations=vazby,prilohy</code></p>

<p>Pozor: položky, které jsou řízené tímto výpisem, lze pouze exportovat z&nbsp;ABRA Flexi. Nelze je využít pro import do ABRA Flexi. Pro import je nutné vkládat data do samotné tabulky.</p>

<table class="table table-striped table-condensed">
    <thead>
    <tr>
        <th>Kód</th>
        <th>Název</th>
        <th>Popis</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th><code>vazby</code></th>
        <td>Vazby mezi doklady</td>
        <td></td>
    </tr>
    <tr>
        <th><code>prilohy</code></th>
        <td>Přílohy</td>
        <td></td>
    </tr>
    <tr>
        <th><code>sklad-karty</code></th>
        <td>Skladové karty (u ceníku)</td>
        <td></td>
    </tr>
    <tr>
        <th><code>polozky</code></th>
        <td>Položky dokladu</td>
        <td></td>
    </tr>
<!--    <tr>
        <th><code>serialNumbers</code></th>
        <td>Sériová čísla</td>
        <td></td>
    </tr>
    <tr>
        <th><code>storeNumber</code></th>
        <td>Množství na skladě</td>
        <td></td>
    </tr>
-->
    </tbody>
</table>

<p>Lze také určit, aby místo pouhého ID ve vazbě se vložil celý objekt.</p>
<p>Ukázka: <code>/c/firma/adresar?includes=/adresar/stat/</code></p>

<p>Pokud chcete includes aplikovat vícekrát oddělte je čárkou: <code>?includes=/adresar/stat/,/adresar/stredisko/</code></p>

<#include "/i-devdoc-footer.ftl" />
</#escape>
