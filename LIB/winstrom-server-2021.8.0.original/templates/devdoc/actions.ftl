<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Provádění akcí"/>
<#include "/i-devdoc-header.ftl" />


<p>Při importu lze namísto běžného vytvoření či změny záznamu provést jinou akci, např. smazání či storno dokladu. K tomu slouží atribut <code>action</code>:</p>

<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;faktura-vydana action="delete"&gt;
    &lt;id&gt;123&lt;/id&gt;
    &lt;id&gt;uuid:123456&lt;/id&gt;
  &lt;/faktura-vydana&gt;
&lt;/winstrom&gt;
</pre>

<table class="table table-striped table-condensed">
    <tr>
    <th>Akce</th>
    <th>Popis</th>
    </tr>

    <tr>
    <th>Smazání<br/> (<code>delete</code>)</th>
    <td>Záznam bude smazán.</td>
    </tr>

    <tr>
    <th>Storno<br/> (<code>storno</code>)</th>
    <td>Záznam bude stornován. Lze použít pouze pro doklady.</td>
    </tr>

</table>

<p>Při provádění akcí nejsou záznamy jinak modifikovány, nemá tedy smysl uvádět jiné elementy než <code>id</code>. Zároveň musí záznamy již existovat, nelze např. vytvořit novou smazanou fakturu.</p>


<h2>Akce na položkách</h2>

<p>Akce lze vyvolávat také na položkách dokladu. Jen ne přímo, ale stejně jako při aktualizaci, je zapotřebí uvést požadavek prostřednictvím kolekce položek na odpovídajícím dokladu.</p>

<p>Ukázka pro smazání položky s ID rovno 456 v <code>XML</code> a <code>JSON</code> formátu:</p>

<pre class="brush: xml">
&lt;winstrom version="1.0"&gt;
  &lt;faktura-vydana&gt;
    &lt;id&gt;123&lt;/id&gt;
    &lt;polozkyFaktury&gt;
      &lt;faktura-vydana-polozka id="456" action="delete" /&gt;
    &lt;/polozkyFaktury&gt;
  &lt;/faktura-vydana&gt;
&lt;/winstrom&gt;</pre>

<pre class="brush: js">
{
    "winstrom": {
        "@version": "1.0",
        "faktura-vydana": [{
            "id": "123",
            "polozkyFaktury": [{
                "id": "456",
                "@action": "delete"
            }]
        }]
    }
}
</pre>

<#include "/i-devdoc-footer.ftl" />
</#escape>
