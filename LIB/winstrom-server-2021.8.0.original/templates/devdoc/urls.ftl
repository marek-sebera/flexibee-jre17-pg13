<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Sestavování URL"/>
<#include "/i-devdoc-header.ftl" />

<p>Struktura URL pro ABRA Flexi se skládá z&nbsp;několika částí:</p>
<pre>/c/<font style="color:green">&lt;identifikátor firmy&gt;</font>/<font style="color:red">&lt;evidence&gt;</font>/<font style="color:blue">&lt;ID záznamu&gt;</font>.<font style="color:brown">&lt;výstupní formát&gt;</font></pre>

<ul>
    <li><font style="color:green">&lt;identifikátor firmy&gt;</font>: jednoznačná <a href="company-identifier">identifikace firmy</a></li>
    <li><font style="color:red">&lt;evidence&gt;</font>: typ evidence (adresář, objednávka, faktura, ...)</li>
    <li><font style="color:blue">&lt;ID záznamu&gt;</font>: <a href="identifiers">identifikátor záznamu</a></li>
    <li><font style="color:brown">&lt;výstupní formát&gt;</font>: <a href="format-types">výstupní formát</a> (XML, JSON, ...). Pokud není uvedeno zohledňuje se hlavička <code>Accept</code>, případně je vrácena HTML forma.</li>
</ul>

<h2>Výpis evidence</h2>
<p>Pokud chcete výpis položek evidence (viz <a href="list">výpis</a>), neuvádějte žádný identifikátor záznamu:</p>
<pre>/c/<font style="color:green">&lt;identifikátor firmy&gt;</font>/<font style="color:red">&lt;evidence&gt;</font></pre>
<p>Je také možné použít <a href="filters">filtraci</a>:</p>
<pre>/c/<font style="color:green">&lt;identifikátor firmy&gt;</font>/<font style="color:red">&lt;evidence&gt;</font>/(<font style="color:orange">&lt;filtr&gt;</font>)</pre>

<h2>Sumace záznamů</h2>
<p>Pokud potřebujete získat základní sumace o dané evidenci, použijte <a href="sumace">sumaci</a>:</p>
<pre>/c/<font style="color:green">&lt;identifikátor firmy&gt;</font>/<font style="color:red">&lt;evidence&gt;</font>/$sum</pre>
<p>Je také možné kombinovat <a href="filters">filtraci</a> a <a href="sumace">sumaci</a>:</p>
<pre>/c/<font style="color:green">&lt;identifikátor firmy&gt;</font>/<font style="color:red">&lt;evidence&gt;</font>/(<font style="color:orange">&lt;filtr&gt;</font>)/$sum</pre>

<h2>Přehled atributů</h2>
<p>Pro každou evidenci je možné získat seznam atributů, které tato evidence podporuje. Tento přehled zohledňuje přístupová práva a licencování.</p>
<pre>/c/<font style="color:green">&lt;identifikátor firmy&gt;</font>/<font style="color:red">&lt;evidence&gt;</font>/properties</pre>

<h2>Přehled tiskových reportů</h2>
<p>U evidence si můžete zobrazit seznam podporovaných reportů pro tisk do PDF:</p>
<pre>/c/<font style="color:green">&lt;identifikátor firmy&gt;</font>/<font style="color:red">&lt;evidence&gt;</font>/reports</pre>

<h2>Přehled podevidencí</h2>
<p>Každá evidence může mít podevidenci (relaci). Příkladem může být položka faktury nebo kontakty u adresáře. Tyto záznamy jsou obvykle přístupné i přímo jako evidence. Rozdíl je v&nbsp;tom, že podevidence je filtrována danou relací. Přehled podevidencí lze získat takto:</p>
<pre>/c/<font style="color:green">&lt;identifikátor firmy&gt;</font>/<font style="color:red">&lt;evidence&gt;</font>/relations</pre>
<p>S podevidencemi lze pak pracovat stejně jako s&nbsp;evidencí:</p>

<pre>/c/<font style="color:green">&lt;identifikátor firmy&gt;</font>/<font style="color:red">&lt;evidence&gt;</font>/<font style="color:blue">&lt;ID záznamu&gt;</font>/<font style="color:red">&lt;podevidence&gt;</font></pre>

<p>Pokud chcete při exportu z ABRA Flexi exportovat se záznamem i jeho podevidence, použijte parametr  <code>?relations=vazby,prilohy,bankovniSpojeni</code>.</p>

<h2 id="parameters">Všechny podporované parametry</h2>

<p>ABRA Flexi podporuje mnoho atributů a na této stránce je jejich popis.</p>
<table class="table table-striped table-condensed">

    <tr><th><code>?dry-run=true</code</th><td> <a href="dry-run">Testovací uložení (dry-run)</a></td></tr>
    <tr><th><code>?fail-on-warning=true</code</th><td> Pokud nastane varování, neukládej záznam (<a href="validations">Validace dat</a>)</td></tr>
    <tr><th><code>?report-name=faktura</code</th><td> Jméno tiskového výstupu při <a href="pdf">exportu do PDF</a></td></tr>
    <tr><th><code>?report-lang=en</code</th><td> Jazyk, ve kterém se má vygenerovat tiskový výstup při <a href="pdf">exportu do PDF</a></td></tr>
    <tr><th><code>?report-sign=true</code</th><td> Zda se má <a href="pdf">PDF vyexportovat</a> elektronicky podepsané</td></tr>
    <tr><th><code>?detail=summary</code</th><td> Definice <a href="detail-levels">úrovně detailu</a></td></tr>
    <tr><th><code>?mode=ruby</code</th><td> <a href="ruby">Podpora pro RubyOnRails</a></td></tr>
    <tr><th><code>?limit=100</code</th><td> <a href="paging">Stránkování</a></td></tr>
    <tr><th><code>?start=10</code</th><td> <a href="paging">Stránkování</a></td></tr>
    <tr><th><code>?order=nazev@A</code</th><td> <a href="ordering">Řazení záznamů</a></td></tr>
    <tr><th><code>?sort=nazev&amp;dir=desc</code</th><td> <a href="extjs">Řazení záznamů pro ExtJS</a></td></tr>
    <tr><th><code>?add-row-count=true</code</th><td> Přidání celkového počtu záznamů do výstupu (<a href="paging">Stránkování</a>)</td></tr>
<!--    <tr><th><code>?xpath=//winstrom/adresar/email/text()</th><td> Umožňuje aplikovat XPath na výsledné XML (<a href="xpath">XPath</a>)</td></tr> -->
    <tr><th><code>?relations=vazby</code></th><td> Doplnění dat z relace (viz <a href="detail-levels">úrovně detailu</a>) Přehled relací lze získat pro každou evidenci (<code>/relations</code>).</td></tr>
    <tr><th><code>?includes=faktura-vydana/stredisko</code></th><td> Zahrnutí souvisejícího objektu <a href="detail-levels">úrovně detailu</a></td></tr>
    <tr><th><code>?use-ext-id=ESHOP,MUJ</code></th><td>Pokud objekt obsahuje externí ID typu ESHOP nebo MUJ, používej jej jako vazební.</td></tr>
    <!-- kvůli zpětné kompatibilitě podporujeme i useExtId -->
    <!-- <tr><th><code>?useExtId=ESHOP,MUJ</code></th><td> Pokud objekt obsahuje externí ID typu SHOW nebo MUJ, používej jej jako vazební.</td></tr> -->
    <tr><th><code>?use-internal-id=true</code></th><td> Kromě atributů <code>ref</code> a <code>showAs</code> u objektů dodá i atribut <code>internalId</code>, který obsahuje vnitřní ID záznamu</td></tr>
    <tr><th><code>?stitky-as-ids=true</code></th><td> Štítky se budou exportovat i importovat ne jako seznam kódů, ale jako seznam číselných ID</td></tr>
    <tr><th><code>?only-ext-ids=true</code></th><td> Primární klíč se nebude exportovat, elementy <code>&lt;id&gt;</code> budou obsahovat pouze externí ID</td></tr>
    <tr><th><code>?no-ext-ids=true</code></th><td> Odpověď nebude obsahovat externí identifikátory (optimalizace výkonu)</td></tr>
    <tr><th><code>?no-ids=true</code></th><td> Odpověď nebude obsahovat žádné primární identifikátory (optimalizace výkonu).</td></tr>
    <tr><th><code>?code-as-id=true</code></th><td> Pokud má objekt unikátní kód, vyexportuje se (kromě elementu <code>&lt;kod&gt;</code>) i jako <code>&lt;id&gt;code:...&lt;/id&gt;</code></td></tr>
    <tr><th><code>?no-http-errors=true</code></th><td> Pokud nastane při zpracování požadavku chyba typu 4xx, server stejně pošle 200 OK</td></tr>
    <tr><th><code>?export-settings=true</code></th><td> Exportuj na začátku i jeden extra záznam s aktuálním nastavením</td></tr>
    <tr><th><code>?as-gui=true</code></th><td> Zapne funkce, které doplní výstupy pro zpracování GUI <!--(<a href="postvals">Zpracování jako GUI</a>)--></td></tr>
    <tr><th><code>?code-in-response=true</code></th><td> V odpovědi bude u každého objektu nejen ID a URL, ale i kód.</td></tr>
    <tr><th><code>?add-global-version=true</code></th><td> Odpověď bude obsahovat číslo globální verze aktuální při provádění exportu.</td></tr>

    <!-- <tr><th><code>?csv-attrs=true</code></th><td> Přepne konverzi do CSV do režimu čtení z atributů a ne nodů. </td></tr> -->
    <tr><th><code>?encoding=iso-8859-2</code></th><td> Určuje kódování vstupního/výstupního souboru ve formátu CSV. </td></tr>
    <tr><th><code>?delimeter=;</code></th><td> Určuje oddělovač vstupního/výstupního souboru ve formátu CSV. </td></tr>

    <tr><th><code>?format=awis</code></th><td> Na výstupní XML bude aplikována jedna ze zakompilovaných XSL transformací (zde <code>awis</code>).</td></tr>

    <tr><th><code>?auth=http</code></th><td> Vynutí přihlášení pomocí <a href="login">HTTP autentizace</a>, čímž lze změnit například výchozí způsob přihlašování ve <abbr title="Web User Interface (webové rozhraní)">WUI</abbr>.</td></tr>
    <tr><th><code>?auth=html</code></th><td> Vynutí autentizaci pomocí HTML formuláře. To může být užitečné k potlačení automatické <abbr title="Single Sign-On">SSO</abbr> autentizace.</td></tr>
    <tr><th><code>?skupina-stitku=SKUPINA1,SKUPINA2</code></th><td> Umožní seskupení štítků při exportu dle skupiny (více <a href="stitky">štítky</a>). </td></tr>

</table>

<#include "/i-devdoc-footer.ftl" />
</#escape>
