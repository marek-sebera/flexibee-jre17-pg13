<#ftl strip_text=true encoding="utf-8" strip_whitespace=true>
<#escape x as (x!'-')?html>

<#macro place object name items marker level="1" it=it>
<#compress>
    <#local cnt = 0 />
    <#if !name?is_sequence>
        <#local list=[ name ] />
    <#else>
        <#local list=name />
    </#if>
    <#list list as lname>
    <#if items[lname]??>
        <#local myItem = items[lname] />
        <#local editInfo = it.getEditInfo(object, myItem.propertyName)/>
        <#if myItem.showToUser && editInfo.visible><#if cnt &gt; 0>, </#if>${marker.mark(lname)}
            <#if level != '1'><span class="flexibee-level-${level}"></#if>
            <#if editInfo.link??><a href="${editInfo.link}" class="flexibee-external" target="_blank"></#if>
            <#local val=tool.getValue(myItem, object)>
            <#if val??>
            <#if myItem.type == 'date'>
                ${val.time?date}
            <#elseif myItem.type == 'datetime'>
                ${val.time?datetime}
            <#elseif myItem.type == 'logic'>
                <#if val>${lang('buttons', 'ano')}<#else>${lang('buttons', 'ne')}</#if>
            <#elseif myItem.type == 'numeric'>
                ${val?string(",##0.00")}
            <#elseif myItem.type == 'integer'>
                ${val?string("0")}
            <#elseif myItem.type == 'select'>
                <#if val??><#if myItem.possibleValuesMap[val]??>${myItem.possibleValuesMap[val].name}<#else>--</#if></#if>
            <#elseif myItem.type == 'relation'>
                <#local url = formListUrl(it.companyResource, myItem.fkEvidenceType)!"" />
                <#if url != '' && val.id??><a href="${url}/${val.id?c}">${val}</a><#else>${val}</#if>
            <#else>
                ${val?string}
            </#if>
            </#if>
            <#if editInfo.link??></a></#if>
            <#if level != '1'></span></#if>
        </#if>
    </#if>
    </#list>
</#compress>
</#macro>

<#macro placeList object name items marker it=it>
    <#local cnt = 0 />
    <#local limit = 35/>
    <#if isMobile>
    <#local limit = 25 />
    </#if>

    <#if !name?is_sequence>
        <#local list=[ name ] />
    <#else>
    <#local list=name />
    </#if>
    <#list list as lname>
    <#if items[lname]??>
        <#local myItem = items[lname] />
        <#if myItem.showToUser && tool.hasValue(lname, object, items, marker)>
        <#if cnt &gt; 0>, </#if>${marker.mark(lname)}
        <#local val=tool.getValue(myItem, object)>
        <#if val??>
            <#if myItem.type == 'string'>
            <#if val?length &gt; limit>${val?substring(0, limit - 5)?trim}&hellip;<#else>${val?string}</#if>
            <#elseif myItem.type == 'relation'>
            <#if val.nazev??><#if val.nazev?length &gt; limit>${val.nazev?substring(0, limit - 5)?trim}&hellip;<#else>${val.nazev}</#if><#else>${val}</#if>
            <#else>
            <@place object=object name=myItem.propertyName it=it items=items marker=marker/>
            </#if>
        </#if>
        <#local cnt = cnt + 1 />
        </#if>
    </#if>
    </#list>
</#macro>

<#macro placeArea object name items marker level="1" it=it>
    <#if items[name]??>
    <#local myItem = items[name] />
    <#if !myItem.showToUser><#return></#if>
    ${marker.mark(name)}
    <#if object[myItem.propertyName]??>
    <#if myItem.type == 'string'>
        <#noescape>${object[myItem.propertyName]?string?html?replace("\n", "<br/>\n")}</#noescape>
    <#else>
        <@place object=object name=name it=it items=items marker=marker/>
    </#if>
    </#if></#if>
</#macro>

<#macro placeTel object name items marker level="1" it=it>
        <@place object=object name=name it=it items=items marker=marker level=level/>
</#macro>

<#macro placeWww object name items marker level="1" it=it>
    <#if items[name]??>
    <#local myItem = items[name] />
    <#if !myItem.showToUser><#return></#if>
    ${marker.mark(name)}
    <#if object[myItem.propertyName]??>
    <#if myItem.type == 'string'>
        <a href="<#if !object[myItem.propertyName]?starts_with('http')>http://</#if>${object[myItem.propertyName]?string}" class="flexibee-external" target="_blank">${object[myItem.propertyName]?string}</a>
    <#else>
        <@place object=object name=name it=it items=items marker=marker/>
    </#if>
    </#if></#if>
</#macro>

<#macro placeEmail object name items marker level="1" it=it>
    <#if items[name]??>
    <#local myItem = items[name] />
    <#if !myItem.showToUser><#return></#if>
    ${marker.mark(name)}
    <#if object[myItem.propertyName]??>
    <#if myItem.type == 'string'>
        <a href="mailto:${object[myItem.propertyName]?string}" class="flexibee-external">${object[myItem.propertyName]?string}</a>
    <#else>
        <@place object=object name=name it=it items=items marker=marker/>
    </#if>
    </#if></#if>
</#macro>


<#macro label name items level="1" asInList=false>
<#if items[name]??><#local myItem = items[name] /><#if !myItem.showToUser><#return></#if><#if level != '1'><span class="flexibee-level-${level}-label"></#if><#if asInList>${myItem.name}<#else>${myItem.title}</#if><#if level != '1'></span></#if></#if
></#macro> <#-- kvůli mezerám :-( -->

<#function hasItems items object marker>
    <#list items?values as item>
        <#if (marker.isMarked(item.propertyName) == false) && item.showToUser && object[item.propertyName]?? && ((item.type!='numeric' && item.type != 'integer') || object[item.propertyName] != 0)
            && (item.type!='string' || object[item.propertyName] != '')>
            <#return true/>

    <#break/>
</#if></#list>
        <#return false/>
</#function>

<#function getValue myItem object>
    <#local valpname = myItem.originalPropertyName!myItem.propertyName>
    <#return object[valpname]!object.getValue(valpname)!>
</#function>

<#function hasValue propertyName object items marker>
    ${marker.mark(propertyName)}
    <#local myItem = items[propertyName]>
    <#local val=tool.getValue(myItem, object)>
    <#return val?? && myItem.showToUser && val?string?trim != ''>
</#function>

<#macro listItems items object marker it=it>
<#if isDevel == true && false>
<table class="flexibee-tbl-form-2">
    <tbody>
    <#list items?values as item>
        <#local editInfo = it.getEditInfo(object, item.propertyName)/>
        <#if !marker.isMarked(item.propertyName) && editInfo.visible && item.showToUser && object[item.propertyName]?? && ((item.type!='numeric' && item.type != 'integer') || object[item.propertyName] != 0)
            && (item.type!='string' || object[item.propertyName] != '')>
        <tr>
            <th><@tool.label name=item.propertyName items=items /></th>
            <td><@tool.place marker=marker object=object name=item.propertyName it=it items=items /></td>
        </tr></#if>
    </#list>
    </tbody>
</table>
</#if>
</#macro>

<#macro showSubEvidences marker subEvidences = ''>
<#if subEvidences?? && subEvidences?is_hash && subEvidences.list?size &gt; 0 && hideRelations == false>
${marker.mark('polozkyDokladu')} <#-- položky dokladu vypisujeme přímo ve stránce -->
<#list subEvidences.list as sub>
    <#if marker.isMarked(sub.url) == false>
    <div class="btn btn-default <#if sub_index == 0 && false>active</#if>"><a href="/c/${companyId}/${it.evidenceName}/${it.id}/${sub.url}"><span>${sub.name}</span></a></div>
    </#if>
</#list>
</#if>
</#macro>


<#macro showTitleBack url="" name="">
    <#if url?? && url != ''>
    <div class="flexibee-goback">
            <a class="flexibee-menu-company flexibee-clickable" href="${url}">${name}</a>
    </div>
    <#else>
        ${naviPath("/i-navipath-goback.ftl")}
    </#if>
</#macro>

<#macro showTitleLogo title=title  showLogoInTitle=false>
        <h1>${title}</h1>
</#macro>

<#macro showTitle titleId="root" back=true menu=true logo=true title="" finish=true  showLogoInTitle=false showAsTitle=true fallBack="$$$none$$$" showPath=true showRight=false showToolbar=false leftMenu="/i-left-menu-company.ftl" rightMenu="/i-right-menu-view.ftl">
<#if isPanel==false && isModal==false>
    <#assign  isShowTitleFirst = true/>

<!--FLEXIBEE:PAGE:START-->
<!--FLEXIBEE:BEFORE:TAG-->
<div class="container-fluid desktop body wrapper">
<div class="<#if inDesktopApp>box-desktop<#else>box</#if>">
<#if companyId??>
<div class="row row-offcanvas row-offcanvas-left <#if useSmallMenu == true>small-sidebar</#if>">
</#if>
<header>
<div class="navbar navbar-default navbar-fixed-top <#if inDesktopApp>navbar-not-visible</#if>" role="navigation">
<#if !inDesktopApp>
  <div class="container-fluid">

       <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                           <span class="sr-only">Toggle</span>
                           <span class="icon-bar"></span>
                             <span class="icon-bar"></span>
                             <span class="icon-bar"></span>
     </button>
    <div class="navbar-header">
        <div class="navbar-brand">
              <#if companyId??>
                <a class="sidebar-toggle" data-toggle="offcanvas"><i class="fa fa-angle-double-<#if useSmallMenu == false>left<#else>right</#if>"></i></a>
             </#if>
         <a class="brand-logo" href="<#if companyId??>/c/${companyId}<#else>/c</#if>"><strong><img src="${staticPrefix}/img/logo-abraflexi.png" alt="ABRA Flexi" title="ABRA Flexi" width="101" height="19"/></strong></a>
        </div>
    </div>


            <#if loggedUser??>
                <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><#if it.companyResource??>${it.companyResource.databaseInfo.nazev}<#else>${lblText('filterFirmaNeomezena')}</#if> <span class="caret"></span></a>
                          <ul class="dropdown-menu" role="menu">
                              <#assign showMore = false/>
                            <#list companies() as di>
                                <#if di_index &gt; 15>
                                      <#assign showMore = true/>
                                    <#break>
                                <#else>
                                    <#if !companyId?? || di.dbNazev != companyId>
                                    <li><a href="/c/${di.dbNazev}" class="flexibee-show-wait">${di.nazev}</a></li>
                                    </#if>
                                </#if>
                            </#list>
                            <li class="divider"></li>
                            <#if showMore>
                            <li><a href="/c" class="flexibee-show-wait"><i class="fa fa-list"></i> ${btnText('vyberFirmyZvolitFirmuBT')}</a></li>
                            </#if>
                            <#if loggedUser.userInfo.isCreateCompany()>
                            <li><a href="/admin/zalozeni-firmy" class="flexibee-show-wait"><span class="glyphicon glyphicon-plus"></span> ${lang('formsInfo', 'novaFirmaWiz', 'Založení nové firmy')}</a></li>
                            </#if>
                          </ul>
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                <#if licenseHelper.isFlexiAppSupported() >
                <li><a href="/flexi/${companyId!''}" target="_blank" style=" padding-top: 10px; "><button type="button" class="btn btn-primary"><span><img type="logo" src="${staticPrefix}/img/logo-abra-s.png" alt="New Interface" title="ABRA Logo" width="27" height="16"/></span>${lblText('newInterfaceLink')}</button></a></li>
                </#if>
                <li class="hidden-xs"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon icon-help"></span> <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="/help/"><span class="glyphicon glyphicon-question-sign"></span> ${lang('actions', 'help')}</a></li>
                        <li><a href="https://www.flexibee.eu/podpora/akademie/" target="_blank" rel="external" class="flexibee-external">Akademie</a></li>
                        <li><a href="https://www.flexibee.eu/qa" target="_blank"><span class="glyphicon glyphicon-book"></span> ${lang('formsInfo', 'casteDotazy')}</a></li>
                        <li class="divider"></li>
                        <li><a href="https://www.flexibee.eu/podpora/technicka-podpora/" target="_blank" rel="external" class="flexibee-external"><span class="glyphicon glyphicon-user"></span> ${lang('formsInfo', 'podpora')}</a></li>
                    </ul>
                </li>
                <#if companyId??>
                <li><a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span> <span class="visible-xs-inline">${lang('formsInfo', 'nastav000')}</span>  <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="/c/${companyId}/nastaveni">${lang('labels', 'company.settings')}</a></li>
                        <li><a href="/c/${companyId}?menu=nastroje">${lang('formsInfo', 'nastav000')}</a></li>
                        <li><a href="/c/${companyId}/changes/control">${dlgText('changesApi.title')}</a></li>
                    </ul>
                </li>

                </#if>
                <li><a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> <#if loggedUser.prijmeni?? || loggedUser.jmeno??>${loggedUser.prijmeni} ${loggedUser.jmeno}<#else>${loggedUser.name}</#if> <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <#if companyId??>
                        <li><a href="/c/${companyId}/uzivatel/code:${loggedUser.name}/"><span class="icon icon-user"></span> Nastavení uživatele</a></li>
                        </#if>
                        <li><a href="/login-logout/logout" title=""><span class="glyphicon glyphicon-log-out"></span> ${lang('actions', 'logout')}</a></li>
                        <li class="divider"></li>
                        <li><a href="/admin/"><span class="icon icon-nastaveni-large"></span> Systémové nastavení</a></li>
                    </ul>
                </li>
            </#if>
            </ul>
    </div>
  </div>
</#if>
<#if companyId??>
</div>
</#if>
</header>

    <#if leftMenu != '' && !inDesktopApp>
        <#include leftMenu/>
    </#if>
    <#if companyId??>
        <#if showRight && rightMenu != ''>
        <#include rightMenu/>
        </#if>
        <div class="flexibee-application-content column <#if inDesktopApp>flexibee-application-content-not-visible</#if>" role="main">
    <#else>
    <div class="flexibee-application-content column" role="main">
    </#if>
    <!--FLEXIBEE:MENU:END-->
    <#if showPath && !inDesktopApp>
        <#include "/i-navi.ftl" />
    </#if>
<!--FLEXIBEE:CONTENT:START-->
</#if>
</#macro>

<#macro showTitleFinish title="" showAsTitle=true>
<#if isPanel == false && isModal == false && showAsTitle==true>
    <#if isMobile == false && title != ''>
    <h2>${title}</h2>
    </#if>

    <!--FLEXIBEE:CONTENT:HEADER-->
</#if>

</#macro>

<#macro showRelating name fieldId>
    <#if it?? && it.companyResource?? && isMobile == false>

        <script>
            $(document).ready(function() {

                var URL_NEW = '${formNewUrl(it.companyResource, name)}';
                var URL_LIST = '${formListUrl(it.companyResource, name)}';

                var mameUrlNew = URL_NEW.length > 0;
                var mameUrlList = URL_LIST.length > 0;

                if(!mameUrlNew && !mameUrlList) {
                    // nic nezobrazujeme
                    return;
                }

                var parentDiv = $("#${fieldId}").parent();
                var relPanel = parentDiv.find(".relation-button");
                var relLink = parentDiv.find("a[data-toggle='dropdown']"); // odkaz vedle vstup. pole

                // nasleduje uprava odkazu s ikonou pred zobrazenim
                var CLASS_ORIG = 'fa-th-list';
                var CLASS_LIST = 'fa-search';
                var CLASS_NEW = 'fa-plus';

                var changeClass = '';
                var changeHref = '';

                if(mameUrlNew && !mameUrlList) {
                    changeClass = CLASS_NEW;
                    changeHref = URL_NEW;
                }

                if(!mameUrlNew && mameUrlList) {
                    changeClass = CLASS_LIST;
                    changeHref = URL_LIST;
                }

                if(changeClass !== '' && changeHref != '') {

                    // odkaz zobrazi spec. ikonu a povede rovnou na stranku
                    relLink.find("span").removeClass(CLASS_ORIG).addClass(changeClass);
                    relLink.attr("data-toggle", '');
                    relLink.attr("target", '_blank');
                    relLink.attr("href",  changeHref);
                    relLink.find("span.caret").remove();

                    relPanel.show();
                    return;
                }

                // odkaz zobrazi panel s dvema dalsimi odkazy

                var dropDownMenu = parentDiv.find(".dropdown-menu")

                var li = '<li role="presentation" class="dropdown-header">${formInfo(name).name}</li>';

                if(mameUrlNew) {
                    li = li + '<li><a href="' + URL_NEW + '" target="_blank">Přidat</a></li>';
                }

                li = li + '<li><a href="' + URL_LIST + '" target="_blank">Otevřít</a></li>';

                dropDownMenu.append(li);
                relPanel.show();
            });
        </script>
    </#if>
</#macro>

<#macro showFooter>
<#if isMobile == false>
<#if companyId??>
</div> <!-- flexibee-application-content -->
</#if>
<#if isPanel == false && isModal == false && !inDesktopApp>
<footer>
<div class="flexibee-footer hidden-xs hidden-print"><!--FLEXIBEE:FOOTER:START-->
<div class="col-sm-4"><#if customerNumber??><small>${lang('labels', 'statusBar.cisloZakaznika')} ${customerNumber}</small> |</#if> <small>${serverVersion}</small></div>
<#if isDevel>
<div class="col-sm-4">
 <div class="dropdown dropup">
   <a href="#" class="dropdown-toggle" data-toggle="dropdown">${lang('dials', 'jazyk.' + languageCode, '')} <span class="caret"></span></a>
   <ul class="dropdown-menu" role="menu">
   <#list ["cs", "sk", "en", "de"] as l>
       <#if l != languageCode>
           <li><a href="/login-logout/lang/?lang=${l}&amp;returnUrl=${baseUrl}">${lang('dials', 'jazyk.'+l, '')}</a></li>
       </#if>
   </#list>
   </ul>
 </div>
</div>
</#if> <#-- isDevel -->
<div class="col-sm-4 text-right">
1991&ndash;${(now?string('yyyy'))} &nbsp; <a href="http://www.flexibee.eu/" tabindex="-1"><span class="glyphicon glyphicon-heart-empty"></span> &nbsp; ABRA Flexi</a>
<#if loggedUser?? && isLogged && uriInfo?? && !isMobile && !isDesktop && !isModal>
    <img src="https://my.flexibee.eu/my/log/${serverHost?url}/<#if currentLicense()??>${currentLicense().name?url}/" width="1" height="1" alt="" style="vertical-align: top;"></#if>
</#if>
</div>
<!--FLEXIBEE:FOOTER:END--></div>
<!--FLEXIBEE:AFTER:TAG-->
</footer>
</#if>
</#if> <#-- isMobile -->

<!--FLEXIBEE:PAGE:END-->

</#macro>


<#macro listHeader name items url urlSuffix orderProperty orderDirection it=it>
    <#local item = items[name]/>
    <#if item.showToUser && item.isVisible>
        <#-- sestavime url s nastavenim poradi razeni -->
        <#local colOrder="@A">
        <#if item.propertyName == orderProperty>
            <#if orderDirection == "ascending">
                <#local colOrder="@D">
                <#local classOrder="fa fa-sort-asc">
            <#else>
                <#local classOrder="fa fa-sort-desc">
            </#if>
        </#if>
        <a href="${url}?order=${item.propertyName}${colOrder}${urlSuffix}"><@tool.label name=item.propertyName items=it.devDocItemsForHtmlMap asInList=true /> <#if classOrder??><i class="${classOrder}"></i></#if></a>
    </#if>
</#macro>

<#macro menaSymbol mena={}>
<#if mena.symbol??>${mena.symbol}<#else>${mena.kod}</#if>
</#macro>

</#escape>
