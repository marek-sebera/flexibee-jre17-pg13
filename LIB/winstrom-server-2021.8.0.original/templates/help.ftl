<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign headerAddons="<link rel=\"stylesheet\" type=\"text/css\" href=\"${staticPrefix}/css/help.css\" media=\"all\" />" />
<#assign title2=it.title/>
<#assign title=it.title+" | Nápověda"/>
<#include "/header-begin.ftl" />
<#include "/i-toolbar.ftl"/>

<div class="flexibee-article-view">
<#noescape>${it.content}</#noescape>

</div>


<#include "/footer.ftl" />
</#escape>