<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.Loader.setConfig({
    enabled: true,
    disableCaching: ${isDevel?string}, <#-- i pro vývoj se občas hodí false, aby prohlížeče nezapomínaly breakpointy -->
    paths: {
        'Ext.ux': '${staticPrefix}/extjs-ux',
        'FlexiBee.root': '/sencha',
        'FlexiBee.loginLogout': '/login-logout',

        'FlexiBee.main': '/c/${it.companyId}/sencha',

        <#list it.getEvidenceList().evidenceDescriptors as e>
        'FlexiBee.${e.senchaName}': '/c/${it.companyId}/${e.tagName}/sencha'<#if e_has_next>,</#if>
        </#list>
    }
});

<#-- TODO insert <link>s to CSS dynamically here? -->

</#escape>
