<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#include "/header-html-start.ftl" />

<@tool.showTitle title=title>
</@tool.showTitle>
<@tool.showTitleFinish />

</#escape>
