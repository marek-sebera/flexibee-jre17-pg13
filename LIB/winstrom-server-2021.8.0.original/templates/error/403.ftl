<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Přístup odepřen"/>
<#include "/error/header.ftl" />

<p>Chyba 403</p>

<#include "i-error.ftl" />

<#include "/error/footer.ftl" />
</#escape>