<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Neznámý uživatel"/>
<#include "/error/header.ftl" />

<p>Neznámý uživatel pro Single Sign-On přihlášení: &apos;<strong>${uid}</strong>&apos;.</p>

<#include "/error/footer.ftl" />
</#escape>