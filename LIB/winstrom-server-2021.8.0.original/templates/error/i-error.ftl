<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<div class="flexibee-article-view">
    <div class="flexibee-note-error"><code>${it.origException}</code></div>
</div>

</#escape>
