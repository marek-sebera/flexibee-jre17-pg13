<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Licence nedovoluje tuto funkci"/>
<#include "/error/header.ftl" />


    <div class="flexibee-note-error">Licence nedovoluje tuto funkci. Pro její využití musíte zakoupit vyšší variantu.</div>

<p>Snažíme se systém ABRA Flexi zpřístupnit co nejširší skupině uživatelů. Proto nabízíme zlevněné varianty, které neobsahují některé pokročilejší funkce.</p>

<#include "/error/footer.ftl" />
</#escape>
