<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#assign title="Přístup odepřen"/>
<#include "/error/header.ftl" />

    <div class="flexibee-note-error">K provedení požadované operace nemáte oprávnění.</div>

<#include "/error/footer.ftl" />
</#escape>