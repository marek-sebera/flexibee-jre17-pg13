<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=dlgText("error.403-access_denied", "403 - Access denied")/>
<#include "/error/header.ftl" />

    <div class="flexibee-note-error">${it.origException.localizedMessage}</div>

    <#include "/error/footer.ftl" />
</#escape>