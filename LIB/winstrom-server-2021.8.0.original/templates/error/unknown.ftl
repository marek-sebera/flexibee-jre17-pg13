<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Chyba (neznámá)"/>
<#include "/error/header.ftl" />

<p>Neznámá chyba.</p>

<#include "i-error.ftl" />

<#include "/error/footer.ftl" />
</#escape>