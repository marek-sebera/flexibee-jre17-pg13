<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Přístup odepřen"/>
<#include "/error/header.ftl" />

    <div class="flexibee-note-error">${it.origException.message}</div>

<#include "/error/footer.ftl" />
</#escape>