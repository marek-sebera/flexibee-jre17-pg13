<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#assign isDevel = true />

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>${lang('dialogs', it.name, it.name)} | FlexiBee</title>

    <meta name="CSRF-Token" content="${csrfToken}">

    <#if isDevel || true>
    <script type="text/javascript" charset="utf-8" src="${staticPrefix}/js/sencha/ext-all-dev.js?v=${serverVersion}"></script>
    <#else>
    <script type="text/javascript" charset="utf-8" src="${staticPrefix}/js/sencha/ext-all.js?v=${serverVersion}"></script>
    </#if>
    <#if it.companyResource??>
    <script type="text/javascript" charset="utf-8" src="/c/${it.companyId}/sencha/classes.js?v=${serverVersion}"></script>
    <#else>
        <#if loggedUser??>
            <script type="text/javascript" charset="utf-8" src="/sencha/classes.js?v=${serverVersion}"></script>
        </#if>
    </#if>

    <script type="text/javascript" charset="utf-8" src="${staticPrefix}/js/sencha/ext-lang-cs.js?v=${serverVersion}"></script>

    <#list override("assets/flexibee", "*.js") as o>
    <script type="text/javascript" charset="utf-8" src="/c/${it.companyId}/sencha/assets/flexibee/${o.fileNameWithoutPath}?v=${serverVersion}"></script>
    </#list>
<#if isDesktop>
    <link rel="stylesheet" type="text/css" href="${staticPrefix}/css/ext-all-desktop.css?v=${serverVersion}" />
<#else>
    <link rel="stylesheet" type="text/css" href="${staticPrefix}/css/ext-all-flexibee.css?v=${serverVersion}" />
</#if>

    <#list override("assets/flexibee", "*.css") as o>
    <link rel="stylesheet" type="text/css" href="/c/${it.companyId}/sencha/assets/flexibee/${o.fileNameWithoutPath}?v=${serverVersion}" />
    </#list>

    <link rel="shortcut icon" href="${staticPrefix}/img/winstrom.ico" />
</head>
<body <#if isDesktop == false>style="background: #499D38;"</#if>>

</#escape>
