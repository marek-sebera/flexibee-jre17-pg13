<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <title>FlexiBee mobile</title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
        <meta name="HandheldFriendly" content="true" />

        <meta name="format-detection" content="telephone=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="default">

        <link rel="apple-touch-icon-precomposed" href="${staticPrefix}/img/flexibee-icon-iphone.png" />
        <link rel="apple-touch-startup-image" href="${staticPrefix}/img/iphone-startup.gif" />

        <script src="${staticPrefix}/js/sencha/sencha-touch-all.js" type="text/javascript"></script>
        <link href="${staticPrefix}/css/sencha-touch.css" rel="stylesheet" type="text/css" />
        <script src="/c/${it.companyId}/sencha/classes.js?v=${serverVersion}" type="text/javascript"></script>

        <link rel="shortcut icon" href="${staticPrefix}/img/winstrom.ico" />

        <style type="text/css">
        /**
         * Example of an initial loading indicator.
         * It is recommended to keep this as minimal as possible to provide instant feedback
         * while other resources are still being loaded for the first time
         */
        html, body {
            height: 100%;
        }

        #appLoadingIndicator {
            position: absolute;
            top: 50%;
            left: 50%;
            margin-top: -10px;
            margin-left: -50px;
            width: 100px;
            height: 20px;
        }

        #appLoadingIndicator > * {
            background-color: #FFFFFF;
            float: left;
            height: 20px;
            margin-left: 11px;
            width: 20px;
            -webkit-animation-name: appLoadingIndicator;
            -webkit-border-radius: 13px;
            -webkit-animation-duration: 0.8s;
            -webkit-animation-iteration-count: infinite;
            -webkit-animation-direction: linear;
            opacity: 0.3
        }

        #appLoadingIndicator > :nth-child(1) {
            -webkit-animation-delay: 0.18s;
        }

        #appLoadingIndicator > :nth-child(2) {
            -webkit-animation-delay: 0.42s;
        }

        #appLoadingIndicator > :nth-child(3) {
            -webkit-animation-delay: 0.54s;
        }

        @-webkit-keyframes appLoadingIndicator{
            0% {
                opacity: 0.3
            }

            50% {
                opacity: 1;
                background-color:#499D38
            }

            100% {
                opacity:0.3
            }
        }
        </style>

    </head>
    <body>

<script type="text/javascript">

    Ext.Loader.setPath({
        'Ext': '${staticPrefix}/js/sencha',
    });

Ext.application({
    name: 'FlexiBee',

    requires: [
        'FlexiBee.adresar.Store',
        'FlexiBee.adresar.List',
        'FlexiBee.adr_udalost.List',
        'FlexiBee.adr_kontakt.List',
    ],

    icon: {
        57: '${staticPrefix}/img/flexibee-icon-iphone.png',
        72: 'resources/icons/Icon~ipad.png',
        114: 'resources/icons/Icon@2x.png',
        144: 'resources/icons/Icon~ipad@2x.png'
    },

    phoneStartupScreen: '${staticPrefix}/img/iphone-startup.gif',
    tabletStartupScreen: 'resources/loading/Homescreen~ipad.jpg',

    launch: function () {

        // Destroy the #appLoadingIndicator element
        Ext.fly('appLoadingIndicator').destroy();

        var adresar = Ext.create("FlexiBee.adresar.List");
        var udalost = Ext.create("FlexiBee.adr_udalost.List");
        var kontakt = Ext.create("FlexiBee.adr_kontakt.List");

        var list = Ext.create('Ext.tab.Panel', {
            fullscreen: true,
            tabBarPosition: 'bottom',

            items: [
                adresar,
                udalost,
                kontakt,
            ]
        });


    },

    onUpdated: function() {
        Ext.Msg.confirm(
            "Application Update",
            "This application has just successfully been updated to the latest version. Reload now?",
            function() {
                window.location.reload();
            }
        );
    }

/*
        FlexiBee.views.notesList = new Ext.List({
            id: 'notesList',
            store: 'FlexiBee.adresar.Store',
            itemTpl:'<div class="list-item-title">{nazev}</div>' +
            '<div class="list-item-narrative">{kod}</div>',
            onItemDisclosure: function (record) {
                // TODO: Render the selected note in the note editor.
            }
        });

        FlexiBee.views.notesListToolbar = new Ext.Toolbar({
            id: 'notesListToolbar',
            title: 'My Notes',
            layout: 'hbox',
            items: [
                { xtype: 'spacer' },
                {
                    id: 'newNoteButton',
                    text: 'New',
                    ui: 'action',
                    handler: function () {
                        // TODO: Create a blank note and make the note editor visible.
                    }
                }
            ]
        });

        FlexiBee.views.notesListContainer = new Ext.Panel({
            id: 'notesListContainer',
            layout: 'fit',
            html: 'This is the notes list container',
            dockedItems: [FlexiBee.views.notesListToolbar],
            items: [FlexiBee.views.notesList]
        });

        FlexiBee.views.viewport = new Ext.Panel({
            fullscreen: true,
            layout: 'card',
            cardAnimation: 'slide',
            items: [FlexiBee.views.notesListContainer]
        });
    }

    */
});

</script>

    <div id="appLoadingIndicator">
        <div></div>
        <div></div>
        <div></div>
    </div>

        </body>
</html>
</#escape>
