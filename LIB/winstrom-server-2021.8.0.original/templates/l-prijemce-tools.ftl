<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#macro placeCreditCheckJavaScript>
<script type="text/javascript">
    function handleCreditCheck(stat, ic, dic) {
        if (ic != '' || dic != '') {
            $.getJSON('https://support.flexibee.eu/remote/creditcheck/' + stat + '/?ic=' + ic + '&vatid=' + dic + '&format=json', function(data) {
                if (data && data.winstrom && data.winstrom.status[0] && data.winstrom.status[0].status) {
                    var status = data.winstrom.status[0].status;

                    if (status != 'ok') {
                        var msg;
                        if (status == 'end') {
                            msg = '${lang('messages', 'creditcheck.end', 'Společnost byla ukončena nebo je v likvidaci.')}';
                        } else if (status == 'warning') {
                            msg = '${lang('messages', 'creditcheck.warning', 'Jsou k dispozici informace o skutečnostech nebo změnách ve firmě, které mohou (ale nemusí) mít vliv na bonitu.')}';
                        } else if (status == 'problem') {
                            msg = '${lang('messages', 'creditcheck.problem', 'Jsou k dispozici informace mající zpravidla negativní vliv na bonitu firmy.')}';
                        } else {
                            msg = '${lang('messages', 'creditcheck.unknown', 'Stav bonity neznámý.')}';
                        }

                        var img = $('<a href="https://support.flexibee.eu/remote/go-creditcheck/' + stat + '/?ic=' + ic + '&vatid=' + dic + '" class="flexibee-creditcheck-' + status + '" rel="nofollow">' + msg + '<br/><small class="flexibee-external">creditcheck.cz<'+'/small><'+'/a>');
                        $('#creditcheck-container').html(img);
                        $('#creditcheck-container').css('display', 'block');
                    } else {
                        $('#creditcheck-container').hide();
                    }
                } else {
                    $('#creditcheck-container').hide();
                }
            });
        } else {
            $('#creditcheck-container').hide();
        }
    }
</script>
</#macro>

</#escape>