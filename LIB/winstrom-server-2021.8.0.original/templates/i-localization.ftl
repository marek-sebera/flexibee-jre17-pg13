<#ftl encoding="utf-8" />
<#-- Makra pro snadnou lokalizaci Freemarker šablon bez escapování navracených textů -->
<#macro act id def=0><#if def?is_string>${actText(id, def)}<#else>${actText(id)}</#if></#macro>
<#macro btn id def=0><#if def?is_string>${btnText(id, def)}<#else>${btnText(id)}</#if></#macro>
<#macro dlg id def=0><#if def?is_string>${dlgText(id, def)}<#else>${dlgText(id)}</#if></#macro>
<#macro msg id def=0><#if def?is_string>${msgText(id, def)}<#else>${msgText(id)}</#if></#macro>
<#macro lbl id def=0><#if def?is_string>${lblText(id, def)}<#else>${lblText(id)}</#if></#macro>