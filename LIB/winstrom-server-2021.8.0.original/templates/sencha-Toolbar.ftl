<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.main.Toolbar', {
    extend: 'Ext.toolbar.Toolbar',
    alias: 'widget.fbMainToolbar',

    requires: [
    ],

    baseCls: 'flexibee-background-dark',
    margin: '0 15px 0 0',

    defaults: {
        cls: 'flexibee-app-toolbar',
        scale: 'medium'
    },

    items: [
        {
            text: '${lang('dialogs', it.companyResource.name, it.companyResource.name)}',
            <#if it.companyResource.companiesResource.list?size gt 0>
                xtype: 'splitbutton',
                cls: 'flexibee-app-toolbar-company',
                menu: [
                    <#list it.companyResource.companiesResource.list as l>
                        <#if l_index != 0>, </#if>
                        {
                            text: '${l.nazev}',
                            href: "/c/${l.dbNazev}/sencha/web"
                        }
                        <#if l_index gt 8>
                            ,
                            '-',
                            {
                                text: '... ${it.bundleAccessor.getMessage('createCompany.allCompanies', 'další firmy')}',
                                href: '/c'
                            }
                            <#break>
                        </#if>
                    </#list>
                    <#if it.companyResource.companiesResource.userPrincipal.userInfo.isCreateCompany()>,
                        '-',
                        {
                            text: '${it.bundleAccessor.getMessage('createCompany.novaFirma', 'Založení nové firmy')}',
                            href: '/admin/zalozeni-firmy'
                        }
                    </#if>
                ]
            </#if>
        },
        '->',
        {
            xtype: 'button',
            text: '<#if loggedUser.prijmeni?? || loggedUser.jmeno??>${loggedUser.prijmeni} ${loggedUser.jmeno}<#else>${loggedUser.name}</#if>',
            icon: '${staticPrefix}/img/skin/${skin}/sencha/user.png',
            itemId: 'openCurrentUser'
        },
        {
            xtype: 'button',
            text: '${lang('labels', 'ukoncit', 'Odhlásit')}',
            handler: function() {
                window.location = '/login-logout/logout';
            }
        },
        ' ',
        '-',
        ' ',
        {
            text: 'Nástroje',
            icon: '${staticPrefix}/img/skin/${skin}/sencha/services.png'
        }
    ],

    initComponent: function() {
        var me = this;

        var additionalItems = FlexiBee.Plugins.invoke(me, 'defineAdditionalItems');
        Ext.iterate(additionalItems, function(additionalItem) {
            if (Ext.isArray(additionalItem)) {
                me.items = me.items.concat(additionalItem);
            } else {
                me.items.push(additionalItem);
            }
        });

        this.callParent(arguments);
    }
});

<#import "/l-sencha.ftl" as sencha>
<@sencha.includePlugins path="main.Toolbar"/>

</#escape>
