# Upravene JAR soubory

## winstrom-server-2021.8.0 PgSQL syntaxe

1) `unzip winstrom-server-2021.8.0.original.jar -d winstrom-server-2021.8.0.original`
2) Pomocí Java Bytecode Editor 0.1.1 upravit **winstrom-server-2021.8.0.original/cz/winstrom/service/WSPostgreRestore.class**
  - nahradit `p.proisagg=false` za `p.prokind != 'a'`
  - nahradit `p.proisagg=true` za `p.prokind = 'a'`
3) `jar -cvf winstrom-server-2021.8.0.repack.jar -C winstrom-server-2021.8.0.original .`
4) Nahradit soubor v deb nebo přímo v instalaci
  - v instalaci `mv winstrom-server-2021.8.0.repack.jar /usr/share/flexibee/lib/winstrom-server-2021.8.0.jar`
  - v deb (před zabalením `mv winstrom-server-2021.8.0.repack.jar ../flexibee_2021.8.0_all/usr/share/flexibee/lib/winstrom-server-2021.8.0.jar`
