ABRA Flexi
-----------

www.flexibee.eu


Otázky a odpovědi
-----------------

Q: Změnil jsem si nastavení spojení a teď mi nefunguje. 
A: Zkuste se vrátit k výchozí konfigurace. Zkopírujte jej těmito příkazy:

   mkdir -p ~/.winstrom/
   cp /etc/flexibee/ws.cenServer.xml ~/.winstrom/   

Q: Proč si vytváříte vlastní instanci PostgreSQL?
   ABRA Flexi si spravuje databáze v PostgreSQL a chtěli jsme, aby při 
   instalaci nebyl ovlivněn existující konfigurací a naopak, aby nerozbil 
   to co již máte v systému nainstalované. Na Windows a Mac OS X dodáváme 
   vlastní instalaci PostgreSQL. Na Linuxu (debian + ubuntu) používáme 
   vytvoření vlastní instance (pg_createcluster) a přitom nemusíme 
   instalovat druhý PostgreSQL. 


Q: Takže nestačí instance PostgreSQL, kterou již mám v systému?
A: Technicky stačí. Pro každou účtovanou firmu si ABRA Flexi vytváří vlastní databázi. 
   Navíc je zde databáze centralServer, kam se ukládají přehledové informace 
   o firmách. PostgreSQL si dodáváme sami, abychom zjednodušili podporu a 
   nerozbili existující databáze. Cílem také je, abychom při odinstalaci 
   dokázali smazat data, která jsme vytvořili (samozřejmě na dotaz).  
   U ubuntu/debianu využíváme vytvoření instance PostgreSQL. Zajistíme 
   si tím i to, že máme správnou verzi Postgresu.

   Takže důvodem je jen technická podpora. 
   
Q: Nemohu se připojit. Když aplikaci pustím z příkazové řádky, vypíše toto:
   Nepovedlo se připojit k databázi. Server vrátil chybu: FATAL: the database 
   system is shutting down
A: Problém je, že databáze aktuálně neběží nebo je ve vypínacím stavu.
   Zkuste restartovat PostgreSQL příkazem:

   sudo /etc/init.d/postgresql-8.3 restart

   Zkontrolujte, že databáze funguje správně: 
   
   pg_lsclusters
   
   Měli byste vidět i takovýto záznam:
   
   Version Cluster   Port Status Owner    Data directory                     Log file
   8.3     main      5432 online postgres /var/lib/postgresql/8.3/main       /var/log/postgresql/postgresql-8.3-main.log
   8.3     flexibee  5434 online postgres /var/lib/flexibee/                 /var/log/postgresql/postgresql-8.3-flexibee.log

   Důležité je, aby řádek flexibee byl ve stavu online.
  
